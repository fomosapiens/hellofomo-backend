<?php

use Illuminate\Database\Seeder;
use Elidev\ACL\Repositories\Contracts\RoleRepositoryInterface;

class RolesTableSeeder extends Seeder
{
    /**
     * @var $roleRepo
     */
    protected $roleRepo;

    /**
     * @param RoleRepositoryInterface $roleRepo
     */
    public function __construct(RoleRepositoryInterface $roleRepo)
    {
        $this->roleRepo = $roleRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create default roles
         */
        $roles = config('acl.roles');
        foreach($roles as $role) {
            // Only insert if this role does not exist
            $existRole = $this->roleRepo->findWhere(['name'  => $role]);
            if ($existRole->first()) {
                $roleItem = $existRole->first();
            } else {
                $roleItem = $this->roleRepo->create([
                    'name'  => $role
                ]);
            }

            // set default super admin group (@ticket #13644)
            if ($role == config('acl.group_with_full_permissions')) {
                Settings::set(config('acl.settings.super_admin_group'), $roleItem->id);
                Artisan::call('config:cache');
            }
        }

        /**
         * Create default actions
         */
        acl_cache_or_create_roles_permissions();
    }
}

<?php

use App\Models\Category;
use App\Repositories\Contracts\AuthorRepositoryInterface;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * @var \App\Repositories\Contracts\AuthorRepositoryInterface
     */
    protected $authorRepo;

    /**
     * AuthorsTableSeeder constructor.
     *
     * @param \App\Repositories\Contracts\AuthorRepositoryInterface $authorRepo
     */
    public function __construct(AuthorRepositoryInterface $authorRepo)
    {
        $this->authorRepo = $authorRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table) {
			$table->engine = 'InnoDB';

			$table->bigIncrements('id');
			$table->string('title', 255);
			$table->text('introduction')->nullable();
			$table->text('description');
			$table->string('keywords', 255)->nullable();
			$table->string('status', 45)->nullable();
			$table->dateTime('available_date')->nullable();
			$table->text('tags')->nullable();
			$table->bigInteger('image_id')->nullable();
			$table->string('gallery_ids', 255)->nullable();
			$table->string('attachment_ids', 255)->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}

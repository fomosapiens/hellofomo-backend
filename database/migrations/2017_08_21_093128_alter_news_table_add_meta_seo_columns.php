<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNewsTableAddMetaSeoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @ticket #13413 - HFBACKEND-93 News / Edit - New Field in Form and DB
     */
    public function up()
    {
        Schema::table('news', function($table) {
            $table->string('meta_title', 255)->nullable()->comment('Meta tag title value');
            $table->string('meta_permalink', 255)->nullable()->comment('Meta tag permalink value');
            $table->text('meta_description')->nullable()->comment('Meta tag description value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_permalink');
            $table->dropColumn('meta_description');
        });
    }
}

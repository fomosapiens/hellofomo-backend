<?php

if (!function_exists('get_file_data')) {
    /**
     * @param $file
     * @param $convert_to_array
     * @return bool|mixed
     * @author Sang Nguyen
     */
    function get_file_data($file, $convert_to_array = true)
    {
        $file = file_get_contents($file);
        if (!empty($file)) {
            if ($convert_to_array) {
                return json_decode($file, true);
            } else {
                return $file;
            }
        }
        return [];
    }
}

if (!function_exists('json_encode_prettify')) {
    /**
     * @param $data
     * @return string
     */
    function json_encode_prettify($data)
    {
        return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}

if (!function_exists('save_file_data')) {
    /**
     * @param $path
     * @param $data
     * @param $json
     * @return bool|mixed
     * @author Sang Nguyen
     */
    function save_file_data($path, $data, $json = true)
    {
        try {
            if ($json) {
                $data = json_encode_prettify($data);
            }
            file_put_contents($path, $data);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
}

if (!function_exists('elidev_setting')) {
    /**
     * Get the setting instance.
     *
     * @param $key
     * @param $default
     * @return array
     * @author Sang Nguyen
     */
    function elidev_setting($key = null, $default = null)
    {
        if (!empty($key)) {
            try {
                if (!file_exists(storage_path('settings.json'))) {
                    return $default;
                }

                $settings = get_file_data(storage_path('settings.json'), true);
                $value = array_get($settings, $key);
                if (empty($value)) {
                    return $default;
                }

                return unserialize($value);
            } catch (\Exception $exception) {
                info($exception->getMessage());
                return $default;
            }
        }
        return $default;
    }
}

if (!function_exists('scan_language_keys')) {
    /**
     * Scan all __() function then save key to /storage/languages.json
     * @author Sang Nguyen
     * @param $key
     */
    function scan_language_keys($key)
    {
        if (!empty($key)) {
            $languages = [];
            $stored_file = storage_path('languages.json');
            if (file_exists($stored_file)) {
                $languages = get_file_data($stored_file, true);
            }
            $languages[$key] = $key;
            save_file_data($stored_file, $languages, true);
        }
    }
}

if (!function_exists('get_categories_caching_file')) {
    /**
     * Get categories caching file
     * @author vulh
     * @return string
     */
    function get_categories_caching_file()
    {
        return storage_path(). '/app/categories.json';
    }
}

if (!function_exists('fomo_setting')) {
    /**
     * Get the setting instance.
     *
     * @param $key
     * @param $default
     * @return array
     */
    function fomo_setting($key = null, $default = null)
    {
        if (!empty($key)) {
            try {
                if (!file_exists(storage_path('settings.json'))) {
                    return $default;
                }

                $settings = get_file_data(storage_path('settings.json'), true);
                $value = array_get($settings, $key);
                if (empty($value)) {
                    return $default;
                }

                return unserialize($value);
            } catch (Exception $exception) {
                info($exception->getMessage());
                return $default;
            }
        }
        return $default;
    }
}
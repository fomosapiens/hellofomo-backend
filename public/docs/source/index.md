---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_b62b4e9f1725a09a8ec20f8161f406d7 -->
## Get current font in Admin Panel

> Example request:

```bash
curl -X GET "http://fomo.elidev.info/api/v1/settings/font" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://fomo.elidev.info/api/v1/settings/font",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": false,
    "data": "Helvetica"
}
```

### HTTP Request
`GET api/v1/settings/font`

`HEAD api/v1/settings/font`


<!-- END_b62b4e9f1725a09a8ec20f8161f406d7 -->


function validateIsNumber($ele){
    $(document).on('blur', $ele, function () {
        $(this).val(parseInt($(this).val()));
    });
}
jQuery(document).ready(function($) {
    validateIsNumber('input[name="failed_logins"]');
    validateIsNumber('input[name="failed_logins_time_minute"]');
    validateIsNumber('input[name="time_minute_block"]');
});
TableDatatablesAjax = function () {

    var dataTable;

    var defaultSortColumn      = $("table[id$='datatable']").data('sort-default'),
        defaultSortColumnIndex = $("table[id$='datatable']").find('th[data-col-name="' + defaultSortColumn + '"]').index(),
        defaultSortOrder       = $("table[id$='datatable']").data('sort-order-default');

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var addColumnsDynamically = function () {
        // set defined columns name
        var columns = [];
        $("table[id$='datatable'] thead tr th.heading-columns").each(function(key, item) {
            var columnName = $(item).attr('data-col-name');
            if ( typeof columnName != 'undefined' && columnName != '' ) {
                // add custom class names for each cell
                var columnClassName = (columnName == 'status') ? 'dt-body-center text-center' : '';
                if ( columnName == 'category_names_with_link' ) {
                    columnClassName = 'category-cell';
                }
                if ( columnName == 'tag_names_with_link' ) {
                    columnClassName = 'tag-cell';
                }

                let columnClass = $(item).attr('data-col-class');
                if ( typeof columnClass != 'undefined' && columnClass != '' ) {
                    columnClassName += ' ' + columnClass;
                }

                columns.push({
                    data: columnName,
                    name: columnName,
                    orderable: $(item).attr('data-col-orderable') === '1',
                    className: columnClassName,

                    // if data is displayed by HTML format (i.e: status, image) -> render html
                    mRender: function (data, type, row) {
                        var html = $(item).attr('data-html');
                        if ( typeof html != 'undefined' && html != '' ) {
                            if ( columnName == 'status' && data == 'pending' ) {
                                html = html.replace('fa-check font-green-jungle', 'fa-refresh font-red');
                                html = html.replace('Publish', 'Pending');
                            }
                            return html.replace('#data#', row[columnName]);
                        }

                        return data;
                    }
                });
            }
        });

        // render "Actions" column
        var $tableActions = $("table[id$='datatable'] .table-actions");
        if ( typeof $tableActions != 'undefined' && $tableActions.length > 0 ) {
            columns.push({
                orderable: false,
                className: "dt-body-center",
                mRender: function (data, type, row) {
                    var showRoute = $tableActions.attr('data-show-action-route'),
                        editRoute = $tableActions.attr('data-edit-action-route'),
                        deleteRoute = $tableActions.attr('data-delete-action-route'),
                        assignRoute = $tableActions.attr('data-assign-action-route'),
                        restoreRoute = $tableActions.attr('data-restore-action-route'),
                        trashRoute = $tableActions.attr('data-trash-action-route'),
                        unBlockRoute = $tableActions.attr('data-unblock-action-route');

                    var showAction = '',
                        editAction = '',
                        deleteAction = '',
                        assignAction = '',
                        restoreActon = '',
                        trashActon = '',
                        unBlockActon = '';

                    if ( typeof showRoute != 'undefined' && showRoute != '' ) {
                        showAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-show-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-show-role") + '" href="' + showRoute + '" class="tooltips btn btn-table-show ' + $tableActions.attr("data-show-action-color") + '"><i class="' + $tableActions.attr("data-show-action-icon") + '"></i> ' + $tableActions.attr("data-show-action-name") + '</a>';
                        showAction = showAction.replace("-1", row.id);
                    }
                    if ( typeof editRoute != 'undefined' && editRoute != '' ) {
                        editAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-edit-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-edit-role") + '" href="' + editRoute + '" class="tooltips btn btn-table-edit ' + $tableActions.attr("data-edit-action-color") + '"><i class="' + $tableActions.attr("data-edit-action-icon") + '"></i> ' + $tableActions.attr('data-edit-action-name') + '</a>';
                        editAction = editAction.replace("-1", row.id);
                    }
                    if ( typeof assignRoute != 'undefined' && assignRoute != '' ) {
                        assignAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-assign-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-assign-role") + '" href="' + assignRoute + '" class="tooltips btn btn-table-assign ' + $tableActions.attr("data-assign-action-color") + '"><i class="' + $tableActions.attr("data-assign-action-icon") + '"></i> ' + $tableActions.attr('data-assign-action-name') + '</a>';
                        assignAction = assignAction.replace("-1", row.id);
                    }
                    if ( typeof restoreRoute != 'undefined' && restoreRoute != '' ) {
                        restoreActon = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-restore-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-restore-role") + '" href="' + restoreRoute + '" class="tooltips btn btn-table-restore ' + $tableActions.attr("data-restore-action-color") + '"><i class="' + $tableActions.attr("data-restore-action-icon") + '"></i> ' + $tableActions.attr('data-restore-action-name') + '</a>';
                        restoreActon = restoreActon.replace("-1", row.id);
                    }
                    if ( typeof trashRoute != 'undefined' && trashRoute != '' ) {
                        trashActon = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-trash-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-trash-role") + '" href="' + trashRoute + '" class="tooltips btn btn-table-trash ' + $tableActions.attr("data-trash-action-color") + '"><i class="' + $tableActions.attr("data-trash-action-icon") + '"></i> ' + $tableActions.attr('data-trash-action-name') + '</a>';
                        trashActon = trashActon.replace("-1", row.id);
                    }
                    if ( typeof deleteRoute != 'undefined' && deleteRoute != '' ) {
                        deleteAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("tooltips data-delete-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-delete-role") + '" href="#listing-confirm-delete-modal" data-toggle="modal" data-href="' + deleteRoute + '" ' + ' class="btn btn-delete btn-table-destroy ' + $tableActions.attr("data-delete-action-color") + '"><i class="' + $tableActions.attr("data-delete-action-icon") + '"></i> ' + $tableActions.attr('data-delete-action-name') + '</a>';
                        deleteAction = deleteAction.replace("-1", row.id);
                    }
                    if ( typeof unBlockRoute != 'undefined' && unBlockRoute != '' ) {
                        var disableClass = '';
                        if (typeof row.is_disabled != 'undefined' && row.is_disabled) {
                            disableClass = 'anchor-disabled';
                            unBlockRoute= '';
                        }

                        unBlockActon = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-unblock-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-unblock-role") + '" href="#unblock-confirm-modal" data-toggle="modal" data-href="' + unBlockRoute + '" class="tooltips btn btn-table-unblock ' + $tableActions.attr("data-unblock-action-color") + " " + disableClass + '"><i class="' + $tableActions.attr("data-unblock-action-icon") + '"></i> ' + $tableActions.attr("data-unblock-action-name") + '</a>';
                        unBlockActon = unBlockActon.replace("-1", row.id);
                    }

                    return showAction + " " + editAction + " " + trashActon + " " + restoreActon + " " + deleteAction + " " + assignAction + " " + unBlockActon;
                }
            });
        }

        return columns;
    }

    var initTable = function ($table) {

        var grid = new Datatable();

        //check action filter before
        if(localStorage.getItem("datatable_filter") == "trash") {
            localStorage.removeItem('datatable_filter');
            $("table[id$='datatable']").attr('data-only-trashed', 1);
            activeFilterTrash();
        }

        var recordPerPage = parseInt($("table[id$='datatable']").attr('data-record-per-page')),
            currentLocaleFile = $("table[id$='datatable']").attr('data-locale-file');
        grid.init({
            src: $table,
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                "dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                // save datatable state(pagination, sort, etc) in cookie.
                "bStateSave": true,

                // save custom filters to the state
                "fnStateSaveParams":    function ( oSettings, sValue ) {
                    $("table[id$='datatable'] tr.filter .form-control").each(function() {
                        sValue[$(this).attr('name')] = $(this).val();
                    });

                    return sValue;
                },

                // read the custom filters from saved state and populate the filter inputs
                "fnStateLoadParams" : function ( oSettings, oData ) {
                    //Load custom filters
                    $("table[id$='datatable'] tr.filter .form-control").each(function() {
                        var element = $(this);
                        if (oData[element.attr('name')]) {
                            element.val( oData[element.attr('name')] );
                        }
                    });

                    return true;
                },

                "language": {
                    "url": currentLocaleFile
                },

                "lengthMenu": [
                    [recordPerPage, recordPerPage + 10, recordPerPage + 20, -1],
                    [recordPerPage, recordPerPage + 10, recordPerPage + 20, "All"] // change per page values here
                ],
                "pageLength": recordPerPage, // default record count per page
                "ajax": {
                    "url": $("table[id$='datatable']").attr('data-ajax-src'), // ajax source
                    "data": function ( params ) {
                        var isSorting = false,
                            tableKey  = 'DataTables_' + $("table[id$='datatable']").attr('id') + '_' + window.location.pathname,
                            oldData   = JSON.parse(localStorage.getItem(tableKey)),
                            currentSortColumnIndex = $("table[id$='datatable']").find('th[data-col-name="' + params.order[0].column + '"]').index(),
                            $table = $('table[id$="datatable"]').dataTable();

                        // set "page" param before send AJAX request to server
                        params.page = (params.start / params.length) + 1;

                        if ( $("table[id$='datatable']").attr('data-only-trashed') === '1' ) {
                            params.only_trashed = true;
                        }

                        // check the user is performing the sort
                        if (oldData != null) {
                            isSorting = currentSortColumnIndex != oldData.order[0][0] || params.order[0].dir != oldData.order[0][1];
                        }

                        // d.draw = 1 : the first time the datatable is loaded
                        // d.draw > 1 : when the datatable is reloading
                        if (params.draw > 1 && oldData != null && isSorting) {
                            var oSettings      = $table.fnSettings(),
                                $paginationBar = $('.dataTables_paginate').first(),
                                currentPage    = parseInt($paginationBar.find('input.pagination-panel-input').val()),
                                currentPage    = isNaN(currentPage) ? 1 : currentPage,
                                start          = (currentPage - 1) * recordPerPage; // we are minus for 1 because datatables plugin uses page index from 0, 1, 2..., not 1, 2, 3...

                            // force standing in current page
                            oSettings._iDisplayStart = start;
                            params.page = currentPage;

                            // store newest sort info to local storage
                            oldData.order[0][0] = currentSortColumnIndex;
                            oldData.order[0][1] = params.order[0].dir;
                            localStorage.setItem(tableKey, JSON.stringify(oldData));
                        }

                        $.blockUI(FOMO.blockMetronicUI);
                    },
                },
                "columns": addColumnsDynamically(),
                "ordering": true,
                "order": [[ defaultSortColumnIndex, defaultSortOrder ]], // override default sort of datatables plugin
                fnServerParams: function(data) {
                    data['order'].forEach(function(items, index) {
                        data['order'][index]['column'] = data['columns'][items.column]['data'];
                    });
                },
                // hide pagination feature if only one page
                fnDrawCallback: function(oSettings) {
                    $.unblockUI();

                    // set filter select drop down searchable
                    $("select[name='category_names_with_link']").select2();
                    $("select[name='tag_names_with_link']").select2();

                    // remove sorting in checkbox column
                    $("table[id$='datatable'] thead tr th.heading-columns.heading-columns-checkbox").removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting_disabled');

                    // remove default sorting of the first column
                    var $firstColumn = $("table[id$='datatable'] thead tr th.heading-columns").first();
                    if ($firstColumn.attr('data-col-orderable') == '') {
                        $firstColumn.removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting_disabled');
                    }

                    // after change page (next|prev) -> reset Select All checkbox
                    if ( $('table[id$="datatable"] thead input[name="select_all"]').is(':visible') && $('table[id$="datatable"] thead input[name="select_all"]').prop('checked') ) {
                        $('table[id$="datatable"] thead input[name="select_all"]').prop('checked', false);
                    }

                    // show/hide buttons
                    var onlyTrash = $("table[id$='datatable']").attr('data-only-trashed');
                    if ( $('#filter-navigation').is(':visible') && typeof onlyTrash != 'undefined' ) {
                        // if "All"
                        if ( onlyTrash === '' ) {
                            $('.btn-table-restore').hide();
                            $('.btn-table-destroy').hide();

                            $('.btn-table-show').show();
                            $('.btn-table-edit').show();
                            $('.btn-table-trash').show();
                        } else {
                            // if filter by trashed
                            $('.btn-table-restore').show();
                            $('.btn-table-destroy').show();

                            $('.btn-table-show').hide();
                            $('.btn-table-edit').hide();
                            $('.btn-table-trash').hide();
                        }
                    }

                    aclFn.init();

                },
            }
        });

        // handle group action submit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {

                // set multiple categories/tags for news
                try{
                    switch ( action.val() ) {
                        case 'set_categories':
                            FomoNewsServices.setCategories(grid.getSelectedRows());
                            return;
                        case 'set_tags':
                            FomoNewsServices.setTags(grid.getSelectedRows());
                            return;
                        case 'bulk_trash':
                        case 'restore' :
                        case 'delete' :
                            FomoNewsServices.bulkActions(grid.getSelectedRows(), action.find(':selected').data('ajax-url'),  action.find(':selected').data('method'), action.val());
                            return;
                    }
                } catch(err) {

                }
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                if ( typeof toastr != 'undefined' ) {
                    toastr.error('Please select an action');
                } else {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            } else if (grid.getSelectedRowsCount() === 0) {
                if ( typeof toastr != 'undefined' ) {
                    toastr.error('No record selected');
                } else {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            }
        });

        //grid.setAjaxParam("customActionType", "group_action");
        //grid.getDataTable().ajax.reload();
        //grid.clearAjaxParams();

        dataTable = grid.getDataTable();
    }

    /**
     * @var status
     *      + '' : load all items
     *      + 1  : load only trashed items
     *
     * @param status
     */
    var filterTableItems = function (status) {
        // hide un-necessary alerts
        $('div.ustom-alerts.alert').hide();

        // change table status (load trashed item or not)
        $("table[id$='datatable']").attr('data-only-trashed', status);

        // re-get data
        dataTable.draw();
    }

    return {

        //main function to initiate the module
        init: function () {
            var tables = $("table[id$='datatable']");
            if (tables.size() > 0) {
                $(tables).each(function() {
                    var $tbl = $(this);
                    initPickers();
                    initTable($tbl);
                });
            }
        },

        getDataTable: function() {
            return dataTable;
        },

        filterAllOrTrashed: function(status) {
            filterTableItems(status);
        }

    };

}();

function activeFilterTrash(){
    $('.table-container .table-group-action-input').find("option[value*='bulk_trash']").hide();
    $('.table-container .table-group-action-input').find("option[value*='restore']").show();
    $('.table-container .table-group-action-input').find("option[value*='delete']").show();
    $('.table-container .table-group-action-input').val('');
    $("#filter-navigation .btn-group .action-filter").removeClass('active');
    $("#datatable-btn-trashed").addClass('active');
}

function activeFilterAll(){
    $('.table-container .table-group-action-input').find("option").show();
    $('.table-container .table-group-action-input').find("option[value*='restore']").hide();
    $('.table-container .table-group-action-input').find("option[value*='delete']").hide();
    $('.table-container .table-group-action-input').val('');
    $("#filter-navigation .btn-group .action-filter").removeClass('active');
    $("#datatable-btn-all").addClass('active');
}

jQuery(document).ready(function() {
    TableDatatablesAjax.init();

    var dataTable = TableDatatablesAjax.getDataTable();

    // filter column data by input text
    var timer = null,
        inputColumnIndex = null,
        inputValue = null;
    $(document).on('keyup', 'input.form-control.form-filter.input-sm', function() {
        clearTimeout(timer);
        if ( $(this).val().length == 0 || $(this).val().length >= 2 ) {
            inputColumnIndex = $(this).data().columnIndex;
            inputValue = $(this).val();
            timer = setTimeout(function () {
                if ( typeof dataTable != 'undefined' && typeof inputColumnIndex != 'undefined' && typeof inputValue != 'undefined' ) {
                    dataTable.columns( inputColumnIndex ).search( inputValue, true, false ).draw();
                    inputColumnIndex = null;
                    inputValue = null;
                }
            }, 400);
        }
    });

    // filter column data by select drop down
    $(document).on('change', 'select.filter-select', function() {
        var columnIndex = $(this).data().columnIndex;
        if ( typeof dataTable != 'undefined' && typeof columnIndex != 'undefined' ) {
            dataTable.columns(columnIndex).search( $(this).val() ).draw();
        }
    });

    // filter date column
    $(document).on('change', 'input.fomo-datetime-filter', function() {
        var columnIndex = $(this).data().columnIndex;
        if ( typeof dataTable != 'undefined' && typeof columnIndex != 'undefined' ) {
            dataTable.columns(columnIndex).search( $(this).val() ).draw();
        }
    });

    /***
     * delete button clicked
     */
    $(document).on('click', '.btn-delete', function(e) {
        e.preventDefault();
        $('div.modal').find('.btn-action').attr('data-href', $(this).attr('data-href'));
    });

    /***
     * unblock button clicked
     */
    $(document).on('click', '.btn-table-unblock', function(e) {
        e.preventDefault();
        $('div.modal').find('.btn-action').attr('data-href', $(this).attr('data-href'));
    });

    /***
     * confirm button clicked
     */
    $('div.modal').on('click', '.btn-action', function(e) {
        if ( $(e.currentTarget).hasClass('unblock') ) {
            e.preventDefault();
            window.location.replace($(this).attr('data-href'));
        }

        if ( $('div.modal').find('form').length > 0 ) {
            $('div.modal').find('form').attr('action', $(this).attr('data-href'));
        } else {
            e.preventDefault();
            window.location.replace($(this).attr('data-href'));
        }
    });

    /***
     * restore button clicked
     */
    $(document).on('click', 'a.btn-table-restore', function(e) {
        localStorage.setItem("datatable_filter", "trash");
    });

    /***
     * delete button clicked
     */
    $(document).on('submit', '#listing-confirm-delete-modal form', function(e) {
        localStorage.setItem("datatable_filter", "trash");
    });

    /***
     * Filter data by trashed items
     */

    $(document).on('click', '#datatable-btn-trashed', function() {
        activeFilterTrash();
        TableDatatablesAjax.filterAllOrTrashed(1);
    });

    /***
     * Get all (not trashed items)
     */
    $(document).on('click', '#datatable-btn-all', function() {
        activeFilterAll();
        TableDatatablesAjax.filterAllOrTrashed('');
    });

    // handle filter cancel button click
    if ($('.btn-filter-cancel').length) {
        dataTable.on('click', '.btn-filter-cancel', function(e) {
            e.preventDefault();

            // clear filter data
            $('textarea.form-filter, select.form-filter, input.form-filter').each(function() {
                $(this).val('');
            });
            $('input.form-filter[type="checkbox"]').each(function() {
                $(this).attr('checked', false);
            });

            // reset pagination to default page length
            dataTable.page.len(parseInt($("table[id$='datatable']").attr('data-record-per-page')));
            dataTable.columns().search('');

            // reset sorting to desc
            var $table = $('table[id$="datatable"]').dataTable();
            let sortColumn  = $('table[id$="datatable"]').attr('data-sort-index-column');
            let sortOrder  = $('table[id$="datatable"]').attr('data-sort-order-default');
            $table.fnSort([[typeof sortColumn != 'undefined' && sortColumn != '' ? sortColumn : 0, jQuery.inArray(sortOrder, ['asc', 'desc']) != -1 ? sortOrder : 'desc']]);
        });
    }

    // updates "Select all" control in a data table
    function updateDataTableSelectAllCtrl() {
        var $table          = dataTable.table().node();
        var $chkboxAll      = $('tbody input[type="checkbox"]', $table);
        var $chkboxChecked  = $('tbody input[type="checkbox"]:checked', $table);
        var chkboxSelectAll = $('thead input[name="select_all"]', $table).get(0);

        // if none of the checkboxes are checked
        if ( $chkboxChecked.length === 0 ) {
            chkboxSelectAll.checked = false;
            if ( 'indeterminate' in chkboxSelectAll ) {
                chkboxSelectAll.indeterminate = false;
            }
        } else if ( $chkboxChecked.length === $chkboxAll.length ) { // if all of the checkboxes are checked
            chkboxSelectAll.checked = true;
            if ( 'indeterminate' in chkboxSelectAll ) {
                chkboxSelectAll.indeterminate = false;
            }
        } else { // if some of the checkboxes are checked
            chkboxSelectAll.checked = true;
            if ( 'indeterminate' in chkboxSelectAll ) {
                chkboxSelectAll.indeterminate = true;
            }
        }
    }

    // array holding selected row IDs
    var rows_selected = [];

    // handle click on checkbox
    $('table[id$="datatable"] tbody').on('click', 'input[type="checkbox"]', function(e) {
        var $row = $(this).closest('tr');

        // get row data
        var data = dataTable.row($row).data();

        // get row ID
        var rowId = data.id;

        // determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // if checkbox is checked and row ID is not in list of selected row IDs
        if ( this.checked && index === -1 ) {
            rows_selected.push(rowId);
        } else if ( !this.checked && index !== -1 ) { // otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            rows_selected.splice(index, 1);
        }

        // update state of "Select all" control
        updateDataTableSelectAllCtrl();

        // prevent click event from propagating to parent
        e.stopPropagation();
    });

    // handle click on table cells with checkboxes
    $('table[id$="datatable"]').on('click', 'tr td:first-child, thead th:first-child', function(e){
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // handle click on "Select all" control
    if ( typeof dataTable != 'undefined' && dataTable ) {
        $('thead input[name="select_all"]', dataTable.table().container()).on('click', function(e) {
            if ( this.checked ) {
                $('table[id$="datatable"] tbody input[type="checkbox"]:not(:checked)').trigger('click');
            } else {
                $('table[id$="datatable"] tbody input[type="checkbox"]:checked').trigger('click');
            }

            // prevent click event from propagating to parent
            e.stopPropagation();
        });
    }
});

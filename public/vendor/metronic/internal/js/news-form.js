var FomoNewsForm = function () {

    function searchAuthors() {
        var selector = '.js-author-data-ajax';
        if ( $(selector).size() > 0 && $(selector).attr('data-ajax-src') != '' ) {
            $(selector).select2({
                ajax: {
                    url: $(selector).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.items,
                        };
                    },
                },
                minimumInputLength: 2
            });
        }
    }

    function searchNews() {
        var selector = '.js-news-data-ajax';
        if ( $(selector).size() > 0 && $(selector).attr('data-ajax-src') != '' ) {
            var excerptNewsId = $(selector).attr('data-excerpt-news-id');
            if ( typeof excerptNewsId == 'undefined' || excerptNewsId == '' ) {
                excerptNewsId = -1;
            }

            $(selector).select2({
                ajax: {
                    url: $(selector).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            excerpt_news_id: excerptNewsId, // excerpt current news
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.items,
                        };
                    },
                },
                minimumInputLength: 2
            }).on('change', function() {
                /**
                 * Hook to on change event of Select2 plugin to display select result into new div
                 */
                var $selected = $(this).find('option:selected');
                var $container = $('.js-related-news-container');

                var $list = $('<ul>');
                $selected.each(function(k, v) {
                    var $li = $('<li class="tag-selected"><a class="destroy-tag-selected">×</a>' + $(v).text() + '</li>');
                    $li.children('a.destroy-tag-selected')
                        .off('click.select2-copy')
                        .on('click.select2-copy', function(e) {
                            var $opt = $(this).data('select2-opt');
                            $opt.attr('selected', false);
                            $opt.attr('value', ''); // no need to store duplicated data here
                            $opt.parents('select').trigger('change');
                        }).data('select2-opt', $(v));
                    $list.append($li);
                });
                $container.html('').append($list);
            }).trigger('change');
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            searchAuthors();
            searchNews();
        }
    };

}();

jQuery(document).ready(function() {
    FomoNewsForm.init();
});

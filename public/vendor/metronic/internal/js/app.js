/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 40);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * Handle app dashboard layout behaviours
 */
FomoAppDashboardLayout = function () {
    var form = '#frm-new-section',
        originalSection = '#new-section-box',
        appendDestination = '#app-layout-dashboard-portlet-body form#frm-new-section',
        sectionLayout = '.box-item-layout-section',
        sectionLayoutTitle = '.portlet-title .caption',
        btnAddSection = '.group-button-action button.btn-block',
        btnDeleteSection = '#app-layout-dashboard-wrapper .frm-section .delete-section',
        btnSectionCollapse = '#app-layout-dashboard-wrapper .frm-section .tools .collapse',
        btnSectionExpand = '#app-layout-dashboard-wrapper .frm-section .tools .expand',
        btnSaveLayout = '#fomo-save-app-dashboard-layout-btn',
        btnEachSectionSave = '.group-button-action .btn-each-section-save',
        selectDataType = 'select[name="type[]"]',
        selectLayoutType = '#app-layout-dashboard-wrapper select[name="layout[]"]',
        indexBox = $('div[id^=frm-section]').length,
        originalForm = null;

    /**
     * Handle add section button
     */
    function handleAddSectionButton() {
        $(document).on('click', btnAddSection, function (e) {
            e.preventDefault();
            cloneSection();
            toastr.info($(this).data().msg);
        });
    }

    /**
     * Handle delete section button
     */
    function handleDeleteSectionButton() {
        $(document).on('click', btnDeleteSection, function (e) {
            e.preventDefault();
            var itemDelete = $(this).closest('.frm-section');
            swal({
                title: (typeof alertTitle === 'undefined' ? 'undefined' : _typeof(alertTitle)) != undefined ? alertTitle : "Are you sure?",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: (typeof alertText === 'undefined' ? 'undefined' : _typeof(alertText)) != undefined ? alertText : "Yes, delete it!",
                closeOnConfirm: true,
                showCancelButton: true
            }, function () {
                itemDelete.remove();
            });
        });
    }

    /**
     * Handle change background
     */
    function handleChangeBackground() {
        /**
         * Handle change background when collapse the section
         */
        $(document).on('click', btnSectionCollapse, function (e) {
            e.preventDefault();
            $(this).closest(sectionLayout).addClass('green').find(sectionLayoutTitle).removeClass('font-dark');
        });

        /**
         * Handle change background when expand the section
         */
        $(document).on('click', btnSectionExpand, function (e) {
            e.preventDefault();
            $(this).closest(sectionLayout).removeClass('green').find(sectionLayoutTitle).addClass('font-dark');
        });
    }

    /**
     * Handle save for "Save" button of each section in Left Panel
     */
    function handleSaveEachSectionInLeftPanel() {
        $(document).on('click', btnEachSectionSave, function (e) {
            e.preventDefault();
            $(btnSaveLayout).trigger('click');
        });
    }

    /**
     * Handle save settings
     */
    function handleSaveAppDashboardLayoutButton() {
        $(document).on('click', btnSaveLayout, function (e) {
            e.preventDefault();

            // send ajax to save layout settings
            var ajaxUrl = $(this).attr('data-ajax-url');
            var data = new FormData($(form)[0]);
            /** Layout post settings action */
            var current = $(e.currentTarget);
            var postForm = current.closest('body').find('#fomo-update-post-setting-display');
            if (postForm.length > 0) {
                var listItem = postForm.find('.fomo-post-sorting-list li');
                if (listItem.length > 0) {
                    $.each(listItem, function (index, item) {
                        var itemName = $(item).attr('data-id');
                        data.append(itemName, postForm.find('#' + itemName).is(':checked') ? 1 : 0);
                    });
                }
            } else {
                $("#frm-new-section > .frm-section ").each(function (index, item) {
                    data.append("type_value_" + index, $(this).find('select[name="type_value[]"]:not(.hidden)').val());
                });
            }

            if (typeof ajaxUrl != 'undefined' && ajaxUrl != '') {
                $.ajax({
                    url: ajaxUrl,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: data,
                    beforeSend: function beforeSend() {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function success(response) {
                        $.unblockUI();
                        if (response.errors == false) {
                            toastr.success(response.data.message);

                            // after saved form -> update the original form
                            originalForm = $(form).serialize();
                        }
                    },
                    error: function error(jqXHR, exception) {
                        $.unblockUI();
                        if (jqXHR.status === 400) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    }
                });
            }
        });
    }

    /**
     * Handle select data type change
     */
    function handleSelectDataTypeChange() {
        $(selectDataType).on('change', function () {
            var divSelectDataTypeValue = $(this).parent().parent().siblings().closest('div.select-data-type-value'),
                lblDataTypeValue = divSelectDataTypeValue.find('label[for="input-type-value"]'),
                selectCategory = divSelectDataTypeValue.find('#input-category-type-value'),
                selectTag = divSelectDataTypeValue.find('#input-tag-type-value');

            switch (this.value) {
                case 'all':
                case 'top_news':
                    $(selectCategory).prop('disabled', false);
                    $(selectTag).prop('disabled', 'disabled');

                    divSelectDataTypeValue.addClass('hidden');
                    $(selectCategory).addClass('hidden');
                    $(selectTag).addClass('hidden');
                    break;
                case 'category':
                    $(selectCategory).prop('disabled', false);
                    $(selectTag).prop('disabled', 'disabled');

                    divSelectDataTypeValue.removeClass('hidden');
                    $(selectCategory).removeClass('hidden');
                    $(selectTag).addClass('hidden');

                    $(lblDataTypeValue).text('Category');
                    break;
                case 'tag':
                    $(selectTag).prop('disabled', false);
                    $(selectCategory).prop('disabled', 'disabled');

                    divSelectDataTypeValue.removeClass('hidden');
                    $(selectTag).removeClass('hidden');
                    $(selectCategory).addClass('hidden');

                    $(lblDataTypeValue).text('Tag');
                    break;
            }
        });
    }

    /**
     * Handle rename each section when layout type changed
     */
    function handleRenameEachSectionWhenLayoutTypeChange() {
        $(selectLayoutType).on('change', function () {
            renameSectionTitle($(this));
        });
    }

    /**
     * Rename each section title when layout type change
     * @param $element
     */
    function renameSectionTitle($element) {
        var $sectionTitle = $element.closest('div.portlet-body.form').siblings().find('div.caption.text-capitalize'),
            $selectSectionType = $element.parent().parent().parent().siblings().find('select[name="section_type[]"]');

        $sectionTitle.html($selectSectionType.val() + " " + $element.val());
    }

    /**
     * Handle validate data offset
     */
    function handleValidateDataOffset() {
        $(document).on('blur', '#app-layout-new-action-wrapper input[name="data_offset[]"]', function () {
            $(this).val(parseInt($(this).val()));
        });

        $(document).on('blur', '#app-layout-dashboard-wrapper input[name="data_offset[]"]', function () {
            $(this).val(parseInt($(this).val()));
        });
    }

    /**
     * Clone new section to main form
     */
    function cloneSection() {
        $("#app-layout-new-action-wrapper .select2-multiple").select2('destroy');
        var $original = $(originalSection),
            $cloned = $original.clone(true).attr('id', 'new_form' + indexBox);

        // get original selects into a jq object
        var $originalSelects = $original.find('select');
        $cloned.find('select').each(function (index, item) {
            // set new select to value of old select
            $(item).val($originalSelects.eq(index).val());
        });

        // get original textareas into a jq object
        var $originalTextareas = $original.find('textarea');
        $cloned.find('textarea').each(function (index, item) {
            // set new textareas to value of old textareas
            $(item).val($originalTextareas.eq(index).val());
        });

        // remove button "Add Section" and add button "Save"
        $cloned.find('.group-button-action button').remove();
        $cloned.find('.group-button-action').append('<button type="button" class="btn green pull-right btn-each-section-save"><i class="fa fa-save"></i>&nbsp;Save</button>');

        var html = $("#box-item-container").clone(true).removeClass('hidden').attr('id', 'frm-section' + indexBox);
        html.find('.caption').html($cloned.find("select[name='section_type[]']").val() + " " + $cloned.find("select[name='layout[]']").val());
        $cloned.appendTo(html.find('.portlet-body'));
        html.appendTo($(appendDestination));

        $("#app-layout-new-action-wrapper .select2-multiple").select2();
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .select2-multiple").select2();
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .select2-container--bootstrap").css('width', '100%');
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .select2-container--bootstrap .select2-search__field").css('width', '100%');
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-count").html($("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-count .input_touchspin_vertical2").clone());
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-offset").html($("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-offset .input_touchspin_vertical2").clone());
        $("#app-layout-new-action-wrapper .select2-container--bootstrap").css('width', '100%');
        $("#app-layout-new-action-wrapper .select2-container--bootstrap .select2-search__field").css('width', '100%');
        indexBox++;

        // subscribe change event to modify section title when Layout type changed
        $($cloned.find("select[name='layout[]']")).on('change', function () {
            renameSectionTitle($(this));
        });
        resetTouchSpin();
    }

    /**
     * Set min height right panel
     */
    function setMinHeightRightPanel() {
        $("#app-layout-new-action-wrapper > .portlet").css('min-height', $("#app-layout-dashboard-wrapper > .portlet").css('height'));
    }

    /**
     * Set min height left panel
     */
    function setMinHeightLeftPanel() {
        $("#app-layout-dashboard-wrapper > .portlet").css('min-height', $("#app-layout-new-action-wrapper > .portlet").css('height'));
    }

    /**
     * Save original form to ask user before they are leaving
     */
    function saveOriginalForm() {
        setTimeout(function () {
            if ($(form).length > 0) {
                originalForm = $(form).serialize();
            }
        }, 1000);
    }

    /**
     * When the form has changed, ask user before they leave
     */
    function _askBeforeLeave() {
        // if the form has changed and we are in app dashboard layout setting page
        if (originalForm != null && $(form).length > 0 && $(form).serialize() != originalForm) {
            // Refer: https://stackoverflow.com/questions/1119289/how-to-show-the-are-you-sure-you-want-to-navigate-away-from-this-page-when-ch
            return true;
        }
    }

    function resetTouchSpin() {
        if ($.fn.TouchSpin) {
            $(".input_touchspin_vertical2").TouchSpin({
                min: 0,
                max: 10000,
                step: 1,
                verticalbuttons: true,
                verticalupclass: 'glyphicon glyphicon-plus',
                verticaldownclass: 'glyphicon glyphicon-minus'
            });
        }
    }

    return {
        init: function init() {
            // enable sort by dragging items
            var sortElement = document.getElementById('frm-new-section');
            if (sortElement != null) {
                Sortable.create(sortElement, {});
            }

            // collapse all sections if have
            $('#app-layout-dashboard-portlet-body a.collapse').trigger('click');

            setMinHeightLeftPanel();

            // register event handlers
            handleAddSectionButton();
            handleDeleteSectionButton();
            handleChangeBackground();
            handleSaveAppDashboardLayoutButton();
            handleSelectDataTypeChange();
            handleSaveEachSectionInLeftPanel();
            handleRenameEachSectionWhenLayoutTypeChange();
            handleValidateDataOffset();
            saveOriginalForm();
            resetTouchSpin();
        },
        askBeforeLeave: function askBeforeLeave() {
            return _askBeforeLeave();
        }
    };
}();

jQuery(document).ready(function () {
    FomoAppDashboardLayout.init();

    $(window).on("beforeunload", function () {
        return FomoAppDashboardLayout.askBeforeLeave();
    });
});

/***/ }),
/* 8 */
/***/ (function(module, exports) {

(function ($) {
    $(document).ready(function () {

        var postSortNestable = null;
        var sorting = $('#fomo-update-post-setting-display .nestable');
        if (sorting.length > 0) {
            postSortNestable = sorting.nestable({
                group: 1,
                maxDepth: 1, // this is important if you have the same case of the question
                reject: [{
                    rule: function rule() {
                        // The this object refers to dragRootEl i.e. the dragged element.
                        // The drag action is cancelled if this function returns true
                        var ils = $(this).find('>ol.dd-list > li.dd-item');
                        for (var i = 0; i < ils.length; i++) {
                            var datatype = $(ils[i]).data('type');
                            if (datatype === 'child') return true;
                        }
                        return false;
                    },
                    action: function action(nestable) {
                        // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element,
                        // and nestable is, well, the nestable root element
                    }
                }]
            });
        }

        $('#fomo-update-post-setting-display').off('click', '.fomo-app-setting-post-display').on('click', '.fomo-app-setting-post-display', function (e) {
            var current = $(e.currentTarget);
            var sortingList = current.closest('form').find('.fomo-post-sorting-list');
            if (sortingList.length > 0) {
                var currentId = current.attr('id');
                var currentItem = sortingList.find('li#' + currentId);
                if (current.is(':checked')) {
                    currentItem.removeClass('disabled');
                } else {
                    currentItem.addClass('disabled');
                }
            }
        });
    });
})(jQuery);

/***/ }),
/* 9 */
/***/ (function(module, exports) {

AppPushNotification = function () {

    var $pushBtn = $('.fomo-push-noti-btn'),
        $customText = $('#fomo-push-custom-text');

    /**
     * Auto focus on the fist child
     * */
    function pushManualMessage() {
        $pushBtn.on('click', function (e) {

            e.preventDefault();

            var $this = $(this),
                content = $this.closest('.tab-pane').find('textarea').val(),
                form = $this.closest('form');

            // Find news id
            var news_id = $this.closest('#push-tab-news').find('select').val();

            if (!content && !news_id) {
                toastr.error($this.data().errorMessage);
                return;
            }

            $.ajax({
                url: $this.data().source,
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                data: new FormData($(form)[0]),
                beforeSend: function beforeSend() {
                    $.blockUI(FOMO.blockMetronicUI);
                },
                success: function success(response) {
                    $.unblockUI();
                    toastr.success(response.data.message);
                },
                error: function error(jqXHR, exception) {
                    $.unblockUI();
                    if (jqXHR.status === 400) {
                        toastr.error(jqXHR.responseJSON.message);
                    }
                }
            });
        });
    }

    /**
     * Validate max characters of custom message before push notification
     */
    function validateMaxLengthOfCustomMessage() {
        $customText.maxlength({
            alwaysShow: true,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger",
            preText: 'You typed ',
            separator: ' out of ',
            postText: ' chars available.',
            validate: true
        });
    }

    /**
     * Set active tab to hidden input
     */
    function trackActiveTab() {
        $('.tabbable-custom a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var $form = $(this).closest('form'),
                target = $(e.target).attr('href'),
                active = target == '#push-tab-news' ? 'news' : 'custom';
            $form.find('input[name="active_tab"]').val(active);
        });
    }

    return {
        //main function to initiate the module
        init: function init() {
            pushManualMessage();
            validateMaxLengthOfCustomMessage();
            trackActiveTab();
        }
    };
}();

jQuery(document).ready(function () {
    AppPushNotification.init();
});

/***/ }),
/* 10 */
/***/ (function(module, exports) {

(function ($) {
    $(document).ready(function () {
        $('#app_font').off('change').on('change', function (e) {
            var current = $(e.currentTarget);
            var exampleText = current.closest('.tab-pane').find('#fomo-font-example');
            if (exampleText.length > 0) {
                var option = current.find('option:selected');
                var fontClass = option.attr('data-font-name');
                fontClass = typeof fontClass != 'undefined' ? fontClass : '';
                exampleText.attr('class', fontClass);
            }
        });
    });
})(jQuery);

/***/ }),
/* 11 */
/***/ (function(module, exports) {

$(document).ready(function () {

    var sortNestable = null;
    var sorting = $('#fomo-update-sorting .nestable');
    if (sorting.length > 0) {
        sortNestable = sorting.nestable({
            group: 1,
            maxDepth: 1, // this is important if you have the same case of the question
            reject: [{
                rule: function rule() {
                    // The this object refers to dragRootEl i.e. the dragged element.
                    // The drag action is cancelled if this function returns true
                    var ils = $(this).find('>ol.dd-list > li.dd-item');
                    for (var i = 0; i < ils.length; i++) {
                        var datatype = $(ils[i]).data('type');
                        if (datatype === 'child') return true;
                    }
                    return false;
                },
                action: function action(nestable) {
                    // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element,
                    // and nestable is, well, the nestable root element
                }
            }]
        });
    }

    $('#authors-sorting-full').off('click').on('click', function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var myData = current.closest('form').find('#myData');
        if (myData.length > 0) {
            myData.val(JSON.stringify(sortNestable.nestable('serialize')));
        }
        current.closest('form').submit();
    });

    $('body').off('click', '#btn-open-sorting-all-author').on('click', '#btn-open-sorting-all-author', function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var url = current.data('url');
        if (typeof url !== 'undefined' && url != '') {
            window.open(url, '_blank');
        }
    });
});

/***/ }),
/* 12 */
/***/ (function(module, exports) {

$(document).ready(function () {
    var sortNestable = null;
    var modalSorting = '#fomo-modal-update-sorting';

    $(document).on('click', "#btn-open-sorting-nestablelist", function () {
        $(modalSorting).modal('show');
        var dataRow = $("table#authors-datatable tbody >tr");
        var dataThRow = $("table#authors-datatable thead tr.heading").clone();

        dataThRow.find("th:first-child").remove();
        dataThRow.find("th:last-child").remove();

        var max_column = 4;
        var text_li = '<div class="tb-fomo-nestable">';
        dataThRow.find('th').each(function (i, e) {
            text_li += "<div class='st-col-left st-col st-col-" + i + "'>" + $(e).html() + "</div>";
            if (i + 1 >= max_column) {
                return false;
            }
        });
        text_li += "</div>";

        $(modalSorting + " .modal-body ol.dd-list").html('<li class="item-heading"><div class="dd-heading">' + text_li + '</div></li>');

        dataRow.each(function (index, element) {
            ele = $(element).clone();
            var dataId = $(ele).find('input[name="id[]"]').val();
            $(ele).find("td:first-child").remove();
            $(ele).find("td:last-child").remove();
            var text_li = '<div class="tb-fomo-nestable">';
            ele.find('td').each(function (i, e) {
                text_li += "<div class='st-col-left st-col st-col-" + i + "'>" + $(e).html() + "</div>";
                if (i + 1 >= max_column) {
                    return false;
                }
            });
            text_li += "</div>";
            $(modalSorting + " .modal-body ol.dd-list").append('<li class="dd-item" data-id="' + dataId + '"> <div class="dd-handle">' + text_li + '</div></li>');
        });

        sortNestable = $(modalSorting + ' .nestable').nestable({
            group: 1,
            maxDepth: 1, // this is important if you have the same case of the question
            reject: [{
                rule: function rule() {
                    // The this object refers to dragRootEl i.e. the dragged element.
                    // The drag action is cancelled if this function returns true
                    var ils = $(this).find('>ol.dd-list > li.dd-item');
                    for (var i = 0; i < ils.length; i++) {
                        var datatype = $(ils[i]).data('type');
                        if (datatype === 'child') return true;
                    }
                    return false;
                },
                action: function action(nestable) {
                    // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element,
                    // and nestable is, well, the nestable root element
                }
            }]
        });
    });

    //save
    $(document).on('submit', modalSorting + ' form', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: { myData: JSON.stringify(sortNestable.nestable('serialize')) },
            beforeSend: function beforeSend() {
                $(modalSorting + " .modal-content .spinner-loading").show();
            },
            success: function success(response) {
                $(modalSorting + " .modal-content .spinner-loading").hide();
                $(modalSorting).modal('hide');
                if (response.errors == false) {
                    toastr.success(response.data.message);
                }
                //reload table
                $("table#authors-datatable .btn-filter-cancel").trigger('click');
            },
            error: function error(jqXHR, exception) {
                $.unblockUI();
                $(modalSorting + " .modal-content .spinner-loading").hide();
                if (jqXHR.status === 422) {
                    var errors = "";
                    data = jqXHR.responseJSON;
                    for (var k in data) {
                        if (data.hasOwnProperty(k)) {
                            data[k].forEach(function (val) {
                                errors += val;
                            });
                        }
                    }
                    toastr.error(errors);
                }
            }
        });
    });
});

/***/ }),
/* 13 */
/***/ (function(module, exports) {

FomoForm = function () {

    /**
     * Auto focus on the fist child
     * */
    function autoFocusOnTheFistElm() {
        $('form').find('input:not(:hidden)').first().focus();
    }

    function enableDateTimePicker() {
        if ($('.fomo-list-page-date-filter').size() > 0) {
            $('.fomo-list-page-date-filter').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                minView: 2
            });
        }
        if ($('input.fomo-list-page-time-filter').size() > 0) {
            $('input.fomo-list-page-time-filter').inputmask('hh:mm', {
                placeholder: "00:00"
            });
        }
        if ($('.fomo-list-page-datetime-filter').size() > 0) {
            $('.fomo-list-page-datetime-filter').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                minView: 2
            });
        }

        if ($('.date-time-picker').size() > 0) {
            $('.date-time-picker').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                pickTime: false,
                minView: 2
            });
        }

        if ($('.btn-clear-date-filter').size() > 0) {
            $(document).on('click', '.btn-clear-date-filter', function () {
                $(this).parent().siblings('input').val('');
                $(this).parent().siblings('input').trigger('change');
            });
        }
    }

    function enableUploadFile() {
        var btnUploadImage = '.btn-upload-image',
            btnRemoveImage = '.btn-remove-image';

        if (!$.fn.rvMedia) {
            return;
        }

        // upload image
        $(btnUploadImage).rvMedia({
            multiple: false,
            onSelectFiles: function onSelectFiles(files, $el) {
                var firstItem = _.first(files);
                if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && !firstItem.type.match('image')) {} else if (typeof $el !== 'undefined' && checkFileTypeSelect(firstItem, $el)) {
                    $el.closest('.image-box').find('.image-data').val(firstItem.id);

                    var dataRVMedia = $el.closest('.image-box').find('.preview_image').data('rv-media');
                    dataRVMedia[0].selected_file_id = firstItem.id;

                    $el.closest('.image-box').find('.preview_image').attr('src', firstItem.thumb).data('rv-media', dataRVMedia).show();

                    if (typeof dataRVMedia[0].full_url !== 'undefined' && dataRVMedia[0].full_url) {
                        $el.closest('.image-box').find('.preview_image').attr('src', firstItem.full_url).data('rv-media', dataRVMedia).show();
                    } else {
                        $el.closest('.image-box').find('.preview_image').attr('src', firstItem.thumb).data('rv-media', dataRVMedia).show();
                    }

                    $el.closest('.image-box').find(btnRemoveImage).show();
                    $el.closest('.image-box').find(btnUploadImage).hide();
                }
            }
        });

        // remove image
        $('.image-box').on('click', btnRemoveImage, function (event) {
            event.preventDefault();
            $(this).closest('.image-box').find('input').val('');

            var $previewImg = $(this).closest('.image-box').find('img.preview_image'),
                defaultSrc = $previewImg.attr('data-default-src');
            if (typeof defaultSrc !== 'undefined' && defaultSrc !== '') {
                $previewImg.attr('src', defaultSrc).show();
            } else {
                $(this).closest('.image-box').find('img').hide();
            }

            $(this).closest('.image-box').find(btnUploadImage).show();
            $(this).closest('.image-box').find(btnRemoveImage).hide();
        });
        previewImageMedia();
    }

    //preview image
    function previewImageMedia() {
        $(".rvMedia_preview_file").each(function () {
            $(this).rvMedia({
                multiple: false,
                onSelectFiles: function onSelectFiles(files, $el) {
                    var firstItem = _.first(files);
                    if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && firstItem.type.match('image') && typeof $el !== 'undefined' && checkFileTypeSelect(firstItem, $el)) {
                        $el.parent().find('.image-data').val(firstItem.id);
                        var dataRVMedia = $el.data('rv-media');
                        dataRVMedia[0].selected_file_id = firstItem.id;
                        if (typeof dataRVMedia[0].full_url !== 'undefined' && dataRVMedia[0].full_url) {
                            $el.attr('src', firstItem.full_url).data('rv-media', dataRVMedia).show();
                        } else {
                            $el.attr('src', firstItem.thumb).data('rv-media', dataRVMedia).show();
                        }
                    }
                }
            });
        });
    };

    function enableUploadGalleryImages() {
        var btnUploadImage = '.btn-gallery-upload-image';
        if ($(btnUploadImage).is(':visible')) {
            // upload gallery images
            $(btnUploadImage).rvMedia({
                multiple: true,
                onSelectFiles: function onSelectFiles(files, $el) {
                    var $gallery = $('.gallery-images'),
                        imagePlaceHolder = $gallery.find('li.image-placeholder');
                    if (typeof files !== 'undefined') {
                        for (var i = 0; i < files.length; i++) {
                            // only pics are processed
                            if (!files[i].type.match('image')) {
                                continue;
                            }

                            var $image = imagePlaceHolder.clone().removeClass('image-placeholder').removeClass('hidden').insertBefore(imagePlaceHolder);
                            if (typeof $image !== 'undefined' && $image) {
                                $image.attr('data-id', files[i].id);
                                $image.find('img').addClass('rvMedia_preview_file').attr('src', files[i].url).data('rv-media', [{ 'selected_file_id': files[i].id }]);
                                $image.find('a.btn-delete-gallery-img').attr('data-id', files[i].id);
                                $image.find('input[type="hidden"]').attr('value', files[i].id);
                                $image.find('input[type="hidden"]').attr('name', 'galleries[]');
                            }
                        }
                        previewImageMedia();
                    }
                }
            });

            // handle remove gallery images
            $(document).on('click', '.btn-delete-gallery-img', function (e) {
                var dataId = $(this).attr('data-id');
                if (typeof dataId !== 'undefined' && dataId && $(this).parent().attr('data-id') === dataId) {
                    $(this).parent().remove();
                }
            });
        }
    }

    function checkFileTypeSelect(selectedFile, ele) {
        if (ele !== 'undefined') {
            var ele_options = ele.data('rv-media');
            if (typeof ele_options !== 'undefined' && typeof ele_options[0] !== 'undefined' && typeof ele_options[0].file_type !== 'undefined' && selectedFile !== 'undefined' && selectedFile.type !== 'undefined') {
                if (!ele_options[0].file_type.match(selectedFile.type)) {
                    return false;
                } else {
                    if (typeof ele_options[0].ext_allowed !== 'undefined' && $.isArray(ele_options[0].ext_allowed)) {
                        if ($.inArray(selectedFile.mime_type, ele_options[0].ext_allowed) == -1) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    function handleSelectFiles() {
        var btnTrigger = '.btn-gallery-select-file';
        if ($(btnTrigger).is(':visible')) {
            $(btnTrigger).rvMedia({
                multiple: true,
                onSelectFiles: function onSelectFiles(files, $el) {
                    var $list = $('.list-files'),
                        filePlaceHolder = $list.find('li.file-placeholder');
                    for (var i = 0; i < files.length; i++) {
                        var $file = filePlaceHolder.clone().removeClass('file-placeholder').removeClass('hidden').insertBefore(filePlaceHolder);
                        $file.attr('data-id', files[i].id);
                        $file.find('.file-icon').attr('class', files[i].icon);
                        $file.find('.file-item').attr('href', files[i].url).text(files[i].basename);
                        $file.find('a.btn-delete-file').attr('data-id', files[i].id);
                        $file.find('input[type="hidden"]').attr('value', files[i].id).attr('name', 'attachments[]');
                    }
                }
            });

            // handle remove gallery images
            $(document).on('click', '.btn-delete-file', function (e) {
                var dataId = $(this).attr('data-id');
                if (typeof dataId !== 'undefined' && dataId && $(this).parent().attr('data-id') === dataId) {
                    $(this).parent().remove();
                }
            });
        }
    }

    function initResources() {
        $('.select2, .select2-multiple').select2({
            placeholder: $(this).data('placeholder'),
            width: null
        });

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5,
                defaultTime: false
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            enableDateTimePicker();
            enableUploadFile();
            enableUploadGalleryImages();
            initResources();
            handleSelectFiles();
            autoFocusOnTheFistElm();
        }
    };
}();

jQuery(document).ready(function () {
    FomoForm.init();
});

/***/ }),
/* 14 */
/***/ (function(module, exports) {

FomoLinkMarkedText = function () {
    var fomoRange = null;
    var fomoPopupBtnApply = '#fomo-link-marked-text .btn-link-marked-text';
    var fomoPopupSelectNews = '#fomo-news-internal-link';
    var fomoPopupLinkMarkedText = '#fomo-link-marked-text';
    var fomoBtnLinkMarkedText = '.link-marked-text';
    var fomoClassAttr = 'fomo-custom-link';

    function handleSubmitButton() {
        $(fomoPopupBtnApply).off('click').on('click', function (e) {
            // not process when button is disabled
            e.preventDefault();
            if ($(this).prop('disabled') || fomoRange == null) {
                showHideCustomLinkPopup('hide', 0);
                return;
            }
            var current = $(e.currentTarget);
            var rng = fomoRange;
            var text = rng.toString();
            rng = rng.deleteContents();
            //get url
            var newsId = $(fomoPopupSelectNews).val();
            var newsTitle = $(fomoPopupSelectNews + ' option:selected').text();
            var linkUrl = '';
            if (newsId == '' || newsId == null) {
                return;
            } else {
                linkUrl = $(fomoPopupBtnApply).attr('pattern-href') + newsId;
            }
            if (rng.nodes().length > 0) {
                if (current.attr('action') != 0) {
                    var anchor = rng.nodes()[0].parentNode;
                    current.attr('action', 0);
                } else {
                    if (text == '') {
                        return;
                    }
                    if ($(rng.nodes()[0]).prop("tagName") != "A") {
                        var anchor = rng.insertNode($('<A>' + text + '</A>')[0]);
                    } else {
                        var anchor = rng.nodes()[0];
                    }
                    anchor.text = text;
                }
            } else {
                if (text == '') {
                    return;
                }
                var anchor = rng.insertNode($('<A>' + text + '</A>')[0]);
            }

            $(anchor).attr('class', 'fomo-custom-link');
            $(anchor).attr('href', linkUrl);
            $(anchor).attr('data-news-id', newsId);
            $(anchor).attr('title', newsTitle);
            $(anchor).attr('data-type', 'text link');
            $('#description').summernote('editor.insertText', '');
            fomoRange = null;
            showHideCustomLinkPopup('hide', 0);
        });
    }

    function showHideCustomLinkPopup(action, type) {
        var markedLinkModal = $(fomoPopupLinkMarkedText);
        if (action == 'show') {
            if (fomoRange != null && typeof fomoRange.nodes()[0] !== 'undefined' && $(fomoRange.nodes()[0].parentNode).prop('tagName') == 'A') {
                var newsId = $(fomoRange.nodes()[0].parentNode).attr('data-news-id');
                var newsTitle = $(fomoRange.nodes()[0].parentNode).attr('title');
                var select = $(fomoPopupSelectNews + ' option[value="' + newsId + '"]');
                if (select.length <= 0) {
                    $(fomoPopupSelectNews).append('<option value="' + newsId + '">' + newsTitle + '</option>');
                }
                $(fomoPopupSelectNews).val(newsId).trigger('change');
                //get news title
            } else {
                $(fomoPopupSelectNews).val('').trigger('change');
            }
        }

        var btnApply = $(fomoPopupBtnApply);
        if (btnApply.length > 0) {
            btnApply.attr('action', type);
        }

        markedLinkModal.modal(action);
    }

    function handleShowCustomLinkDialog() {
        var classLink = '.' + fomoClassAttr;
        $('.note-editor .note-editable').off('click', classLink).on('click', classLink, function (e) {
            e.preventDefault();
            fomoRange = $('#description').summernote('createRange');
            showHideCustomLinkPopup('show', 1);
        });
    }

    function searchNewsPopup() {
        if ($(fomoPopupSelectNews).size() > 0 && $(fomoPopupSelectNews).attr('data-ajax-src') != '') {
            // get per page
            var perPage = $(fomoPopupSelectNews).attr('data-per-page');
            if (typeof perPage == 'undefined' || perPage == '') {
                perPage = 10;
            }
            $(fomoPopupSelectNews).select2({
                placeholder: $(fomoPopupSelectNews).attr('data-placeholder'),
                width: '100%',
                ajax: {
                    url: $(fomoPopupSelectNews).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function data(params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function processResults(data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: { more: params.page * perPage < data.total_count }
                        };
                    }
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            handleSubmitButton();
            handleShowCustomLinkDialog();
            searchNewsPopup();
            $(fomoBtnLinkMarkedText).off('click').on('click', function (e) {
                // description
                e.preventDefault();
                fomoRange = $('#description').summernote('createRange');
                if (fomoRange.toString() == '') {
                    return;
                }
                showHideCustomLinkPopup('show', 0);
            });
        }
    };
}();

jQuery(document).ready(function () {
    FomoLinkMarkedText.init();
});

/***/ }),
/* 15 */
/***/ (function(module, exports) {

FomoNewsServices = function () {

    var modalSelector = '#fomo-news-listing-set-categories-tags',
        applyCategoryTagForm = '#apply-categories-tags-form',
        modalTitleSelector = modalSelector + ' .modal-title',
        categoriesList = modalSelector + ' .categories-list',
        tagsList = modalSelector + ' .tags-list',
        newsSelector = modalSelector + ' #news',
        typeHiddenInputSelector = modalSelector + ' input[name="type"]',
        categoryCheckboxSelector = modalSelector + ' input#category-checkbox-',
        tagCheckboxSelector = modalSelector + ' input#tag-checkbox-',
        btnSubmitSelector = modalSelector + ' .modal-footer button.btn-set-categories-tags',
        type = '',
        newsIds = [],
        goodArray = [],
        badArray = [],
        indeterminateArray = [];

    /**
     * Set categories for each selected news
     */
    function _setCategories() {
        if (newsIds.length == 0) {
            return;
        }

        $(newsSelector).val(newsIds); // assign selected news IDs to hidden input
        $(typeHiddenInputSelector).val(type); // assign type of bulk action : set categories
        openModal();
        closeModal();
        selectCheckboxes();
    }

    /**
     * Set tags for each selected news
     */
    function _setTags() {
        if (newsIds.length == 0) {
            return;
        }

        $(newsSelector).val(newsIds); // assign selected news IDs to hidden input
        $(typeHiddenInputSelector).val(type); // assign type of bulk action : set tags
        openModal();
        closeModal();
        selectCheckboxes();
    }

    /**
     * Select checkboxes when opening the modal
     */
    function selectCheckboxes() {
        var array1 = [],
            array2 = [],
            currentIds = [];

        // not do if not know set categories or set tags
        if (typeof type == 'undefined' || type == '' || $.inArray(type, ['category', 'tag']) == -1) {
            console.log('invalid type');
            return;
        }

        // not do if not have selected news
        if (newsIds.length == 0) {
            console.log('invalid selected news');
            return;
        }

        goodArray = [], badArray = [], indeterminateArray = [];
        var selector = type == 'category' ? categoryCheckboxSelector : tagCheckboxSelector,
            dataSelector = type == 'category' ? 'data-categories' : 'data-tags';
        $.each(newsIds, function (index, newsId) {
            // get id (category Ids or tag Ids) of each news and convert to array
            currentIds = $("input.checkboxes[data-news-id='" + newsId + "']").attr(dataSelector);
            currentIds = currentIds.split(',');

            // for the first loop, just add these ids to the array
            if (goodArray.length == 0) {
                goodArray = $.unique(currentIds);
                return true; // continue to next loop
            }

            array1 = $.unique(goodArray); // previous ids
            array2 = $.unique(currentIds); // current ids
            if (array1.length != array2.length || array1.join(',') !== array2.join(',')) {
                // get different items from 2 arrays
                var difference = $.unique($.merge($(array1).not(array2).get(), $(array2).not(array1).get()));
                $.merge(badArray, difference);
            }
        });

        // always disable submit button for the first time the popup dialog open
        $(btnSubmitSelector).prop('disabled', true);

        // get unique elements
        goodArray = $.unique(goodArray);
        badArray = jQuery.grep($.unique(badArray), function (n, i) {
            return n !== "" && n != null && n != 0; // remove empty item
        });

        // if element is existing in badArray, it's not allowed to exist in goodArray
        goodArray = jQuery.grep(goodArray, function (value) {
            return $.inArray(value, badArray);
        });

        // mark these checkboxes as checked state
        if (goodArray.length > 0) {
            $.each(goodArray, function (index, id) {
                $(selector + id).prop('checked', true);
            });
        }

        // mark these checkboxes as indeterminate state
        if (badArray.length > 0) {
            indeterminateArray = badArray;
            $.each(badArray, function (index, id) {
                $(selector + id).prop('indeterminate', true);
                $(selector + id).prop('checked', false);
            });
        }
    }

    /**
     * Enable submit button if "indeterminate" checkboxes are all checked
     *
     * @param string selector
     * @param int totalCheckboxesAreSelected
     */
    function enableSubmitButton(selector, totalCheckboxesSelected) {
        if (typeof selector == 'undefined' || selector == '') {
            return;
        }

        var checkboxSelector = type == 'category' ? '.categories-list input[name="categories[]"]:checked' : '.tags-list input[name="tags[]"]:checked',
            dataSelector = type == 'category' ? 'data-categories' : 'data-tags',
            currentSelectedIds = $(checkboxSelector).map(function () {
            return this.value;
        }).get(),
            finalSelectedIds = indeterminateArray.concat(currentSelectedIds),
            isDisabled = false;

        // must set categories & tags for each news row
        $.each(newsIds, function (index, newsId) {
            var currentIds = $("input.checkboxes[data-news-id='" + newsId + "']").attr(dataSelector).split(',');
            var finalIdsExistInCurrentRow = finalSelectedIds.filter(function (elem) {
                return currentIds.indexOf(elem) != -1;
            }).length;
            if (finalIdsExistInCurrentRow == 0 && totalCheckboxesSelected <= 0) {
                $(btnSubmitSelector).prop('disabled', true);
                isDisabled = true;
                return false;
            }
        });

        if (isDisabled) {
            return;
        }

        if (totalCheckboxesSelected > 0 || totalCheckboxesSelected == 0 && indeterminateArray.length == badArray.length) {
            $(btnSubmitSelector).removeAttr('disabled');
        }
    }

    /**
     * Open modal for 2 type: category or tag
     */
    function openModal() {
        if (type == 'category') {
            $(modalTitleSelector).text('Set categories for selected news');
            $(categoriesList).removeClass('hidden');
            $(tagsList).addClass('hidden');
            $(modalSelector).modal('show');

            // listen on change each checkboxes to enable submit button
            $(document).on('change', modalSelector + ' input[name="categories[]"]', function () {
                handleStateOfIndeterminateCheckboxes($(this));
                var totalSelectedCheckboxes = $('.categories-list input[name="categories[]"]:checked').length;
                enableSubmitButton(modalSelector + ' input[name="categories[]"]', totalSelectedCheckboxes);
            });
        } else if (type == 'tag') {
            $(modalTitleSelector).text('Set tags for selected news');
            $(categoriesList).addClass('hidden');
            $(tagsList).removeClass('hidden');
            $(modalSelector).modal('show');

            // listen on change each checkboxes to enable submit button
            $(document).on('change', modalSelector + ' input[name="tags[]"]', function () {
                handleStateOfIndeterminateCheckboxes($(this));
                var totalSelectedCheckboxes = $('.tags-list input[name="tags[]"]:checked').length;
                enableSubmitButton(modalSelector + ' input[name="tags[]"]', totalSelectedCheckboxes);
            });
        }
    }

    /**
     * On closing the modal, reset controls to default value and remove all event listeners
     */
    function closeModal() {
        $(modalSelector).on('hidden.bs.modal', function () {
            // restore these controls to default value
            $(modalSelector + ' input[name="categories[]"]').removeAttr('checked');
            $(modalSelector + ' input[name="categories[]"]').prop('indeterminate', false);
            $(modalSelector + ' input[name="tags[]"]').removeAttr('checked');
            $(modalSelector + ' input[name="tags[]"]').prop('indeterminate', false);
            $(btnSubmitSelector).removeAttr('disabled');
            $(applyCategoryTagForm + ' input[name="indeterminate_checkboxes"]').remove();
            type = '', goodArray = [], badArray = [], indeterminateArray = [], newsIds = [];

            // remove event listeners
            $(modalSelector).off('hidden.bs.modal');
            $(modalSelector + ' input[name="categories[]"]').off('change');
            $(modalSelector + ' input[name="tags[]"]').off('change');
            $(btnSubmitSelector).off('click');
        });
    }

    /**
     * Call ajax to store categories/tags for selected news
     */
    function handleSubmitButton() {
        $(btnSubmitSelector).off('click');
        $(document).on('click', btnSubmitSelector, function () {
            // not process when button is disabled
            if ($(this).prop('disabled')) {
                return;
            }

            var ajaxUrl = $(modalSelector).find('input#ajax-url').val();
            if (typeof ajaxUrl != 'undefined' && ajaxUrl != '') {

                // also send indeterminate checkboxes to the server
                $(applyCategoryTagForm + ' input[name="indeterminate_checkboxes"]').remove();
                $('<input>').attr({
                    type: 'hidden',
                    name: 'indeterminate_checkboxes',
                    value: indeterminateArray.join(',')
                }).appendTo(applyCategoryTagForm);

                $.ajax({
                    url: ajaxUrl,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: new FormData($(applyCategoryTagForm)[0]),
                    beforeSend: function beforeSend() {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function success(response) {
                        $.unblockUI();
                        $(modalSelector).modal('hide');
                        if (response.errors == false) {
                            toastr.success(response.data.message);

                            // update UI for newly categories/tags
                            if (typeof response.data.news != 'undefined' && response.data.news.length) {
                                $.each(response.data.news, function (index, newsItem) {
                                    // update categories
                                    if (response.data.type == 'category') {
                                        $('input[data-news-id="' + newsItem.id + '"]').attr('data-categories', newsItem.category_ids);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.category-cell').html(newsItem.category_names_with_link);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.category-cell').pulsate({
                                            color: "#51a351",
                                            repeat: false
                                        });
                                    } else if (response.data.type == 'tag') {
                                        // update tags
                                        $('input[data-news-id="' + newsItem.id + '"]').attr('data-tags', newsItem.tag_ids);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.tag-cell').html(newsItem.tag_names_with_link);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.tag-cell').pulsate({
                                            color: "#51a351",
                                            repeat: false
                                        });
                                    }
                                });
                            }
                        }
                    },
                    error: function error(jqXHR, exception) {
                        $.unblockUI();
                        if (jqXHR.status === 400) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    }
                });
            }
        });
    }

    /**
     * Change to checked instead of un-check when the checkbox is in "indeterminate" state
     * @param checkbox $element
     */
    function handleStateOfIndeterminateCheckboxes($element) {
        // if this checkbox is one of indeterminate checkboxes
        if ($.inArray($element.attr('data-id'), indeterminateArray) != -1) {
            // if after clicked, it's still not check -> check it
            if (!$element.prop('checked')) {
                $element.indeterminate = false;
                $element.prop('checked', true);
            }

            // remove indeterminate item
            var removeItem = $element.attr('data-id');
            indeterminateArray = jQuery.grep(indeterminateArray, function (value) {
                return value != removeItem;
            });
        }
    }

    function _bulkActions(ids, ajaxUrl, method, filter) {
        if (ids.length == 0) {
            return;
        }
        $.ajax({
            url: ajaxUrl,
            type: method,
            cache: false,
            data: { 'ids': ids },
            beforeSend: function beforeSend() {
                $.blockUI(FOMO.blockMetronicUI);
            },
            success: function success(response) {
                $.unblockUI();
                $('.table-container .table-group-action-input').val('');
                if (response.errors == false) {
                    toastr.success(response.data.message);
                    AjaxDatatable.filter(filter);
                }
            },
            error: function error(jqXHR, exception) {
                $.unblockUI();
                if (jqXHR.status === 400) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            }
        });
    }

    return {
        init: function init() {
            handleSubmitButton();
        },

        setCategories: function setCategories(ids) {
            newsIds = ids;
            type = 'category';
            _setCategories();
        },

        setTags: function setTags(ids) {
            newsIds = ids;
            type = 'tag';
            _setTags();
        },
        bulkActions: function bulkActions(ids, ajaxUrl, method, filter) {
            if (typeof method == 'undefined') {
                method = 'post';
            }
            filter = filter == 'bulk_trash' ? '' : 1;
            _bulkActions(ids, ajaxUrl, method, filter);
        }
    };
}();

jQuery(document).ready(function () {
    FomoNewsServices.init();
});

/***/ }),
/* 16 */
/***/ (function(module, exports) {

FormValidation = function () {

    var handleFormValidation = function handleFormValidation() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var forms = $('.form-validation');
        var error = $('.alert-danger', forms);
        var success = $('.alert-success', forms);

        forms.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input

            invalidHandler: function invalidHandler(event, validator) {
                //display error alert on form submit
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function errorPlacement(error, element) {
                // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function highlight(element) {
                // hightlight error inputs

                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function unhighlight(element) {
                // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function success(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            }

            //submitHandler: function (form) {
            //    success.show();
            //    error.hide();
            //}
        });

        // validate these controls when changing
        $('.select2, .multi-select').change(function () {
            forms.validate().element($(this));
        });
    };

    var handleWysihtml5 = function handleWysihtml5() {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"],
                "events": {
                    "blur": function blur() {
                        // hide error message if the editor is not empty
                        if (this.textareaElement.value.length > 0) {
                            $(this.textareaElement).closest('.form-group').removeClass('has-error');
                            $(this.textareaElement).siblings().find('.help-block.help-block-error').hide();
                        }
                    }
                }
            });
        }
    };

    var handleSummernote = function handleSummernote() {
        if ($('.summernote').size() > 0) {
            $('.summernote').summernote({
                // customize summernote toolbar, @ticket #13187
                toolbar: [['style', ['style']], ['font', ['bold', 'italic', 'underline', 'clear']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['insert', ['link']], ['misc', ['fullscreen']]],
                height: 300,
                callbacks: {
                    onBlur: function onBlur() {
                        if ($(this).summernote('isEmpty')) {
                            $(this).parent().parent('.form-group').addClass('has-error');
                            $(this).parent().find('.help-block.help-block-error').show();
                            $(this).summernote('code', '');
                        } else {
                            // hide error message if the editor is not empty
                            $(this).parent().parent('.form-group').removeClass('has-error');
                            $(this).parent().find('.help-block.help-block-error').hide();
                        }
                    }
                }
            });
        }
    };

    return {
        //main function to initiate the module
        init: function init() {

            handleSummernote();
            handleWysihtml5();
            handleFormValidation();
        }

    };
}();

jQuery(document).ready(function () {
    FormValidation.init();
});

/***/ }),
/* 17 */
/***/ (function(module, exports) {

FOMO = {
    blockUI: {
        message: '<div class="loader"></div>',
        css: { "border": "none", "background": "none" }
    },
    blockMetronicUI: {
        message: '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>',
        css: { "border": "none", "background": "none" },
        centerX: true,
        centerY: true
    },
    'saveBtnForm': '#fomo-save-btn',
    'overrideSaveFrmClass': 'fomo-override-save-action',
    'listeningFrm': 'form.submit-frm'
};

(function ($) {
    $(function () {
        /**
         * Get query string parameter by name
         *
         * @param name
         * @param url
         * @returns {*}
         */
        function getQueryStringParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Handle ajax error
        $(document).ajaxError(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {

            if (xhr.status === 401) {}
        });

        // handle finish ajax calls
        $(document).ajaxStop(function () {
            $('.tooltips').tooltip();
        });
    });
})(jQuery);

/***/ }),
/* 18 */
/***/ (function(module, exports) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* ========================================================================
 * AddMedia.js v1.0
 * Requires Botble Media
 *
 * ======================================================================== */

+function ($) {
    'use strict';

    var AddMedia = function AddMedia(element, options) {
        this.options = options;
        $(element).rvMedia({
            multiple: true,
            onSelectFiles: function onSelectFiles(files, $el) {
                if (typeof files != 'undefined') {
                    if (typeof $(options.destination).attr('data-editor') != 'undefined' && $(options.destination).attr('data-editor') == 'summernote') {
                        handleInsertImagesForSummerNote(files);
                    } else {
                        var editor = $(options.destination).data("wysihtml5").editor;
                        handleInsertImagesForWysihtml5Editor(editor, files);
                    }
                }
            }
        });
    };

    AddMedia.VERSION = '1.1.0';

    /**
     * Insert images to summernote editor
     * @param files
     */
    function handleInsertImagesForSummerNote(files) {
        if (files.length == 0) {
            return;
        }

        for (var i = 0; i < files.length; i++) {
            if (!files[i].type.match('image')) {
                continue;
            }
            var image_url = typeof base_url != 'undefined' ? base_url + files[i].url : files[i].url;
            $('.summernote').summernote('insertImage', image_url, files[i].basename);
        }
    }

    /**
     * Insert images to Wysihtml5 editor
     * @param editor
     * @param files
     */
    function handleInsertImagesForWysihtml5Editor(editor, files) {
        if (files.length == 0) {
            return;
        }

        // insert images for the wysihtml5 editor
        var s = '';
        for (var i = 0; i < files.length; i++) {
            if (!files[i].type.match('image')) {
                continue;
            }
            s += '<img src="' + RV_MEDIA_URL.base_url + files[i].url + '">';
        }

        if (editor.getValue().length > 0) {
            var length = editor.getValue();
            editor.composer.commands.exec("insertHTML", s);
            if (editor.getValue() == length) editor.setValue(editor.getValue() + s);
        } else {
            editor.setValue(editor.getValue() + s);
        }
    }

    function CallAction(option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('bs.media');
            var options = $.extend({}, $this.data(), (typeof option === 'undefined' ? 'undefined' : _typeof(option)) == 'object' && option);
            if (!data) $this.data('bs.media', data = new AddMedia(this, options));
        });
    }

    $.fn.addMedia = CallAction;
    $.fn.addMedia.Constructor = AddMedia;

    $(window).on('load', function () {
        $('[data-add-media="app-add-media"]').each(function () {
            var $addMedia = $(this);
            CallAction.call($addMedia, $addMedia.data());
        });
    });
}(jQuery);

/***/ }),
/* 19 */
/***/ (function(module, exports) {

FomoNewsForm = function () {
    function searchAuthors() {
        var selector = '.js-author-data-ajax';
        if ($(selector).size() > 0 && $(selector).attr('data-ajax-src') != '') {
            // get per page
            var perPage = $(selector).attr('data-per-page');
            if (typeof perPage == 'undefined' || perPage == '') {
                perPage = 10;
            }

            $(selector).select2({
                placeholder: $(selector).attr('data-placeholder'),
                ajax: {
                    url: $(selector).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function data(params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function processResults(data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: { more: params.page * perPage < data.total_count }
                        };
                    }
                }
            });
        }
    }

    function searchNews() {
        var selector = '.js-news-data-ajax';
        if ($(selector).size() > 0 && $(selector).attr('data-ajax-src') != '') {
            var excerptNewsId = $(selector).attr('data-excerpt-news-id');
            if (typeof excerptNewsId == 'undefined' || excerptNewsId == '') {
                excerptNewsId = -1;
            }

            $(selector).select2({
                ajax: {
                    url: $(selector).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function data(params) {
                        return {
                            q: params.term, // search term
                            excerpt_news_id: excerptNewsId // excerpt current news
                        };
                    },
                    processResults: function processResults(data, params) {
                        return {
                            results: data.items
                        };
                    }
                }
            }).on('change', function () {
                /**
                 * Hook to on change event of Select2 plugin to display select result into new div
                 */
                var $selected = $(this).find('option:selected');
                var $container = $('.js-related-news-container');

                var $list = $('<ul>');
                $selected.each(function (k, v) {
                    // build detail news link
                    var detailUrl = 'javascript:;';
                    if (typeof $(selector).attr('data-news-detail-url') != 'undefined') {
                        detailUrl = $(selector).attr('data-news-detail-url').replace('-1', $(v).attr('value'));
                    }
                    var detailNewsLink = '<a target="_blank" href="' + detailUrl + '">' + $(v).text() + '</a>';

                    var $li = $('<li class="tag-selected"><a class="destroy-tag-selected">×</a>' + detailNewsLink + '</li>');
                    $li.children('a.destroy-tag-selected').off('click.select2-copy').on('click.select2-copy', function (e) {
                        var $opt = $(this).data('select2-opt');
                        $opt.attr('selected', false);
                        $opt.attr('value', ''); // no need to store duplicated data here
                        $opt.parents('select').trigger('change');
                    }).data('select2-opt', $(v));
                    $list.append($li);
                });
                $container.html('').append($list);
            }).trigger('change');
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            searchAuthors();
            searchNews();
        }
    };
}();

jQuery(document).ready(function () {
    FomoNewsForm.init();
});

/***/ }),
/* 20 */
/***/ (function(module, exports) {

$(document).ready(function () {
    $(window).on('load', function () {
        $(".news-show #load-more-related-news").trigger("click");
    });

    $(".fancybox").fancybox();

    var $picContainer = $('.blog-single-img'),
        $pic = $('.blog-single-img img'),
        widthPercentage = $pic.prop('naturalWidth') / $picContainer.width() * 100;

    // we will not apply focus point plugin if the image width is less than the pic container width
    if ($picContainer.length && $picContainer.hasClass('focuspoint') && widthPercentage <= 50) {
        $picContainer.removeClass('focuspoint');
        $pic.addClass('img-responsive pic-bordered pic-cover');
    } else {
        $('.focuspoint').focusPoint();
    }
});

/***/ }),
/* 21 */
/***/ (function(module, exports) {

FomoPageBar = function () {
    var originForm;

    /**
     * Handle click on the Save icon on the page bar
     * @author vulh
     * */
    function handleSaveFormBtn() {
        $('.page-bar').on('click', FOMO.saveBtnForm, function (e) {
            if ($(this).hasClass(FOMO.overrideSaveFrmClass)) {
                return false;
            }
            e.preventDefault();

            var pageContent = $(this).closest('.page-content');
            var form = FOMO.listeningFrm;
            var formIsValid = true;
            try {
                formIsValid = $(form).valid();
            } catch (e) {
                formIsValid = true;
            }

            if (formIsValid) {
                pageContent.find(form).submit();
            } else {
                toastr.error($(this).data().errorMsg);
            }
        });
    }

    /**
     * Handle click on the Delete forever icon on the page bar
     * @author vulh
     * */
    function handleDeleteForeverFormBtn() {
        $('.page-bar').on('click', '#fomo-single-delete-btn', function (e) {
            e.preventDefault();
            $('#fomo-single-confirm-delete-modal').modal('show');
        });
    }

    /**
     * Handle click on the View icon on the page bar
     * */
    function handleViewFormBtn() {
        $('.page-bar').on('click', '#fomo-view-btn', function (e) {
            askBeforeLeave(e);
        });
    }

    /**
     * Handle click on the Create icon on the page bar
     * */
    function handleCreateFormBtn() {
        $('.page-bar').on('click', '#fomo-create-btn', function (e) {
            askBeforeLeave(e);
        });
    }

    /**
     * Ask to confirm user before leave to new page (handle for View & Create buttons in the breadcrumb page bar)
     */
    function askBeforeLeave(e) {
        // if the form has changed
        if (typeof originForm != 'undefined' && originForm && $('form.form-validation:not(.rv-form)').serialize() != originForm) {
            e.preventDefault();

            var $anchor = $(e.currentTarget);
            swal({
                type: 'info',
                title: 'Confirm',
                text: 'Do you want to leave?',
                allowOutsideClick: true,
                showCancelButton: true,
                confirmButtonClass: 'green'
            }, function (isConfirmOK) {
                if (isConfirmOK) {
                    window.location.href = $anchor.attr('href');
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function init() {

            setTimeout(function () {
                if ($('form.form-validation:not(.rv-form)')) {
                    originForm = $('form.form-validation:not(.rv-form)').serialize();
                }
            }, 1000);

            handleSaveFormBtn();
            handleDeleteForeverFormBtn();

            // ask user before leave
            handleViewFormBtn();
            handleCreateFormBtn();
        }
    };
}();

jQuery(document).ready(function () {
    FomoPageBar.init();
});

/***/ }),
/* 22 */
/***/ (function(module, exports) {

function validateIsNumber($ele) {
    $(document).on('blur', $ele, function () {
        $(this).val(parseInt($(this).val()));
    });
}
jQuery(document).ready(function ($) {
    validateIsNumber('input[name="failed_logins"]');
    validateIsNumber('input[name="failed_logins_time_minute"]');
    validateIsNumber('input[name="time_minute_block"]');
});

/***/ }),
/* 23 */
/***/ (function(module, exports) {

jQuery(document).ready(function () {
    var onChangeColumnClassesCallback = function onChangeColumnClassesCallback(columnName) {
        var name = '';
        if (columnName == 'status') {
            name = 'dt-body-center text-center';
        }
        if (columnName == 'category_names_with_link') {
            name = 'category-cell';
        }
        if (columnName == 'tag_names_with_link') {
            name = 'tag-cell';
        }

        return name;
    };

    AjaxDatatable.init(onChangeColumnClassesCallback);

    AjaxDatatable.onAjaxLoaded(function (oSettings) {
        aclFn.init();

        // set filter select drop down searchable
        $("select[name='category_names_with_link']").select2();
        $("select[name='tag_names_with_link']").select2();
    });

    /**
     * Bulk actions support
     */
    AjaxDatatable.onGroupActionSubmit(function (action, grid) {
        var behaviour = action.val(),
            selectedRows = grid.getSelectedRows();

        switch (behaviour) {
            case 'bulk_set_categories':
                FomoNewsServices.setCategories(selectedRows);
                return;

            case 'bulk_set_tags':
                FomoNewsServices.setTags(selectedRows);
                return;

            case 'bulk_trash':
            case 'bulk_restore':
            case 'bulk_delete':
                var ajaxUrl = action.find(':selected').data('ajax-url'),
                    ajaxMethod = action.find(':selected').data('method');

                FomoNewsServices.bulkActions(selectedRows, ajaxUrl, ajaxMethod, behaviour);
                return;
        }

        grid.setAjaxParam("customActionType", "group_action");
        grid.setAjaxParam("customActionName", action.val());
        grid.setAjaxParam("id", grid.getSelectedRows());
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();
    });
});

/***/ }),
/* 24 */
/***/ (function(module, exports) {

var FomoToastr = function () {

    /**
     * Handle display message by toastr
     * @author vulh
     * */
    function handleDisplayAlertMessageByToastr() {
        if ($('#fomo-alert-success').length) {
            toastr.success($('#fomo-alert-success').html());
        }

        if ($('#fomo-alert-error').length) {
            toastr.error($('#fomo-alert-error').html());
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            handleDisplayAlertMessageByToastr();
        }
    };
}();

jQuery(document).ready(function () {
    FomoToastr.init();
});

/***/ }),
/* 25 */
/***/ (function(module, exports) {

FomoUserForm = function () {

    /**
     * Handle click on the Save icon on the page bar
     * @author vandd
     * */
    function handleSaveFormBtn() {
        $('.page-bar').on('click', FOMO.saveBtnForm, function (e) {
            if ($(this).attr('data-ajax-url') != '') {
                $.ajax({
                    url: $(this).attr('data-ajax-url'),
                    type: 'POST',
                    data: {
                        'email': $('#email').val(),
                        'user_id': $(this).attr('user-id')
                    },
                    beforeSend: function beforeSend() {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function success(response) {
                        $.unblockUI();
                        response = JSON.parse(response);
                        var emailElement = $('#email').closest('.form-group');
                        if (response.data.email) {
                            var errorText = emailElement.find('#email-error');
                            if (!emailElement.hasClass('has-error')) {
                                emailElement.addClass('has-error');
                            }

                            if (errorText.length > 0) {
                                $(errorText).text(response.data.message);
                            }
                        } else {
                            $(FOMO.saveBtnForm).closest('.page-content').find('form:not(.rv-form)').submit();
                        }
                    },
                    error: function error(jqXHR, exception) {
                        $.unblockUI();
                    }
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function init() {
            handleSaveFormBtn();
        }
    };
}();

jQuery(document).ready(function () {
    FomoUserForm.init();
});

/***/ }),
/* 26 */
/***/ (function(module, exports) {

FomoUserUpdateProfile = function () {

    /**
     *
     * @author hieuluong
     * */
    function handle() {
        $(document).on('click', '#admin-bar-edit-profile', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).data('url'),
                type: 'GET',
                beforeSend: function beforeSend() {
                    $.blockUI(FOMO.blockMetronicUI);
                    $("#fomo-modal-update-profile").modal('show').find(".modal-content .spinner-loading").show();
                },

                success: function success(response) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    $("#fomo-modal-update-profile .modal-body").html(response);
                    $("#fomo-modal-update-profile .rvMedia_preview_file_avatar, #fomo-modal-update-profile .btn-upload-image").rvMedia({
                        multiple: false,
                        onSelectFiles: function onSelectFiles(files, $el) {
                            var firstItem = _.first(files);
                            if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && firstItem.type.match('image') && typeof $el !== 'undefined') {
                                console.log($el.parent().find('.preview_image'));
                                $el.parent().find('.image-data').val(firstItem.id);
                                $el.parent().find('.preview_image').attr('src', firstItem.thumb).data('rv-media', [{ 'selected_file_id': firstItem.id }]).show();
                                $el.parent().find(".btn-remove-image").show();
                            }
                        }
                    });
                    $("#fomo-modal-update-profile .select2").select2();
                },
                error: function error(jqXHR, exception) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                }
            });
        });

        $(document).on('submit', '#fomo-modal-update-profile form', function (e) {
            e.preventDefault();
            var formData = $(this).serializeArray();
            $.each(formData, function (i, input) {
                if (input.name != 'password' && input.name != 'language' && input.value == '') delete input.name;
            });
            formData.push({ name: 'image_id', value: $("#fomo-modal-update-profile form input[name=image_id]").val() });

            $.ajax({
                url: $(this).data('url'),
                type: 'PUT',
                data: formData,
                beforeSend: function beforeSend() {
                    $.blockUI(FOMO.blockMetronicUI);
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").show();
                },
                success: function success(response) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    $("#fomo-modal-update-profile").modal('hide');
                    if (response.errors == false) {
                        toastr.success(response.data.message);
                    }
                },
                error: function error(jqXHR, exception) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    if (jqXHR.status === 422) {
                        errors = "";
                        data = jqXHR.responseJSON;
                        for (var k in data) {
                            if (data.hasOwnProperty(k)) {
                                data[k].forEach(function (val) {
                                    errors += val;
                                });
                            }
                        }
                        toastr.error(errors);
                    }
                }
            });
        });

        $(document).on('click', '#fomo-modal-update-profile .btn-remove-image', function (e) {
            $(this).closest('.image-box').find('.image-data').val('');

            var $previewImg = $(this).closest('.image-box').find('img.preview_image'),
                defaultSrc = $previewImg.attr('data-default-src');
            if (typeof defaultSrc !== 'undefined' && defaultSrc !== '') {
                $previewImg.attr('src', defaultSrc).show();
            } else {
                $(this).closest('.image-box').find('img').hide();
            }

            $(this).closest('.image-box').find(".btn-upload-image").show();
            $(this).closest('.image-box').find(".btn-remove-image").hide();
        });
    }

    return {
        //main function to initiate the module
        init: function init() {
            handle();
        }
    };
}();

jQuery(document).ready(function () {
    FomoUserUpdateProfile.init();
});

/***/ }),
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 32 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(23);
__webpack_require__(16);
__webpack_require__(13);
__webpack_require__(19);
__webpack_require__(15);
__webpack_require__(21);
__webpack_require__(25);
__webpack_require__(24);
__webpack_require__(26);
__webpack_require__(14);
__webpack_require__(7);
__webpack_require__(9);
__webpack_require__(10);
__webpack_require__(8);
__webpack_require__(22);
__webpack_require__(20);
__webpack_require__(12);
__webpack_require__(11);
__webpack_require__(17);
__webpack_require__(18);
__webpack_require__(31);
module.exports = __webpack_require__(32);


/***/ })
/******/ ]);
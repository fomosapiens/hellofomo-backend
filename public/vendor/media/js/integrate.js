/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 19);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Helpers; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__ = __webpack_require__(1);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Helpers = function () {
    function Helpers() {
        _classCallCheck(this, Helpers);
    }

    _createClass(Helpers, null, [{
        key: 'getUrlParam',
        value: function getUrlParam(paramName) {
            var url = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            if (!url) {
                url = window.location.search;
            }
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = url.match(reParam);
            return match && match.length > 1 ? match[1] : null;
        }
    }, {
        key: 'asset',
        value: function asset(url) {
            if (url.substring(0, 2) === '//' || url.substring(0, 7) === 'http://' || url.substring(0, 8) === 'https://') {
                return url;
            }

            var baseUrl = RV_MEDIA_URL.base_url.substr(-1, 1) !== '/' ? RV_MEDIA_URL.base_url + '/' : RV_MEDIA_URL.base_url;

            if (url.substring(0, 1) === '/') {
                return baseUrl + url.substring(1);
            }
            return baseUrl + url;
        }
    }, {
        key: 'showAjaxLoading',
        value: function showAjaxLoading() {
            var $element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('.rv-media-main');

            $element.addClass('on-loading').append($('#rv_media_loading').html());
        }
    }, {
        key: 'hideAjaxLoading',
        value: function hideAjaxLoading() {
            var $element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('.rv-media-main');

            $element.removeClass('on-loading').find('.loading-wrapper').remove();
        }
    }, {
        key: 'isOnAjaxLoading',
        value: function isOnAjaxLoading() {
            var $element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('.rv-media-items');

            return $element.hasClass('on-loading');
        }
    }, {
        key: 'jsonEncode',
        value: function jsonEncode(object) {
            "use strict";

            if (typeof object === 'undefined') {
                object = null;
            }
            return JSON.stringify(object);
        }
    }, {
        key: 'jsonDecode',
        value: function jsonDecode(jsonString, defaultValue) {
            "use strict";

            if (!jsonString) {
                return defaultValue;
            }
            if (typeof jsonString === 'string') {
                var result = void 0;
                try {
                    result = $.parseJSON(jsonString);
                } catch (err) {
                    result = defaultValue;
                }
                return result;
            }
            return jsonString;
        }
    }, {
        key: 'getRequestParams',
        value: function getRequestParams() {
            if (window.rvMedia.options && window.rvMedia.options.open_in === 'modal') {
                return $.extend(true, __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */].request_params, window.rvMedia.options || {});
            }
            return __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */].request_params;
        }
    }, {
        key: 'setSelectedFile',
        value: function setSelectedFile($file_id) {
            if (typeof window.rvMedia.options !== 'undefined') {
                window.rvMedia.options.selected_file_id = $file_id;
            } else {
                __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */].request_params.selected_file_id = $file_id;
            }
        }
    }, {
        key: 'getConfigs',
        value: function getConfigs() {
            return __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */];
        }
    }, {
        key: 'storeConfig',
        value: function storeConfig() {
            localStorage.setItem('MediaConfig', Helpers.jsonEncode(__WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */]));
        }
    }, {
        key: 'storeRecentItems',
        value: function storeRecentItems() {
            localStorage.setItem('RecentItems', Helpers.jsonEncode(__WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */]));
        }

        /**
         * We currently allow searching all place so need to Keep prev folder when remove keyword. In this case
         * the selected folder will be re-assigned
         * */

    }, {
        key: 'setPrevFolderID',
        value: function setPrevFolderID(folderID) {
            localStorage.setItem('prevFolder', folderID);
        }
    }, {
        key: 'getPrevFolderID',
        value: function getPrevFolderID() {
            return localStorage.getItem('prevFolder');
        }
    }, {
        key: 'addToRecent',
        value: function addToRecent(id) {
            if (id instanceof Array) {
                _.each(id, function (value) {
                    __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */].push(value);
                });
            } else {
                __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */].push(id);
            }
        }
    }, {
        key: 'getItems',
        value: function getItems() {
            var items = [];
            $('.js-media-list-title').each(function () {
                var $box = $(this);
                var data = $box.data() || {};
                data.index_key = $box.index();
                items.push(data);
            });
            return items;
        }
    }, {
        key: 'getSelectedItems',
        value: function getSelectedItems() {
            var selected = [];
            $('.js-media-list-title input[type=checkbox]:checked').each(function () {
                var $box = $(this).closest('.js-media-list-title');
                var data = $box.data() || {};
                data.index_key = $box.index();
                selected.push(data);
            });
            return selected;
        }
    }, {
        key: 'getSelectedFiles',
        value: function getSelectedFiles() {
            var selected = [];
            $('.js-media-list-title[data-context=file] input[type=checkbox]:checked').each(function () {
                var $box = $(this).closest('.js-media-list-title');
                var data = $box.data() || {};
                data.index_key = $box.index();
                selected.push(data);
            });
            return selected;
        }
    }, {
        key: 'getSelectedFolder',
        value: function getSelectedFolder() {
            var selected = [];
            $('.js-media-list-title[data-context=folder] input[type=checkbox]:checked').each(function () {
                var $box = $(this).closest('.js-media-list-title');
                var data = $box.data() || {};
                data.index_key = $box.index();
                selected.push(data);
            });
            return selected;
        }
    }, {
        key: 'isUseInModal',
        value: function isUseInModal() {
            return Helpers.getUrlParam('media-action') === 'select-files' || window.rvMedia && window.rvMedia.options && window.rvMedia.options.open_in === 'modal';
        }
    }, {
        key: 'resetPagination',
        value: function resetPagination() {
            RV_MEDIA_CONFIG.pagination = { paged: 1, posts_per_page: 40, in_process_get_media: false, has_more: true, folder_id: 0 };
        }
    }]);

    return Helpers;
}();

/***/ }),

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return RecentItems; });
var MediaConfig = $.parseJSON(localStorage.getItem('MediaConfig')) || {};

var defaultConfig = {
    app_key: '483a0xyzytz1242c0d520426e8ba366c530c3d9dxxx',
    request_params: {
        view_type: 'tiles',
        filter: 'everything',
        view_in: 'my_media',
        search: '',
        sort_by: 'created_at-desc',
        folder_id: 0
    },
    hide_details_pane: false,
    icons: {
        folder: 'fa fa-folder-o'
    },
    actions_list: {
        basic: [{
            icon: 'fa fa-eye',
            name: 'Preview',
            action: 'preview',
            order: 0,
            class: 'rv-action-preview'
        }],
        file: [{
            icon: 'fa fa-link',
            name: 'Copy link',
            action: 'copy_link',
            order: 0,
            class: 'rv-action-copy-link'
        }, {
            icon: 'fa fa-pencil',
            name: 'Rename',
            action: 'rename',
            order: 1,
            class: 'rv-action-rename'
        }, {
            icon: 'fa fa-copy',
            name: 'Make a copy',
            action: 'make_copy',
            order: 2,
            class: 'rv-action-make-copy'
        }, {
            icon: 'fa fa-dot-circle-o',
            name: 'Set focus point',
            action: 'set_focus_point',
            order: 3,
            class: 'rv-action-set-focus-point'
        }],
        user: [{
            icon: 'fa fa-share-alt',
            name: 'Share',
            action: 'share',
            order: 0,
            class: 'rv-action-share'
        }, {
            icon: 'fa fa-ban',
            name: 'Remove share',
            action: 'remove_share',
            order: 1,
            class: 'rv-action-remove-share'
        }, {
            icon: 'fa fa-star',
            name: 'Favorite',
            action: 'favorite',
            order: 2,
            class: 'rv-action-favorite'
        }, {
            icon: 'fa fa-star-o',
            name: 'Remove favorite',
            action: 'remove_favorite',
            order: 3,
            class: 'rv-action-favorite'
        }],
        other: [{
            icon: 'fa fa-download',
            name: 'Download',
            action: 'download',
            order: 0,
            class: 'rv-action-download'
        }, {
            icon: 'fa fa-trash',
            name: 'Move to trash',
            action: 'trash',
            order: 1,
            class: 'rv-action-trash'
        }, {
            icon: 'fa fa-eraser',
            name: 'Delete permanently',
            action: 'delete',
            order: 2,
            class: 'rv-action-delete'
        }, {
            icon: 'fa fa-undo',
            name: 'Restore',
            action: 'restore',
            order: 3,
            class: 'rv-action-restore'
        }]
    },
    denied_download: ['youtube']
};

if (!MediaConfig.app_key || MediaConfig.app_key !== defaultConfig.app_key) {
    MediaConfig = defaultConfig;
}

var RecentItems = $.parseJSON(localStorage.getItem('RecentItems')) || [];



/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditorService", function() { return EditorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__ = __webpack_require__(1);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var EditorService = function () {
    function EditorService() {
        _classCallCheck(this, EditorService);
    }

    _createClass(EditorService, null, [{
        key: 'editorSelectFile',
        value: function editorSelectFile(selectedFiles) {

            var is_ckeditor = __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getUrlParam('CKEditor') || __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getUrlParam('CKEditorFuncNum');

            if (window.opener && is_ckeditor) {
                var firstItem = _.first(selectedFiles);

                window.opener.CKEDITOR.tools.callFunction(__WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getUrlParam('CKEditorFuncNum'), firstItem.url);

                if (window.opener) {
                    window.close();
                }
            } else {
                // No WYSIWYG editor found, use custom method.
            }
        }
    }]);

    return EditorService;
}();

var rvMedia = function rvMedia(selector, options) {
    _classCallCheck(this, rvMedia);

    window.rvMedia = window.rvMedia || {};

    var $body = $('body');

    var defaultOptions = {
        multiple: true,
        type: '*',
        onSelectFiles: function onSelectFiles(files, $el) {}
    };

    options = $.extend(true, defaultOptions, options);

    var clickCallback = function clickCallback(event) {
        event.preventDefault();
        var $current = $(this);
        $('#rv_media_modal').modal();

        window.rvMedia.options = options;
        window.rvMedia.options.open_in = 'modal';

        window.rvMedia.$el = $current;

        __WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.filter = 'everything';
        __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();

        var ele_options = window.rvMedia.$el.data('rv-media');
        if (typeof ele_options !== 'undefined' && ele_options.length > 0) {
            ele_options = ele_options[0];
            window.rvMedia.options = $.extend(true, window.rvMedia.options, ele_options || {});
            if (typeof ele_options.selected_file_id !== 'undefined') {
                window.rvMedia.options.is_popup = true;
            } else if (typeof window.rvMedia.options.is_popup !== 'undefined') {
                window.rvMedia.options.is_popup = undefined;
            }
        }

        if ($('#rv_media_body .rv-media-container').length === 0) {
            $('#rv_media_body').load(RV_MEDIA_URL.popup, function (data) {
                if (data.error) {
                    alert(data.message);
                }
                $('#rv_media_body').removeClass('media-modal-loading').closest('.modal-content').removeClass('bb-loading');
            });
        } else {
            $(document).find('.rv-media-container .js-change-action[data-type=refresh]').trigger('click');
        }
    };

    if (typeof selector === 'string') {
        $body.on('click', selector, clickCallback);
    } else {
        selector.on('click', clickCallback);
    }
};

window.RvMediaStandAlone = rvMedia;

$('.js-insert-to-editor').off('click').on('click', function (event) {
    event.preventDefault();
    var selectedFiles = __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
    if (_.size(selectedFiles) > 0) {
        EditorService.editorSelectFile(selectedFiles);
    }
});

$.fn.rvMedia = function (options) {
    var $selector = $(this);

    __WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.filter = 'everything';
    if (__WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.view_in === 'trash') {
        $(document).find('.js-insert-to-editor').prop('disabled', true);
    } else {
        $(document).find('.js-insert-to-editor').prop('disabled', false);
    }
    __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();

    new rvMedia($selector, options);
};

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMGQxMzcyYmJiMDY0NzlkYmVjN2UiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvSGVscGVycy9IZWxwZXJzLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL0NvbmZpZy9NZWRpYUNvbmZpZy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL2ludGVncmF0ZS5qcyJdLCJuYW1lcyI6WyJIZWxwZXJzIiwicGFyYW1OYW1lIiwidXJsIiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJyZVBhcmFtIiwiUmVnRXhwIiwibWF0Y2giLCJsZW5ndGgiLCJzdWJzdHJpbmciLCJiYXNlVXJsIiwiUlZfTUVESUFfVVJMIiwiYmFzZV91cmwiLCJzdWJzdHIiLCIkZWxlbWVudCIsIiQiLCJhZGRDbGFzcyIsImFwcGVuZCIsImh0bWwiLCJyZW1vdmVDbGFzcyIsImZpbmQiLCJyZW1vdmUiLCJoYXNDbGFzcyIsIm9iamVjdCIsIkpTT04iLCJzdHJpbmdpZnkiLCJqc29uU3RyaW5nIiwiZGVmYXVsdFZhbHVlIiwicmVzdWx0IiwicGFyc2VKU09OIiwiZXJyIiwicnZNZWRpYSIsIm9wdGlvbnMiLCJvcGVuX2luIiwiZXh0ZW5kIiwiTWVkaWFDb25maWciLCJyZXF1ZXN0X3BhcmFtcyIsIiRmaWxlX2lkIiwic2VsZWN0ZWRfZmlsZV9pZCIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJqc29uRW5jb2RlIiwiZm9sZGVySUQiLCJnZXRJdGVtIiwiaWQiLCJBcnJheSIsIl8iLCJlYWNoIiwidmFsdWUiLCJSZWNlbnRJdGVtcyIsInB1c2giLCJpdGVtcyIsIiRib3giLCJkYXRhIiwiaW5kZXhfa2V5IiwiaW5kZXgiLCJzZWxlY3RlZCIsImNsb3Nlc3QiLCJnZXRVcmxQYXJhbSIsIlJWX01FRElBX0NPTkZJRyIsInBhZ2luYXRpb24iLCJwYWdlZCIsInBvc3RzX3Blcl9wYWdlIiwiaW5fcHJvY2Vzc19nZXRfbWVkaWEiLCJoYXNfbW9yZSIsImZvbGRlcl9pZCIsImRlZmF1bHRDb25maWciLCJhcHBfa2V5Iiwidmlld190eXBlIiwiZmlsdGVyIiwidmlld19pbiIsInNvcnRfYnkiLCJoaWRlX2RldGFpbHNfcGFuZSIsImljb25zIiwiZm9sZGVyIiwiYWN0aW9uc19saXN0IiwiYmFzaWMiLCJpY29uIiwibmFtZSIsImFjdGlvbiIsIm9yZGVyIiwiY2xhc3MiLCJmaWxlIiwidXNlciIsIm90aGVyIiwiZGVuaWVkX2Rvd25sb2FkIiwiRWRpdG9yU2VydmljZSIsInNlbGVjdGVkRmlsZXMiLCJpc19ja2VkaXRvciIsIm9wZW5lciIsImZpcnN0SXRlbSIsImZpcnN0IiwiQ0tFRElUT1IiLCJ0b29scyIsImNhbGxGdW5jdGlvbiIsImNsb3NlIiwic2VsZWN0b3IiLCIkYm9keSIsImRlZmF1bHRPcHRpb25zIiwibXVsdGlwbGUiLCJ0eXBlIiwib25TZWxlY3RGaWxlcyIsImZpbGVzIiwiJGVsIiwiY2xpY2tDYWxsYmFjayIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCIkY3VycmVudCIsIm1vZGFsIiwic3RvcmVDb25maWciLCJlbGVfb3B0aW9ucyIsImlzX3BvcHVwIiwidW5kZWZpbmVkIiwibG9hZCIsInBvcHVwIiwiZXJyb3IiLCJhbGVydCIsIm1lc3NhZ2UiLCJkb2N1bWVudCIsInRyaWdnZXIiLCJvbiIsIlJ2TWVkaWFTdGFuZEFsb25lIiwib2ZmIiwiZ2V0U2VsZWN0ZWRGaWxlcyIsInNpemUiLCJlZGl0b3JTZWxlY3RGaWxlIiwiZm4iLCIkc2VsZWN0b3IiLCJwcm9wIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQzdEQTs7QUFFQSxJQUFhQSxPQUFiO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQSxvQ0FDdUJDLFNBRHZCLEVBQzhDO0FBQUEsZ0JBQVpDLEdBQVksdUVBQU4sSUFBTTs7QUFDdEMsZ0JBQUksQ0FBQ0EsR0FBTCxFQUFVO0FBQ05BLHNCQUFNQyxPQUFPQyxRQUFQLENBQWdCQyxNQUF0QjtBQUNIO0FBQ0QsZ0JBQUlDLFVBQVUsSUFBSUMsTUFBSixDQUFXLGdCQUFnQk4sU0FBaEIsR0FBNEIsVUFBdkMsRUFBbUQsR0FBbkQsQ0FBZDtBQUNBLGdCQUFJTyxRQUFRTixJQUFJTSxLQUFKLENBQVVGLE9BQVYsQ0FBWjtBQUNBLG1CQUFTRSxTQUFTQSxNQUFNQyxNQUFOLEdBQWUsQ0FBMUIsR0FBZ0NELE1BQU0sQ0FBTixDQUFoQyxHQUEyQyxJQUFsRDtBQUNIO0FBUkw7QUFBQTtBQUFBLDhCQVVpQk4sR0FWakIsRUFVc0I7QUFDZCxnQkFBSUEsSUFBSVEsU0FBSixDQUFjLENBQWQsRUFBaUIsQ0FBakIsTUFBd0IsSUFBeEIsSUFBZ0NSLElBQUlRLFNBQUosQ0FBYyxDQUFkLEVBQWlCLENBQWpCLE1BQXdCLFNBQXhELElBQXFFUixJQUFJUSxTQUFKLENBQWMsQ0FBZCxFQUFpQixDQUFqQixNQUF3QixVQUFqRyxFQUE2RztBQUN6Ryx1QkFBT1IsR0FBUDtBQUNIOztBQUVELGdCQUFJUyxVQUFVQyxhQUFhQyxRQUFiLENBQXNCQyxNQUF0QixDQUE2QixDQUFDLENBQTlCLEVBQWlDLENBQWpDLE1BQXdDLEdBQXhDLEdBQThDRixhQUFhQyxRQUFiLEdBQXdCLEdBQXRFLEdBQTRFRCxhQUFhQyxRQUF2Rzs7QUFFQSxnQkFBSVgsSUFBSVEsU0FBSixDQUFjLENBQWQsRUFBaUIsQ0FBakIsTUFBd0IsR0FBNUIsRUFBaUM7QUFDN0IsdUJBQU9DLFVBQVVULElBQUlRLFNBQUosQ0FBYyxDQUFkLENBQWpCO0FBQ0g7QUFDRCxtQkFBT0MsVUFBVVQsR0FBakI7QUFDSDtBQXJCTDtBQUFBO0FBQUEsMENBdUIyRDtBQUFBLGdCQUFoQ2EsUUFBZ0MsdUVBQXJCQyxFQUFFLGdCQUFGLENBQXFCOztBQUNuREQscUJBQ0tFLFFBREwsQ0FDYyxZQURkLEVBRUtDLE1BRkwsQ0FFWUYsRUFBRSxtQkFBRixFQUF1QkcsSUFBdkIsRUFGWjtBQUdIO0FBM0JMO0FBQUE7QUFBQSwwQ0E2QjJEO0FBQUEsZ0JBQWhDSixRQUFnQyx1RUFBckJDLEVBQUUsZ0JBQUYsQ0FBcUI7O0FBQ25ERCxxQkFDS0ssV0FETCxDQUNpQixZQURqQixFQUVLQyxJQUZMLENBRVUsa0JBRlYsRUFFOEJDLE1BRjlCO0FBR0g7QUFqQ0w7QUFBQTtBQUFBLDBDQW1DNEQ7QUFBQSxnQkFBakNQLFFBQWlDLHVFQUF0QkMsRUFBRSxpQkFBRixDQUFzQjs7QUFDcEQsbUJBQU9ELFNBQVNRLFFBQVQsQ0FBa0IsWUFBbEIsQ0FBUDtBQUNIO0FBckNMO0FBQUE7QUFBQSxtQ0F1Q3NCQyxNQXZDdEIsRUF1QzhCO0FBQ3RCOztBQUNBLGdCQUFJLE9BQU9BLE1BQVAsS0FBa0IsV0FBdEIsRUFBbUM7QUFDL0JBLHlCQUFTLElBQVQ7QUFDSDtBQUNELG1CQUFPQyxLQUFLQyxTQUFMLENBQWVGLE1BQWYsQ0FBUDtBQUNIO0FBN0NMO0FBQUE7QUFBQSxtQ0ErQ3NCRyxVQS9DdEIsRUErQ2tDQyxZQS9DbEMsRUErQ2dEO0FBQ3hDOztBQUNBLGdCQUFJLENBQUNELFVBQUwsRUFBaUI7QUFDYix1QkFBT0MsWUFBUDtBQUNIO0FBQ0QsZ0JBQUksT0FBT0QsVUFBUCxLQUFzQixRQUExQixFQUFvQztBQUNoQyxvQkFBSUUsZUFBSjtBQUNBLG9CQUFJO0FBQ0FBLDZCQUFTYixFQUFFYyxTQUFGLENBQVlILFVBQVosQ0FBVDtBQUNILGlCQUZELENBRUUsT0FBT0ksR0FBUCxFQUFZO0FBQ1ZGLDZCQUFTRCxZQUFUO0FBQ0g7QUFDRCx1QkFBT0MsTUFBUDtBQUNIO0FBQ0QsbUJBQU9GLFVBQVA7QUFDSDtBQTlETDtBQUFBO0FBQUEsMkNBZ0U4QjtBQUN0QixnQkFBSXhCLE9BQU82QixPQUFQLENBQWVDLE9BQWYsSUFBMEI5QixPQUFPNkIsT0FBUCxDQUFlQyxPQUFmLENBQXVCQyxPQUF2QixLQUFtQyxPQUFqRSxFQUEwRTtBQUN0RSx1QkFBT2xCLEVBQUVtQixNQUFGLENBQVMsSUFBVCxFQUFlLHdFQUFBQyxDQUFZQyxjQUEzQixFQUEyQ2xDLE9BQU82QixPQUFQLENBQWVDLE9BQWYsSUFBMEIsRUFBckUsQ0FBUDtBQUNIO0FBQ0QsbUJBQU8sd0VBQUFHLENBQVlDLGNBQW5CO0FBQ0g7QUFyRUw7QUFBQTtBQUFBLHdDQXVFMkJDLFFBdkUzQixFQXVFcUM7QUFDN0IsZ0JBQUksT0FBT25DLE9BQU82QixPQUFQLENBQWVDLE9BQXRCLEtBQWtDLFdBQXRDLEVBQW1EO0FBQy9DOUIsdUJBQU82QixPQUFQLENBQWVDLE9BQWYsQ0FBdUJNLGdCQUF2QixHQUEwQ0QsUUFBMUM7QUFDSCxhQUZELE1BRU87QUFDSEYsZ0JBQUEsd0VBQUFBLENBQVlDLGNBQVosQ0FBMkJFLGdCQUEzQixHQUE4Q0QsUUFBOUM7QUFDSDtBQUNKO0FBN0VMO0FBQUE7QUFBQSxxQ0ErRXdCO0FBQ2hCLG1CQUFPLHdFQUFQO0FBQ0g7QUFqRkw7QUFBQTtBQUFBLHNDQW1GeUI7QUFDakJFLHlCQUFhQyxPQUFiLENBQXFCLGFBQXJCLEVBQW9DekMsUUFBUTBDLFVBQVIsQ0FBbUIsd0VBQW5CLENBQXBDO0FBQ0g7QUFyRkw7QUFBQTtBQUFBLDJDQXVGOEI7QUFDdEJGLHlCQUFhQyxPQUFiLENBQXFCLGFBQXJCLEVBQW9DekMsUUFBUTBDLFVBQVIsQ0FBbUIsd0VBQW5CLENBQXBDO0FBQ0g7O0FBRUQ7Ozs7O0FBM0ZKO0FBQUE7QUFBQSx3Q0ErRjJCQyxRQS9GM0IsRUErRnFDO0FBQzdCSCx5QkFBYUMsT0FBYixDQUFxQixZQUFyQixFQUFtQ0UsUUFBbkM7QUFDSDtBQWpHTDtBQUFBO0FBQUEsMENBbUc2QjtBQUNyQixtQkFBT0gsYUFBYUksT0FBYixDQUFxQixZQUFyQixDQUFQO0FBQ0g7QUFyR0w7QUFBQTtBQUFBLG9DQXVHdUJDLEVBdkd2QixFQXVHMkI7QUFDbkIsZ0JBQUlBLGNBQWNDLEtBQWxCLEVBQXlCO0FBQ3JCQyxrQkFBRUMsSUFBRixDQUFPSCxFQUFQLEVBQVcsVUFBVUksS0FBVixFQUFpQjtBQUN4QkMsb0JBQUEsd0VBQUFBLENBQVlDLElBQVosQ0FBaUJGLEtBQWpCO0FBQ0gsaUJBRkQ7QUFHSCxhQUpELE1BSU87QUFDSEMsZ0JBQUEsd0VBQUFBLENBQVlDLElBQVosQ0FBaUJOLEVBQWpCO0FBQ0g7QUFDSjtBQS9HTDtBQUFBO0FBQUEsbUNBaUhzQjtBQUNkLGdCQUFJTyxRQUFRLEVBQVo7QUFDQXBDLGNBQUUsc0JBQUYsRUFBMEJnQyxJQUExQixDQUErQixZQUFZO0FBQ3ZDLG9CQUFJSyxPQUFPckMsRUFBRSxJQUFGLENBQVg7QUFDQSxvQkFBSXNDLE9BQU9ELEtBQUtDLElBQUwsTUFBZSxFQUExQjtBQUNBQSxxQkFBS0MsU0FBTCxHQUFpQkYsS0FBS0csS0FBTCxFQUFqQjtBQUNBSixzQkFBTUQsSUFBTixDQUFXRyxJQUFYO0FBQ0gsYUFMRDtBQU1BLG1CQUFPRixLQUFQO0FBQ0g7QUExSEw7QUFBQTtBQUFBLDJDQTRIOEI7QUFDdEIsZ0JBQUlLLFdBQVcsRUFBZjtBQUNBekMsY0FBRSxtREFBRixFQUF1RGdDLElBQXZELENBQTRELFlBQVk7QUFDcEUsb0JBQUlLLE9BQU9yQyxFQUFFLElBQUYsRUFBUTBDLE9BQVIsQ0FBZ0Isc0JBQWhCLENBQVg7QUFDQSxvQkFBSUosT0FBT0QsS0FBS0MsSUFBTCxNQUFlLEVBQTFCO0FBQ0FBLHFCQUFLQyxTQUFMLEdBQWlCRixLQUFLRyxLQUFMLEVBQWpCO0FBQ0FDLHlCQUFTTixJQUFULENBQWNHLElBQWQ7QUFDSCxhQUxEO0FBTUEsbUJBQU9HLFFBQVA7QUFDSDtBQXJJTDtBQUFBO0FBQUEsMkNBdUk4QjtBQUN0QixnQkFBSUEsV0FBVyxFQUFmO0FBQ0F6QyxjQUFFLHNFQUFGLEVBQTBFZ0MsSUFBMUUsQ0FBK0UsWUFBWTtBQUN2RixvQkFBSUssT0FBT3JDLEVBQUUsSUFBRixFQUFRMEMsT0FBUixDQUFnQixzQkFBaEIsQ0FBWDtBQUNBLG9CQUFJSixPQUFPRCxLQUFLQyxJQUFMLE1BQWUsRUFBMUI7QUFDQUEscUJBQUtDLFNBQUwsR0FBaUJGLEtBQUtHLEtBQUwsRUFBakI7QUFDQUMseUJBQVNOLElBQVQsQ0FBY0csSUFBZDtBQUNILGFBTEQ7QUFNQSxtQkFBT0csUUFBUDtBQUNIO0FBaEpMO0FBQUE7QUFBQSw0Q0FrSitCO0FBQ3ZCLGdCQUFJQSxXQUFXLEVBQWY7QUFDQXpDLGNBQUUsd0VBQUYsRUFBNEVnQyxJQUE1RSxDQUFpRixZQUFZO0FBQ3pGLG9CQUFJSyxPQUFPckMsRUFBRSxJQUFGLEVBQVEwQyxPQUFSLENBQWdCLHNCQUFoQixDQUFYO0FBQ0Esb0JBQUlKLE9BQU9ELEtBQUtDLElBQUwsTUFBZSxFQUExQjtBQUNBQSxxQkFBS0MsU0FBTCxHQUFpQkYsS0FBS0csS0FBTCxFQUFqQjtBQUNBQyx5QkFBU04sSUFBVCxDQUFjRyxJQUFkO0FBQ0gsYUFMRDtBQU1BLG1CQUFPRyxRQUFQO0FBQ0g7QUEzSkw7QUFBQTtBQUFBLHVDQTZKMEI7QUFDbEIsbUJBQU96RCxRQUFRMkQsV0FBUixDQUFvQixjQUFwQixNQUF3QyxjQUF4QyxJQUEyRHhELE9BQU82QixPQUFQLElBQWtCN0IsT0FBTzZCLE9BQVAsQ0FBZUMsT0FBakMsSUFBNEM5QixPQUFPNkIsT0FBUCxDQUFlQyxPQUFmLENBQXVCQyxPQUF2QixLQUFtQyxPQUFqSjtBQUNIO0FBL0pMO0FBQUE7QUFBQSwwQ0FpSzZCO0FBQ3JCMEIsNEJBQWdCQyxVQUFoQixHQUE2QixFQUFFQyxPQUFPLENBQVQsRUFBWUMsZ0JBQWdCLEVBQTVCLEVBQWdDQyxzQkFBc0IsS0FBdEQsRUFBNkRDLFVBQVUsSUFBdkUsRUFBNkVDLFdBQVcsQ0FBeEYsRUFBN0I7QUFDSDtBQW5LTDs7QUFBQTtBQUFBLEk7Ozs7Ozs7OztBQ0ZBO0FBQUEsSUFBSTlCLGNBQWNwQixFQUFFYyxTQUFGLENBQVlVLGFBQWFJLE9BQWIsQ0FBcUIsYUFBckIsQ0FBWixLQUFvRCxFQUF0RTs7QUFFQSxJQUFJdUIsZ0JBQWdCO0FBQ2hCQyxhQUFTLDZDQURPO0FBRWhCL0Isb0JBQWdCO0FBQ1pnQyxtQkFBVyxPQURDO0FBRVpDLGdCQUFRLFlBRkk7QUFHWkMsaUJBQVMsVUFIRztBQUlabEUsZ0JBQVEsRUFKSTtBQUtabUUsaUJBQVMsaUJBTEc7QUFNWk4sbUJBQVc7QUFOQyxLQUZBO0FBVWhCTyx1QkFBbUIsS0FWSDtBQVdoQkMsV0FBTztBQUNIQyxnQkFBUTtBQURMLEtBWFM7QUFjaEJDLGtCQUFjO0FBQ1ZDLGVBQU8sQ0FDSDtBQUNJQyxrQkFBTSxXQURWO0FBRUlDLGtCQUFNLFNBRlY7QUFHSUMsb0JBQVEsU0FIWjtBQUlJQyxtQkFBTyxDQUpYO0FBS0lDLG1CQUFPO0FBTFgsU0FERyxDQURHO0FBVVZDLGNBQU0sQ0FDRjtBQUNJTCxrQkFBTSxZQURWO0FBRUlDLGtCQUFNLFdBRlY7QUFHSUMsb0JBQVEsV0FIWjtBQUlJQyxtQkFBTyxDQUpYO0FBS0lDLG1CQUFPO0FBTFgsU0FERSxFQVFGO0FBQ0lKLGtCQUFNLGNBRFY7QUFFSUMsa0JBQU0sUUFGVjtBQUdJQyxvQkFBUSxRQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQVJFLEVBZUY7QUFDSUosa0JBQU0sWUFEVjtBQUVJQyxrQkFBTSxhQUZWO0FBR0lDLG9CQUFRLFdBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBZkUsRUFzQkY7QUFDSUosa0JBQU0sb0JBRFY7QUFFSUMsa0JBQU0saUJBRlY7QUFHSUMsb0JBQVEsaUJBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBdEJFLENBVkk7QUF3Q1ZFLGNBQU0sQ0FDRjtBQUNJTixrQkFBTSxpQkFEVjtBQUVJQyxrQkFBTSxPQUZWO0FBR0lDLG9CQUFRLE9BSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBREUsRUFRRjtBQUNJSixrQkFBTSxXQURWO0FBRUlDLGtCQUFNLGNBRlY7QUFHSUMsb0JBQVEsY0FIWjtBQUlJQyxtQkFBTyxDQUpYO0FBS0lDLG1CQUFPO0FBTFgsU0FSRSxFQWVGO0FBQ0lKLGtCQUFNLFlBRFY7QUFFSUMsa0JBQU0sVUFGVjtBQUdJQyxvQkFBUSxVQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQWZFLEVBc0JGO0FBQ0lKLGtCQUFNLGNBRFY7QUFFSUMsa0JBQU0saUJBRlY7QUFHSUMsb0JBQVEsaUJBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBdEJFLENBeENJO0FBc0VWRyxlQUFPLENBQ0g7QUFDSVAsa0JBQU0sZ0JBRFY7QUFFSUMsa0JBQU0sVUFGVjtBQUdJQyxvQkFBUSxVQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQURHLEVBUUg7QUFDSUosa0JBQU0sYUFEVjtBQUVJQyxrQkFBTSxlQUZWO0FBR0lDLG9CQUFRLE9BSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBUkcsRUFlSDtBQUNJSixrQkFBTSxjQURWO0FBRUlDLGtCQUFNLG9CQUZWO0FBR0lDLG9CQUFRLFFBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBZkcsRUFzQkg7QUFDSUosa0JBQU0sWUFEVjtBQUVJQyxrQkFBTSxTQUZWO0FBR0lDLG9CQUFRLFNBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBdEJHO0FBdEVHLEtBZEU7QUFtSGhCSSxxQkFBaUIsQ0FDYixTQURhO0FBbkhELENBQXBCOztBQXdIQSxJQUFJLENBQUNsRCxZQUFZZ0MsT0FBYixJQUF3QmhDLFlBQVlnQyxPQUFaLEtBQXdCRCxjQUFjQyxPQUFsRSxFQUEyRTtBQUN2RWhDLGtCQUFjK0IsYUFBZDtBQUNIOztBQUVELElBQUlqQixjQUFjbEMsRUFBRWMsU0FBRixDQUFZVSxhQUFhSSxPQUFiLENBQXFCLGFBQXJCLENBQVosS0FBb0QsRUFBdEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUhBO0FBQ0E7O0FBRUEsSUFBYTJDLGFBQWI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBLHlDQUM0QkMsYUFENUIsRUFDMkM7O0FBRW5DLGdCQUFJQyxjQUFjLHFFQUFBekYsQ0FBUTJELFdBQVIsQ0FBb0IsVUFBcEIsS0FBbUMscUVBQUEzRCxDQUFRMkQsV0FBUixDQUFvQixpQkFBcEIsQ0FBckQ7O0FBRUEsZ0JBQUl4RCxPQUFPdUYsTUFBUCxJQUFpQkQsV0FBckIsRUFBa0M7QUFDOUIsb0JBQUlFLFlBQVk1QyxFQUFFNkMsS0FBRixDQUFRSixhQUFSLENBQWhCOztBQUVBckYsdUJBQU91RixNQUFQLENBQWNHLFFBQWQsQ0FBdUJDLEtBQXZCLENBQTZCQyxZQUE3QixDQUEwQyxxRUFBQS9GLENBQVEyRCxXQUFSLENBQW9CLGlCQUFwQixDQUExQyxFQUFrRmdDLFVBQVV6RixHQUE1Rjs7QUFFQSxvQkFBSUMsT0FBT3VGLE1BQVgsRUFBbUI7QUFDZnZGLDJCQUFPNkYsS0FBUDtBQUNIO0FBQ0osYUFSRCxNQVFPO0FBQ0g7QUFDSDtBQUNKO0FBaEJMOztBQUFBO0FBQUE7O0lBbUJNaEUsTyxHQUNGLGlCQUFZaUUsUUFBWixFQUFzQmhFLE9BQXRCLEVBQStCO0FBQUE7O0FBQzNCOUIsV0FBTzZCLE9BQVAsR0FBaUI3QixPQUFPNkIsT0FBUCxJQUFrQixFQUFuQzs7QUFFQSxRQUFJa0UsUUFBUWxGLEVBQUUsTUFBRixDQUFaOztBQUVBLFFBQUltRixpQkFBaUI7QUFDakJDLGtCQUFVLElBRE87QUFFakJDLGNBQU0sR0FGVztBQUdqQkMsdUJBQWUsdUJBQVVDLEtBQVYsRUFBaUJDLEdBQWpCLEVBQXNCLENBRXBDO0FBTGdCLEtBQXJCOztBQVFBdkUsY0FBVWpCLEVBQUVtQixNQUFGLENBQVMsSUFBVCxFQUFlZ0UsY0FBZixFQUErQmxFLE9BQS9CLENBQVY7O0FBRUEsUUFBSXdFLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBVUMsS0FBVixFQUFpQjtBQUNqQ0EsY0FBTUMsY0FBTjtBQUNBLFlBQUlDLFdBQVc1RixFQUFFLElBQUYsQ0FBZjtBQUNBQSxVQUFFLGlCQUFGLEVBQXFCNkYsS0FBckI7O0FBRUExRyxlQUFPNkIsT0FBUCxDQUFlQyxPQUFmLEdBQXlCQSxPQUF6QjtBQUNBOUIsZUFBTzZCLE9BQVAsQ0FBZUMsT0FBZixDQUF1QkMsT0FBdkIsR0FBaUMsT0FBakM7O0FBRUEvQixlQUFPNkIsT0FBUCxDQUFld0UsR0FBZixHQUFxQkksUUFBckI7O0FBRUF4RSxRQUFBLDRFQUFBQSxDQUFZQyxjQUFaLENBQTJCaUMsTUFBM0IsR0FBb0MsWUFBcEM7QUFDQXRFLFFBQUEscUVBQUFBLENBQVE4RyxXQUFSOztBQUVBLFlBQUlDLGNBQWM1RyxPQUFPNkIsT0FBUCxDQUFld0UsR0FBZixDQUFtQmxELElBQW5CLENBQXdCLFVBQXhCLENBQWxCO0FBQ0EsWUFBSSxPQUFPeUQsV0FBUCxLQUF1QixXQUF2QixJQUFzQ0EsWUFBWXRHLE1BQVosR0FBcUIsQ0FBL0QsRUFBa0U7QUFDOURzRywwQkFBY0EsWUFBWSxDQUFaLENBQWQ7QUFDQTVHLG1CQUFPNkIsT0FBUCxDQUFlQyxPQUFmLEdBQXlCakIsRUFBRW1CLE1BQUYsQ0FBUyxJQUFULEVBQWVoQyxPQUFPNkIsT0FBUCxDQUFlQyxPQUE5QixFQUF1QzhFLGVBQWUsRUFBdEQsQ0FBekI7QUFDQSxnQkFBSSxPQUFPQSxZQUFZeEUsZ0JBQW5CLEtBQXdDLFdBQTVDLEVBQXlEO0FBQ3JEcEMsdUJBQU82QixPQUFQLENBQWVDLE9BQWYsQ0FBdUIrRSxRQUF2QixHQUFrQyxJQUFsQztBQUNILGFBRkQsTUFFTyxJQUFJLE9BQU83RyxPQUFPNkIsT0FBUCxDQUFlQyxPQUFmLENBQXVCK0UsUUFBOUIsS0FBMkMsV0FBL0MsRUFBNEQ7QUFDL0Q3Ryx1QkFBTzZCLE9BQVAsQ0FBZUMsT0FBZixDQUF1QitFLFFBQXZCLEdBQWtDQyxTQUFsQztBQUNIO0FBQ0o7O0FBRUQsWUFBSWpHLEVBQUUsb0NBQUYsRUFBd0NQLE1BQXhDLEtBQW1ELENBQXZELEVBQTBEO0FBQ3RETyxjQUFFLGdCQUFGLEVBQW9Ca0csSUFBcEIsQ0FBeUJ0RyxhQUFhdUcsS0FBdEMsRUFBNkMsVUFBVTdELElBQVYsRUFBZ0I7QUFDekQsb0JBQUlBLEtBQUs4RCxLQUFULEVBQWdCO0FBQ1pDLDBCQUFNL0QsS0FBS2dFLE9BQVg7QUFDSDtBQUNEdEcsa0JBQUUsZ0JBQUYsRUFDS0ksV0FETCxDQUNpQixxQkFEakIsRUFFS3NDLE9BRkwsQ0FFYSxnQkFGYixFQUUrQnRDLFdBRi9CLENBRTJDLFlBRjNDO0FBR0gsYUFQRDtBQVFILFNBVEQsTUFTTztBQUNISixjQUFFdUcsUUFBRixFQUFZbEcsSUFBWixDQUFpQiwwREFBakIsRUFBNkVtRyxPQUE3RSxDQUFxRixPQUFyRjtBQUNIO0FBQ0osS0FwQ0Q7O0FBc0NBLFFBQUksT0FBT3ZCLFFBQVAsS0FBb0IsUUFBeEIsRUFBa0M7QUFDOUJDLGNBQU11QixFQUFOLENBQVMsT0FBVCxFQUFrQnhCLFFBQWxCLEVBQTRCUSxhQUE1QjtBQUNILEtBRkQsTUFFTztBQUNIUixpQkFBU3dCLEVBQVQsQ0FBWSxPQUFaLEVBQXFCaEIsYUFBckI7QUFDSDtBQUNKLEM7O0FBR0x0RyxPQUFPdUgsaUJBQVAsR0FBMkIxRixPQUEzQjs7QUFFQWhCLEVBQUUsc0JBQUYsRUFBMEIyRyxHQUExQixDQUE4QixPQUE5QixFQUF1Q0YsRUFBdkMsQ0FBMEMsT0FBMUMsRUFBbUQsVUFBVWYsS0FBVixFQUFpQjtBQUNoRUEsVUFBTUMsY0FBTjtBQUNBLFFBQUluQixnQkFBZ0IscUVBQUF4RixDQUFRNEgsZ0JBQVIsRUFBcEI7QUFDQSxRQUFJN0UsRUFBRThFLElBQUYsQ0FBT3JDLGFBQVAsSUFBd0IsQ0FBNUIsRUFBK0I7QUFDM0JELHNCQUFjdUMsZ0JBQWQsQ0FBK0J0QyxhQUEvQjtBQUNIO0FBQ0osQ0FORDs7QUFRQXhFLEVBQUUrRyxFQUFGLENBQUsvRixPQUFMLEdBQWUsVUFBVUMsT0FBVixFQUFtQjtBQUM5QixRQUFJK0YsWUFBWWhILEVBQUUsSUFBRixDQUFoQjs7QUFFQW9CLElBQUEsNEVBQUFBLENBQVlDLGNBQVosQ0FBMkJpQyxNQUEzQixHQUFvQyxZQUFwQztBQUNBLFFBQUksNEVBQUFsQyxDQUFZQyxjQUFaLENBQTJCa0MsT0FBM0IsS0FBdUMsT0FBM0MsRUFBb0Q7QUFDaER2RCxVQUFFdUcsUUFBRixFQUFZbEcsSUFBWixDQUFpQixzQkFBakIsRUFBeUM0RyxJQUF6QyxDQUE4QyxVQUE5QyxFQUEwRCxJQUExRDtBQUNILEtBRkQsTUFFTztBQUNIakgsVUFBRXVHLFFBQUYsRUFBWWxHLElBQVosQ0FBaUIsc0JBQWpCLEVBQXlDNEcsSUFBekMsQ0FBOEMsVUFBOUMsRUFBMEQsS0FBMUQ7QUFDSDtBQUNEakksSUFBQSxxRUFBQUEsQ0FBUThHLFdBQVI7O0FBRUEsUUFBSTlFLE9BQUosQ0FBWWdHLFNBQVosRUFBdUIvRixPQUF2QjtBQUNILENBWkQsQyIsImZpbGUiOiIvanMvaW50ZWdyYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMTkpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDBkMTM3MmJiYjA2NDc5ZGJlYzdlIiwiaW1wb3J0IHtNZWRpYUNvbmZpZywgUmVjZW50SXRlbXN9IGZyb20gJy4uL0NvbmZpZy9NZWRpYUNvbmZpZyc7XG5cbmV4cG9ydCBjbGFzcyBIZWxwZXJzIHtcbiAgICBzdGF0aWMgZ2V0VXJsUGFyYW0ocGFyYW1OYW1lLCB1cmwgPSBudWxsKSB7XG4gICAgICAgIGlmICghdXJsKSB7XG4gICAgICAgICAgICB1cmwgPSB3aW5kb3cubG9jYXRpb24uc2VhcmNoO1xuICAgICAgICB9XG4gICAgICAgIGxldCByZVBhcmFtID0gbmV3IFJlZ0V4cCgnKD86W1xcPyZdfCYpJyArIHBhcmFtTmFtZSArICc9KFteJl0rKScsICdpJyk7XG4gICAgICAgIGxldCBtYXRjaCA9IHVybC5tYXRjaChyZVBhcmFtKTtcbiAgICAgICAgcmV0dXJuICggbWF0Y2ggJiYgbWF0Y2gubGVuZ3RoID4gMSApID8gbWF0Y2hbMV0gOiBudWxsO1xuICAgIH1cblxuICAgIHN0YXRpYyBhc3NldCh1cmwpIHtcbiAgICAgICAgaWYgKHVybC5zdWJzdHJpbmcoMCwgMikgPT09ICcvLycgfHwgdXJsLnN1YnN0cmluZygwLCA3KSA9PT0gJ2h0dHA6Ly8nIHx8IHVybC5zdWJzdHJpbmcoMCwgOCkgPT09ICdodHRwczovLycpIHtcbiAgICAgICAgICAgIHJldHVybiB1cmw7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgYmFzZVVybCA9IFJWX01FRElBX1VSTC5iYXNlX3VybC5zdWJzdHIoLTEsIDEpICE9PSAnLycgPyBSVl9NRURJQV9VUkwuYmFzZV91cmwgKyAnLycgOiBSVl9NRURJQV9VUkwuYmFzZV91cmw7XG5cbiAgICAgICAgaWYgKHVybC5zdWJzdHJpbmcoMCwgMSkgPT09ICcvJykge1xuICAgICAgICAgICAgcmV0dXJuIGJhc2VVcmwgKyB1cmwuc3Vic3RyaW5nKDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBiYXNlVXJsICsgdXJsO1xuICAgIH1cblxuICAgIHN0YXRpYyBzaG93QWpheExvYWRpbmcoJGVsZW1lbnQgPSAkKCcucnYtbWVkaWEtbWFpbicpKSB7XG4gICAgICAgICRlbGVtZW50XG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ29uLWxvYWRpbmcnKVxuICAgICAgICAgICAgLmFwcGVuZCgkKCcjcnZfbWVkaWFfbG9hZGluZycpLmh0bWwoKSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGhpZGVBamF4TG9hZGluZygkZWxlbWVudCA9ICQoJy5ydi1tZWRpYS1tYWluJykpIHtcbiAgICAgICAgJGVsZW1lbnRcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnb24tbG9hZGluZycpXG4gICAgICAgICAgICAuZmluZCgnLmxvYWRpbmctd3JhcHBlcicpLnJlbW92ZSgpO1xuICAgIH1cblxuICAgIHN0YXRpYyBpc09uQWpheExvYWRpbmcoJGVsZW1lbnQgPSAkKCcucnYtbWVkaWEtaXRlbXMnKSkge1xuICAgICAgICByZXR1cm4gJGVsZW1lbnQuaGFzQ2xhc3MoJ29uLWxvYWRpbmcnKTtcbiAgICB9XG5cbiAgICBzdGF0aWMganNvbkVuY29kZShvYmplY3QpIHtcbiAgICAgICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgICAgIGlmICh0eXBlb2Ygb2JqZWN0ID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgb2JqZWN0ID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkob2JqZWN0KTtcbiAgICB9O1xuXG4gICAgc3RhdGljIGpzb25EZWNvZGUoanNvblN0cmluZywgZGVmYXVsdFZhbHVlKSB7XG4gICAgICAgIFwidXNlIHN0cmljdFwiO1xuICAgICAgICBpZiAoIWpzb25TdHJpbmcpIHtcbiAgICAgICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGVvZiBqc29uU3RyaW5nID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgbGV0IHJlc3VsdDtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gJC5wYXJzZUpTT04oanNvblN0cmluZyk7XG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSBkZWZhdWx0VmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBqc29uU3RyaW5nO1xuICAgIH07XG5cbiAgICBzdGF0aWMgZ2V0UmVxdWVzdFBhcmFtcygpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMgJiYgd2luZG93LnJ2TWVkaWEub3B0aW9ucy5vcGVuX2luID09PSAnbW9kYWwnKSB7XG4gICAgICAgICAgICByZXR1cm4gJC5leHRlbmQodHJ1ZSwgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMsIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMgfHwge30pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcztcbiAgICB9XG5cbiAgICBzdGF0aWMgc2V0U2VsZWN0ZWRGaWxlKCRmaWxlX2lkKSB7XG4gICAgICAgIGlmICh0eXBlb2Ygd2luZG93LnJ2TWVkaWEub3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuc2VsZWN0ZWRfZmlsZV9pZCA9ICRmaWxlX2lkO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuc2VsZWN0ZWRfZmlsZV9pZCA9ICRmaWxlX2lkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc3RhdGljIGdldENvbmZpZ3MoKSB7XG4gICAgICAgIHJldHVybiBNZWRpYUNvbmZpZztcbiAgICB9XG5cbiAgICBzdGF0aWMgc3RvcmVDb25maWcoKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdNZWRpYUNvbmZpZycsIEhlbHBlcnMuanNvbkVuY29kZShNZWRpYUNvbmZpZykpO1xuICAgIH1cblxuICAgIHN0YXRpYyBzdG9yZVJlY2VudEl0ZW1zKCkge1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnUmVjZW50SXRlbXMnLCBIZWxwZXJzLmpzb25FbmNvZGUoUmVjZW50SXRlbXMpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBXZSBjdXJyZW50bHkgYWxsb3cgc2VhcmNoaW5nIGFsbCBwbGFjZSBzbyBuZWVkIHRvIEtlZXAgcHJldiBmb2xkZXIgd2hlbiByZW1vdmUga2V5d29yZC4gSW4gdGhpcyBjYXNlXG4gICAgICogdGhlIHNlbGVjdGVkIGZvbGRlciB3aWxsIGJlIHJlLWFzc2lnbmVkXG4gICAgICogKi9cbiAgICBzdGF0aWMgc2V0UHJldkZvbGRlcklEKGZvbGRlcklEKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdwcmV2Rm9sZGVyJywgZm9sZGVySUQpO1xuICAgIH1cblxuICAgIHN0YXRpYyBnZXRQcmV2Rm9sZGVySUQoKSB7XG4gICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgncHJldkZvbGRlcicpO1xuICAgIH1cblxuICAgIHN0YXRpYyBhZGRUb1JlY2VudChpZCkge1xuICAgICAgICBpZiAoaWQgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgICAgXy5lYWNoKGlkLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICBSZWNlbnRJdGVtcy5wdXNoKHZhbHVlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgUmVjZW50SXRlbXMucHVzaChpZCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0SXRlbXMoKSB7XG4gICAgICAgIGxldCBpdGVtcyA9IFtdO1xuICAgICAgICAkKCcuanMtbWVkaWEtbGlzdC10aXRsZScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgbGV0ICRib3ggPSAkKHRoaXMpO1xuICAgICAgICAgICAgbGV0IGRhdGEgPSAkYm94LmRhdGEoKSB8fCB7fTtcbiAgICAgICAgICAgIGRhdGEuaW5kZXhfa2V5ID0gJGJveC5pbmRleCgpO1xuICAgICAgICAgICAgaXRlbXMucHVzaChkYXRhKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBpdGVtcztcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0U2VsZWN0ZWRJdGVtcygpIHtcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gW107XG4gICAgICAgICQoJy5qcy1tZWRpYS1saXN0LXRpdGxlIGlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGxldCAkYm94ID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtbWVkaWEtbGlzdC10aXRsZScpO1xuICAgICAgICAgICAgbGV0IGRhdGEgPSAkYm94LmRhdGEoKSB8fCB7fTtcbiAgICAgICAgICAgIGRhdGEuaW5kZXhfa2V5ID0gJGJveC5pbmRleCgpO1xuICAgICAgICAgICAgc2VsZWN0ZWQucHVzaChkYXRhKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBzZWxlY3RlZDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0U2VsZWN0ZWRGaWxlcygpIHtcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gW107XG4gICAgICAgICQoJy5qcy1tZWRpYS1saXN0LXRpdGxlW2RhdGEtY29udGV4dD1maWxlXSBpbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBsZXQgJGJveCA9ICQodGhpcykuY2xvc2VzdCgnLmpzLW1lZGlhLWxpc3QtdGl0bGUnKTtcbiAgICAgICAgICAgIGxldCBkYXRhID0gJGJveC5kYXRhKCkgfHwge307XG4gICAgICAgICAgICBkYXRhLmluZGV4X2tleSA9ICRib3guaW5kZXgoKTtcbiAgICAgICAgICAgIHNlbGVjdGVkLnB1c2goZGF0YSk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gc2VsZWN0ZWQ7XG4gICAgfVxuXG4gICAgc3RhdGljIGdldFNlbGVjdGVkRm9sZGVyKCkge1xuICAgICAgICBsZXQgc2VsZWN0ZWQgPSBbXTtcbiAgICAgICAgJCgnLmpzLW1lZGlhLWxpc3QtdGl0bGVbZGF0YS1jb250ZXh0PWZvbGRlcl0gaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgbGV0ICRib3ggPSAkKHRoaXMpLmNsb3Nlc3QoJy5qcy1tZWRpYS1saXN0LXRpdGxlJyk7XG4gICAgICAgICAgICBsZXQgZGF0YSA9ICRib3guZGF0YSgpIHx8IHt9O1xuICAgICAgICAgICAgZGF0YS5pbmRleF9rZXkgPSAkYm94LmluZGV4KCk7XG4gICAgICAgICAgICBzZWxlY3RlZC5wdXNoKGRhdGEpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHNlbGVjdGVkO1xuICAgIH1cblxuICAgIHN0YXRpYyBpc1VzZUluTW9kYWwoKSB7XG4gICAgICAgIHJldHVybiBIZWxwZXJzLmdldFVybFBhcmFtKCdtZWRpYS1hY3Rpb24nKSA9PT0gJ3NlbGVjdC1maWxlcycgfHwgKHdpbmRvdy5ydk1lZGlhICYmIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMgJiYgd2luZG93LnJ2TWVkaWEub3B0aW9ucy5vcGVuX2luID09PSAnbW9kYWwnKTtcbiAgICB9XG5cbiAgICBzdGF0aWMgcmVzZXRQYWdpbmF0aW9uKCkge1xuICAgICAgICBSVl9NRURJQV9DT05GSUcucGFnaW5hdGlvbiA9IHsgcGFnZWQ6IDEsIHBvc3RzX3Blcl9wYWdlOiA0MCwgaW5fcHJvY2Vzc19nZXRfbWVkaWE6IGZhbHNlLCBoYXNfbW9yZTogdHJ1ZSwgZm9sZGVyX2lkOiAwfTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9IZWxwZXJzL0hlbHBlcnMuanMiLCJsZXQgTWVkaWFDb25maWcgPSAkLnBhcnNlSlNPTihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnTWVkaWFDb25maWcnKSkgfHwge307XG5cbmxldCBkZWZhdWx0Q29uZmlnID0ge1xuICAgIGFwcF9rZXk6ICc0ODNhMHh5enl0ejEyNDJjMGQ1MjA0MjZlOGJhMzY2YzUzMGMzZDlkeHh4JyxcbiAgICByZXF1ZXN0X3BhcmFtczoge1xuICAgICAgICB2aWV3X3R5cGU6ICd0aWxlcycsXG4gICAgICAgIGZpbHRlcjogJ2V2ZXJ5dGhpbmcnLFxuICAgICAgICB2aWV3X2luOiAnbXlfbWVkaWEnLFxuICAgICAgICBzZWFyY2g6ICcnLFxuICAgICAgICBzb3J0X2J5OiAnY3JlYXRlZF9hdC1kZXNjJyxcbiAgICAgICAgZm9sZGVyX2lkOiAwLFxuICAgIH0sXG4gICAgaGlkZV9kZXRhaWxzX3BhbmU6IGZhbHNlLFxuICAgIGljb25zOiB7XG4gICAgICAgIGZvbGRlcjogJ2ZhIGZhLWZvbGRlci1vJyxcbiAgICB9LFxuICAgIGFjdGlvbnNfbGlzdDoge1xuICAgICAgICBiYXNpYzogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1leWUnLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdQcmV2aWV3JyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdwcmV2aWV3JyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMCxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1wcmV2aWV3JyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICAgIGZpbGU6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtbGluaycsXG4gICAgICAgICAgICAgICAgbmFtZTogJ0NvcHkgbGluaycsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnY29weV9saW5rJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMCxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1jb3B5LWxpbmsnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtcGVuY2lsJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnUmVuYW1lJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZW5hbWUnLFxuICAgICAgICAgICAgICAgIG9yZGVyOiAxLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncnYtYWN0aW9uLXJlbmFtZScsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1jb3B5JyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnTWFrZSBhIGNvcHknLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ21ha2VfY29weScsXG4gICAgICAgICAgICAgICAgb3JkZXI6IDIsXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdydi1hY3Rpb24tbWFrZS1jb3B5JyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWRvdC1jaXJjbGUtbycsXG4gICAgICAgICAgICAgICAgbmFtZTogJ1NldCBmb2N1cyBwb2ludCcsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc2V0X2ZvY3VzX3BvaW50JyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMyxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1zZXQtZm9jdXMtcG9pbnQnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgICAgdXNlcjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1zaGFyZS1hbHQnLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdTaGFyZScsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc2hhcmUnLFxuICAgICAgICAgICAgICAgIG9yZGVyOiAwLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncnYtYWN0aW9uLXNoYXJlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWJhbicsXG4gICAgICAgICAgICAgICAgbmFtZTogJ1JlbW92ZSBzaGFyZScsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAncmVtb3ZlX3NoYXJlJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMSxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1yZW1vdmUtc2hhcmUnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtc3RhcicsXG4gICAgICAgICAgICAgICAgbmFtZTogJ0Zhdm9yaXRlJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdmYXZvcml0ZScsXG4gICAgICAgICAgICAgICAgb3JkZXI6IDIsXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdydi1hY3Rpb24tZmF2b3JpdGUnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtc3Rhci1vJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnUmVtb3ZlIGZhdm9yaXRlJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZW1vdmVfZmF2b3JpdGUnLFxuICAgICAgICAgICAgICAgIG9yZGVyOiAzLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncnYtYWN0aW9uLWZhdm9yaXRlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICAgIG90aGVyOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWRvd25sb2FkJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnRG93bmxvYWQnLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2Rvd25sb2FkJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMCxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1kb3dubG9hZCcsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS10cmFzaCcsXG4gICAgICAgICAgICAgICAgbmFtZTogJ01vdmUgdG8gdHJhc2gnLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3RyYXNoJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMSxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi10cmFzaCcsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1lcmFzZXInLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdEZWxldGUgcGVybWFuZW50bHknLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2RlbGV0ZScsXG4gICAgICAgICAgICAgICAgb3JkZXI6IDIsXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdydi1hY3Rpb24tZGVsZXRlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLXVuZG8nLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdSZXN0b3JlJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZXN0b3JlJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMyxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1yZXN0b3JlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgfSxcbiAgICBkZW5pZWRfZG93bmxvYWQ6IFtcbiAgICAgICAgJ3lvdXR1YmUnLFxuICAgIF0sXG59O1xuXG5pZiAoIU1lZGlhQ29uZmlnLmFwcF9rZXkgfHwgTWVkaWFDb25maWcuYXBwX2tleSAhPT0gZGVmYXVsdENvbmZpZy5hcHBfa2V5KSB7XG4gICAgTWVkaWFDb25maWcgPSBkZWZhdWx0Q29uZmlnO1xufVxuXG5sZXQgUmVjZW50SXRlbXMgPSAkLnBhcnNlSlNPTihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnUmVjZW50SXRlbXMnKSkgfHwgW107XG5cbmV4cG9ydCB7TWVkaWFDb25maWcsIFJlY2VudEl0ZW1zfTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL0NvbmZpZy9NZWRpYUNvbmZpZy5qcyIsImltcG9ydCB7SGVscGVyc30gZnJvbSAnLi9BcHAvSGVscGVycy9IZWxwZXJzJztcbmltcG9ydCB7TWVkaWFDb25maWd9IGZyb20gJy4vQXBwL0NvbmZpZy9NZWRpYUNvbmZpZyc7XG5cbmV4cG9ydCBjbGFzcyBFZGl0b3JTZXJ2aWNlIHtcbiAgICBzdGF0aWMgZWRpdG9yU2VsZWN0RmlsZShzZWxlY3RlZEZpbGVzKSB7XG5cbiAgICAgICAgbGV0IGlzX2NrZWRpdG9yID0gSGVscGVycy5nZXRVcmxQYXJhbSgnQ0tFZGl0b3InKSB8fCBIZWxwZXJzLmdldFVybFBhcmFtKCdDS0VkaXRvckZ1bmNOdW0nKTtcblxuICAgICAgICBpZiAod2luZG93Lm9wZW5lciAmJiBpc19ja2VkaXRvcikge1xuICAgICAgICAgICAgbGV0IGZpcnN0SXRlbSA9IF8uZmlyc3Qoc2VsZWN0ZWRGaWxlcyk7XG5cbiAgICAgICAgICAgIHdpbmRvdy5vcGVuZXIuQ0tFRElUT1IudG9vbHMuY2FsbEZ1bmN0aW9uKEhlbHBlcnMuZ2V0VXJsUGFyYW0oJ0NLRWRpdG9yRnVuY051bScpLCBmaXJzdEl0ZW0udXJsKTtcblxuICAgICAgICAgICAgaWYgKHdpbmRvdy5vcGVuZXIpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cuY2xvc2UoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIE5vIFdZU0lXWUcgZWRpdG9yIGZvdW5kLCB1c2UgY3VzdG9tIG1ldGhvZC5cbiAgICAgICAgfVxuICAgIH1cbn1cblxuY2xhc3MgcnZNZWRpYSB7XG4gICAgY29uc3RydWN0b3Ioc2VsZWN0b3IsIG9wdGlvbnMpIHtcbiAgICAgICAgd2luZG93LnJ2TWVkaWEgPSB3aW5kb3cucnZNZWRpYSB8fCB7fTtcblxuICAgICAgICBsZXQgJGJvZHkgPSAkKCdib2R5Jyk7XG5cbiAgICAgICAgbGV0IGRlZmF1bHRPcHRpb25zID0ge1xuICAgICAgICAgICAgbXVsdGlwbGU6IHRydWUsXG4gICAgICAgICAgICB0eXBlOiAnKicsXG4gICAgICAgICAgICBvblNlbGVjdEZpbGVzOiBmdW5jdGlvbiAoZmlsZXMsICRlbCkge1xuXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcblxuICAgICAgICBsZXQgY2xpY2tDYWxsYmFjayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGxldCAkY3VycmVudCA9ICQodGhpcyk7XG4gICAgICAgICAgICAkKCcjcnZfbWVkaWFfbW9kYWwnKS5tb2RhbCgpO1xuXG4gICAgICAgICAgICB3aW5kb3cucnZNZWRpYS5vcHRpb25zID0gb3B0aW9ucztcbiAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMub3Blbl9pbiA9ICdtb2RhbCc7XG5cbiAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLiRlbCA9ICRjdXJyZW50O1xuXG4gICAgICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5maWx0ZXIgPSAnZXZlcnl0aGluZyc7XG4gICAgICAgICAgICBIZWxwZXJzLnN0b3JlQ29uZmlnKCk7XG5cbiAgICAgICAgICAgIGxldCBlbGVfb3B0aW9ucyA9IHdpbmRvdy5ydk1lZGlhLiRlbC5kYXRhKCdydi1tZWRpYScpO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBlbGVfb3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcgJiYgZWxlX29wdGlvbnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIGVsZV9vcHRpb25zID0gZWxlX29wdGlvbnNbMF07XG4gICAgICAgICAgICAgICAgd2luZG93LnJ2TWVkaWEub3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMsIGVsZV9vcHRpb25zIHx8IHt9KTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGVsZV9vcHRpb25zLnNlbGVjdGVkX2ZpbGVfaWQgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuaXNfcG9wdXAgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuaXNfcG9wdXAgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuaXNfcG9wdXAgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoJCgnI3J2X21lZGlhX2JvZHkgLnJ2LW1lZGlhLWNvbnRhaW5lcicpLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICQoJyNydl9tZWRpYV9ib2R5JykubG9hZChSVl9NRURJQV9VUkwucG9wdXAsIGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChkYXRhLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICQoJyNydl9tZWRpYV9ib2R5JylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnbWVkaWEtbW9kYWwtbG9hZGluZycpXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2xvc2VzdCgnLm1vZGFsLWNvbnRlbnQnKS5yZW1vdmVDbGFzcygnYmItbG9hZGluZycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKGRvY3VtZW50KS5maW5kKCcucnYtbWVkaWEtY29udGFpbmVyIC5qcy1jaGFuZ2UtYWN0aW9uW2RhdGEtdHlwZT1yZWZyZXNoXScpLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHR5cGVvZiBzZWxlY3RvciA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICRib2R5Lm9uKCdjbGljaycsIHNlbGVjdG9yLCBjbGlja0NhbGxiYWNrKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHNlbGVjdG9yLm9uKCdjbGljaycsIGNsaWNrQ2FsbGJhY2spO1xuICAgICAgICB9XG4gICAgfVxufVxuXG53aW5kb3cuUnZNZWRpYVN0YW5kQWxvbmUgPSBydk1lZGlhO1xuXG4kKCcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLm9mZignY2xpY2snKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGxldCBzZWxlY3RlZEZpbGVzID0gSGVscGVycy5nZXRTZWxlY3RlZEZpbGVzKCk7XG4gICAgaWYgKF8uc2l6ZShzZWxlY3RlZEZpbGVzKSA+IDApIHtcbiAgICAgICAgRWRpdG9yU2VydmljZS5lZGl0b3JTZWxlY3RGaWxlKHNlbGVjdGVkRmlsZXMpO1xuICAgIH1cbn0pO1xuXG4kLmZuLnJ2TWVkaWEgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgIGxldCAkc2VsZWN0b3IgPSAkKHRoaXMpO1xuXG4gICAgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuZmlsdGVyID0gJ2V2ZXJ5dGhpbmcnO1xuICAgIGlmIChNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy52aWV3X2luID09PSAndHJhc2gnKSB7XG4gICAgICAgICQoZG9jdW1lbnQpLmZpbmQoJy5qcy1pbnNlcnQtdG8tZWRpdG9yJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKGRvY3VtZW50KS5maW5kKCcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIH1cbiAgICBIZWxwZXJzLnN0b3JlQ29uZmlnKCk7XG5cbiAgICBuZXcgcnZNZWRpYSgkc2VsZWN0b3IsIG9wdGlvbnMpO1xufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvaW50ZWdyYXRlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==
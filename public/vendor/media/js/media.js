/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Helpers; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__ = __webpack_require__(1);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Helpers = function () {
    function Helpers() {
        _classCallCheck(this, Helpers);
    }

    _createClass(Helpers, null, [{
        key: 'getUrlParam',
        value: function getUrlParam(paramName) {
            var url = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            if (!url) {
                url = window.location.search;
            }
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = url.match(reParam);
            return match && match.length > 1 ? match[1] : null;
        }
    }, {
        key: 'asset',
        value: function asset(url) {
            if (url.substring(0, 2) === '//' || url.substring(0, 7) === 'http://' || url.substring(0, 8) === 'https://') {
                return url;
            }

            var baseUrl = RV_MEDIA_URL.base_url.substr(-1, 1) !== '/' ? RV_MEDIA_URL.base_url + '/' : RV_MEDIA_URL.base_url;

            if (url.substring(0, 1) === '/') {
                return baseUrl + url.substring(1);
            }
            return baseUrl + url;
        }
    }, {
        key: 'showAjaxLoading',
        value: function showAjaxLoading() {
            var $element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('.rv-media-main');

            $element.addClass('on-loading').append($('#rv_media_loading').html());
        }
    }, {
        key: 'hideAjaxLoading',
        value: function hideAjaxLoading() {
            var $element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('.rv-media-main');

            $element.removeClass('on-loading').find('.loading-wrapper').remove();
        }
    }, {
        key: 'isOnAjaxLoading',
        value: function isOnAjaxLoading() {
            var $element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('.rv-media-items');

            return $element.hasClass('on-loading');
        }
    }, {
        key: 'jsonEncode',
        value: function jsonEncode(object) {
            "use strict";

            if (typeof object === 'undefined') {
                object = null;
            }
            return JSON.stringify(object);
        }
    }, {
        key: 'jsonDecode',
        value: function jsonDecode(jsonString, defaultValue) {
            "use strict";

            if (!jsonString) {
                return defaultValue;
            }
            if (typeof jsonString === 'string') {
                var result = void 0;
                try {
                    result = $.parseJSON(jsonString);
                } catch (err) {
                    result = defaultValue;
                }
                return result;
            }
            return jsonString;
        }
    }, {
        key: 'getRequestParams',
        value: function getRequestParams() {
            if (window.rvMedia.options && window.rvMedia.options.open_in === 'modal') {
                return $.extend(true, __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */].request_params, window.rvMedia.options || {});
            }
            return __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */].request_params;
        }
    }, {
        key: 'setSelectedFile',
        value: function setSelectedFile($file_id) {
            if (typeof window.rvMedia.options !== 'undefined') {
                window.rvMedia.options.selected_file_id = $file_id;
            } else {
                __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */].request_params.selected_file_id = $file_id;
            }
        }
    }, {
        key: 'getConfigs',
        value: function getConfigs() {
            return __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */];
        }
    }, {
        key: 'storeConfig',
        value: function storeConfig() {
            localStorage.setItem('MediaConfig', Helpers.jsonEncode(__WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["a" /* MediaConfig */]));
        }
    }, {
        key: 'storeRecentItems',
        value: function storeRecentItems() {
            localStorage.setItem('RecentItems', Helpers.jsonEncode(__WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */]));
        }

        /**
         * We currently allow searching all place so need to Keep prev folder when remove keyword. In this case
         * the selected folder will be re-assigned
         * */

    }, {
        key: 'setPrevFolderID',
        value: function setPrevFolderID(folderID) {
            localStorage.setItem('prevFolder', folderID);
        }
    }, {
        key: 'getPrevFolderID',
        value: function getPrevFolderID() {
            return localStorage.getItem('prevFolder');
        }
    }, {
        key: 'addToRecent',
        value: function addToRecent(id) {
            if (id instanceof Array) {
                _.each(id, function (value) {
                    __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */].push(value);
                });
            } else {
                __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */].push(id);
            }
        }
    }, {
        key: 'getItems',
        value: function getItems() {
            var items = [];
            $('.js-media-list-title').each(function () {
                var $box = $(this);
                var data = $box.data() || {};
                data.index_key = $box.index();
                items.push(data);
            });
            return items;
        }
    }, {
        key: 'getSelectedItems',
        value: function getSelectedItems() {
            var selected = [];
            $('.js-media-list-title input[type=checkbox]:checked').each(function () {
                var $box = $(this).closest('.js-media-list-title');
                var data = $box.data() || {};
                data.index_key = $box.index();
                selected.push(data);
            });
            return selected;
        }
    }, {
        key: 'getSelectedFiles',
        value: function getSelectedFiles() {
            var selected = [];
            $('.js-media-list-title[data-context=file] input[type=checkbox]:checked').each(function () {
                var $box = $(this).closest('.js-media-list-title');
                var data = $box.data() || {};
                data.index_key = $box.index();
                selected.push(data);
            });
            return selected;
        }
    }, {
        key: 'getSelectedFolder',
        value: function getSelectedFolder() {
            var selected = [];
            $('.js-media-list-title[data-context=folder] input[type=checkbox]:checked').each(function () {
                var $box = $(this).closest('.js-media-list-title');
                var data = $box.data() || {};
                data.index_key = $box.index();
                selected.push(data);
            });
            return selected;
        }
    }, {
        key: 'isUseInModal',
        value: function isUseInModal() {
            return Helpers.getUrlParam('media-action') === 'select-files' || window.rvMedia && window.rvMedia.options && window.rvMedia.options.open_in === 'modal';
        }
    }, {
        key: 'resetPagination',
        value: function resetPagination() {
            RV_MEDIA_CONFIG.pagination = { paged: 1, posts_per_page: 40, in_process_get_media: false, has_more: true, folder_id: 0 };
        }
    }]);

    return Helpers;
}();

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return RecentItems; });
var MediaConfig = $.parseJSON(localStorage.getItem('MediaConfig')) || {};

var defaultConfig = {
    app_key: '483a0xyzytz1242c0d520426e8ba366c530c3d9dxxx',
    request_params: {
        view_type: 'tiles',
        filter: 'everything',
        view_in: 'my_media',
        search: '',
        sort_by: 'created_at-desc',
        folder_id: 0
    },
    hide_details_pane: false,
    icons: {
        folder: 'fa fa-folder-o'
    },
    actions_list: {
        basic: [{
            icon: 'fa fa-eye',
            name: 'Preview',
            action: 'preview',
            order: 0,
            class: 'rv-action-preview'
        }],
        file: [{
            icon: 'fa fa-link',
            name: 'Copy link',
            action: 'copy_link',
            order: 0,
            class: 'rv-action-copy-link'
        }, {
            icon: 'fa fa-pencil',
            name: 'Rename',
            action: 'rename',
            order: 1,
            class: 'rv-action-rename'
        }, {
            icon: 'fa fa-copy',
            name: 'Make a copy',
            action: 'make_copy',
            order: 2,
            class: 'rv-action-make-copy'
        }, {
            icon: 'fa fa-dot-circle-o',
            name: 'Set focus point',
            action: 'set_focus_point',
            order: 3,
            class: 'rv-action-set-focus-point'
        }],
        user: [{
            icon: 'fa fa-share-alt',
            name: 'Share',
            action: 'share',
            order: 0,
            class: 'rv-action-share'
        }, {
            icon: 'fa fa-ban',
            name: 'Remove share',
            action: 'remove_share',
            order: 1,
            class: 'rv-action-remove-share'
        }, {
            icon: 'fa fa-star',
            name: 'Favorite',
            action: 'favorite',
            order: 2,
            class: 'rv-action-favorite'
        }, {
            icon: 'fa fa-star-o',
            name: 'Remove favorite',
            action: 'remove_favorite',
            order: 3,
            class: 'rv-action-favorite'
        }],
        other: [{
            icon: 'fa fa-download',
            name: 'Download',
            action: 'download',
            order: 0,
            class: 'rv-action-download'
        }, {
            icon: 'fa fa-trash',
            name: 'Move to trash',
            action: 'trash',
            order: 1,
            class: 'rv-action-trash'
        }, {
            icon: 'fa fa-eraser',
            name: 'Delete permanently',
            action: 'delete',
            order: 2,
            class: 'rv-action-delete'
        }, {
            icon: 'fa fa-undo',
            name: 'Restore',
            action: 'restore',
            order: 3,
            class: 'rv-action-restore'
        }]
    },
    denied_download: ['youtube']
};

if (!MediaConfig.app_key || MediaConfig.app_key !== defaultConfig.app_key) {
    MediaConfig = defaultConfig;
}

var RecentItems = $.parseJSON(localStorage.getItem('RecentItems')) || [];



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageService; });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MessageService = function () {
    function MessageService() {
        _classCallCheck(this, MessageService);
    }

    _createClass(MessageService, null, [{
        key: 'showMessage',
        value: function showMessage(type, message, messageHeader) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                positionClass: 'toast-bottom-right',
                onclick: null,
                showDuration: 1000,
                hideDuration: 1000,
                timeOut: 10000,
                extendedTimeOut: 1000,
                showEasing: 'swing',
                hideEasing: 'linear',
                showMethod: 'fadeIn',
                hideMethod: 'fadeOut'
            };
            toastr[type](message, messageHeader);
        }
    }, {
        key: 'handleError',
        value: function handleError(data) {
            if (typeof data.responseJSON !== 'undefined') {
                if (typeof data.responseJSON.message !== 'undefined') {
                    MessageService.showMessage('error', data.responseJSON.message, RV_MEDIA_CONFIG.translations.message.error_header);
                } else {
                    $.each(data.responseJSON, function (index, el) {
                        $.each(el, function (key, item) {
                            MessageService.showMessage('error', item, RV_MEDIA_CONFIG.translations.message.error_header);
                        });
                    });
                }
            } else {
                MessageService.showMessage('error', data.statusText, RV_MEDIA_CONFIG.translations.message.error_header);
            }
        }
    }]);

    return MessageService;
}();

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_ActionsService__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_ContextMenuService__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Views_MediaList__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Views_MediaDetails__ = __webpack_require__(10);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }









var MediaService = function () {
    function MediaService() {
        _classCallCheck(this, MediaService);

        this.MediaList = new __WEBPACK_IMPORTED_MODULE_5__Views_MediaList__["a" /* MediaList */]();
        this.MediaDetails = new __WEBPACK_IMPORTED_MODULE_6__Views_MediaDetails__["a" /* MediaDetails */]();
        this.breadcrumbTemplate = $('#rv_media_breadcrumb_item').html();
    }

    _createClass(MediaService, [{
        key: 'getMedia',
        value: function getMedia() {
            var reload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            var is_popup = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
            var load_more_file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            if (typeof RV_MEDIA_CONFIG.pagination != 'undefined') {
                if (RV_MEDIA_CONFIG.pagination.in_process_get_media) {
                    return;
                } else {
                    RV_MEDIA_CONFIG.pagination.in_process_get_media = true;
                }
            }

            var _self = this;

            _self.getFileDetails({
                icon: 'fa fa-picture-o',
                nothing_selected: ''
            });

            var params = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams();

            if (params.view_in === 'recent') {
                params.recent_items = __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */];
            }

            if (is_popup === true) {
                params.is_popup = true;
            } else {
                params.is_popup = undefined;
            }

            params.onSelectFiles = undefined;

            if (typeof params.search != 'undefined' && params.search != '' && typeof params.selected_file_id != 'undefined') {
                params.selected_file_id = undefined;
            }

            params.load_more_file = load_more_file;
            if (typeof RV_MEDIA_CONFIG.pagination != 'undefined') {
                params.paged = RV_MEDIA_CONFIG.pagination.paged;
                params.posts_per_page = RV_MEDIA_CONFIG.pagination.posts_per_page;
            }
            $.ajax({
                url: RV_MEDIA_URL.get_media,
                type: 'GET',
                data: params,
                dataType: 'json',
                beforeSend: function beforeSend() {
                    __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].showAjaxLoading();
                },
                success: function success(res) {
                    _self.MediaList.renderData(res.data, reload, load_more_file);
                    _self.fetchQuota();
                    _self.renderBreadcrumbs(res.data.breadcrumbs);
                    MediaService.refreshFilter();
                    __WEBPACK_IMPORTED_MODULE_3__Services_ActionsService__["a" /* ActionsService */].renderActions();

                    if (typeof RV_MEDIA_CONFIG.pagination != 'undefined') {
                        if (typeof RV_MEDIA_CONFIG.pagination.paged != 'undefined') {
                            RV_MEDIA_CONFIG.pagination.paged += 1;
                        }

                        if (typeof RV_MEDIA_CONFIG.pagination.in_process_get_media != 'undefined') {
                            RV_MEDIA_CONFIG.pagination.in_process_get_media = false;
                        }

                        if (typeof RV_MEDIA_CONFIG.pagination.posts_per_page != 'undefined' && res.data.files.length < RV_MEDIA_CONFIG.pagination.posts_per_page && typeof RV_MEDIA_CONFIG.pagination.has_more != 'undefined') {
                            RV_MEDIA_CONFIG.pagination.has_more = false;
                        }
                    }
                },
                complete: function complete(data) {
                    __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].hideAjaxLoading();
                },
                error: function error(data) {
                    __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].handleError(data);
                }
            });
        }
    }, {
        key: 'getFileDetails',
        value: function getFileDetails(data) {
            this.MediaDetails.renderData(data);
        }
    }, {
        key: 'fetchQuota',
        value: function fetchQuota() {
            $.ajax({
                url: RV_MEDIA_URL.get_quota,
                type: 'GET',
                dataType: 'json',
                beforeSend: function beforeSend() {},
                success: function success(res) {
                    var data = res.data;

                    $('.rv-media-aside-bottom .used-analytics span').html(data.used + ' / ' + data.quota);
                    $('.rv-media-aside-bottom .progress-bar').css({
                        width: data.percent + '%'
                    });
                },
                error: function error(data) {
                    __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].handleError(data);
                }
            });
        }
    }, {
        key: 'renderBreadcrumbs',
        value: function renderBreadcrumbs(breadcrumbItems) {
            var _self = this;
            var $breadcrumbContainer = $('.rv-media-breadcrumb .breadcrumb');
            $breadcrumbContainer.find('li').remove();

            _.each(breadcrumbItems, function (value, index) {
                var template = _self.breadcrumbTemplate;
                template = template.replace(/__name__/gi, value.name || '').replace(/__icon__/gi, value.icon ? '<i class="' + value.icon + '"></i>' : '').replace(/__folderId__/gi, value.id || 0);
                $breadcrumbContainer.append($(template));
            });
            $('.rv-media-container').attr('data-breadcrumb-count', _.size(breadcrumbItems));
        }
    }], [{
        key: 'refreshFilter',
        value: function refreshFilter() {
            var $rvMediaContainer = $('.rv-media-container');
            var view_in = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_in;
            if (view_in !== 'my_media' && (view_in !== 'shared' && view_in !== 'shared_with_me' && view_in !== 'public' || __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams().folder_id == 0)) {
                $('.rv-media-actions .btn:not([data-type="refresh"]):not(label)').addClass('disabled');
                $rvMediaContainer.attr('data-allow-upload', 'false');
            } else {
                $('.rv-media-actions .btn:not([data-type="refresh"]):not(label)').removeClass('disabled');
                $rvMediaContainer.attr('data-allow-upload', 'true');
            }

            $('.rv-media-actions .btn.js-rv-media-change-filter-group').removeClass('disabled');

            var $empty_trash_btn = $('.rv-media-actions .btn[data-action="empty_trash"]');
            if (view_in === 'trash') {
                $empty_trash_btn.removeClass('hidden').removeClass('disabled');
                if (!_.size(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getItems())) {
                    $empty_trash_btn.addClass('hidden').addClass('disabled');
                }
            } else {
                $empty_trash_btn.addClass('hidden');
            }

            __WEBPACK_IMPORTED_MODULE_4__Services_ContextMenuService__["a" /* ContextMenuService */].destroyContext();
            __WEBPACK_IMPORTED_MODULE_4__Services_ContextMenuService__["a" /* ContextMenuService */].initContext();

            $rvMediaContainer.attr('data-view-in', view_in);
        }
    }]);

    return MediaService;
}();

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__ = __webpack_require__(2);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }





var ActionsService = function () {
    function ActionsService() {
        _classCallCheck(this, ActionsService);
    }

    _createClass(ActionsService, null, [{
        key: 'handleDropdown',
        value: function handleDropdown() {
            var selected = _.size(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedItems());

            ActionsService.renderActions();

            if (selected > 0) {
                $('.rv-dropdown-actions').removeClass('disabled');
            } else {
                $('.rv-dropdown-actions').addClass('disabled');
            }
        }
    }, {
        key: 'handlePreview',
        value: function handlePreview() {
            var selected = [];

            _.each(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedFiles(), function (value, index) {
                if (_.contains(['image', 'youtube', 'pdf', 'text', 'video'], value.type)) {
                    selected.push({
                        src: value.url
                    });
                    __WEBPACK_IMPORTED_MODULE_0__Config_MediaConfig__["b" /* RecentItems */].push(value.id);
                }
            });

            if (_.size(selected) > 0) {
                $.fancybox.open(selected);
                __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].storeRecentItems();
            } else {
                //turn off auto download file/folder func when double click preview file/folder
                //this.handleGlobalAction('download');
            }
        }
    }, {
        key: 'handleCopyLink',
        value: function handleCopyLink() {
            var links = '';
            _.each(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedFiles(), function (value, index) {
                if (!_.isEmpty(links)) {
                    links += '\n';
                }
                links += value.full_url;
            });
            var $clipboardTemp = $('.js-rv-clipboard-temp');
            $clipboardTemp.data('clipboard-text', links);
            new Clipboard('.js-rv-clipboard-temp', {
                text: function text(trigger) {
                    return links;
                }
            });
            __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].showMessage('success', RV_MEDIA_CONFIG.translations.clipboard.success, RV_MEDIA_CONFIG.translations.message.success_header);
            $clipboardTemp.trigger('click');
        }
    }, {
        key: 'handleGlobalAction',
        value: function handleGlobalAction(type, callback) {
            var selected = [];
            _.each(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedItems(), function (value, index) {
                selected.push({
                    is_folder: value.is_folder,
                    id: value.id,
                    full_url: value.full_url,
                    focus: value.focus
                });
            });

            switch (type) {
                case 'rename':
                    $('#modal_rename_items').modal('show').find('form.rv-form').data('action', type);
                    break;
                case 'copy_link':
                    ActionsService.handleCopyLink();
                    break;
                case 'preview':
                    ActionsService.handlePreview();
                    break;
                case 'set_focus_point':
                    var modal = $('#modal_set_focus_point');
                    if (selected[0].focus.length === 0) {
                        modal.find('.helper-tool-data-attr').val('');
                        modal.find('.helper-tool-css3-val').val('');
                        modal.find('.helper-tool-reticle-css').val('');
                        $('.reticle').removeAttr('style');
                    } else {
                        modal.find('.helper-tool-data-attr').val(selected[0].focus.data_attribute);
                        modal.find('.helper-tool-css3-val').val(selected[0].focus.css_bg_position);
                        modal.find('.helper-tool-reticle-css').val(selected[0].focus.retice_css);
                        $('.reticle').prop('style', selected[0].focus.retice_css);
                    }
                    modal.modal('show').find('form.rv-form').data('action', type).data('image', selected[0].full_url);
                    break;
                case 'trash':
                    $('#modal_trash_items').modal('show').find('form.rv-form').data('action', type);
                    break;
                case 'delete':
                    $('#modal_delete_items').modal('show').find('form.rv-form').data('action', type);
                    break;
                case 'share':
                    $('#modal_share_items').modal('show').find('form.rv-form').data('action', type);
                    break;
                case 'empty_trash':
                    $('#modal_empty_trash').modal('show').find('form.rv-form').data('action', type);
                    break;
                case 'download':
                    var downloadLink = RV_MEDIA_URL.download;
                    var count = 0;
                    _.each(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedItems(), function (value, index) {
                        if (!_.contains(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getConfigs().denied_download, value.mime_type)) {
                            downloadLink += (count === 0 ? '?' : '&') + 'selected[' + count + '][is_folder]=' + value.is_folder + '&selected[' + count + '][id]=' + value.id;
                            count++;
                        }
                    });
                    if (downloadLink !== RV_MEDIA_URL.download) {
                        window.open(downloadLink, '_blank');
                    } else {
                        __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].showMessage('error', RV_MEDIA_CONFIG.translations.download.error, RV_MEDIA_CONFIG.translations.message.error_header);
                    }
                    break;
                default:
                    ActionsService.processAction({
                        selected: selected,
                        action: type
                    }, callback);
                    break;
            }
        }
    }, {
        key: 'processAction',
        value: function processAction(data) {
            var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            $.ajax({
                url: RV_MEDIA_URL.global_actions,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function beforeSend() {
                    __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].showAjaxLoading();
                },
                success: function success(res) {
                    __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].resetPagination();
                    if (!res.error) {
                        __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].showMessage('success', res.message, RV_MEDIA_CONFIG.translations.message.success_header);
                    } else {
                        __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].showMessage('error', res.message, RV_MEDIA_CONFIG.translations.message.error_header);
                    }
                    if (callback) {
                        callback(res);
                    }
                },
                complete: function complete(data) {
                    __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].hideAjaxLoading();
                },
                error: function error(data) {
                    __WEBPACK_IMPORTED_MODULE_2__Services_MessageService__["a" /* MessageService */].handleError(data);
                }
            });
        }
    }, {
        key: 'renderRenameItems',
        value: function renderRenameItems() {
            var VIEW = $('#rv_media_rename_item').html();
            var $itemsWrapper = $('#modal_rename_items .rename-items').empty();

            _.each(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedItems(), function (value, index) {
                var item = VIEW.replace(/__icon__/gi, value.icon || 'fa fa-file-o').replace(/__placeholder__/gi, 'Input file name').replace(/__value__/gi, value.name);
                var $item = $(item);
                $item.data('id', value.id);
                $item.data('is_folder', value.is_folder);
                $item.data('name', value.name);
                $itemsWrapper.append($item);
            });
        }
    }, {
        key: 'renderActions',
        value: function renderActions() {
            var hasFolderSelected = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedFolder().length > 0;

            var ACTION_TEMPLATE = $('#rv_action_item').html();
            var initialized_item = 0;
            var $dropdownActions = $('.rv-dropdown-actions .dropdown-menu');
            $dropdownActions.empty();

            var actionsList = $.extend({}, true, __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getConfigs().actions_list);

            if (hasFolderSelected) {
                actionsList.basic = _.reject(actionsList.basic, function (item) {
                    return item.action === 'preview';
                });
                actionsList.file = _.reject(actionsList.file, function (item) {
                    return item.action === 'copy_link';
                });
                actionsList.file = _.reject(actionsList.file, function (item) {
                    return item.action === 'set_focus_point';
                });

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.create')) {
                    actionsList.file = _.reject(actionsList.file, function (item) {
                        return item.action === 'make_copy';
                    });
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.edit')) {
                    actionsList.file = _.reject(actionsList.file, function (item) {
                        return _.contains(['rename'], item.action);
                    });

                    actionsList.user = _.reject(actionsList.user, function (item) {
                        return _.contains(['rename', 'share', 'remove_share', 'un_share'], item.action);
                    });
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.trash')) {
                    actionsList.other = _.reject(actionsList.other, function (item) {
                        return _.contains(['trash', 'restore'], item.action);
                    });
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.delete')) {
                    actionsList.other = _.reject(actionsList.other, function (item) {
                        return _.contains(['delete'], item.action);
                    });
                }
            }

            var selectedFiles = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
            if (selectedFiles.length > 0 && selectedFiles[0].type !== 'image') {
                actionsList.file = _.reject(actionsList.file, function (item) {
                    return item.action === 'set_focus_point';
                });
            }

            var can_preview = false;
            _.each(selectedFiles, function (value) {
                if (_.contains(['image', 'youtube', 'pdf', 'text', 'video'], value.type)) {
                    can_preview = true;
                }
            });

            if (!can_preview) {
                actionsList.basic = _.reject(actionsList.basic, function (item) {
                    return item.action === 'preview';
                });
            }

            if (RV_MEDIA_CONFIG.mode === 'simple') {
                actionsList.user = _.reject(actionsList.user, function (item) {
                    return _.contains(['share', 'remove_share', 'un_share'], item.action);
                });
            }

            if (selectedFiles.length > 0) {
                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.create')) {
                    actionsList.file = _.reject(actionsList.file, function (item) {
                        return item.action === 'make_copy';
                    });
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.edit')) {
                    actionsList.file = _.reject(actionsList.file, function (item) {
                        return _.contains(['rename', 'set_focus_point'], item.action);
                    });

                    actionsList.user = _.reject(actionsList.user, function (item) {
                        return _.contains(['share', 'remove_share', 'un_share'], item.action);
                    });
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.trash')) {
                    actionsList.other = _.reject(actionsList.other, function (item) {
                        return _.contains(['trash', 'restore'], item.action);
                    });
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.delete')) {
                    actionsList.other = _.reject(actionsList.other, function (item) {
                        return _.contains(['delete'], item.action);
                    });
                }
            }

            _.each(actionsList, function (action, key) {
                _.each(action, function (item, index) {
                    var is_break = false;
                    switch (__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_in) {
                        case 'my_media':
                            if (_.contains(['remove_favorite', 'delete', 'restore', 'remove_share'], item.action)) {
                                is_break = true;
                            }
                            break;
                        case 'shared':
                            if (_.contains(['remove_favorite', 'delete', 'restore', 'make_copy', 'remove_share'], item.action)) {
                                is_break = true;
                            }
                            break;
                        case 'shared_with_me':
                            if (_.contains(['remove_favorite', 'delete', 'restore', 'make_copy', 'share'], item.action)) {
                                is_break = true;
                            }
                            break;
                        case 'public':
                            if (_.contains(['remove_favorite', 'delete', 'restore', 'make_copy', 'share', 'remove_share'], item.action)) {
                                is_break = true;
                            }
                            break;
                        case 'recent':
                            if (_.contains(['remove_favorite', 'delete', 'restore', 'make_copy', 'remove_share'], item.action)) {
                                is_break = true;
                            }
                            break;
                        case 'favorites':
                            if (_.contains(['favorite', 'delete', 'restore', 'make_copy', 'remove_share'], item.action)) {
                                is_break = true;
                            }
                            break;
                        case 'trash':
                            if (!_.contains(['preview', 'delete', 'restore', 'rename', 'download'], item.action)) {
                                is_break = true;
                            }
                            break;
                    }
                    if (!is_break) {
                        var actions_list = '';
                        try {
                            actions_list = RV_MEDIA_CONFIG.translations.actions_list[key][item.action];
                        } catch (err) {}
                        var template = ACTION_TEMPLATE.replace(/__action__/gi, item.action || '').replace(/__icon__/gi, item.icon || '').replace(/__name__/gi, actions_list || item.name);
                        if (!index && initialized_item) {
                            template = '<li role="separator" class="divider"></li>' + template;
                        }
                        $dropdownActions.append(template);
                    }
                });
                if (action.length > 0) {
                    initialized_item++;
                }
            });
        }
    }]);

    return ActionsService;
}();

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditorService", function() { return EditorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__ = __webpack_require__(1);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var EditorService = function () {
    function EditorService() {
        _classCallCheck(this, EditorService);
    }

    _createClass(EditorService, null, [{
        key: 'editorSelectFile',
        value: function editorSelectFile(selectedFiles) {

            var is_ckeditor = __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getUrlParam('CKEditor') || __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getUrlParam('CKEditorFuncNum');

            if (window.opener && is_ckeditor) {
                var firstItem = _.first(selectedFiles);

                window.opener.CKEDITOR.tools.callFunction(__WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getUrlParam('CKEditorFuncNum'), firstItem.url);

                if (window.opener) {
                    window.close();
                }
            } else {
                // No WYSIWYG editor found, use custom method.
            }
        }
    }]);

    return EditorService;
}();

var rvMedia = function rvMedia(selector, options) {
    _classCallCheck(this, rvMedia);

    window.rvMedia = window.rvMedia || {};

    var $body = $('body');

    var defaultOptions = {
        multiple: true,
        type: '*',
        onSelectFiles: function onSelectFiles(files, $el) {}
    };

    options = $.extend(true, defaultOptions, options);

    var clickCallback = function clickCallback(event) {
        event.preventDefault();
        var $current = $(this);
        $('#rv_media_modal').modal();

        window.rvMedia.options = options;
        window.rvMedia.options.open_in = 'modal';

        window.rvMedia.$el = $current;

        __WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.filter = 'everything';
        __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();

        var ele_options = window.rvMedia.$el.data('rv-media');
        if (typeof ele_options !== 'undefined' && ele_options.length > 0) {
            ele_options = ele_options[0];
            window.rvMedia.options = $.extend(true, window.rvMedia.options, ele_options || {});
            if (typeof ele_options.selected_file_id !== 'undefined') {
                window.rvMedia.options.is_popup = true;
            } else if (typeof window.rvMedia.options.is_popup !== 'undefined') {
                window.rvMedia.options.is_popup = undefined;
            }
        }

        if ($('#rv_media_body .rv-media-container').length === 0) {
            $('#rv_media_body').load(RV_MEDIA_URL.popup, function (data) {
                if (data.error) {
                    alert(data.message);
                }
                $('#rv_media_body').removeClass('media-modal-loading').closest('.modal-content').removeClass('bb-loading');
            });
        } else {
            $(document).find('.rv-media-container .js-change-action[data-type=refresh]').trigger('click');
        }
    };

    if (typeof selector === 'string') {
        $body.on('click', selector, clickCallback);
    } else {
        selector.on('click', clickCallback);
    }
};

window.RvMediaStandAlone = rvMedia;

$('.js-insert-to-editor').off('click').on('click', function (event) {
    event.preventDefault();
    var selectedFiles = __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
    if (_.size(selectedFiles) > 0) {
        EditorService.editorSelectFile(selectedFiles);
    }
});

$.fn.rvMedia = function (options) {
    var $selector = $(this);

    __WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.filter = 'everything';
    if (__WEBPACK_IMPORTED_MODULE_1__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.view_in === 'trash') {
        $(document).find('.js-insert-to-editor').prop('disabled', true);
    } else {
        $(document).find('.js-insert-to-editor').prop('disabled', false);
    }
    __WEBPACK_IMPORTED_MODULE_0__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();

    new rvMedia($selector, options);
};

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaList; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_ActionsService__ = __webpack_require__(4);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var MediaList = function () {
    function MediaList() {
        _classCallCheck(this, MediaList);

        this.group = {};
        this.group.list = $('#rv_media_items_list').html();
        this.group.tiles = $('#rv_media_items_tiles').html();

        this.item = {};
        this.item.list = $('#rv_media_items_list_element').html();
        this.item.tiles = $('#rv_media_items_tiles_element').html();

        this.$groupContainer = $('.rv-media-items');
    }

    _createClass(MediaList, [{
        key: 'renderData',
        value: function renderData(data) {
            var reload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
            var load_more_file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            var _self = this;
            var MediaConfig = __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getConfigs();
            var template = _self.group[__WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_type];

            var view_in = __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_in;

            if (!_.contains(['my_media', 'public', 'trash', 'favorites', 'shared', 'shared_with_me', 'recent'], view_in)) {
                view_in = 'my_media';
            }

            var translations_view_icon = '';
            var translations_view_title = '';
            var translations_view_message = '';
            try {
                translations_view_icon = RV_MEDIA_CONFIG.translations.no_item[view_in].icon;
                translations_view_title = RV_MEDIA_CONFIG.translations.no_item[view_in].title;
                translations_view_message = RV_MEDIA_CONFIG.translations.no_item[view_in].message;
            } catch (err) {}

            template = template.replace(/__noItemIcon__/gi, translations_view_icon).replace(/__noItemTitle__/gi, translations_view_title).replace(/__noItemMessage__/gi, translations_view_message);

            var $result = $(template);
            var $itemsWrapper = $result.find('ul');

            if (load_more_file && this.$groupContainer.find('.rv-media-grid ul').length > 0) {
                $itemsWrapper = this.$groupContainer.find('.rv-media-grid ul');
            }

            if (_.size(data.folders) > 0 || _.size(data.files) > 0) {
                $('.rv-media-items').addClass('has-items');
            } else {
                $('.rv-media-items').removeClass('has-items');
            }

            _.forEach(data.folders, function (value, index) {
                var item = _self.item[__WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_type];
                item = item.replace(/__type__/gi, 'folder').replace(/__id__/gi, value.id).replace(/__name__/gi, value.name || '').replace(/__size__/gi, '').replace(/__date__/gi, value.created_at || '').replace(/__thumb__/gi, '<i class="fa fa-folder-o"></i>');
                var $item = $(item);
                _.forEach(value, function (val, index) {
                    $item.data(index, val);
                });
                $item.data('is_folder', true);
                $item.data('icon', MediaConfig.icons.folder);
                $itemsWrapper.append($item);
            });

            _.forEach(data.files, function (value) {
                var item = _self.item[__WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_type];
                item = item.replace(/__type__/gi, 'file').replace(/__id__/gi, value.id).replace(/__name__/gi, value.name || '').replace(/__size__/gi, value.size || '').replace(/__date__/gi, value.created_at || '');
                if (__WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_type === 'list') {
                    item = item.replace(/__thumb__/gi, '<i class="' + value.icon + '"></i>');
                } else {
                    switch (value.mime_type) {
                        case 'youtube':
                            item = item.replace(/__thumb__/gi, '<img src="' + value.options.thumb + '" alt="' + value.name + '">');
                            break;
                        default:
                            item = item.replace(/__thumb__/gi, value.thumb ? '<img src="' + value.thumb + '?' + new Date().getTime() + '" alt="' + value.name + '">' : '<i class="' + value.icon + '"></i>');
                            break;
                    }
                }
                var $item = $(item);
                $item.data('is_folder', false);
                _.forEach(value, function (val, index) {
                    $item.data(index, val);
                });
                $itemsWrapper.append($item);
            });
            if (reload !== false) {
                _self.$groupContainer.empty();
            }

            if (load_more_file && this.$groupContainer.find('.rv-media-grid ul').length > 0) {} else {
                _self.$groupContainer.append($result);
            }
            _self.$groupContainer.find('.loading-wrapper').remove();
            __WEBPACK_IMPORTED_MODULE_1__Services_ActionsService__["a" /* ActionsService */].handleDropdown();

            //trigger event click for file selected
            $('.js-media-list-title[data-id=' + data.selected_file_id + ']').trigger('click');
        }
    }]);

    return MediaList;
}();

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(8);
module.exports = __webpack_require__(16);


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__App_Services_MediaService__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__App_Services_MessageService__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__App_Services_FolderService__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__App_Services_UploadService__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__App_Externals_ExternalServices__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__integrate__ = __webpack_require__(5);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }











var MediaManagement = function () {
    function MediaManagement() {
        _classCallCheck(this, MediaManagement);

        this.MediaService = new __WEBPACK_IMPORTED_MODULE_2__App_Services_MediaService__["a" /* MediaService */]();
        this.UploadService = new __WEBPACK_IMPORTED_MODULE_5__App_Services_UploadService__["a" /* UploadService */]();
        this.FolderService = new __WEBPACK_IMPORTED_MODULE_4__App_Services_FolderService__["a" /* FolderService */]();

        new __WEBPACK_IMPORTED_MODULE_7__App_Externals_ExternalServices__["a" /* ExternalServices */]();

        this.$body = $('body');
    }

    _createClass(MediaManagement, [{
        key: 'init',
        value: function init() {
            __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();
            this.setupLayout();

            this.handleMediaList();
            this.changeViewType();
            this.changeFilter();
            this.search();
            this.handleActions();

            this.UploadService.init();

            this.handleModals();
            this.scrollGetMore();

            // remove keyword once refreshing
            __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search = '';

            $('.js-rv-media-change-view-type .btn[data-type="' + __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_type + '"]').trigger('click');
        }
    }, {
        key: 'setupLayout',
        value: function setupLayout() {
            /**
             * Sidebar
             */
            var $current_filter = $('.js-rv-media-change-filter[data-type="filter"][data-value="' + __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getRequestParams().filter + '"]');

            $current_filter.closest('li').addClass('active').closest('.dropdown').find('.js-rv-media-filter-current').html('(' + $current_filter.html() + ')');

            var $current_view_in = $('.js-rv-media-change-filter[data-type="view_in"][data-value="' + __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_in + '"]');

            $current_view_in.closest('li').addClass('active').closest('.dropdown').find('.js-rv-media-filter-current').html('(' + $current_view_in.html() + ')');

            if (__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].isUseInModal()) {
                $('.rv-media-footer').removeClass('hidden');
            }

            /**
             * Sort
             */
            $('.js-rv-media-change-filter[data-type="sort_by"][data-value="' + __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getRequestParams().sort_by + '"]').closest('li').addClass('active');

            /**
             * Details pane
             */
            var $mediaDetailsCheckbox = $('#media_details_collapse');
            $mediaDetailsCheckbox.prop('checked', __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].hide_details_pane || false);
            setTimeout(function () {
                $('.rv-media-details').removeClass('hidden');
            }, 300);
            $mediaDetailsCheckbox.on('change', function (event) {
                event.preventDefault();
                __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].hide_details_pane = $(this).is(':checked');
                __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();
            });

            $(document).on('click', 'button[data-dismiss-modal]', function () {
                var modal = $(this).data('dismiss-modal');
                $(modal).modal('hide');
            });
        }
    }, {
        key: 'handleMediaList',
        value: function handleMediaList() {
            var _self = this;

            /*Ctrl key in Windows*/
            var ctrl_key = false;

            /*Command key in MAC*/
            var meta_key = false;

            /*Shift key*/
            var shift_key = false;

            $(document).on('keyup keydown', function (e) {
                /*User hold ctrl key*/
                ctrl_key = e.ctrlKey;
                /*User hold command key*/
                meta_key = e.metaKey;
                /*User hold shift key*/
                shift_key = e.shiftKey;
            });

            _self.$body.on('click', '.js-media-list-title', function (event) {
                event.preventDefault();
                var $current = $(this);

                if (shift_key) {
                    var firstItem = _.first(__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedItems());
                    if (firstItem) {
                        var firstIndex = firstItem.index_key;
                        var currentIndex = $current.index();
                        $('.rv-media-items li').each(function (index) {
                            if (index > firstIndex && index <= currentIndex) {
                                $(this).find('input[type=checkbox]').prop('checked', true);
                            }
                        });
                    }
                } else {
                    if (!ctrl_key && !meta_key) {
                        $current.closest('.rv-media-items').find('input[type=checkbox]').prop('checked', false);
                    }
                }

                var $lineCheckBox = $current.find('input[type=checkbox]');
                $lineCheckBox.prop('checked', true);
                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].handleDropdown();

                _self.MediaService.getFileDetails($current.data());
            }).on('dblclick', '.js-media-list-title', function (event) {
                event.preventDefault();

                var data = $(this).data();
                if (data.is_folder === true) {

                    // remove keyword search to load file for the current folder
                    __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search = '';
                    $('#rv-media-text-box-search').val('');

                    __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();
                    _self.FolderService.changeFolder(data.id);
                } else {
                    if (!__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].isUseInModal()) {
                        __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].handlePreview();
                    } else if (__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getConfigs().request_params.view_in !== 'trash') {
                        var selectedFiles = __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
                        if (_.size(selectedFiles) > 0) {
                            __WEBPACK_IMPORTED_MODULE_8__integrate__["EditorService"].editorSelectFile(selectedFiles);
                        }
                    }
                }
            }).on('dblclick', '.js-up-one-level', function (event) {
                event.preventDefault();
                var count = $('.rv-media-breadcrumb .breadcrumb li').length;
                $('.rv-media-breadcrumb .breadcrumb li:nth-child(' + (count - 1) + ') a').trigger('click');
            }).on('contextmenu', '.js-context-menu', function (e) {
                if (!$(this).find('input[type=checkbox]').is(':checked')) {
                    $(this).trigger('click');
                }
            }).on('click contextmenu', '.rv-media-items', function (e) {
                if (!_.size(e.target.closest('.js-context-menu'))) {
                    $('.rv-media-items input[type="checkbox"]').prop('checked', false);
                    $('.rv-dropdown-actions').addClass('disabled');
                    _self.MediaService.getFileDetails({
                        icon: 'fa fa-picture-o',
                        nothing_selected: ''
                    });
                }
            });
        }
    }, {
        key: 'changeViewType',
        value: function changeViewType() {
            var _self = this;
            _self.$body.on('click', '.js-rv-media-change-view-type .btn', function (event) {
                event.preventDefault();
                var $current = $(this);
                if ($current.hasClass('active')) {
                    return;
                }
                $current.closest('.js-rv-media-change-view-type').find('.btn').removeClass('active');
                $current.addClass('active');

                __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.view_type = $current.data('type');

                if ($current.data('type') === 'trash') {
                    $(document).find('.js-insert-to-editor').prop('disabled', true);
                } else {
                    $(document).find('.js-insert-to-editor').prop('disabled', false);
                }

                __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();

                _self.MediaService.getMedia(true, true);
            });
            this.bindIntegrateModalEvents();
        }
    }, {
        key: 'changeFilter',
        value: function changeFilter() {
            var _self = this;
            _self.$body.on('click', '.js-rv-media-change-filter', function (event) {
                event.preventDefault();
                if (!__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].isOnAjaxLoading()) {
                    var $current = $(this);
                    var $parent = $current.closest('ul');
                    var data = $current.data();
                    __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params[data.type] = data.value;

                    if (data.type === 'view_in') {
                        __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id = 0;
                        if (data.value === 'trash') {
                            $(document).find('.js-insert-to-editor').prop('disabled', true);
                        } else {
                            $(document).find('.js-insert-to-editor').prop('disabled', false);
                        }
                    }

                    $current.closest('.dropdown').find('.js-rv-media-filter-current').html('(' + $current.html() + ')');

                    __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();
                    __WEBPACK_IMPORTED_MODULE_2__App_Services_MediaService__["a" /* MediaService */].refreshFilter();

                    __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();
                    _self.MediaService.getMedia(true);

                    $parent.find('> li').removeClass('active');
                    $current.closest('li').addClass('active');
                }
            });
        }
    }, {
        key: 'search',
        value: function search() {

            var _self = this;

            var _resetFolder = function _resetFolder() {

                // Once refreshing data
                if (__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id === -1) {

                    if (_typeof(__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search) == undefined || !__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search) {
                        var prevFolderID = __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getPrevFolderID();
                        __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id = prevFolderID != -1 ? prevFolderID : 0;
                    }
                } else {
                    // Keep the prev folder ID
                    __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].setPrevFolderID(__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id);
                }

                // Allow searching all folders
                __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search = $('#rv-media-text-box-search').val(); // re-assign keyword to reset value if this value is storing in local storage
                if (__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search) {
                    __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id = -1;
                } else {
                    // Avoid find all folders and files when leave to another page and get back after
                    if (__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id == -1) {
                        __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id = 0;
                    }
                }
            };

            _resetFolder();

            var _delaySearch = function () {
                var timer = 0;
                return function (callback, ms) {
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms);
                };
            }(),
                textBox = '#rv-media-text-box-search',
                $searchTextbox = $(textBox),
                deleteIcon = '#rv-icon-rm-search-text',
                _searchFn = function _searchFn(keyword) {
                __WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search = keyword;
                _resetFolder();
                __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].storeConfig();
                __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();
                _self.MediaService.getMedia(true);
            };

            // Hide the remove icon once the text is empty
            if (!$searchTextbox.val()) {
                $(deleteIcon).addClass('hidden');
            }

            // remove keyword
            $('.rv-media-search').off('click', deleteIcon).on('click', deleteIcon, function () {
                $searchTextbox.val('');
                _searchFn('');
            });

            $('.rv-media-search .input-search-wrapper').off('keyup', textBox).on('keyup', textBox, function (e) {
                e.preventDefault();
                var $this = $(this);
                var current = $(e.currentTarget);
                var iconRemove = current.closest('.search-input-wrapper').find(deleteIcon);
                if (iconRemove.length > 0) {
                    if (current.val() != '') {
                        if (iconRemove.hasClass('hidden')) {
                            iconRemove.removeClass('hidden');
                        }
                    } else {
                        if (!iconRemove.hasClass('hidden')) {
                            iconRemove.addClass('hidden');
                        }
                    }
                    _delaySearch(function () {
                        _searchFn($this.val());
                    }, 500);
                }
            });

            // Prevent submit form
            _self.$body.on('submit', '.input-search-wrapper', function (event) {
                event.preventDefault();
                return false;
            });
        }
    }, {
        key: 'handleActions',
        value: function handleActions() {
            var _self = this;

            _self.$body.on('click', '.rv-media-actions .js-change-action[data-type="refresh"]', function (event) {
                event.preventDefault();

                __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();

                var ele_options = typeof window.rvMedia.$el !== 'undefined' ? window.rvMedia.$el.data('rv-media') : undefined;
                if (typeof ele_options !== 'undefined' && ele_options.length > 0 && typeof ele_options[0].selected_file_id !== 'undefined') {
                    _self.MediaService.getMedia(true, true);
                } else _self.MediaService.getMedia(true, false);
            }).on('click', '.rv-media-items li.no-items', function (event) {
                event.preventDefault();
                $('.rv-media-header .rv-media-top-header .rv-media-actions .js-dropzone-upload').trigger('click');
            }).on('submit', '.form-add-folder', function (event) {
                event.preventDefault();
                var $input = $(this).find('input[type=text]');
                var folderName = $input.val();
                _self.FolderService.create(folderName);
                $input.val('');
            }).on('click', '.js-change-folder', function (event) {
                event.preventDefault();
                var folderId = !__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.search ? $(this).data('folder') : -1;
                __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();
                _self.FolderService.changeFolder(folderId);
            }).on('click', '.js-files-action', function (event) {
                event.preventDefault();
                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].handleGlobalAction($(this).data('action'), function (res) {
                    __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].resetPagination();
                    _self.MediaService.getMedia(true);
                });
            });
        }
    }, {
        key: 'handleModals',
        value: function handleModals() {
            var _self = this;
            /*Rename files*/
            _self.$body.on('show.bs.modal', '#modal_rename_items', function (event) {
                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].renderRenameItems();
            });
            _self.$body.on('submit', '#modal_rename_items .form-rename', function (event) {
                event.preventDefault();
                var items = [];
                var $form = $(this);

                $('#modal_rename_items .form-control').each(function () {
                    var $current = $(this);
                    var data = $current.closest('.form-group').data();
                    data.name = $current.val();
                    items.push(data);
                });

                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].processAction({
                    action: $form.data('action'),
                    selected: items
                }, function (res) {
                    if (!res.error) {
                        $form.closest('.modal').modal('hide');
                        _self.MediaService.getMedia(true);
                    } else {
                        $('#modal_rename_items .form-group').each(function () {
                            var $current = $(this);
                            if (_.contains(res.data, $current.data('id'))) {
                                $current.addClass('has-error');
                            } else {
                                $current.removeClass('has-error');
                            }
                        });
                    }
                });
            });

            /*Delete files*/
            _self.$body.on('submit', '.form-delete-items', function (event) {
                event.preventDefault();
                var items = [];
                var $form = $(this);

                _.each(__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedItems(), function (value) {
                    items.push({
                        id: value.id,
                        is_folder: value.is_folder
                    });
                });

                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].processAction({
                    action: $form.data('action'),
                    selected: items
                }, function (res) {
                    $form.closest('.modal').modal('hide');
                    if (!res.error) {
                        _self.MediaService.getMedia(true);
                    }
                });
            });

            /*Empty trash*/
            _self.$body.on('submit', '#modal_empty_trash .rv-form', function (event) {
                event.preventDefault();
                var $form = $(this);

                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].processAction({
                    action: $form.data('action')
                }, function (res) {
                    $form.closest('.modal').modal('hide');
                    _self.MediaService.getMedia(true);
                });
            });

            /*Share files*/
            var users = [];
            var $shareOption = $('#share_option');
            var $shareToUsers = $('#share_to_users');
            $shareOption.on('change', function (event) {
                event.preventDefault();
                if ($(this).val() === 'user') {
                    $shareToUsers.closest('.form-group').removeClass('hidden');
                } else {
                    $shareToUsers.closest('.form-group').addClass('hidden');
                }
            }).trigger('change');
            _self.$body.on('show.bs.modal', '#modal_share_items', function (event) {
                $shareOption.val('no_share').trigger('change');
                $shareToUsers.val('');

                var selectedItems = __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedItems();

                if (_.size(selectedItems) !== 1) {

                    var is_public = true;
                    $.each(selectedItems, function (index, el) {
                        if (el.is_public == 0) {
                            is_public = false;
                        }
                    });

                    if (is_public) {
                        $shareOption.val('everyone').trigger('change');
                    } else {
                        $.ajax({
                            url: RV_MEDIA_URL.get_users,
                            type: 'GET',
                            dataType: 'json',
                            success: function success(res) {
                                if (!res.error) {
                                    $shareToUsers.html('');
                                    users = res.data;
                                    _.each(users, function (value) {
                                        var option = '<option value="' + value.id + '">' + value.name + '</option>';
                                        $shareToUsers.append(option);
                                    });
                                } else {
                                    __WEBPACK_IMPORTED_MODULE_3__App_Services_MessageService__["a" /* MessageService */].showMessage('error', res.message, RV_MEDIA_CONFIG.translations.message.error_header);
                                }
                            },
                            error: function error(data) {
                                __WEBPACK_IMPORTED_MODULE_3__App_Services_MessageService__["a" /* MessageService */].handleError(data);
                            }
                        });
                    }
                } else {
                    var selectedItem = _.first(selectedItems);

                    if (selectedItem.is_public) {
                        $shareOption.val('everyone').trigger('change');
                    } else {
                        $.ajax({
                            url: RV_MEDIA_URL.get_shared_users,
                            type: 'GET',
                            data: {
                                share_id: selectedItem.id,
                                is_folder: selectedItem.is_folder
                            },
                            dataType: 'json',
                            success: function success(res) {
                                if (!res.error) {
                                    $shareToUsers.html('');
                                    users = res.data.users;
                                    var totalSelected = 0;
                                    _.each(users, function (value) {
                                        var isSelected = value.is_selected;
                                        if (isSelected) {
                                            totalSelected++;
                                        }
                                        var option = '<option value="' + value.id + '" ' + (isSelected ? 'selected' : '') + '>' + value.name + '</option>';
                                        $shareToUsers.append(option);
                                    });
                                    if (totalSelected > 0) {
                                        $shareOption.val('user').trigger('change');
                                    }
                                } else {
                                    __WEBPACK_IMPORTED_MODULE_3__App_Services_MessageService__["a" /* MessageService */].showMessage('error', res.message, RV_MEDIA_CONFIG.translations.message.error_header);
                                }
                            },
                            error: function error(data) {
                                __WEBPACK_IMPORTED_MODULE_3__App_Services_MessageService__["a" /* MessageService */].handleError(data);
                            }
                        });
                    }
                }
            }).on('submit', '#modal_share_items .rv-form', function (event) {
                event.preventDefault();
                var $form = $(this);

                var items = [];
                _.each(__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedItems(), function (value) {
                    items.push({
                        id: value.id,
                        is_folder: value.is_folder
                    });
                });

                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].processAction({
                    action: $form.data('action'),
                    selected: items,
                    share_option: $shareOption.val(),
                    users: $shareToUsers.val()
                }, function (res) {
                    $form.closest('.modal').modal('hide');
                    _self.MediaService.getMedia(true);
                });
            }).on('submit', '#modal_set_focus_point .rv-form', function (event) {
                event.preventDefault();
                var $form = $(this);

                var items = [];
                var selected_items = __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedItems();
                _.each(selected_items, function (value) {
                    items.push({
                        id: value.id,
                        is_folder: value.is_folder
                    });
                });

                __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].processAction({
                    action: $form.data('action'),
                    selected: items,
                    data_attribute: $('.helper-tool-data-attr').val(),
                    css_bg_position: $('.helper-tool-css3-val').val(),
                    retice_css: $('.helper-tool-reticle-css').val()
                }, function (res) {
                    $form.closest('.modal').modal('hide');
                    _.each(selected_items, function (value) {
                        if (value.id === res.data.id) {
                            $('.js-media-list-title[data-id=' + value.id + ']').data(res.data);
                        }
                    });
                });
            });

            if (__WEBPACK_IMPORTED_MODULE_0__App_Config_MediaConfig__["a" /* MediaConfig */].request_params.view_in === 'trash') {
                $(document).find('.js-insert-to-editor').prop('disabled', true);
            } else {
                $(document).find('.js-insert-to-editor').prop('disabled', false);
            }

            this.bindIntegrateModalEvents();
        }
    }, {
        key: 'checkFileTypeSelect',
        value: function checkFileTypeSelect(selectedFiles) {
            if (typeof window.rvMedia.$el !== 'undefined') {
                var firstItem = _.first(selectedFiles);
                var ele_options = window.rvMedia.$el.data('rv-media');
                if (typeof ele_options !== 'undefined' && typeof ele_options[0] !== 'undefined' && typeof ele_options[0].file_type !== 'undefined' && firstItem !== 'undefined' && firstItem.type !== 'undefined') {
                    if (!ele_options[0].file_type.match(firstItem.type)) {
                        return false;
                    } else {
                        if (typeof ele_options[0].ext_allowed !== 'undefined' && $.isArray(ele_options[0].ext_allowed)) {
                            if ($.inArray(firstItem.mime_type, ele_options[0].ext_allowed) == -1) {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }, {
        key: 'bindIntegrateModalEvents',
        value: function bindIntegrateModalEvents() {
            var $main_modal = $('#rv_media_modal');
            var _self = this;
            $main_modal.off('click', '.js-insert-to-editor').on('click', '.js-insert-to-editor', function (event) {
                event.preventDefault();
                var selectedFiles = __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
                if (_.size(selectedFiles) > 0) {
                    window.rvMedia.options.onSelectFiles(selectedFiles, window.rvMedia.$el);
                    if (_self.checkFileTypeSelect(selectedFiles)) {
                        $main_modal.find('.close').trigger('click');
                    }
                }
            });

            $main_modal.off('dblclick', '.js-media-list-title').on('dblclick', '.js-media-list-title', function (event) {
                event.preventDefault();
                debugger;

                if (__WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getConfigs().request_params.view_in !== 'trash') {
                    var selectedFiles = __WEBPACK_IMPORTED_MODULE_1__App_Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
                    if (_.size(selectedFiles) > 0) {
                        window.rvMedia.options.onSelectFiles(selectedFiles, window.rvMedia.$el);
                        if (_self.checkFileTypeSelect(selectedFiles)) {
                            $main_modal.find('.close').trigger('click');
                        }
                    }
                } else {
                    __WEBPACK_IMPORTED_MODULE_6__App_Services_ActionsService__["a" /* ActionsService */].handlePreview();
                }
            });
        }
    }, {
        key: 'scrollGetMore',


        //scroll get more media
        value: function scrollGetMore() {
            var _self = this;
            $('.rv-media-main .rv-media-items').bind('DOMMouseScroll mousewheel', function (e) {
                if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
                    var $load_more = false;
                    if ($(this).closest('.media-modal').length > 0) {
                        $load_more = $(this).scrollTop() + $(this).innerHeight() / 2 >= $(this)[0].scrollHeight - 450;
                    } else {
                        $load_more = $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 150;
                    }

                    if ($load_more) {
                        if (typeof RV_MEDIA_CONFIG.pagination != 'undefined' && RV_MEDIA_CONFIG.pagination.has_more) {
                            _self.MediaService.getMedia(false, false, true);
                        } else {
                            return;
                        }
                    }
                }
            });
        }
    }], [{
        key: 'setupSecurity',
        value: function setupSecurity() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }
    }]);

    return MediaManagement;
}();

$(document).ready(function () {
    window.rvMedia = window.rvMedia || {};

    MediaManagement.setupSecurity();
    new MediaManagement().init();

    $('#modal_rename_items').off('keypress', 'input').on('keypress', 'input', function (e) {
        var value = e.key;
        var pattern = /^[A-Za-z0-9 '.-]+$/;
        if (!pattern.test(value)) {
            return false;
        }
    });

    $('#modal_rename_items').off('click', '.media-rename-custom-action').on('click', '.media-rename-custom-action', function (e) {
        var current = $(e.currentTarget);
        var listInput = current.closest('form').find('input');
        if (listInput.length > 0) {
            var checkValue = true;
            $.each(listInput, function (index, item) {
                item = $(item);
                if (item.val() == '' || $.trim(item.val()) == '') {
                    checkValue = false;
                    item.addClass('name-required');
                    item.focus();
                } else {
                    item.removeClass('name-required');
                }
            });
            if (!checkValue) {
                return false;
            }
        }
    });
});

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContextMenuService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ActionsService__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var ContextMenuService = function () {
    function ContextMenuService() {
        _classCallCheck(this, ContextMenuService);
    }

    _createClass(ContextMenuService, null, [{
        key: 'initContext',
        value: function initContext() {
            if (jQuery().contextMenu) {
                $.contextMenu({
                    selector: '.js-context-menu[data-context="file"]',
                    build: function build($element, event) {
                        return {
                            items: ContextMenuService._fileContextMenu()
                        };
                    }
                });

                $.contextMenu({
                    selector: '.js-context-menu[data-context="folder"]',
                    build: function build($element, event) {
                        return {
                            items: ContextMenuService._folderContextMenu()
                        };
                    }
                });
            }
        }
    }, {
        key: '_fileContextMenu',
        value: function _fileContextMenu() {
            var items = {
                preview: {
                    name: 'Preview',
                    icon: function icon(opt, $itemElement, itemKey, item) {
                        $itemElement.html('<i class="fa fa-eye" aria-hidden="true"></i> ' + item.name);

                        return 'context-menu-icon-updated';
                    },
                    callback: function callback(key, opt) {
                        __WEBPACK_IMPORTED_MODULE_0__ActionsService__["a" /* ActionsService */].handlePreview();
                    }
                },
                set_focus_point: {
                    name: "Set focus point",
                    icon: function icon(opt, $itemElement, itemKey, item) {
                        $itemElement.html('<i class="fa fa-dot-circle-o" aria-hidden="true"></i> ' + item.name);

                        return 'context-menu-icon-updated';
                    },
                    callback: function callback(key, opt) {
                        $('.js-files-action[data-action="set_focus_point"]').trigger('click');
                    }
                }
            };

            _.each(__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getConfigs().actions_list, function (actionGroup, key) {
                _.each(actionGroup, function (value) {
                    items[value.action] = {
                        name: value.name,
                        icon: function icon(opt, $itemElement, itemKey, item) {
                            $itemElement.html('<i class="' + value.icon + '" aria-hidden="true"></i> ' + (RV_MEDIA_CONFIG.translations.actions_list[key][value.action] || item.name));

                            return 'context-menu-icon-updated';
                        },
                        callback: function callback(key, opt) {
                            $('.js-files-action[data-action="' + value.action + '"]').trigger('click');
                        }
                    };
                });
            });

            var except = [];

            switch (__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_in) {
                case 'my_media':
                    except = ['remove_favorite', 'delete', 'restore', 'remove_share'];
                    break;
                case 'public':
                    except = ['remove_favorite', 'delete', 'restore', 'remove_share'];
                    break;
                case 'shared':
                    except = ['make_copy', 'remove_favorite', 'delete', 'restore', 'remove_share'];
                    break;
                case 'shared_with_me':
                    except = ['share', 'remove_favorite', 'delete', 'restore', 'make_copy'];
                    break;
                case 'recent':
                    except = ['remove_favorite', 'delete', 'restore', 'remove_share', 'make_copy'];
                    break;
                case 'favorites':
                    except = ['favorite', 'delete', 'restore', 'remove_share', 'make_copy'];
                    break;
                case 'trash':
                    items = {
                        preview: items.preview,
                        rename: items.rename,
                        download: items.download,
                        delete: items.delete,
                        restore: items.restore
                    };
                    break;
            }

            _.each(except, function (value) {
                items[value] = undefined;
            });

            if (__WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedItems().length > 1) {
                items.set_focus_point = undefined;
            }

            var hasFolderSelected = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedFolder().length > 0;

            if (hasFolderSelected) {
                items.preview = undefined;
                items.set_focus_point = undefined;
                items.copy_link = undefined;

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.create')) {
                    items.make_copy = undefined;
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.edit')) {
                    items.rename = undefined;
                    items.share = undefined;
                    items.remove_share = undefined;
                    items.un_share = undefined;
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.trash')) {
                    items.trash = undefined;
                    items.restore = undefined;
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'folders.delete')) {
                    items.delete = undefined;
                }
            }

            var selectedFiles = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getSelectedFiles();
            if (selectedFiles.length > 0 && selectedFiles[0].type !== 'image') {
                items.set_focus_point = undefined;
            }

            if (selectedFiles.length > 0) {
                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.create')) {
                    items.make_copy = undefined;
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.edit')) {
                    items.rename = undefined;
                    items.set_focus_point = undefined;
                    items.share = undefined;
                    items.remove_share = undefined;
                    items.un_share = undefined;
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.trash')) {
                    items.trash = undefined;
                    items.restore = undefined;
                }

                if (!_.contains(RV_MEDIA_CONFIG.permissions, 'files.delete')) {
                    items.delete = undefined;
                }
            }

            var can_preview = false;
            _.each(selectedFiles, function (value) {
                if (_.contains(['image', 'youtube', 'pdf', 'text', 'video'], value.type)) {
                    can_preview = true;
                }
            });

            if (!can_preview) {
                items.preview = undefined;
            }

            if (RV_MEDIA_CONFIG.mode === 'simple') {
                items.share = undefined;
                items.un_share = undefined;
                items.remove_share = undefined;
            }

            return items;
        }
    }, {
        key: '_folderContextMenu',
        value: function _folderContextMenu() {
            var items = ContextMenuService._fileContextMenu();

            items.preview = undefined;
            items.set_focus_point = undefined;
            items.copy_link = undefined;

            return items;
        }
    }, {
        key: 'destroyContext',
        value: function destroyContext() {
            if (jQuery().contextMenu) {
                $.contextMenu('destroy');
            }
        }
    }]);

    return ContextMenuService;
}();

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MediaDetails; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var MediaDetails = function () {
    function MediaDetails() {
        _classCallCheck(this, MediaDetails);

        this.$detailsWrapper = $('.rv-media-main .rv-media-details');

        this.descriptionItemTemplate = '<div class="rv-media-name"><p>__title__</p>__url__</div>';

        this.onlyFields = ['name', 'full_url', 'size', 'mime_type', 'created_at', 'updated_at', 'nothing_selected'];

        this.externalTypes = ['youtube', 'vimeo', 'metacafe', 'dailymotion', 'vine', 'instagram'];
    }

    _createClass(MediaDetails, [{
        key: 'renderData',
        value: function renderData(data) {
            var _self = this;
            var thumb = data.type === 'image' ? '<img src="' + data.full_url + '" alt="' + data.name + '">' : data.mime_type === 'youtube' ? '<img src="' + data.options.thumb + '" alt="' + data.name + '">' : '<i class="' + data.icon + '"></i>';
            var description = '';
            var useClipboard = false;
            _.forEach(data, function (val, index) {
                if (_.contains(_self.onlyFields, index)) {
                    if (!_.contains(_self.externalTypes, data.type) || _.contains(_self.externalTypes, data.type) && !_.contains(['size', 'mime_type'], index)) {
                        description += _self.descriptionItemTemplate.replace(/__title__/gi, RV_MEDIA_CONFIG.translations[index]).replace(/__url__/gi, val ? index === 'full_url' ? '<div class="input-group"><input id="file_details_url" type="text" value="' + val + '" class="form-control"><span class="input-group-btn"><button class="btn btn-default js-btn-copy-to-clipboard" type="button" data-clipboard-target="#file_details_url" title="Copied" data-trigger="click"><img class="clippy" src="' + __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].asset('/vendor/media/images/clippy.svg') + '" width="13" alt="Copy to clipboard"></button></span></div>' : '<span title="' + val + '">' + val + '</span>' : '');
                        if (index === 'full_url') {
                            useClipboard = true;
                        }
                    }
                }
            });
            _self.$detailsWrapper.find('.rv-media-thumbnail').html(thumb);
            _self.$detailsWrapper.find('.rv-media-description').html(description);
            if (useClipboard) {
                var clipboard = new Clipboard('.js-btn-copy-to-clipboard');
                $('.js-btn-copy-to-clipboard').tooltip().on('mouseleave', function (event) {
                    $(this).tooltip('hide');
                });
            }
        }
    }]);

    return MediaDetails;
}();

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FolderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Views_MediaList__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Config_MediaConfig__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__MediaService__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }







var FolderService = function () {
    function FolderService() {
        _classCallCheck(this, FolderService);

        this.MediaList = new __WEBPACK_IMPORTED_MODULE_0__Views_MediaList__["a" /* MediaList */]();
        this.MediaService = new __WEBPACK_IMPORTED_MODULE_2__MediaService__["a" /* MediaService */]();

        $('body').on('shown.bs.modal', '#modal_add_folder', function () {
            $(this).find('.form-add-folder input[type=text]').focus();
        });
    }

    _createClass(FolderService, [{
        key: 'create',
        value: function create(folderName) {
            var _self = this;
            $.ajax({
                url: RV_MEDIA_URL.create_folder,
                type: 'POST',
                data: {
                    parent_id: __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__["a" /* Helpers */].getRequestParams().folder_id,
                    name: folderName
                },
                dataType: 'json',
                beforeSend: function beforeSend() {
                    __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__["a" /* Helpers */].showAjaxLoading();
                },
                success: function success(res) {
                    if (res.error) {
                        __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__["a" /* MessageService */].showMessage('error', res.message, RV_MEDIA_CONFIG.translations.message.error_header);
                    } else {
                        __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__["a" /* MessageService */].showMessage('success', res.message, RV_MEDIA_CONFIG.translations.message.success_header);
                        __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__["a" /* Helpers */].resetPagination();
                        _self.MediaService.getMedia(true);
                        FolderService.closeModal();
                    }
                },
                complete: function complete(data) {
                    __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__["a" /* Helpers */].hideAjaxLoading();
                },
                error: function error(data) {
                    __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__["a" /* MessageService */].handleError(data);
                }
            });
        }
    }, {
        key: 'changeFolder',
        value: function changeFolder(folderId) {
            __WEBPACK_IMPORTED_MODULE_1__Config_MediaConfig__["a" /* MediaConfig */].request_params.folder_id = folderId;
            __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__["a" /* Helpers */].setPrevFolderID(folderId);
            __WEBPACK_IMPORTED_MODULE_4__Helpers_Helpers__["a" /* Helpers */].storeConfig();
            this.MediaService.getMedia(true);
        }
    }], [{
        key: 'closeModal',
        value: function closeModal() {
            $('#modal_add_folder').modal('hide');
        }
    }]);

    return FolderService;
}();

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Services_MediaService__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var UploadService = function () {
    function UploadService() {
        _classCallCheck(this, UploadService);

        this.$body = $('body');

        this.dropZone = null;

        this.uploadUrl = RV_MEDIA_URL.upload_file;

        this.uploadProgressBox = $('.rv-upload-progress');

        this.uploadProgressContainer = $('.rv-upload-progress .rv-upload-progress-table');

        this.uploadProgressTemplate = $('#rv_media_upload_progress_item').html();

        this.totalQueued = 1;

        this.MediaService = new __WEBPACK_IMPORTED_MODULE_0__Services_MediaService__["a" /* MediaService */]();

        this.totalError = 0;
    }

    _createClass(UploadService, [{
        key: 'init',
        value: function init() {
            if (_.contains(RV_MEDIA_CONFIG.permissions, 'files.create') && $('.rv-media-items').length > 0) {
                this.setupDropZone();
            }
            this.handleEvents();
        }
    }, {
        key: 'setupDropZone',
        value: function setupDropZone() {
            var _self = this;
            _self.dropZone = new Dropzone(document.querySelector('.rv-media-items'), {
                url: _self.uploadUrl,
                thumbnailWidth: false,
                thumbnailHeight: false,
                parallelUploads: 1,
                autoQueue: true,
                clickable: '.js-dropzone-upload',
                previewTemplate: false,
                previewsContainer: false,
                uploadMultiple: true,
                sending: function sending(file, xhr, formData) {
                    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                    formData.append('folder_id', __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams().folder_id);
                    formData.append('view_in', __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].getRequestParams().view_in);
                }
            });

            _self.dropZone.on('addedfile', function (file) {
                file.index = _self.totalQueued;
                _self.totalQueued++;
            });

            _self.dropZone.on('sending', function (file) {
                _self.initProgress(file.name, file.size);
            });

            _self.dropZone.on('success', function (file) {});

            _self.dropZone.on('complete', function (file) {
                _self.changeProgressStatus(file);
            });

            _self.dropZone.on('queuecomplete', function () {
                __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].resetPagination();
                _self.MediaService.getMedia(true);
                if (_self.totalError === 0) {
                    setTimeout(function () {
                        $('.rv-upload-progress .close-pane').trigger('click');
                    }, 5000);
                }
            });
        }
    }, {
        key: 'handleEvents',
        value: function handleEvents() {
            var _self = this;
            /**
             * Close upload progress pane
             */
            _self.$body.on('click', '.rv-upload-progress .close-pane', function (event) {
                event.preventDefault();
                $('.rv-upload-progress').addClass('hide-the-pane');
                _self.totalError = 0;
                setTimeout(function () {
                    $('.rv-upload-progress li').remove();
                    _self.totalQueued = 1;
                }, 300);
            });
        }
    }, {
        key: 'initProgress',
        value: function initProgress($fileName, $fileSize) {
            var template = this.uploadProgressTemplate.replace(/__fileName__/gi, $fileName).replace(/__fileSize__/gi, UploadService.formatFileSize($fileSize)).replace(/__status__/gi, 'warning').replace(/__message__/gi, 'Uploading');
            this.uploadProgressContainer.append(template);
            this.uploadProgressBox.removeClass('hide-the-pane');
            this.uploadProgressBox.find('.panel-body').animate({ scrollTop: this.uploadProgressContainer.height() }, 150);
        }
    }, {
        key: 'changeProgressStatus',
        value: function changeProgressStatus(file) {
            var _self = this;
            var $progressLine = _self.uploadProgressContainer.find('li:nth-child(' + file.index + ')');
            var $label = $progressLine.find('.label');
            $label.removeClass('label-success label-danger label-warning');

            var response = __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].jsonDecode(file.xhr.responseText || '', {});

            _self.totalError = _self.totalError + (response.error === true || file.status === 'error' ? 1 : 0);

            $label.addClass(response.error === true || file.status === 'error' ? 'label-danger' : 'label-success');
            $label.html(response.error === true || file.status === 'error' ? 'Error' : 'Uploaded');
            if (file.status === 'error') {
                if (file.xhr.status === 422) {
                    var error_html = '';
                    $.each(response, function (key, item) {
                        error_html += '<span class="text-danger">' + item + '</span><br>';
                    });
                    $progressLine.find('.file-error').html(error_html);
                } else if (file.xhr.status === 500) {
                    $progressLine.find('.file-error').html('<span class="text-danger">' + file.xhr.statusText + '</span>');
                }
            } else if (response.error) {
                $progressLine.find('.file-error').html('<span class="text-danger">' + response.message + '</span>');
            } else {
                __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].addToRecent(response.data.id);
                __WEBPACK_IMPORTED_MODULE_1__Helpers_Helpers__["a" /* Helpers */].setSelectedFile(response.data.id);
            }
        }
    }], [{
        key: 'formatFileSize',
        value: function formatFileSize(bytes) {
            var si = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var thresh = si ? 1000 : 1024;
            if (Math.abs(bytes) < thresh) {
                return bytes + ' B';
            }
            var units = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            var u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while (Math.abs(bytes) >= thresh && u < units.length - 1);
            return bytes.toFixed(1) + ' ' + units[u];
        }
    }]);

    return UploadService;
}();

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalServices; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Youtube__ = __webpack_require__(14);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var ExternalServices = function ExternalServices() {
    _classCallCheck(this, ExternalServices);

    new __WEBPACK_IMPORTED_MODULE_0__Youtube__["a" /* Youtube */]();
};

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Youtube; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Config_ExternalServiceConfig__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_MediaService__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__ = __webpack_require__(2);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }






var Youtube = function () {
    function Youtube() {
        _classCallCheck(this, Youtube);

        this.MediaService = new __WEBPACK_IMPORTED_MODULE_2__Services_MediaService__["a" /* MediaService */]();

        this.$body = $('body');

        this.$modal = $('#modal_add_from_youtube');

        var _self = this;

        try {
            this.setMessage(RV_MEDIA_CONFIG.translations.add_from.youtube.original_msg);
        } catch (err) {}

        this.$modal.on('hidden.bs.modal', function () {
            try {
                _self.setMessage(RV_MEDIA_CONFIG.translations.add_from.youtube.original_msg);
            } catch (err) {}
        });

        this.$body.on('click', '#modal_add_from_youtube .rv-btn-add-youtube-url', function (event) {
            event.preventDefault();

            _self.checkYouTubeVideo($(this).closest('#modal_add_from_youtube').find('.rv-youtube-url'));
        });
    }

    _createClass(Youtube, [{
        key: 'setMessage',
        value: function setMessage(msg) {
            this.$modal.find('.modal-notice').html(msg);
        }
    }, {
        key: 'checkYouTubeVideo',
        value: function checkYouTubeVideo($input) {
            var _self = this;
            if (!Youtube.validateYouTubeLink($input.val()) || !__WEBPACK_IMPORTED_MODULE_1__Config_ExternalServiceConfig__["a" /* ExternalServiceConfig */].youtube.api_key) {
                if (__WEBPACK_IMPORTED_MODULE_1__Config_ExternalServiceConfig__["a" /* ExternalServiceConfig */].youtube.api_key) {
                    try {
                        _self.setMessage(RV_MEDIA_CONFIG.translations.add_from.youtube.invalid_url_msg);
                    } catch (err) {}
                } else {
                    try {
                        _self.setMessage(RV_MEDIA_CONFIG.translations.add_from.youtube.no_api_key_msg);
                    } catch (err) {}
                }
            } else {
                var youtubeId = Youtube.getYouTubeId($input.val());
                var requestUrl = 'https://www.googleapis.com/youtube/v3/videos?id=' + youtubeId;
                var isPlaylist = _self.$modal.find('.custom-checkbox input[type="checkbox"]').is(':checked');

                if (isPlaylist) {
                    youtubeId = Youtube.getYoutubePlaylistId($input.val());
                    requestUrl = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=' + youtubeId;
                }

                $.ajax({
                    url: requestUrl + '&key=' + __WEBPACK_IMPORTED_MODULE_1__Config_ExternalServiceConfig__["a" /* ExternalServiceConfig */].youtube.api_key + '&part=snippet',
                    type: "GET",
                    success: function success(data) {
                        if (isPlaylist) {
                            playlistVideoCallback(data, $input.val());
                        } else {
                            singleVideoCallback(data, $input.val());
                        }
                    },
                    error: function error(data) {
                        try {
                            _self.setMessage(RV_MEDIA_CONFIG.translations.add_from.youtube.error_msg);
                        } catch (err) {}
                    }
                });
            }

            function singleVideoCallback(data, url) {
                $.ajax({
                    url: RV_MEDIA_URL.add_external_service,
                    type: "POST",
                    dataType: 'json',
                    data: {
                        type: 'youtube',
                        name: data.items[0].snippet.title,
                        folder_id: __WEBPACK_IMPORTED_MODULE_0__Helpers_Helpers__["a" /* Helpers */].getRequestParams().folder_id,
                        url: url,
                        options: {
                            thumb: 'https://img.youtube.com/vi/' + data.items[0].id + '/maxresdefault.jpg'
                        }
                    },
                    success: function success(res) {
                        if (res.error) {
                            __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__["a" /* MessageService */].showMessage('error', res.message, RV_MEDIA_CONFIG.translations.message.error_header);
                        } else {
                            __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__["a" /* MessageService */].showMessage('success', res.message, RV_MEDIA_CONFIG.translations.message.success_header);
                            _self.MediaService.getMedia(true);
                        }
                    },
                    error: function error(data) {
                        __WEBPACK_IMPORTED_MODULE_3__Services_MessageService__["a" /* MessageService */].handleError(data);
                    }
                });
                _self.$modal.modal('hide');
            }

            function playlistVideoCallback(data, url) {
                _self.$modal.modal('hide');
            }
        }
    }], [{
        key: 'validateYouTubeLink',
        value: function validateYouTubeLink(url) {
            var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
            return url.match(p) ? RegExp.$1 : false;
        }
    }, {
        key: 'getYouTubeId',
        value: function getYouTubeId(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length === 11) {
                return match[2];
            }
            return null;
        }
    }, {
        key: 'getYoutubePlaylistId',
        value: function getYoutubePlaylistId(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?list=|\&list=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match) {
                return match[2];
            }
            return null;
        }
    }]);

    return Youtube;
}();

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExternalServiceConfig; });
var ExternalServiceConfig = {
    youtube: {
        api_key: "AIzaSyCV4fmfdgsValGNR3sc-0W3cbpEZ8uOd60"
    }
};



/***/ }),
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMGQxMzcyYmJiMDY0NzlkYmVjN2UiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvSGVscGVycy9IZWxwZXJzLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL0NvbmZpZy9NZWRpYUNvbmZpZy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9TZXJ2aWNlcy9NZXNzYWdlU2VydmljZS5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9TZXJ2aWNlcy9NZWRpYVNlcnZpY2UuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvU2VydmljZXMvQWN0aW9uc1NlcnZpY2UuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9pbnRlZ3JhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvVmlld3MvTWVkaWFMaXN0LmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvbWVkaWEuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvU2VydmljZXMvQ29udGV4dE1lbnVTZXJ2aWNlLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL1ZpZXdzL01lZGlhRGV0YWlscy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9TZXJ2aWNlcy9Gb2xkZXJTZXJ2aWNlLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL1NlcnZpY2VzL1VwbG9hZFNlcnZpY2UuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvRXh0ZXJuYWxzL0V4dGVybmFsU2VydmljZXMuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvRXh0ZXJuYWxzL1lvdXR1YmUuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvQ29uZmlnL0V4dGVybmFsU2VydmljZUNvbmZpZy5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL3Nhc3MvbWVkaWEuc2Nzcz8wODljIl0sIm5hbWVzIjpbIkhlbHBlcnMiLCJwYXJhbU5hbWUiLCJ1cmwiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInNlYXJjaCIsInJlUGFyYW0iLCJSZWdFeHAiLCJtYXRjaCIsImxlbmd0aCIsInN1YnN0cmluZyIsImJhc2VVcmwiLCJSVl9NRURJQV9VUkwiLCJiYXNlX3VybCIsInN1YnN0ciIsIiRlbGVtZW50IiwiJCIsImFkZENsYXNzIiwiYXBwZW5kIiwiaHRtbCIsInJlbW92ZUNsYXNzIiwiZmluZCIsInJlbW92ZSIsImhhc0NsYXNzIiwib2JqZWN0IiwiSlNPTiIsInN0cmluZ2lmeSIsImpzb25TdHJpbmciLCJkZWZhdWx0VmFsdWUiLCJyZXN1bHQiLCJwYXJzZUpTT04iLCJlcnIiLCJydk1lZGlhIiwib3B0aW9ucyIsIm9wZW5faW4iLCJleHRlbmQiLCJNZWRpYUNvbmZpZyIsInJlcXVlc3RfcGFyYW1zIiwiJGZpbGVfaWQiLCJzZWxlY3RlZF9maWxlX2lkIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsImpzb25FbmNvZGUiLCJmb2xkZXJJRCIsImdldEl0ZW0iLCJpZCIsIkFycmF5IiwiXyIsImVhY2giLCJ2YWx1ZSIsIlJlY2VudEl0ZW1zIiwicHVzaCIsIml0ZW1zIiwiJGJveCIsImRhdGEiLCJpbmRleF9rZXkiLCJpbmRleCIsInNlbGVjdGVkIiwiY2xvc2VzdCIsImdldFVybFBhcmFtIiwiUlZfTUVESUFfQ09ORklHIiwicGFnaW5hdGlvbiIsInBhZ2VkIiwicG9zdHNfcGVyX3BhZ2UiLCJpbl9wcm9jZXNzX2dldF9tZWRpYSIsImhhc19tb3JlIiwiZm9sZGVyX2lkIiwiZGVmYXVsdENvbmZpZyIsImFwcF9rZXkiLCJ2aWV3X3R5cGUiLCJmaWx0ZXIiLCJ2aWV3X2luIiwic29ydF9ieSIsImhpZGVfZGV0YWlsc19wYW5lIiwiaWNvbnMiLCJmb2xkZXIiLCJhY3Rpb25zX2xpc3QiLCJiYXNpYyIsImljb24iLCJuYW1lIiwiYWN0aW9uIiwib3JkZXIiLCJjbGFzcyIsImZpbGUiLCJ1c2VyIiwib3RoZXIiLCJkZW5pZWRfZG93bmxvYWQiLCJNZXNzYWdlU2VydmljZSIsInR5cGUiLCJtZXNzYWdlIiwibWVzc2FnZUhlYWRlciIsInRvYXN0ciIsImNsb3NlQnV0dG9uIiwicHJvZ3Jlc3NCYXIiLCJwb3NpdGlvbkNsYXNzIiwib25jbGljayIsInNob3dEdXJhdGlvbiIsImhpZGVEdXJhdGlvbiIsInRpbWVPdXQiLCJleHRlbmRlZFRpbWVPdXQiLCJzaG93RWFzaW5nIiwiaGlkZUVhc2luZyIsInNob3dNZXRob2QiLCJoaWRlTWV0aG9kIiwicmVzcG9uc2VKU09OIiwic2hvd01lc3NhZ2UiLCJ0cmFuc2xhdGlvbnMiLCJlcnJvcl9oZWFkZXIiLCJlbCIsImtleSIsIml0ZW0iLCJzdGF0dXNUZXh0IiwiTWVkaWFTZXJ2aWNlIiwiTWVkaWFMaXN0IiwiTWVkaWFEZXRhaWxzIiwiYnJlYWRjcnVtYlRlbXBsYXRlIiwicmVsb2FkIiwiaXNfcG9wdXAiLCJsb2FkX21vcmVfZmlsZSIsIl9zZWxmIiwiZ2V0RmlsZURldGFpbHMiLCJub3RoaW5nX3NlbGVjdGVkIiwicGFyYW1zIiwiZ2V0UmVxdWVzdFBhcmFtcyIsInJlY2VudF9pdGVtcyIsInVuZGVmaW5lZCIsIm9uU2VsZWN0RmlsZXMiLCJhamF4IiwiZ2V0X21lZGlhIiwiZGF0YVR5cGUiLCJiZWZvcmVTZW5kIiwic2hvd0FqYXhMb2FkaW5nIiwic3VjY2VzcyIsInJlcyIsInJlbmRlckRhdGEiLCJmZXRjaFF1b3RhIiwicmVuZGVyQnJlYWRjcnVtYnMiLCJicmVhZGNydW1icyIsInJlZnJlc2hGaWx0ZXIiLCJBY3Rpb25zU2VydmljZSIsInJlbmRlckFjdGlvbnMiLCJmaWxlcyIsImNvbXBsZXRlIiwiaGlkZUFqYXhMb2FkaW5nIiwiZXJyb3IiLCJoYW5kbGVFcnJvciIsImdldF9xdW90YSIsInVzZWQiLCJxdW90YSIsImNzcyIsIndpZHRoIiwicGVyY2VudCIsImJyZWFkY3J1bWJJdGVtcyIsIiRicmVhZGNydW1iQ29udGFpbmVyIiwidGVtcGxhdGUiLCJyZXBsYWNlIiwiYXR0ciIsInNpemUiLCIkcnZNZWRpYUNvbnRhaW5lciIsIiRlbXB0eV90cmFzaF9idG4iLCJnZXRJdGVtcyIsIkNvbnRleHRNZW51U2VydmljZSIsImRlc3Ryb3lDb250ZXh0IiwiaW5pdENvbnRleHQiLCJnZXRTZWxlY3RlZEl0ZW1zIiwiZ2V0U2VsZWN0ZWRGaWxlcyIsImNvbnRhaW5zIiwic3JjIiwiZmFuY3lib3giLCJvcGVuIiwic3RvcmVSZWNlbnRJdGVtcyIsImxpbmtzIiwiaXNFbXB0eSIsImZ1bGxfdXJsIiwiJGNsaXBib2FyZFRlbXAiLCJDbGlwYm9hcmQiLCJ0ZXh0IiwidHJpZ2dlciIsImNsaXBib2FyZCIsInN1Y2Nlc3NfaGVhZGVyIiwiY2FsbGJhY2siLCJpc19mb2xkZXIiLCJmb2N1cyIsIm1vZGFsIiwiaGFuZGxlQ29weUxpbmsiLCJoYW5kbGVQcmV2aWV3IiwidmFsIiwicmVtb3ZlQXR0ciIsImRhdGFfYXR0cmlidXRlIiwiY3NzX2JnX3Bvc2l0aW9uIiwicmV0aWNlX2NzcyIsInByb3AiLCJkb3dubG9hZExpbmsiLCJkb3dubG9hZCIsImNvdW50IiwiZ2V0Q29uZmlncyIsIm1pbWVfdHlwZSIsInByb2Nlc3NBY3Rpb24iLCJnbG9iYWxfYWN0aW9ucyIsInJlc2V0UGFnaW5hdGlvbiIsIlZJRVciLCIkaXRlbXNXcmFwcGVyIiwiZW1wdHkiLCIkaXRlbSIsImhhc0ZvbGRlclNlbGVjdGVkIiwiZ2V0U2VsZWN0ZWRGb2xkZXIiLCJBQ1RJT05fVEVNUExBVEUiLCJpbml0aWFsaXplZF9pdGVtIiwiJGRyb3Bkb3duQWN0aW9ucyIsImFjdGlvbnNMaXN0IiwicmVqZWN0IiwicGVybWlzc2lvbnMiLCJzZWxlY3RlZEZpbGVzIiwiY2FuX3ByZXZpZXciLCJtb2RlIiwiaXNfYnJlYWsiLCJFZGl0b3JTZXJ2aWNlIiwiaXNfY2tlZGl0b3IiLCJvcGVuZXIiLCJmaXJzdEl0ZW0iLCJmaXJzdCIsIkNLRURJVE9SIiwidG9vbHMiLCJjYWxsRnVuY3Rpb24iLCJjbG9zZSIsInNlbGVjdG9yIiwiJGJvZHkiLCJkZWZhdWx0T3B0aW9ucyIsIm11bHRpcGxlIiwiJGVsIiwiY2xpY2tDYWxsYmFjayIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCIkY3VycmVudCIsInN0b3JlQ29uZmlnIiwiZWxlX29wdGlvbnMiLCJsb2FkIiwicG9wdXAiLCJhbGVydCIsImRvY3VtZW50Iiwib24iLCJSdk1lZGlhU3RhbmRBbG9uZSIsIm9mZiIsImVkaXRvclNlbGVjdEZpbGUiLCJmbiIsIiRzZWxlY3RvciIsImdyb3VwIiwibGlzdCIsInRpbGVzIiwiJGdyb3VwQ29udGFpbmVyIiwidHJhbnNsYXRpb25zX3ZpZXdfaWNvbiIsInRyYW5zbGF0aW9uc192aWV3X3RpdGxlIiwidHJhbnNsYXRpb25zX3ZpZXdfbWVzc2FnZSIsIm5vX2l0ZW0iLCJ0aXRsZSIsIiRyZXN1bHQiLCJmb2xkZXJzIiwiZm9yRWFjaCIsImNyZWF0ZWRfYXQiLCJ0aHVtYiIsIkRhdGUiLCJnZXRUaW1lIiwiaGFuZGxlRHJvcGRvd24iLCJNZWRpYU1hbmFnZW1lbnQiLCJVcGxvYWRTZXJ2aWNlIiwiRm9sZGVyU2VydmljZSIsInNldHVwTGF5b3V0IiwiaGFuZGxlTWVkaWFMaXN0IiwiY2hhbmdlVmlld1R5cGUiLCJjaGFuZ2VGaWx0ZXIiLCJoYW5kbGVBY3Rpb25zIiwiaW5pdCIsImhhbmRsZU1vZGFscyIsInNjcm9sbEdldE1vcmUiLCIkY3VycmVudF9maWx0ZXIiLCIkY3VycmVudF92aWV3X2luIiwiaXNVc2VJbk1vZGFsIiwiJG1lZGlhRGV0YWlsc0NoZWNrYm94Iiwic2V0VGltZW91dCIsImlzIiwiY3RybF9rZXkiLCJtZXRhX2tleSIsInNoaWZ0X2tleSIsImUiLCJjdHJsS2V5IiwibWV0YUtleSIsInNoaWZ0S2V5IiwiZmlyc3RJbmRleCIsImN1cnJlbnRJbmRleCIsIiRsaW5lQ2hlY2tCb3giLCJjaGFuZ2VGb2xkZXIiLCJ0YXJnZXQiLCJnZXRNZWRpYSIsImJpbmRJbnRlZ3JhdGVNb2RhbEV2ZW50cyIsImlzT25BamF4TG9hZGluZyIsIiRwYXJlbnQiLCJfcmVzZXRGb2xkZXIiLCJwcmV2Rm9sZGVySUQiLCJnZXRQcmV2Rm9sZGVySUQiLCJzZXRQcmV2Rm9sZGVySUQiLCJfZGVsYXlTZWFyY2giLCJ0aW1lciIsIm1zIiwiY2xlYXJUaW1lb3V0IiwidGV4dEJveCIsIiRzZWFyY2hUZXh0Ym94IiwiZGVsZXRlSWNvbiIsIl9zZWFyY2hGbiIsImtleXdvcmQiLCIkdGhpcyIsImN1cnJlbnQiLCJjdXJyZW50VGFyZ2V0IiwiaWNvblJlbW92ZSIsIiRpbnB1dCIsImZvbGRlck5hbWUiLCJjcmVhdGUiLCJmb2xkZXJJZCIsImhhbmRsZUdsb2JhbEFjdGlvbiIsInJlbmRlclJlbmFtZUl0ZW1zIiwiJGZvcm0iLCJ1c2VycyIsIiRzaGFyZU9wdGlvbiIsIiRzaGFyZVRvVXNlcnMiLCJzZWxlY3RlZEl0ZW1zIiwiaXNfcHVibGljIiwiZ2V0X3VzZXJzIiwib3B0aW9uIiwic2VsZWN0ZWRJdGVtIiwiZ2V0X3NoYXJlZF91c2VycyIsInNoYXJlX2lkIiwidG90YWxTZWxlY3RlZCIsImlzU2VsZWN0ZWQiLCJpc19zZWxlY3RlZCIsInNoYXJlX29wdGlvbiIsInNlbGVjdGVkX2l0ZW1zIiwiZmlsZV90eXBlIiwiZXh0X2FsbG93ZWQiLCJpc0FycmF5IiwiaW5BcnJheSIsIiRtYWluX21vZGFsIiwiY2hlY2tGaWxlVHlwZVNlbGVjdCIsImJpbmQiLCJvcmlnaW5hbEV2ZW50IiwiZGV0YWlsIiwid2hlZWxEZWx0YSIsIiRsb2FkX21vcmUiLCJzY3JvbGxUb3AiLCJpbm5lckhlaWdodCIsInNjcm9sbEhlaWdodCIsImFqYXhTZXR1cCIsImhlYWRlcnMiLCJyZWFkeSIsInNldHVwU2VjdXJpdHkiLCJwYXR0ZXJuIiwidGVzdCIsImxpc3RJbnB1dCIsImNoZWNrVmFsdWUiLCJ0cmltIiwialF1ZXJ5IiwiY29udGV4dE1lbnUiLCJidWlsZCIsIl9maWxlQ29udGV4dE1lbnUiLCJfZm9sZGVyQ29udGV4dE1lbnUiLCJwcmV2aWV3Iiwib3B0IiwiJGl0ZW1FbGVtZW50IiwiaXRlbUtleSIsInNldF9mb2N1c19wb2ludCIsImFjdGlvbkdyb3VwIiwiZXhjZXB0IiwicmVuYW1lIiwiZGVsZXRlIiwicmVzdG9yZSIsImNvcHlfbGluayIsIm1ha2VfY29weSIsInNoYXJlIiwicmVtb3ZlX3NoYXJlIiwidW5fc2hhcmUiLCJ0cmFzaCIsIiRkZXRhaWxzV3JhcHBlciIsImRlc2NyaXB0aW9uSXRlbVRlbXBsYXRlIiwib25seUZpZWxkcyIsImV4dGVybmFsVHlwZXMiLCJkZXNjcmlwdGlvbiIsInVzZUNsaXBib2FyZCIsImFzc2V0IiwidG9vbHRpcCIsImNyZWF0ZV9mb2xkZXIiLCJwYXJlbnRfaWQiLCJjbG9zZU1vZGFsIiwiZHJvcFpvbmUiLCJ1cGxvYWRVcmwiLCJ1cGxvYWRfZmlsZSIsInVwbG9hZFByb2dyZXNzQm94IiwidXBsb2FkUHJvZ3Jlc3NDb250YWluZXIiLCJ1cGxvYWRQcm9ncmVzc1RlbXBsYXRlIiwidG90YWxRdWV1ZWQiLCJ0b3RhbEVycm9yIiwic2V0dXBEcm9wWm9uZSIsImhhbmRsZUV2ZW50cyIsIkRyb3B6b25lIiwicXVlcnlTZWxlY3RvciIsInRodW1ibmFpbFdpZHRoIiwidGh1bWJuYWlsSGVpZ2h0IiwicGFyYWxsZWxVcGxvYWRzIiwiYXV0b1F1ZXVlIiwiY2xpY2thYmxlIiwicHJldmlld1RlbXBsYXRlIiwicHJldmlld3NDb250YWluZXIiLCJ1cGxvYWRNdWx0aXBsZSIsInNlbmRpbmciLCJ4aHIiLCJmb3JtRGF0YSIsImluaXRQcm9ncmVzcyIsImNoYW5nZVByb2dyZXNzU3RhdHVzIiwiJGZpbGVOYW1lIiwiJGZpbGVTaXplIiwiZm9ybWF0RmlsZVNpemUiLCJhbmltYXRlIiwiaGVpZ2h0IiwiJHByb2dyZXNzTGluZSIsIiRsYWJlbCIsInJlc3BvbnNlIiwianNvbkRlY29kZSIsInJlc3BvbnNlVGV4dCIsInN0YXR1cyIsImVycm9yX2h0bWwiLCJhZGRUb1JlY2VudCIsInNldFNlbGVjdGVkRmlsZSIsImJ5dGVzIiwic2kiLCJ0aHJlc2giLCJNYXRoIiwiYWJzIiwidW5pdHMiLCJ1IiwidG9GaXhlZCIsIkV4dGVybmFsU2VydmljZXMiLCJZb3V0dWJlIiwiJG1vZGFsIiwic2V0TWVzc2FnZSIsImFkZF9mcm9tIiwieW91dHViZSIsIm9yaWdpbmFsX21zZyIsImNoZWNrWW91VHViZVZpZGVvIiwibXNnIiwidmFsaWRhdGVZb3VUdWJlTGluayIsIkV4dGVybmFsU2VydmljZUNvbmZpZyIsImFwaV9rZXkiLCJpbnZhbGlkX3VybF9tc2ciLCJub19hcGlfa2V5X21zZyIsInlvdXR1YmVJZCIsImdldFlvdVR1YmVJZCIsInJlcXVlc3RVcmwiLCJpc1BsYXlsaXN0IiwiZ2V0WW91dHViZVBsYXlsaXN0SWQiLCJwbGF5bGlzdFZpZGVvQ2FsbGJhY2siLCJzaW5nbGVWaWRlb0NhbGxiYWNrIiwiZXJyb3JfbXNnIiwiYWRkX2V4dGVybmFsX3NlcnZpY2UiLCJzbmlwcGV0IiwicCIsIiQxIiwicmVnRXhwIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDN0RBOztBQUVBLElBQWFBLE9BQWI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBLG9DQUN1QkMsU0FEdkIsRUFDOEM7QUFBQSxnQkFBWkMsR0FBWSx1RUFBTixJQUFNOztBQUN0QyxnQkFBSSxDQUFDQSxHQUFMLEVBQVU7QUFDTkEsc0JBQU1DLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQXRCO0FBQ0g7QUFDRCxnQkFBSUMsVUFBVSxJQUFJQyxNQUFKLENBQVcsZ0JBQWdCTixTQUFoQixHQUE0QixVQUF2QyxFQUFtRCxHQUFuRCxDQUFkO0FBQ0EsZ0JBQUlPLFFBQVFOLElBQUlNLEtBQUosQ0FBVUYsT0FBVixDQUFaO0FBQ0EsbUJBQVNFLFNBQVNBLE1BQU1DLE1BQU4sR0FBZSxDQUExQixHQUFnQ0QsTUFBTSxDQUFOLENBQWhDLEdBQTJDLElBQWxEO0FBQ0g7QUFSTDtBQUFBO0FBQUEsOEJBVWlCTixHQVZqQixFQVVzQjtBQUNkLGdCQUFJQSxJQUFJUSxTQUFKLENBQWMsQ0FBZCxFQUFpQixDQUFqQixNQUF3QixJQUF4QixJQUFnQ1IsSUFBSVEsU0FBSixDQUFjLENBQWQsRUFBaUIsQ0FBakIsTUFBd0IsU0FBeEQsSUFBcUVSLElBQUlRLFNBQUosQ0FBYyxDQUFkLEVBQWlCLENBQWpCLE1BQXdCLFVBQWpHLEVBQTZHO0FBQ3pHLHVCQUFPUixHQUFQO0FBQ0g7O0FBRUQsZ0JBQUlTLFVBQVVDLGFBQWFDLFFBQWIsQ0FBc0JDLE1BQXRCLENBQTZCLENBQUMsQ0FBOUIsRUFBaUMsQ0FBakMsTUFBd0MsR0FBeEMsR0FBOENGLGFBQWFDLFFBQWIsR0FBd0IsR0FBdEUsR0FBNEVELGFBQWFDLFFBQXZHOztBQUVBLGdCQUFJWCxJQUFJUSxTQUFKLENBQWMsQ0FBZCxFQUFpQixDQUFqQixNQUF3QixHQUE1QixFQUFpQztBQUM3Qix1QkFBT0MsVUFBVVQsSUFBSVEsU0FBSixDQUFjLENBQWQsQ0FBakI7QUFDSDtBQUNELG1CQUFPQyxVQUFVVCxHQUFqQjtBQUNIO0FBckJMO0FBQUE7QUFBQSwwQ0F1QjJEO0FBQUEsZ0JBQWhDYSxRQUFnQyx1RUFBckJDLEVBQUUsZ0JBQUYsQ0FBcUI7O0FBQ25ERCxxQkFDS0UsUUFETCxDQUNjLFlBRGQsRUFFS0MsTUFGTCxDQUVZRixFQUFFLG1CQUFGLEVBQXVCRyxJQUF2QixFQUZaO0FBR0g7QUEzQkw7QUFBQTtBQUFBLDBDQTZCMkQ7QUFBQSxnQkFBaENKLFFBQWdDLHVFQUFyQkMsRUFBRSxnQkFBRixDQUFxQjs7QUFDbkRELHFCQUNLSyxXQURMLENBQ2lCLFlBRGpCLEVBRUtDLElBRkwsQ0FFVSxrQkFGVixFQUU4QkMsTUFGOUI7QUFHSDtBQWpDTDtBQUFBO0FBQUEsMENBbUM0RDtBQUFBLGdCQUFqQ1AsUUFBaUMsdUVBQXRCQyxFQUFFLGlCQUFGLENBQXNCOztBQUNwRCxtQkFBT0QsU0FBU1EsUUFBVCxDQUFrQixZQUFsQixDQUFQO0FBQ0g7QUFyQ0w7QUFBQTtBQUFBLG1DQXVDc0JDLE1BdkN0QixFQXVDOEI7QUFDdEI7O0FBQ0EsZ0JBQUksT0FBT0EsTUFBUCxLQUFrQixXQUF0QixFQUFtQztBQUMvQkEseUJBQVMsSUFBVDtBQUNIO0FBQ0QsbUJBQU9DLEtBQUtDLFNBQUwsQ0FBZUYsTUFBZixDQUFQO0FBQ0g7QUE3Q0w7QUFBQTtBQUFBLG1DQStDc0JHLFVBL0N0QixFQStDa0NDLFlBL0NsQyxFQStDZ0Q7QUFDeEM7O0FBQ0EsZ0JBQUksQ0FBQ0QsVUFBTCxFQUFpQjtBQUNiLHVCQUFPQyxZQUFQO0FBQ0g7QUFDRCxnQkFBSSxPQUFPRCxVQUFQLEtBQXNCLFFBQTFCLEVBQW9DO0FBQ2hDLG9CQUFJRSxlQUFKO0FBQ0Esb0JBQUk7QUFDQUEsNkJBQVNiLEVBQUVjLFNBQUYsQ0FBWUgsVUFBWixDQUFUO0FBQ0gsaUJBRkQsQ0FFRSxPQUFPSSxHQUFQLEVBQVk7QUFDVkYsNkJBQVNELFlBQVQ7QUFDSDtBQUNELHVCQUFPQyxNQUFQO0FBQ0g7QUFDRCxtQkFBT0YsVUFBUDtBQUNIO0FBOURMO0FBQUE7QUFBQSwyQ0FnRThCO0FBQ3RCLGdCQUFJeEIsT0FBTzZCLE9BQVAsQ0FBZUMsT0FBZixJQUEwQjlCLE9BQU82QixPQUFQLENBQWVDLE9BQWYsQ0FBdUJDLE9BQXZCLEtBQW1DLE9BQWpFLEVBQTBFO0FBQ3RFLHVCQUFPbEIsRUFBRW1CLE1BQUYsQ0FBUyxJQUFULEVBQWUsd0VBQUFDLENBQVlDLGNBQTNCLEVBQTJDbEMsT0FBTzZCLE9BQVAsQ0FBZUMsT0FBZixJQUEwQixFQUFyRSxDQUFQO0FBQ0g7QUFDRCxtQkFBTyx3RUFBQUcsQ0FBWUMsY0FBbkI7QUFDSDtBQXJFTDtBQUFBO0FBQUEsd0NBdUUyQkMsUUF2RTNCLEVBdUVxQztBQUM3QixnQkFBSSxPQUFPbkMsT0FBTzZCLE9BQVAsQ0FBZUMsT0FBdEIsS0FBa0MsV0FBdEMsRUFBbUQ7QUFDL0M5Qix1QkFBTzZCLE9BQVAsQ0FBZUMsT0FBZixDQUF1Qk0sZ0JBQXZCLEdBQTBDRCxRQUExQztBQUNILGFBRkQsTUFFTztBQUNIRixnQkFBQSx3RUFBQUEsQ0FBWUMsY0FBWixDQUEyQkUsZ0JBQTNCLEdBQThDRCxRQUE5QztBQUNIO0FBQ0o7QUE3RUw7QUFBQTtBQUFBLHFDQStFd0I7QUFDaEIsbUJBQU8sd0VBQVA7QUFDSDtBQWpGTDtBQUFBO0FBQUEsc0NBbUZ5QjtBQUNqQkUseUJBQWFDLE9BQWIsQ0FBcUIsYUFBckIsRUFBb0N6QyxRQUFRMEMsVUFBUixDQUFtQix3RUFBbkIsQ0FBcEM7QUFDSDtBQXJGTDtBQUFBO0FBQUEsMkNBdUY4QjtBQUN0QkYseUJBQWFDLE9BQWIsQ0FBcUIsYUFBckIsRUFBb0N6QyxRQUFRMEMsVUFBUixDQUFtQix3RUFBbkIsQ0FBcEM7QUFDSDs7QUFFRDs7Ozs7QUEzRko7QUFBQTtBQUFBLHdDQStGMkJDLFFBL0YzQixFQStGcUM7QUFDN0JILHlCQUFhQyxPQUFiLENBQXFCLFlBQXJCLEVBQW1DRSxRQUFuQztBQUNIO0FBakdMO0FBQUE7QUFBQSwwQ0FtRzZCO0FBQ3JCLG1CQUFPSCxhQUFhSSxPQUFiLENBQXFCLFlBQXJCLENBQVA7QUFDSDtBQXJHTDtBQUFBO0FBQUEsb0NBdUd1QkMsRUF2R3ZCLEVBdUcyQjtBQUNuQixnQkFBSUEsY0FBY0MsS0FBbEIsRUFBeUI7QUFDckJDLGtCQUFFQyxJQUFGLENBQU9ILEVBQVAsRUFBVyxVQUFVSSxLQUFWLEVBQWlCO0FBQ3hCQyxvQkFBQSx3RUFBQUEsQ0FBWUMsSUFBWixDQUFpQkYsS0FBakI7QUFDSCxpQkFGRDtBQUdILGFBSkQsTUFJTztBQUNIQyxnQkFBQSx3RUFBQUEsQ0FBWUMsSUFBWixDQUFpQk4sRUFBakI7QUFDSDtBQUNKO0FBL0dMO0FBQUE7QUFBQSxtQ0FpSHNCO0FBQ2QsZ0JBQUlPLFFBQVEsRUFBWjtBQUNBcEMsY0FBRSxzQkFBRixFQUEwQmdDLElBQTFCLENBQStCLFlBQVk7QUFDdkMsb0JBQUlLLE9BQU9yQyxFQUFFLElBQUYsQ0FBWDtBQUNBLG9CQUFJc0MsT0FBT0QsS0FBS0MsSUFBTCxNQUFlLEVBQTFCO0FBQ0FBLHFCQUFLQyxTQUFMLEdBQWlCRixLQUFLRyxLQUFMLEVBQWpCO0FBQ0FKLHNCQUFNRCxJQUFOLENBQVdHLElBQVg7QUFDSCxhQUxEO0FBTUEsbUJBQU9GLEtBQVA7QUFDSDtBQTFITDtBQUFBO0FBQUEsMkNBNEg4QjtBQUN0QixnQkFBSUssV0FBVyxFQUFmO0FBQ0F6QyxjQUFFLG1EQUFGLEVBQXVEZ0MsSUFBdkQsQ0FBNEQsWUFBWTtBQUNwRSxvQkFBSUssT0FBT3JDLEVBQUUsSUFBRixFQUFRMEMsT0FBUixDQUFnQixzQkFBaEIsQ0FBWDtBQUNBLG9CQUFJSixPQUFPRCxLQUFLQyxJQUFMLE1BQWUsRUFBMUI7QUFDQUEscUJBQUtDLFNBQUwsR0FBaUJGLEtBQUtHLEtBQUwsRUFBakI7QUFDQUMseUJBQVNOLElBQVQsQ0FBY0csSUFBZDtBQUNILGFBTEQ7QUFNQSxtQkFBT0csUUFBUDtBQUNIO0FBcklMO0FBQUE7QUFBQSwyQ0F1SThCO0FBQ3RCLGdCQUFJQSxXQUFXLEVBQWY7QUFDQXpDLGNBQUUsc0VBQUYsRUFBMEVnQyxJQUExRSxDQUErRSxZQUFZO0FBQ3ZGLG9CQUFJSyxPQUFPckMsRUFBRSxJQUFGLEVBQVEwQyxPQUFSLENBQWdCLHNCQUFoQixDQUFYO0FBQ0Esb0JBQUlKLE9BQU9ELEtBQUtDLElBQUwsTUFBZSxFQUExQjtBQUNBQSxxQkFBS0MsU0FBTCxHQUFpQkYsS0FBS0csS0FBTCxFQUFqQjtBQUNBQyx5QkFBU04sSUFBVCxDQUFjRyxJQUFkO0FBQ0gsYUFMRDtBQU1BLG1CQUFPRyxRQUFQO0FBQ0g7QUFoSkw7QUFBQTtBQUFBLDRDQWtKK0I7QUFDdkIsZ0JBQUlBLFdBQVcsRUFBZjtBQUNBekMsY0FBRSx3RUFBRixFQUE0RWdDLElBQTVFLENBQWlGLFlBQVk7QUFDekYsb0JBQUlLLE9BQU9yQyxFQUFFLElBQUYsRUFBUTBDLE9BQVIsQ0FBZ0Isc0JBQWhCLENBQVg7QUFDQSxvQkFBSUosT0FBT0QsS0FBS0MsSUFBTCxNQUFlLEVBQTFCO0FBQ0FBLHFCQUFLQyxTQUFMLEdBQWlCRixLQUFLRyxLQUFMLEVBQWpCO0FBQ0FDLHlCQUFTTixJQUFULENBQWNHLElBQWQ7QUFDSCxhQUxEO0FBTUEsbUJBQU9HLFFBQVA7QUFDSDtBQTNKTDtBQUFBO0FBQUEsdUNBNkowQjtBQUNsQixtQkFBT3pELFFBQVEyRCxXQUFSLENBQW9CLGNBQXBCLE1BQXdDLGNBQXhDLElBQTJEeEQsT0FBTzZCLE9BQVAsSUFBa0I3QixPQUFPNkIsT0FBUCxDQUFlQyxPQUFqQyxJQUE0QzlCLE9BQU82QixPQUFQLENBQWVDLE9BQWYsQ0FBdUJDLE9BQXZCLEtBQW1DLE9BQWpKO0FBQ0g7QUEvSkw7QUFBQTtBQUFBLDBDQWlLNkI7QUFDckIwQiw0QkFBZ0JDLFVBQWhCLEdBQTZCLEVBQUVDLE9BQU8sQ0FBVCxFQUFZQyxnQkFBZ0IsRUFBNUIsRUFBZ0NDLHNCQUFzQixLQUF0RCxFQUE2REMsVUFBVSxJQUF2RSxFQUE2RUMsV0FBVyxDQUF4RixFQUE3QjtBQUNIO0FBbktMOztBQUFBO0FBQUEsSTs7Ozs7Ozs7QUNGQTtBQUFBLElBQUk5QixjQUFjcEIsRUFBRWMsU0FBRixDQUFZVSxhQUFhSSxPQUFiLENBQXFCLGFBQXJCLENBQVosS0FBb0QsRUFBdEU7O0FBRUEsSUFBSXVCLGdCQUFnQjtBQUNoQkMsYUFBUyw2Q0FETztBQUVoQi9CLG9CQUFnQjtBQUNaZ0MsbUJBQVcsT0FEQztBQUVaQyxnQkFBUSxZQUZJO0FBR1pDLGlCQUFTLFVBSEc7QUFJWmxFLGdCQUFRLEVBSkk7QUFLWm1FLGlCQUFTLGlCQUxHO0FBTVpOLG1CQUFXO0FBTkMsS0FGQTtBQVVoQk8sdUJBQW1CLEtBVkg7QUFXaEJDLFdBQU87QUFDSEMsZ0JBQVE7QUFETCxLQVhTO0FBY2hCQyxrQkFBYztBQUNWQyxlQUFPLENBQ0g7QUFDSUMsa0JBQU0sV0FEVjtBQUVJQyxrQkFBTSxTQUZWO0FBR0lDLG9CQUFRLFNBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBREcsQ0FERztBQVVWQyxjQUFNLENBQ0Y7QUFDSUwsa0JBQU0sWUFEVjtBQUVJQyxrQkFBTSxXQUZWO0FBR0lDLG9CQUFRLFdBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBREUsRUFRRjtBQUNJSixrQkFBTSxjQURWO0FBRUlDLGtCQUFNLFFBRlY7QUFHSUMsb0JBQVEsUUFIWjtBQUlJQyxtQkFBTyxDQUpYO0FBS0lDLG1CQUFPO0FBTFgsU0FSRSxFQWVGO0FBQ0lKLGtCQUFNLFlBRFY7QUFFSUMsa0JBQU0sYUFGVjtBQUdJQyxvQkFBUSxXQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQWZFLEVBc0JGO0FBQ0lKLGtCQUFNLG9CQURWO0FBRUlDLGtCQUFNLGlCQUZWO0FBR0lDLG9CQUFRLGlCQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQXRCRSxDQVZJO0FBd0NWRSxjQUFNLENBQ0Y7QUFDSU4sa0JBQU0saUJBRFY7QUFFSUMsa0JBQU0sT0FGVjtBQUdJQyxvQkFBUSxPQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQURFLEVBUUY7QUFDSUosa0JBQU0sV0FEVjtBQUVJQyxrQkFBTSxjQUZWO0FBR0lDLG9CQUFRLGNBSFo7QUFJSUMsbUJBQU8sQ0FKWDtBQUtJQyxtQkFBTztBQUxYLFNBUkUsRUFlRjtBQUNJSixrQkFBTSxZQURWO0FBRUlDLGtCQUFNLFVBRlY7QUFHSUMsb0JBQVEsVUFIWjtBQUlJQyxtQkFBTyxDQUpYO0FBS0lDLG1CQUFPO0FBTFgsU0FmRSxFQXNCRjtBQUNJSixrQkFBTSxjQURWO0FBRUlDLGtCQUFNLGlCQUZWO0FBR0lDLG9CQUFRLGlCQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQXRCRSxDQXhDSTtBQXNFVkcsZUFBTyxDQUNIO0FBQ0lQLGtCQUFNLGdCQURWO0FBRUlDLGtCQUFNLFVBRlY7QUFHSUMsb0JBQVEsVUFIWjtBQUlJQyxtQkFBTyxDQUpYO0FBS0lDLG1CQUFPO0FBTFgsU0FERyxFQVFIO0FBQ0lKLGtCQUFNLGFBRFY7QUFFSUMsa0JBQU0sZUFGVjtBQUdJQyxvQkFBUSxPQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQVJHLEVBZUg7QUFDSUosa0JBQU0sY0FEVjtBQUVJQyxrQkFBTSxvQkFGVjtBQUdJQyxvQkFBUSxRQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQWZHLEVBc0JIO0FBQ0lKLGtCQUFNLFlBRFY7QUFFSUMsa0JBQU0sU0FGVjtBQUdJQyxvQkFBUSxTQUhaO0FBSUlDLG1CQUFPLENBSlg7QUFLSUMsbUJBQU87QUFMWCxTQXRCRztBQXRFRyxLQWRFO0FBbUhoQkkscUJBQWlCLENBQ2IsU0FEYTtBQW5IRCxDQUFwQjs7QUF3SEEsSUFBSSxDQUFDbEQsWUFBWWdDLE9BQWIsSUFBd0JoQyxZQUFZZ0MsT0FBWixLQUF3QkQsY0FBY0MsT0FBbEUsRUFBMkU7QUFDdkVoQyxrQkFBYytCLGFBQWQ7QUFDSDs7QUFFRCxJQUFJakIsY0FBY2xDLEVBQUVjLFNBQUYsQ0FBWVUsYUFBYUksT0FBYixDQUFxQixhQUFyQixDQUFaLEtBQW9ELEVBQXRFOzs7Ozs7Ozs7Ozs7OztBQzlIQSxJQUFhMkMsY0FBYjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsb0NBQ3VCQyxJQUR2QixFQUM2QkMsT0FEN0IsRUFDc0NDLGFBRHRDLEVBQ3FEO0FBQzdDQyxtQkFBTzFELE9BQVAsR0FBaUI7QUFDYjJELDZCQUFhLElBREE7QUFFYkMsNkJBQWEsSUFGQTtBQUdiQywrQkFBZSxvQkFIRjtBQUliQyx5QkFBUyxJQUpJO0FBS2JDLDhCQUFjLElBTEQ7QUFNYkMsOEJBQWMsSUFORDtBQU9iQyx5QkFBUyxLQVBJO0FBUWJDLGlDQUFpQixJQVJKO0FBU2JDLDRCQUFZLE9BVEM7QUFVYkMsNEJBQVksUUFWQztBQVdiQyw0QkFBWSxRQVhDO0FBWWJDLDRCQUFZO0FBWkMsYUFBakI7QUFjQVosbUJBQU9ILElBQVAsRUFBYUMsT0FBYixFQUFzQkMsYUFBdEI7QUFDSDtBQWpCTDtBQUFBO0FBQUEsb0NBbUJ1QnBDLElBbkJ2QixFQW1CNkI7QUFDckIsZ0JBQUksT0FBUUEsS0FBS2tELFlBQWIsS0FBK0IsV0FBbkMsRUFBZ0Q7QUFDNUMsb0JBQUksT0FBUWxELEtBQUtrRCxZQUFMLENBQWtCZixPQUExQixLQUF1QyxXQUEzQyxFQUF3RDtBQUNwREYsbUNBQWVrQixXQUFmLENBQTJCLE9BQTNCLEVBQW9DbkQsS0FBS2tELFlBQUwsQ0FBa0JmLE9BQXRELEVBQStEN0IsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJqQixPQUE3QixDQUFxQ2tCLFlBQXBHO0FBQ0gsaUJBRkQsTUFFTztBQUNIM0Ysc0JBQUVnQyxJQUFGLENBQU9NLEtBQUtrRCxZQUFaLEVBQTBCLFVBQVVoRCxLQUFWLEVBQWlCb0QsRUFBakIsRUFBcUI7QUFDM0M1RiwwQkFBRWdDLElBQUYsQ0FBTzRELEVBQVAsRUFBVyxVQUFVQyxHQUFWLEVBQWVDLElBQWYsRUFBcUI7QUFDNUJ2QiwyQ0FBZWtCLFdBQWYsQ0FBMkIsT0FBM0IsRUFBb0NLLElBQXBDLEVBQTBDbEQsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJqQixPQUE3QixDQUFxQ2tCLFlBQS9FO0FBQ0gseUJBRkQ7QUFHSCxxQkFKRDtBQUtIO0FBQ0osYUFWRCxNQVVPO0FBQ0hwQiwrQkFBZWtCLFdBQWYsQ0FBMkIsT0FBM0IsRUFBb0NuRCxLQUFLeUQsVUFBekMsRUFBcURuRCxnQkFBZ0I4QyxZQUFoQixDQUE2QmpCLE9BQTdCLENBQXFDa0IsWUFBMUY7QUFDSDtBQUNKO0FBakNMOztBQUFBO0FBQUEsSTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQWFLLFlBQWI7QUFDSSw0QkFBYztBQUFBOztBQUNWLGFBQUtDLFNBQUwsR0FBaUIsSUFBSSxtRUFBSixFQUFqQjtBQUNBLGFBQUtDLFlBQUwsR0FBb0IsSUFBSSx5RUFBSixFQUFwQjtBQUNBLGFBQUtDLGtCQUFMLEdBQTBCbkcsRUFBRSwyQkFBRixFQUErQkcsSUFBL0IsRUFBMUI7QUFDSDs7QUFMTDtBQUFBO0FBQUEsbUNBT3VFO0FBQUEsZ0JBQTFEaUcsTUFBMEQsdUVBQWpELEtBQWlEO0FBQUEsZ0JBQTFDQyxRQUEwQyx1RUFBL0IsS0FBK0I7QUFBQSxnQkFBeEJDLGNBQXdCLHVFQUFQLEtBQU87O0FBQy9ELGdCQUFHLE9BQU8xRCxnQkFBZ0JDLFVBQXZCLElBQXFDLFdBQXhDLEVBQXFEO0FBQ2pELG9CQUFJRCxnQkFBZ0JDLFVBQWhCLENBQTJCRyxvQkFBL0IsRUFBcUQ7QUFDakQ7QUFDSCxpQkFGRCxNQUVPO0FBQ0hKLG9DQUFnQkMsVUFBaEIsQ0FBMkJHLG9CQUEzQixHQUFrRCxJQUFsRDtBQUNIO0FBQ0o7O0FBRUQsZ0JBQUl1RCxRQUFRLElBQVo7O0FBRUFBLGtCQUFNQyxjQUFOLENBQXFCO0FBQ2pCMUMsc0JBQU0saUJBRFc7QUFFakIyQyxrQ0FBa0I7QUFGRCxhQUFyQjs7QUFLQSxnQkFBSUMsU0FBUyxpRUFBQTFILENBQVEySCxnQkFBUixFQUFiOztBQUVBLGdCQUFJRCxPQUFPbkQsT0FBUCxLQUFtQixRQUF2QixFQUFpQztBQUM3Qm1ELHVCQUFPRSxZQUFQLEdBQXNCLHdFQUF0QjtBQUNIOztBQUVELGdCQUFJUCxhQUFhLElBQWpCLEVBQXVCO0FBQ25CSyx1QkFBT0wsUUFBUCxHQUFrQixJQUFsQjtBQUNILGFBRkQsTUFFTTtBQUNGSyx1QkFBT0wsUUFBUCxHQUFrQlEsU0FBbEI7QUFDSDs7QUFFREgsbUJBQU9JLGFBQVAsR0FBdUJELFNBQXZCOztBQUVBLGdCQUFJLE9BQU9ILE9BQU9ySCxNQUFkLElBQXdCLFdBQXhCLElBQXVDcUgsT0FBT3JILE1BQVAsSUFBaUIsRUFBeEQsSUFBOEQsT0FBT3FILE9BQU9uRixnQkFBZCxJQUFrQyxXQUFwRyxFQUFpSDtBQUM3R21GLHVCQUFPbkYsZ0JBQVAsR0FBMEJzRixTQUExQjtBQUNIOztBQUVESCxtQkFBT0osY0FBUCxHQUF3QkEsY0FBeEI7QUFDQSxnQkFBSSxPQUFPMUQsZ0JBQWdCQyxVQUF2QixJQUFxQyxXQUF6QyxFQUFzRDtBQUNsRDZELHVCQUFPNUQsS0FBUCxHQUFlRixnQkFBZ0JDLFVBQWhCLENBQTJCQyxLQUExQztBQUNBNEQsdUJBQU8zRCxjQUFQLEdBQXdCSCxnQkFBZ0JDLFVBQWhCLENBQTJCRSxjQUFuRDtBQUNIO0FBQ0QvQyxjQUFFK0csSUFBRixDQUFPO0FBQ0g3SCxxQkFBS1UsYUFBYW9ILFNBRGY7QUFFSHhDLHNCQUFNLEtBRkg7QUFHSGxDLHNCQUFNb0UsTUFISDtBQUlITywwQkFBVSxNQUpQO0FBS0hDLDRCQUFZLHNCQUFZO0FBQ3BCbEksb0JBQUEsaUVBQUFBLENBQVFtSSxlQUFSO0FBQ0gsaUJBUEU7QUFRSEMseUJBQVMsaUJBQVVDLEdBQVYsRUFBZTtBQUNwQmQsMEJBQU1OLFNBQU4sQ0FBZ0JxQixVQUFoQixDQUEyQkQsSUFBSS9FLElBQS9CLEVBQXFDOEQsTUFBckMsRUFBNkNFLGNBQTdDO0FBQ0FDLDBCQUFNZ0IsVUFBTjtBQUNBaEIsMEJBQU1pQixpQkFBTixDQUF3QkgsSUFBSS9FLElBQUosQ0FBU21GLFdBQWpDO0FBQ0F6QixpQ0FBYTBCLGFBQWI7QUFDQUMsb0JBQUEsZ0ZBQUFBLENBQWVDLGFBQWY7O0FBRUEsd0JBQUksT0FBT2hGLGdCQUFnQkMsVUFBdkIsSUFBcUMsV0FBekMsRUFBc0Q7QUFDbEQsNEJBQUksT0FBT0QsZ0JBQWdCQyxVQUFoQixDQUEyQkMsS0FBbEMsSUFBMkMsV0FBL0MsRUFBNEQ7QUFDeERGLDRDQUFnQkMsVUFBaEIsQ0FBMkJDLEtBQTNCLElBQW9DLENBQXBDO0FBQ0g7O0FBRUQsNEJBQUksT0FBT0YsZ0JBQWdCQyxVQUFoQixDQUEyQkcsb0JBQWxDLElBQTBELFdBQTlELEVBQTJFO0FBQ3ZFSiw0Q0FBZ0JDLFVBQWhCLENBQTJCRyxvQkFBM0IsR0FBa0QsS0FBbEQ7QUFDSDs7QUFFRCw0QkFBSSxPQUFPSixnQkFBZ0JDLFVBQWhCLENBQTJCRSxjQUFsQyxJQUFvRCxXQUFwRCxJQUFtRXNFLElBQUkvRSxJQUFKLENBQVN1RixLQUFULENBQWVwSSxNQUFmLEdBQXdCbUQsZ0JBQWdCQyxVQUFoQixDQUEyQkUsY0FBdEgsSUFBd0ksT0FBT0gsZ0JBQWdCQyxVQUFoQixDQUEyQkksUUFBbEMsSUFBOEMsV0FBMUwsRUFBdU07QUFDbk1MLDRDQUFnQkMsVUFBaEIsQ0FBMkJJLFFBQTNCLEdBQXNDLEtBQXRDO0FBQ0g7QUFDSjtBQUNKLGlCQTVCRTtBQTZCSDZFLDBCQUFVLGtCQUFVeEYsSUFBVixFQUFnQjtBQUN0QnRELG9CQUFBLGlFQUFBQSxDQUFRK0ksZUFBUjtBQUNILGlCQS9CRTtBQWdDSEMsdUJBQU8sZUFBVTFGLElBQVYsRUFBZ0I7QUFDbkJpQyxvQkFBQSxnRkFBQUEsQ0FBZTBELFdBQWYsQ0FBMkIzRixJQUEzQjtBQUNIO0FBbENFLGFBQVA7QUFvQ0g7QUFsRkw7QUFBQTtBQUFBLHVDQW9GbUJBLElBcEZuQixFQW9GeUI7QUFDakIsaUJBQUs0RCxZQUFMLENBQWtCb0IsVUFBbEIsQ0FBNkJoRixJQUE3QjtBQUNIO0FBdEZMO0FBQUE7QUFBQSxxQ0F3RmlCO0FBQ1R0QyxjQUFFK0csSUFBRixDQUFPO0FBQ0g3SCxxQkFBS1UsYUFBYXNJLFNBRGY7QUFFSDFELHNCQUFNLEtBRkg7QUFHSHlDLDBCQUFVLE1BSFA7QUFJSEMsNEJBQVksc0JBQVksQ0FFdkIsQ0FORTtBQU9IRSx5QkFBUyxpQkFBVUMsR0FBVixFQUFlO0FBQ3BCLHdCQUFJL0UsT0FBTytFLElBQUkvRSxJQUFmOztBQUVBdEMsc0JBQUUsNkNBQUYsRUFBaURHLElBQWpELENBQXNEbUMsS0FBSzZGLElBQUwsR0FBWSxLQUFaLEdBQW9CN0YsS0FBSzhGLEtBQS9FO0FBQ0FwSSxzQkFBRSxzQ0FBRixFQUEwQ3FJLEdBQTFDLENBQThDO0FBQzFDQywrQkFBT2hHLEtBQUtpRyxPQUFMLEdBQWU7QUFEb0IscUJBQTlDO0FBR0gsaUJBZEU7QUFlSFAsdUJBQU8sZUFBVTFGLElBQVYsRUFBZ0I7QUFDbkJpQyxvQkFBQSxnRkFBQUEsQ0FBZTBELFdBQWYsQ0FBMkIzRixJQUEzQjtBQUNIO0FBakJFLGFBQVA7QUFtQkg7QUE1R0w7QUFBQTtBQUFBLDBDQThHc0JrRyxlQTlHdEIsRUE4R3VDO0FBQy9CLGdCQUFJakMsUUFBUSxJQUFaO0FBQ0EsZ0JBQUlrQyx1QkFBdUJ6SSxFQUFFLGtDQUFGLENBQTNCO0FBQ0F5SSxpQ0FBcUJwSSxJQUFyQixDQUEwQixJQUExQixFQUFnQ0MsTUFBaEM7O0FBRUF5QixjQUFFQyxJQUFGLENBQU93RyxlQUFQLEVBQXdCLFVBQVV2RyxLQUFWLEVBQWlCTyxLQUFqQixFQUF3QjtBQUM1QyxvQkFBSWtHLFdBQVduQyxNQUFNSixrQkFBckI7QUFDQXVDLDJCQUFXQSxTQUNOQyxPQURNLENBQ0UsWUFERixFQUNnQjFHLE1BQU04QixJQUFOLElBQWMsRUFEOUIsRUFFTjRFLE9BRk0sQ0FFRSxZQUZGLEVBRWdCMUcsTUFBTTZCLElBQU4sR0FBYSxlQUFlN0IsTUFBTTZCLElBQXJCLEdBQTRCLFFBQXpDLEdBQW9ELEVBRnBFLEVBR042RSxPQUhNLENBR0UsZ0JBSEYsRUFHb0IxRyxNQUFNSixFQUFOLElBQVksQ0FIaEMsQ0FBWDtBQUlBNEcscUNBQXFCdkksTUFBckIsQ0FBNEJGLEVBQUUwSSxRQUFGLENBQTVCO0FBQ0gsYUFQRDtBQVFBMUksY0FBRSxxQkFBRixFQUF5QjRJLElBQXpCLENBQThCLHVCQUE5QixFQUF1RDdHLEVBQUU4RyxJQUFGLENBQU9MLGVBQVAsQ0FBdkQ7QUFDSDtBQTVITDtBQUFBO0FBQUEsd0NBOEgyQjtBQUNuQixnQkFBSU0sb0JBQW9COUksRUFBRSxxQkFBRixDQUF4QjtBQUNBLGdCQUFJdUQsVUFBVSxpRUFBQXZFLENBQVEySCxnQkFBUixHQUEyQnBELE9BQXpDO0FBQ0EsZ0JBQUlBLFlBQVksVUFBWixLQUE0QkEsWUFBWSxRQUFaLElBQXdCQSxZQUFZLGdCQUFwQyxJQUF3REEsWUFBWSxRQUFyRSxJQUFrRixpRUFBQXZFLENBQVEySCxnQkFBUixHQUEyQnpELFNBQTNCLElBQXdDLENBQXJKLENBQUosRUFBNko7QUFDekpsRCxrQkFBRSw4REFBRixFQUFrRUMsUUFBbEUsQ0FBMkUsVUFBM0U7QUFDQTZJLGtDQUFrQkYsSUFBbEIsQ0FBdUIsbUJBQXZCLEVBQTRDLE9BQTVDO0FBQ0gsYUFIRCxNQUdPO0FBQ0g1SSxrQkFBRSw4REFBRixFQUFrRUksV0FBbEUsQ0FBOEUsVUFBOUU7QUFDQTBJLGtDQUFrQkYsSUFBbEIsQ0FBdUIsbUJBQXZCLEVBQTRDLE1BQTVDO0FBQ0g7O0FBRUQ1SSxjQUFFLHdEQUFGLEVBQTRESSxXQUE1RCxDQUF3RSxVQUF4RTs7QUFFQSxnQkFBSTJJLG1CQUFtQi9JLEVBQUUsbURBQUYsQ0FBdkI7QUFDQSxnQkFBSXVELFlBQVksT0FBaEIsRUFBeUI7QUFDckJ3RixpQ0FBaUIzSSxXQUFqQixDQUE2QixRQUE3QixFQUF1Q0EsV0FBdkMsQ0FBbUQsVUFBbkQ7QUFDQSxvQkFBSSxDQUFDMkIsRUFBRThHLElBQUYsQ0FBTyxpRUFBQTdKLENBQVFnSyxRQUFSLEVBQVAsQ0FBTCxFQUFpQztBQUM3QkQscUNBQWlCOUksUUFBakIsQ0FBMEIsUUFBMUIsRUFBb0NBLFFBQXBDLENBQTZDLFVBQTdDO0FBQ0g7QUFDSixhQUxELE1BS087QUFDSDhJLGlDQUFpQjlJLFFBQWpCLENBQTBCLFFBQTFCO0FBQ0g7O0FBRURnSixZQUFBLHdGQUFBQSxDQUFtQkMsY0FBbkI7QUFDQUQsWUFBQSx3RkFBQUEsQ0FBbUJFLFdBQW5COztBQUVBTCw4QkFBa0JGLElBQWxCLENBQXVCLGNBQXZCLEVBQXVDckYsT0FBdkM7QUFDSDtBQXpKTDs7QUFBQTtBQUFBLEk7Ozs7Ozs7Ozs7Ozs7OztBQ1JBO0FBQ0E7QUFDQTs7QUFFQSxJQUFhb0UsY0FBYjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEseUNBQzRCO0FBQ3BCLGdCQUFJbEYsV0FBV1YsRUFBRThHLElBQUYsQ0FBTyxpRUFBQTdKLENBQVFvSyxnQkFBUixFQUFQLENBQWY7O0FBRUF6QiwyQkFBZUMsYUFBZjs7QUFFQSxnQkFBSW5GLFdBQVcsQ0FBZixFQUFrQjtBQUNkekMsa0JBQUUsc0JBQUYsRUFBMEJJLFdBQTFCLENBQXNDLFVBQXRDO0FBQ0gsYUFGRCxNQUVPO0FBQ0hKLGtCQUFFLHNCQUFGLEVBQTBCQyxRQUExQixDQUFtQyxVQUFuQztBQUNIO0FBQ0o7QUFYTDtBQUFBO0FBQUEsd0NBYTJCO0FBQ25CLGdCQUFJd0MsV0FBVyxFQUFmOztBQUVBVixjQUFFQyxJQUFGLENBQU8saUVBQUFoRCxDQUFRcUssZ0JBQVIsRUFBUCxFQUFtQyxVQUFVcEgsS0FBVixFQUFpQk8sS0FBakIsRUFBd0I7QUFDdkQsb0JBQUlULEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxPQUFELEVBQVUsU0FBVixFQUFxQixLQUFyQixFQUE0QixNQUE1QixFQUFvQyxPQUFwQyxDQUFYLEVBQXlEckgsTUFBTXVDLElBQS9ELENBQUosRUFBMEU7QUFDdEUvQiw2QkFBU04sSUFBVCxDQUFjO0FBQ1ZvSCw2QkFBS3RILE1BQU0vQztBQURELHFCQUFkO0FBR0FnRCxvQkFBQSx3RUFBQUEsQ0FBWUMsSUFBWixDQUFpQkYsTUFBTUosRUFBdkI7QUFDSDtBQUNKLGFBUEQ7O0FBU0EsZ0JBQUlFLEVBQUU4RyxJQUFGLENBQU9wRyxRQUFQLElBQW1CLENBQXZCLEVBQTBCO0FBQ3RCekMsa0JBQUV3SixRQUFGLENBQVdDLElBQVgsQ0FBZ0JoSCxRQUFoQjtBQUNBekQsZ0JBQUEsaUVBQUFBLENBQVEwSyxnQkFBUjtBQUNILGFBSEQsTUFHTztBQUNIO0FBQ0E7QUFDSDtBQUNKO0FBaENMO0FBQUE7QUFBQSx5Q0FrQzRCO0FBQ3BCLGdCQUFJQyxRQUFRLEVBQVo7QUFDQTVILGNBQUVDLElBQUYsQ0FBTyxpRUFBQWhELENBQVFxSyxnQkFBUixFQUFQLEVBQW1DLFVBQVVwSCxLQUFWLEVBQWlCTyxLQUFqQixFQUF3QjtBQUN2RCxvQkFBSSxDQUFDVCxFQUFFNkgsT0FBRixDQUFVRCxLQUFWLENBQUwsRUFBdUI7QUFDbkJBLDZCQUFTLElBQVQ7QUFDSDtBQUNEQSx5QkFBUzFILE1BQU00SCxRQUFmO0FBQ0gsYUFMRDtBQU1BLGdCQUFJQyxpQkFBaUI5SixFQUFFLHVCQUFGLENBQXJCO0FBQ0E4SiwyQkFBZXhILElBQWYsQ0FBb0IsZ0JBQXBCLEVBQXNDcUgsS0FBdEM7QUFDQSxnQkFBSUksU0FBSixDQUFjLHVCQUFkLEVBQXVDO0FBQ25DQyxzQkFBTSxjQUFVQyxPQUFWLEVBQW1CO0FBQ3JCLDJCQUFPTixLQUFQO0FBQ0g7QUFIa0MsYUFBdkM7QUFLQXBGLFlBQUEsZ0ZBQUFBLENBQWVrQixXQUFmLENBQTJCLFNBQTNCLEVBQXNDN0MsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJ3RSxTQUE3QixDQUF1QzlDLE9BQTdFLEVBQXNGeEUsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJqQixPQUE3QixDQUFxQzBGLGNBQTNIO0FBQ0FMLDJCQUFlRyxPQUFmLENBQXVCLE9BQXZCO0FBQ0g7QUFuREw7QUFBQTtBQUFBLDJDQXFEOEJ6RixJQXJEOUIsRUFxRG9DNEYsUUFyRHBDLEVBcUQ4QztBQUN0QyxnQkFBSTNILFdBQVcsRUFBZjtBQUNBVixjQUFFQyxJQUFGLENBQU8saUVBQUFoRCxDQUFRb0ssZ0JBQVIsRUFBUCxFQUFtQyxVQUFVbkgsS0FBVixFQUFpQk8sS0FBakIsRUFBd0I7QUFDdkRDLHlCQUFTTixJQUFULENBQWM7QUFDVmtJLCtCQUFXcEksTUFBTW9JLFNBRFA7QUFFVnhJLHdCQUFJSSxNQUFNSixFQUZBO0FBR1ZnSSw4QkFBVTVILE1BQU00SCxRQUhOO0FBSVZTLDJCQUFPckksTUFBTXFJO0FBSkgsaUJBQWQ7QUFNSCxhQVBEOztBQVNBLG9CQUFROUYsSUFBUjtBQUNJLHFCQUFLLFFBQUw7QUFDSXhFLHNCQUFFLHFCQUFGLEVBQXlCdUssS0FBekIsQ0FBK0IsTUFBL0IsRUFBdUNsSyxJQUF2QyxDQUE0QyxjQUE1QyxFQUE0RGlDLElBQTVELENBQWlFLFFBQWpFLEVBQTJFa0MsSUFBM0U7QUFDQTtBQUNKLHFCQUFLLFdBQUw7QUFDSW1ELG1DQUFlNkMsY0FBZjtBQUNBO0FBQ0oscUJBQUssU0FBTDtBQUNJN0MsbUNBQWU4QyxhQUFmO0FBQ0E7QUFDSixxQkFBSyxpQkFBTDtBQUNJLHdCQUFJRixRQUFRdkssRUFBRSx3QkFBRixDQUFaO0FBQ0Esd0JBQUl5QyxTQUFTLENBQVQsRUFBWTZILEtBQVosQ0FBa0I3SyxNQUFsQixLQUE2QixDQUFqQyxFQUFvQztBQUNoQzhLLDhCQUFNbEssSUFBTixDQUFXLHdCQUFYLEVBQXFDcUssR0FBckMsQ0FBeUMsRUFBekM7QUFDQUgsOEJBQU1sSyxJQUFOLENBQVcsdUJBQVgsRUFBb0NxSyxHQUFwQyxDQUF3QyxFQUF4QztBQUNBSCw4QkFBTWxLLElBQU4sQ0FBVywwQkFBWCxFQUF1Q3FLLEdBQXZDLENBQTJDLEVBQTNDO0FBQ0ExSywwQkFBRSxVQUFGLEVBQWMySyxVQUFkLENBQXlCLE9BQXpCO0FBQ0gscUJBTEQsTUFLTztBQUNISiw4QkFBTWxLLElBQU4sQ0FBVyx3QkFBWCxFQUFxQ3FLLEdBQXJDLENBQXlDakksU0FBUyxDQUFULEVBQVk2SCxLQUFaLENBQWtCTSxjQUEzRDtBQUNBTCw4QkFBTWxLLElBQU4sQ0FBVyx1QkFBWCxFQUFvQ3FLLEdBQXBDLENBQXdDakksU0FBUyxDQUFULEVBQVk2SCxLQUFaLENBQWtCTyxlQUExRDtBQUNBTiw4QkFBTWxLLElBQU4sQ0FBVywwQkFBWCxFQUF1Q3FLLEdBQXZDLENBQTJDakksU0FBUyxDQUFULEVBQVk2SCxLQUFaLENBQWtCUSxVQUE3RDtBQUNBOUssMEJBQUUsVUFBRixFQUFjK0ssSUFBZCxDQUFtQixPQUFuQixFQUE0QnRJLFNBQVMsQ0FBVCxFQUFZNkgsS0FBWixDQUFrQlEsVUFBOUM7QUFDSDtBQUNEUCwwQkFBTUEsS0FBTixDQUFZLE1BQVosRUFBb0JsSyxJQUFwQixDQUF5QixjQUF6QixFQUF5Q2lDLElBQXpDLENBQThDLFFBQTlDLEVBQXdEa0MsSUFBeEQsRUFBOERsQyxJQUE5RCxDQUFtRSxPQUFuRSxFQUE0RUcsU0FBUyxDQUFULEVBQVlvSCxRQUF4RjtBQUNBO0FBQ0oscUJBQUssT0FBTDtBQUNJN0osc0JBQUUsb0JBQUYsRUFBd0J1SyxLQUF4QixDQUE4QixNQUE5QixFQUFzQ2xLLElBQXRDLENBQTJDLGNBQTNDLEVBQTJEaUMsSUFBM0QsQ0FBZ0UsUUFBaEUsRUFBMEVrQyxJQUExRTtBQUNBO0FBQ0oscUJBQUssUUFBTDtBQUNJeEUsc0JBQUUscUJBQUYsRUFBeUJ1SyxLQUF6QixDQUErQixNQUEvQixFQUF1Q2xLLElBQXZDLENBQTRDLGNBQTVDLEVBQTREaUMsSUFBNUQsQ0FBaUUsUUFBakUsRUFBMkVrQyxJQUEzRTtBQUNBO0FBQ0oscUJBQUssT0FBTDtBQUNJeEUsc0JBQUUsb0JBQUYsRUFBd0J1SyxLQUF4QixDQUE4QixNQUE5QixFQUFzQ2xLLElBQXRDLENBQTJDLGNBQTNDLEVBQTJEaUMsSUFBM0QsQ0FBZ0UsUUFBaEUsRUFBMEVrQyxJQUExRTtBQUNBO0FBQ0oscUJBQUssYUFBTDtBQUNJeEUsc0JBQUUsb0JBQUYsRUFBd0J1SyxLQUF4QixDQUE4QixNQUE5QixFQUFzQ2xLLElBQXRDLENBQTJDLGNBQTNDLEVBQTJEaUMsSUFBM0QsQ0FBZ0UsUUFBaEUsRUFBMEVrQyxJQUExRTtBQUNBO0FBQ0oscUJBQUssVUFBTDtBQUNJLHdCQUFJd0csZUFBZXBMLGFBQWFxTCxRQUFoQztBQUNBLHdCQUFJQyxRQUFRLENBQVo7QUFDQW5KLHNCQUFFQyxJQUFGLENBQU8saUVBQUFoRCxDQUFRb0ssZ0JBQVIsRUFBUCxFQUFtQyxVQUFVbkgsS0FBVixFQUFpQk8sS0FBakIsRUFBd0I7QUFDdkQsNEJBQUksQ0FBQ1QsRUFBRXVILFFBQUYsQ0FBVyxpRUFBQXRLLENBQVFtTSxVQUFSLEdBQXFCN0csZUFBaEMsRUFBaURyQyxNQUFNbUosU0FBdkQsQ0FBTCxFQUF3RTtBQUNwRUosNENBQWdCLENBQUNFLFVBQVUsQ0FBVixHQUFjLEdBQWQsR0FBb0IsR0FBckIsSUFBNEIsV0FBNUIsR0FBMENBLEtBQTFDLEdBQWtELGVBQWxELEdBQW9FakosTUFBTW9JLFNBQTFFLEdBQXNGLFlBQXRGLEdBQXFHYSxLQUFyRyxHQUE2RyxRQUE3RyxHQUF3SGpKLE1BQU1KLEVBQTlJO0FBQ0FxSjtBQUNIO0FBQ0oscUJBTEQ7QUFNQSx3QkFBSUYsaUJBQWlCcEwsYUFBYXFMLFFBQWxDLEVBQTRDO0FBQ3hDOUwsK0JBQU9zSyxJQUFQLENBQVl1QixZQUFaLEVBQTBCLFFBQTFCO0FBQ0gscUJBRkQsTUFFTztBQUNIekcsd0JBQUEsZ0ZBQUFBLENBQWVrQixXQUFmLENBQTJCLE9BQTNCLEVBQW9DN0MsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJ1RixRQUE3QixDQUFzQ2pELEtBQTFFLEVBQWlGcEYsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJqQixPQUE3QixDQUFxQ2tCLFlBQXRIO0FBQ0g7QUFDRDtBQUNKO0FBQ0lnQyxtQ0FBZTBELGFBQWYsQ0FBNkI7QUFDekI1SSxrQ0FBVUEsUUFEZTtBQUV6QnVCLGdDQUFRUTtBQUZpQixxQkFBN0IsRUFHRzRGLFFBSEg7QUFJQTtBQXpEUjtBQTJESDtBQTNITDtBQUFBO0FBQUEsc0NBNkh5QjlILElBN0h6QixFQTZIZ0Q7QUFBQSxnQkFBakI4SCxRQUFpQix1RUFBTixJQUFNOztBQUN4Q3BLLGNBQUUrRyxJQUFGLENBQU87QUFDSDdILHFCQUFLVSxhQUFhMEwsY0FEZjtBQUVIOUcsc0JBQU0sTUFGSDtBQUdIbEMsc0JBQU1BLElBSEg7QUFJSDJFLDBCQUFVLE1BSlA7QUFLSEMsNEJBQVksc0JBQVk7QUFDcEJsSSxvQkFBQSxpRUFBQUEsQ0FBUW1JLGVBQVI7QUFDSCxpQkFQRTtBQVFIQyx5QkFBUyxpQkFBVUMsR0FBVixFQUFlO0FBQ3BCckksb0JBQUEsaUVBQUFBLENBQVF1TSxlQUFSO0FBQ0Esd0JBQUksQ0FBQ2xFLElBQUlXLEtBQVQsRUFBZ0I7QUFDWnpELHdCQUFBLGdGQUFBQSxDQUFla0IsV0FBZixDQUEyQixTQUEzQixFQUFzQzRCLElBQUk1QyxPQUExQyxFQUFtRDdCLGdCQUFnQjhDLFlBQWhCLENBQTZCakIsT0FBN0IsQ0FBcUMwRixjQUF4RjtBQUNILHFCQUZELE1BRU87QUFDSDVGLHdCQUFBLGdGQUFBQSxDQUFla0IsV0FBZixDQUEyQixPQUEzQixFQUFvQzRCLElBQUk1QyxPQUF4QyxFQUFpRDdCLGdCQUFnQjhDLFlBQWhCLENBQTZCakIsT0FBN0IsQ0FBcUNrQixZQUF0RjtBQUNIO0FBQ0Qsd0JBQUl5RSxRQUFKLEVBQWM7QUFDVkEsaUNBQVMvQyxHQUFUO0FBQ0g7QUFDSixpQkFsQkU7QUFtQkhTLDBCQUFVLGtCQUFVeEYsSUFBVixFQUFnQjtBQUN0QnRELG9CQUFBLGlFQUFBQSxDQUFRK0ksZUFBUjtBQUNILGlCQXJCRTtBQXNCSEMsdUJBQU8sZUFBVTFGLElBQVYsRUFBZ0I7QUFDbkJpQyxvQkFBQSxnRkFBQUEsQ0FBZTBELFdBQWYsQ0FBMkIzRixJQUEzQjtBQUNIO0FBeEJFLGFBQVA7QUEwQkg7QUF4Skw7QUFBQTtBQUFBLDRDQTBKK0I7QUFDdkIsZ0JBQUlrSixPQUFPeEwsRUFBRSx1QkFBRixFQUEyQkcsSUFBM0IsRUFBWDtBQUNBLGdCQUFJc0wsZ0JBQWdCekwsRUFBRSxtQ0FBRixFQUF1QzBMLEtBQXZDLEVBQXBCOztBQUVBM0osY0FBRUMsSUFBRixDQUFPLGlFQUFBaEQsQ0FBUW9LLGdCQUFSLEVBQVAsRUFBbUMsVUFBVW5ILEtBQVYsRUFBaUJPLEtBQWpCLEVBQXdCO0FBQ3ZELG9CQUFJc0QsT0FBTzBGLEtBQ0Y3QyxPQURFLENBQ00sWUFETixFQUNvQjFHLE1BQU02QixJQUFOLElBQWMsY0FEbEMsRUFFRjZFLE9BRkUsQ0FFTSxtQkFGTixFQUUyQixpQkFGM0IsRUFHRkEsT0FIRSxDQUdNLGFBSE4sRUFHcUIxRyxNQUFNOEIsSUFIM0IsQ0FBWDtBQUtBLG9CQUFJNEgsUUFBUTNMLEVBQUU4RixJQUFGLENBQVo7QUFDQTZGLHNCQUFNckosSUFBTixDQUFXLElBQVgsRUFBaUJMLE1BQU1KLEVBQXZCO0FBQ0E4SixzQkFBTXJKLElBQU4sQ0FBVyxXQUFYLEVBQXdCTCxNQUFNb0ksU0FBOUI7QUFDQXNCLHNCQUFNckosSUFBTixDQUFXLE1BQVgsRUFBbUJMLE1BQU04QixJQUF6QjtBQUNBMEgsOEJBQWN2TCxNQUFkLENBQXFCeUwsS0FBckI7QUFDSCxhQVhEO0FBWUg7QUExS0w7QUFBQTtBQUFBLHdDQTRLMkI7QUFDbkIsZ0JBQUlDLG9CQUFvQixpRUFBQTVNLENBQVE2TSxpQkFBUixHQUE0QnBNLE1BQTVCLEdBQXFDLENBQTdEOztBQUVBLGdCQUFJcU0sa0JBQWtCOUwsRUFBRSxpQkFBRixFQUFxQkcsSUFBckIsRUFBdEI7QUFDQSxnQkFBSTRMLG1CQUFtQixDQUF2QjtBQUNBLGdCQUFJQyxtQkFBbUJoTSxFQUFFLHFDQUFGLENBQXZCO0FBQ0FnTSw2QkFBaUJOLEtBQWpCOztBQUVBLGdCQUFJTyxjQUFjak0sRUFBRW1CLE1BQUYsQ0FBUyxFQUFULEVBQWEsSUFBYixFQUFtQixpRUFBQW5DLENBQVFtTSxVQUFSLEdBQXFCdkgsWUFBeEMsQ0FBbEI7O0FBRUEsZ0JBQUlnSSxpQkFBSixFQUF1QjtBQUNuQkssNEJBQVlwSSxLQUFaLEdBQW9COUIsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWXBJLEtBQXJCLEVBQTRCLFVBQVVpQyxJQUFWLEVBQWdCO0FBQzVELDJCQUFPQSxLQUFLOUIsTUFBTCxLQUFnQixTQUF2QjtBQUNILGlCQUZtQixDQUFwQjtBQUdBaUksNEJBQVk5SCxJQUFaLEdBQW1CcEMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTlILElBQXJCLEVBQTJCLFVBQVUyQixJQUFWLEVBQWdCO0FBQzFELDJCQUFPQSxLQUFLOUIsTUFBTCxLQUFnQixXQUF2QjtBQUNILGlCQUZrQixDQUFuQjtBQUdBaUksNEJBQVk5SCxJQUFaLEdBQW1CcEMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTlILElBQXJCLEVBQTJCLFVBQVUyQixJQUFWLEVBQWdCO0FBQzFELDJCQUFPQSxLQUFLOUIsTUFBTCxLQUFnQixpQkFBdkI7QUFDSCxpQkFGa0IsQ0FBbkI7O0FBSUEsb0JBQUksQ0FBQ2pDLEVBQUV1SCxRQUFGLENBQVcxRyxnQkFBZ0J1SixXQUEzQixFQUF3QyxnQkFBeEMsQ0FBTCxFQUFnRTtBQUM1REYsZ0NBQVk5SCxJQUFaLEdBQW1CcEMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTlILElBQXJCLEVBQTJCLFVBQVUyQixJQUFWLEVBQWdCO0FBQzFELCtCQUFPQSxLQUFLOUIsTUFBTCxLQUFnQixXQUF2QjtBQUNILHFCQUZrQixDQUFuQjtBQUdIOztBQUVELG9CQUFJLENBQUNqQyxFQUFFdUgsUUFBRixDQUFXMUcsZ0JBQWdCdUosV0FBM0IsRUFBd0MsY0FBeEMsQ0FBTCxFQUE4RDtBQUMxREYsZ0NBQVk5SCxJQUFaLEdBQW1CcEMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTlILElBQXJCLEVBQTJCLFVBQVUyQixJQUFWLEVBQWdCO0FBQzFELCtCQUFPL0QsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLFFBQUQsQ0FBWCxFQUF1QnhELEtBQUs5QixNQUE1QixDQUFQO0FBQ0gscUJBRmtCLENBQW5COztBQUlBaUksZ0NBQVk3SCxJQUFaLEdBQW1CckMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTdILElBQXJCLEVBQTJCLFVBQVUwQixJQUFWLEVBQWdCO0FBQzFELCtCQUFPL0QsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLFFBQUQsRUFBVyxPQUFYLEVBQW9CLGNBQXBCLEVBQW9DLFVBQXBDLENBQVgsRUFBNER4RCxLQUFLOUIsTUFBakUsQ0FBUDtBQUNILHFCQUZrQixDQUFuQjtBQUdIOztBQUVELG9CQUFJLENBQUNqQyxFQUFFdUgsUUFBRixDQUFXMUcsZ0JBQWdCdUosV0FBM0IsRUFBd0MsZUFBeEMsQ0FBTCxFQUErRDtBQUMzREYsZ0NBQVk1SCxLQUFaLEdBQW9CdEMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTVILEtBQXJCLEVBQTRCLFVBQVV5QixJQUFWLEVBQWdCO0FBQzVELCtCQUFPL0QsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLE9BQUQsRUFBVSxTQUFWLENBQVgsRUFBaUN4RCxLQUFLOUIsTUFBdEMsQ0FBUDtBQUNILHFCQUZtQixDQUFwQjtBQUdIOztBQUVELG9CQUFJLENBQUNqQyxFQUFFdUgsUUFBRixDQUFXMUcsZ0JBQWdCdUosV0FBM0IsRUFBd0MsZ0JBQXhDLENBQUwsRUFBZ0U7QUFDNURGLGdDQUFZNUgsS0FBWixHQUFvQnRDLEVBQUVtSyxNQUFGLENBQVNELFlBQVk1SCxLQUFyQixFQUE0QixVQUFVeUIsSUFBVixFQUFnQjtBQUM1RCwrQkFBTy9ELEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxRQUFELENBQVgsRUFBdUJ4RCxLQUFLOUIsTUFBNUIsQ0FBUDtBQUNILHFCQUZtQixDQUFwQjtBQUdIO0FBQ0o7O0FBRUQsZ0JBQUlvSSxnQkFBZ0IsaUVBQUFwTixDQUFRcUssZ0JBQVIsRUFBcEI7QUFDQSxnQkFBSStDLGNBQWMzTSxNQUFkLEdBQXVCLENBQXZCLElBQTRCMk0sY0FBYyxDQUFkLEVBQWlCNUgsSUFBakIsS0FBMEIsT0FBMUQsRUFBbUU7QUFDL0R5SCw0QkFBWTlILElBQVosR0FBbUJwQyxFQUFFbUssTUFBRixDQUFTRCxZQUFZOUgsSUFBckIsRUFBMkIsVUFBVTJCLElBQVYsRUFBZ0I7QUFDMUQsMkJBQU9BLEtBQUs5QixNQUFMLEtBQWdCLGlCQUF2QjtBQUNILGlCQUZrQixDQUFuQjtBQUdIOztBQUVELGdCQUFJcUksY0FBYyxLQUFsQjtBQUNBdEssY0FBRUMsSUFBRixDQUFPb0ssYUFBUCxFQUFzQixVQUFVbkssS0FBVixFQUFpQjtBQUNuQyxvQkFBSUYsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLE9BQUQsRUFBVSxTQUFWLEVBQXFCLEtBQXJCLEVBQTRCLE1BQTVCLEVBQW9DLE9BQXBDLENBQVgsRUFBeURySCxNQUFNdUMsSUFBL0QsQ0FBSixFQUEwRTtBQUN0RTZILGtDQUFjLElBQWQ7QUFDSDtBQUNKLGFBSkQ7O0FBTUEsZ0JBQUksQ0FBQ0EsV0FBTCxFQUFrQjtBQUNkSiw0QkFBWXBJLEtBQVosR0FBb0I5QixFQUFFbUssTUFBRixDQUFTRCxZQUFZcEksS0FBckIsRUFBNEIsVUFBVWlDLElBQVYsRUFBZ0I7QUFDNUQsMkJBQU9BLEtBQUs5QixNQUFMLEtBQWdCLFNBQXZCO0FBQ0gsaUJBRm1CLENBQXBCO0FBR0g7O0FBRUQsZ0JBQUlwQixnQkFBZ0IwSixJQUFoQixLQUF5QixRQUE3QixFQUF1QztBQUNuQ0wsNEJBQVk3SCxJQUFaLEdBQW1CckMsRUFBRW1LLE1BQUYsQ0FBU0QsWUFBWTdILElBQXJCLEVBQTJCLFVBQVUwQixJQUFWLEVBQWdCO0FBQzFELDJCQUFPL0QsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLE9BQUQsRUFBVSxjQUFWLEVBQTBCLFVBQTFCLENBQVgsRUFBa0R4RCxLQUFLOUIsTUFBdkQsQ0FBUDtBQUNILGlCQUZrQixDQUFuQjtBQUdIOztBQUVELGdCQUFJb0ksY0FBYzNNLE1BQWQsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDMUIsb0JBQUksQ0FBQ3NDLEVBQUV1SCxRQUFGLENBQVcxRyxnQkFBZ0J1SixXQUEzQixFQUF3QyxjQUF4QyxDQUFMLEVBQThEO0FBQzFERixnQ0FBWTlILElBQVosR0FBbUJwQyxFQUFFbUssTUFBRixDQUFTRCxZQUFZOUgsSUFBckIsRUFBMkIsVUFBVTJCLElBQVYsRUFBZ0I7QUFDMUQsK0JBQU9BLEtBQUs5QixNQUFMLEtBQWdCLFdBQXZCO0FBQ0gscUJBRmtCLENBQW5CO0FBR0g7O0FBRUQsb0JBQUksQ0FBQ2pDLEVBQUV1SCxRQUFGLENBQVcxRyxnQkFBZ0J1SixXQUEzQixFQUF3QyxZQUF4QyxDQUFMLEVBQTREO0FBQ3hERixnQ0FBWTlILElBQVosR0FBbUJwQyxFQUFFbUssTUFBRixDQUFTRCxZQUFZOUgsSUFBckIsRUFBMkIsVUFBVTJCLElBQVYsRUFBZ0I7QUFDMUQsK0JBQU8vRCxFQUFFdUgsUUFBRixDQUFXLENBQUMsUUFBRCxFQUFXLGlCQUFYLENBQVgsRUFBMEN4RCxLQUFLOUIsTUFBL0MsQ0FBUDtBQUNILHFCQUZrQixDQUFuQjs7QUFJQWlJLGdDQUFZN0gsSUFBWixHQUFtQnJDLEVBQUVtSyxNQUFGLENBQVNELFlBQVk3SCxJQUFyQixFQUEyQixVQUFVMEIsSUFBVixFQUFnQjtBQUMxRCwrQkFBTy9ELEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxPQUFELEVBQVUsY0FBVixFQUEwQixVQUExQixDQUFYLEVBQWtEeEQsS0FBSzlCLE1BQXZELENBQVA7QUFDSCxxQkFGa0IsQ0FBbkI7QUFHSDs7QUFFRCxvQkFBSSxDQUFDakMsRUFBRXVILFFBQUYsQ0FBVzFHLGdCQUFnQnVKLFdBQTNCLEVBQXdDLGFBQXhDLENBQUwsRUFBNkQ7QUFDekRGLGdDQUFZNUgsS0FBWixHQUFvQnRDLEVBQUVtSyxNQUFGLENBQVNELFlBQVk1SCxLQUFyQixFQUE0QixVQUFVeUIsSUFBVixFQUFnQjtBQUM1RCwrQkFBTy9ELEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxPQUFELEVBQVUsU0FBVixDQUFYLEVBQWlDeEQsS0FBSzlCLE1BQXRDLENBQVA7QUFDSCxxQkFGbUIsQ0FBcEI7QUFHSDs7QUFFRCxvQkFBSSxDQUFDakMsRUFBRXVILFFBQUYsQ0FBVzFHLGdCQUFnQnVKLFdBQTNCLEVBQXdDLGNBQXhDLENBQUwsRUFBOEQ7QUFDMURGLGdDQUFZNUgsS0FBWixHQUFvQnRDLEVBQUVtSyxNQUFGLENBQVNELFlBQVk1SCxLQUFyQixFQUE0QixVQUFVeUIsSUFBVixFQUFnQjtBQUM1RCwrQkFBTy9ELEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxRQUFELENBQVgsRUFBdUJ4RCxLQUFLOUIsTUFBNUIsQ0FBUDtBQUNILHFCQUZtQixDQUFwQjtBQUdIO0FBQ0o7O0FBRURqQyxjQUFFQyxJQUFGLENBQU9pSyxXQUFQLEVBQW9CLFVBQVVqSSxNQUFWLEVBQWtCNkIsR0FBbEIsRUFBdUI7QUFDdkM5RCxrQkFBRUMsSUFBRixDQUFPZ0MsTUFBUCxFQUFlLFVBQVU4QixJQUFWLEVBQWdCdEQsS0FBaEIsRUFBdUI7QUFDbEMsd0JBQUkrSixXQUFXLEtBQWY7QUFDQSw0QkFBUSxpRUFBQXZOLENBQVEySCxnQkFBUixHQUEyQnBELE9BQW5DO0FBQ0ksNkJBQUssVUFBTDtBQUNJLGdDQUFJeEIsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLGlCQUFELEVBQW9CLFFBQXBCLEVBQThCLFNBQTlCLEVBQXlDLGNBQXpDLENBQVgsRUFBcUV4RCxLQUFLOUIsTUFBMUUsQ0FBSixFQUF1RjtBQUNuRnVJLDJDQUFXLElBQVg7QUFDSDtBQUNEO0FBQ0osNkJBQUssUUFBTDtBQUNJLGdDQUFJeEssRUFBRXVILFFBQUYsQ0FBVyxDQUFDLGlCQUFELEVBQW9CLFFBQXBCLEVBQThCLFNBQTlCLEVBQXlDLFdBQXpDLEVBQXNELGNBQXRELENBQVgsRUFBa0Z4RCxLQUFLOUIsTUFBdkYsQ0FBSixFQUFvRztBQUNoR3VJLDJDQUFXLElBQVg7QUFDSDtBQUNEO0FBQ0osNkJBQUssZ0JBQUw7QUFDSSxnQ0FBSXhLLEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxpQkFBRCxFQUFvQixRQUFwQixFQUE4QixTQUE5QixFQUF5QyxXQUF6QyxFQUFzRCxPQUF0RCxDQUFYLEVBQTJFeEQsS0FBSzlCLE1BQWhGLENBQUosRUFBNkY7QUFDekZ1SSwyQ0FBVyxJQUFYO0FBQ0g7QUFDRDtBQUNKLDZCQUFLLFFBQUw7QUFDSSxnQ0FBSXhLLEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxpQkFBRCxFQUFvQixRQUFwQixFQUE4QixTQUE5QixFQUF5QyxXQUF6QyxFQUFzRCxPQUF0RCxFQUErRCxjQUEvRCxDQUFYLEVBQTJGeEQsS0FBSzlCLE1BQWhHLENBQUosRUFBNkc7QUFDekd1SSwyQ0FBVyxJQUFYO0FBQ0g7QUFDRDtBQUNKLDZCQUFLLFFBQUw7QUFDSSxnQ0FBSXhLLEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxpQkFBRCxFQUFvQixRQUFwQixFQUE4QixTQUE5QixFQUF5QyxXQUF6QyxFQUFzRCxjQUF0RCxDQUFYLEVBQWtGeEQsS0FBSzlCLE1BQXZGLENBQUosRUFBb0c7QUFDaEd1SSwyQ0FBVyxJQUFYO0FBQ0g7QUFDRDtBQUNKLDZCQUFLLFdBQUw7QUFDSSxnQ0FBSXhLLEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxVQUFELEVBQWEsUUFBYixFQUF1QixTQUF2QixFQUFrQyxXQUFsQyxFQUErQyxjQUEvQyxDQUFYLEVBQTJFeEQsS0FBSzlCLE1BQWhGLENBQUosRUFBNkY7QUFDekZ1SSwyQ0FBVyxJQUFYO0FBQ0g7QUFDRDtBQUNKLDZCQUFLLE9BQUw7QUFDSSxnQ0FBSSxDQUFDeEssRUFBRXVILFFBQUYsQ0FBVyxDQUFDLFNBQUQsRUFBWSxRQUFaLEVBQXNCLFNBQXRCLEVBQWlDLFFBQWpDLEVBQTJDLFVBQTNDLENBQVgsRUFBbUV4RCxLQUFLOUIsTUFBeEUsQ0FBTCxFQUFzRjtBQUNsRnVJLDJDQUFXLElBQVg7QUFDSDtBQUNEO0FBbkNSO0FBcUNBLHdCQUFJLENBQUNBLFFBQUwsRUFBZTtBQUNYLDRCQUFJM0ksZUFBZSxFQUFuQjtBQUNBLDRCQUFJO0FBQ0FBLDJDQUFlaEIsZ0JBQWdCOEMsWUFBaEIsQ0FBNkI5QixZQUE3QixDQUEwQ2lDLEdBQTFDLEVBQStDQyxLQUFLOUIsTUFBcEQsQ0FBZjtBQUNILHlCQUZELENBR0EsT0FBTWpELEdBQU4sRUFBVyxDQUVWO0FBQ0QsNEJBQUkySCxXQUFXb0QsZ0JBQ1ZuRCxPQURVLENBQ0YsY0FERSxFQUNjN0MsS0FBSzlCLE1BQUwsSUFBZSxFQUQ3QixFQUVWMkUsT0FGVSxDQUVGLFlBRkUsRUFFWTdDLEtBQUtoQyxJQUFMLElBQWEsRUFGekIsRUFHVjZFLE9BSFUsQ0FHRixZQUhFLEVBR1kvRSxnQkFBZ0JrQyxLQUFLL0IsSUFIakMsQ0FBZjtBQUlBLDRCQUFJLENBQUN2QixLQUFELElBQVV1SixnQkFBZCxFQUFnQztBQUM1QnJELHVDQUFXLCtDQUErQ0EsUUFBMUQ7QUFDSDtBQUNEc0QseUNBQWlCOUwsTUFBakIsQ0FBd0J3SSxRQUF4QjtBQUNIO0FBQ0osaUJBeEREO0FBeURBLG9CQUFJMUUsT0FBT3ZFLE1BQVAsR0FBZ0IsQ0FBcEIsRUFBdUI7QUFDbkJzTTtBQUNIO0FBQ0osYUE3REQ7QUE4REg7QUFwVkw7O0FBQUE7QUFBQSxJOzs7Ozs7Ozs7Ozs7Ozs7QUNKQTtBQUNBOztBQUVBLElBQWFTLGFBQWI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBLHlDQUM0QkosYUFENUIsRUFDMkM7O0FBRW5DLGdCQUFJSyxjQUFjLHFFQUFBek4sQ0FBUTJELFdBQVIsQ0FBb0IsVUFBcEIsS0FBbUMscUVBQUEzRCxDQUFRMkQsV0FBUixDQUFvQixpQkFBcEIsQ0FBckQ7O0FBRUEsZ0JBQUl4RCxPQUFPdU4sTUFBUCxJQUFpQkQsV0FBckIsRUFBa0M7QUFDOUIsb0JBQUlFLFlBQVk1SyxFQUFFNkssS0FBRixDQUFRUixhQUFSLENBQWhCOztBQUVBak4sdUJBQU91TixNQUFQLENBQWNHLFFBQWQsQ0FBdUJDLEtBQXZCLENBQTZCQyxZQUE3QixDQUEwQyxxRUFBQS9OLENBQVEyRCxXQUFSLENBQW9CLGlCQUFwQixDQUExQyxFQUFrRmdLLFVBQVV6TixHQUE1Rjs7QUFFQSxvQkFBSUMsT0FBT3VOLE1BQVgsRUFBbUI7QUFDZnZOLDJCQUFPNk4sS0FBUDtBQUNIO0FBQ0osYUFSRCxNQVFPO0FBQ0g7QUFDSDtBQUNKO0FBaEJMOztBQUFBO0FBQUE7O0lBbUJNaE0sTyxHQUNGLGlCQUFZaU0sUUFBWixFQUFzQmhNLE9BQXRCLEVBQStCO0FBQUE7O0FBQzNCOUIsV0FBTzZCLE9BQVAsR0FBaUI3QixPQUFPNkIsT0FBUCxJQUFrQixFQUFuQzs7QUFFQSxRQUFJa00sUUFBUWxOLEVBQUUsTUFBRixDQUFaOztBQUVBLFFBQUltTixpQkFBaUI7QUFDakJDLGtCQUFVLElBRE87QUFFakI1SSxjQUFNLEdBRlc7QUFHakJzQyx1QkFBZSx1QkFBVWUsS0FBVixFQUFpQndGLEdBQWpCLEVBQXNCLENBRXBDO0FBTGdCLEtBQXJCOztBQVFBcE0sY0FBVWpCLEVBQUVtQixNQUFGLENBQVMsSUFBVCxFQUFlZ00sY0FBZixFQUErQmxNLE9BQS9CLENBQVY7O0FBRUEsUUFBSXFNLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBVUMsS0FBVixFQUFpQjtBQUNqQ0EsY0FBTUMsY0FBTjtBQUNBLFlBQUlDLFdBQVd6TixFQUFFLElBQUYsQ0FBZjtBQUNBQSxVQUFFLGlCQUFGLEVBQXFCdUssS0FBckI7O0FBRUFwTCxlQUFPNkIsT0FBUCxDQUFlQyxPQUFmLEdBQXlCQSxPQUF6QjtBQUNBOUIsZUFBTzZCLE9BQVAsQ0FBZUMsT0FBZixDQUF1QkMsT0FBdkIsR0FBaUMsT0FBakM7O0FBRUEvQixlQUFPNkIsT0FBUCxDQUFlcU0sR0FBZixHQUFxQkksUUFBckI7O0FBRUFyTSxRQUFBLDRFQUFBQSxDQUFZQyxjQUFaLENBQTJCaUMsTUFBM0IsR0FBb0MsWUFBcEM7QUFDQXRFLFFBQUEscUVBQUFBLENBQVEwTyxXQUFSOztBQUVBLFlBQUlDLGNBQWN4TyxPQUFPNkIsT0FBUCxDQUFlcU0sR0FBZixDQUFtQi9LLElBQW5CLENBQXdCLFVBQXhCLENBQWxCO0FBQ0EsWUFBSSxPQUFPcUwsV0FBUCxLQUF1QixXQUF2QixJQUFzQ0EsWUFBWWxPLE1BQVosR0FBcUIsQ0FBL0QsRUFBa0U7QUFDOURrTywwQkFBY0EsWUFBWSxDQUFaLENBQWQ7QUFDQXhPLG1CQUFPNkIsT0FBUCxDQUFlQyxPQUFmLEdBQXlCakIsRUFBRW1CLE1BQUYsQ0FBUyxJQUFULEVBQWVoQyxPQUFPNkIsT0FBUCxDQUFlQyxPQUE5QixFQUF1QzBNLGVBQWUsRUFBdEQsQ0FBekI7QUFDQSxnQkFBSSxPQUFPQSxZQUFZcE0sZ0JBQW5CLEtBQXdDLFdBQTVDLEVBQXlEO0FBQ3JEcEMsdUJBQU82QixPQUFQLENBQWVDLE9BQWYsQ0FBdUJvRixRQUF2QixHQUFrQyxJQUFsQztBQUNILGFBRkQsTUFFTyxJQUFJLE9BQU9sSCxPQUFPNkIsT0FBUCxDQUFlQyxPQUFmLENBQXVCb0YsUUFBOUIsS0FBMkMsV0FBL0MsRUFBNEQ7QUFDL0RsSCx1QkFBTzZCLE9BQVAsQ0FBZUMsT0FBZixDQUF1Qm9GLFFBQXZCLEdBQWtDUSxTQUFsQztBQUNIO0FBQ0o7O0FBRUQsWUFBSTdHLEVBQUUsb0NBQUYsRUFBd0NQLE1BQXhDLEtBQW1ELENBQXZELEVBQTBEO0FBQ3RETyxjQUFFLGdCQUFGLEVBQW9CNE4sSUFBcEIsQ0FBeUJoTyxhQUFhaU8sS0FBdEMsRUFBNkMsVUFBVXZMLElBQVYsRUFBZ0I7QUFDekQsb0JBQUlBLEtBQUswRixLQUFULEVBQWdCO0FBQ1o4RiwwQkFBTXhMLEtBQUttQyxPQUFYO0FBQ0g7QUFDRHpFLGtCQUFFLGdCQUFGLEVBQ0tJLFdBREwsQ0FDaUIscUJBRGpCLEVBRUtzQyxPQUZMLENBRWEsZ0JBRmIsRUFFK0J0QyxXQUYvQixDQUUyQyxZQUYzQztBQUdILGFBUEQ7QUFRSCxTQVRELE1BU087QUFDSEosY0FBRStOLFFBQUYsRUFBWTFOLElBQVosQ0FBaUIsMERBQWpCLEVBQTZFNEosT0FBN0UsQ0FBcUYsT0FBckY7QUFDSDtBQUNKLEtBcENEOztBQXNDQSxRQUFJLE9BQU9nRCxRQUFQLEtBQW9CLFFBQXhCLEVBQWtDO0FBQzlCQyxjQUFNYyxFQUFOLENBQVMsT0FBVCxFQUFrQmYsUUFBbEIsRUFBNEJLLGFBQTVCO0FBQ0gsS0FGRCxNQUVPO0FBQ0hMLGlCQUFTZSxFQUFULENBQVksT0FBWixFQUFxQlYsYUFBckI7QUFDSDtBQUNKLEM7O0FBR0xuTyxPQUFPOE8saUJBQVAsR0FBMkJqTixPQUEzQjs7QUFFQWhCLEVBQUUsc0JBQUYsRUFBMEJrTyxHQUExQixDQUE4QixPQUE5QixFQUF1Q0YsRUFBdkMsQ0FBMEMsT0FBMUMsRUFBbUQsVUFBVVQsS0FBVixFQUFpQjtBQUNoRUEsVUFBTUMsY0FBTjtBQUNBLFFBQUlwQixnQkFBZ0IscUVBQUFwTixDQUFRcUssZ0JBQVIsRUFBcEI7QUFDQSxRQUFJdEgsRUFBRThHLElBQUYsQ0FBT3VELGFBQVAsSUFBd0IsQ0FBNUIsRUFBK0I7QUFDM0JJLHNCQUFjMkIsZ0JBQWQsQ0FBK0IvQixhQUEvQjtBQUNIO0FBQ0osQ0FORDs7QUFRQXBNLEVBQUVvTyxFQUFGLENBQUtwTixPQUFMLEdBQWUsVUFBVUMsT0FBVixFQUFtQjtBQUM5QixRQUFJb04sWUFBWXJPLEVBQUUsSUFBRixDQUFoQjs7QUFFQW9CLElBQUEsNEVBQUFBLENBQVlDLGNBQVosQ0FBMkJpQyxNQUEzQixHQUFvQyxZQUFwQztBQUNBLFFBQUksNEVBQUFsQyxDQUFZQyxjQUFaLENBQTJCa0MsT0FBM0IsS0FBdUMsT0FBM0MsRUFBb0Q7QUFDaER2RCxVQUFFK04sUUFBRixFQUFZMU4sSUFBWixDQUFpQixzQkFBakIsRUFBeUMwSyxJQUF6QyxDQUE4QyxVQUE5QyxFQUEwRCxJQUExRDtBQUNILEtBRkQsTUFFTztBQUNIL0ssVUFBRStOLFFBQUYsRUFBWTFOLElBQVosQ0FBaUIsc0JBQWpCLEVBQXlDMEssSUFBekMsQ0FBOEMsVUFBOUMsRUFBMEQsS0FBMUQ7QUFDSDtBQUNEL0wsSUFBQSxxRUFBQUEsQ0FBUTBPLFdBQVI7O0FBRUEsUUFBSTFNLE9BQUosQ0FBWXFOLFNBQVosRUFBdUJwTixPQUF2QjtBQUNILENBWkQsQzs7Ozs7Ozs7Ozs7Ozs7QUM5RkE7QUFDQTs7QUFFQSxJQUFhZ0YsU0FBYjtBQUNJLHlCQUFjO0FBQUE7O0FBQ1YsYUFBS3FJLEtBQUwsR0FBYSxFQUFiO0FBQ0EsYUFBS0EsS0FBTCxDQUFXQyxJQUFYLEdBQWtCdk8sRUFBRSxzQkFBRixFQUEwQkcsSUFBMUIsRUFBbEI7QUFDQSxhQUFLbU8sS0FBTCxDQUFXRSxLQUFYLEdBQW1CeE8sRUFBRSx1QkFBRixFQUEyQkcsSUFBM0IsRUFBbkI7O0FBRUEsYUFBSzJGLElBQUwsR0FBWSxFQUFaO0FBQ0EsYUFBS0EsSUFBTCxDQUFVeUksSUFBVixHQUFpQnZPLEVBQUUsOEJBQUYsRUFBa0NHLElBQWxDLEVBQWpCO0FBQ0EsYUFBSzJGLElBQUwsQ0FBVTBJLEtBQVYsR0FBa0J4TyxFQUFFLCtCQUFGLEVBQW1DRyxJQUFuQyxFQUFsQjs7QUFFQSxhQUFLc08sZUFBTCxHQUF1QnpPLEVBQUUsaUJBQUYsQ0FBdkI7QUFDSDs7QUFYTDtBQUFBO0FBQUEsbUNBY2VzQyxJQWRmLEVBYzZEO0FBQUEsZ0JBQXhDOEQsTUFBd0MsdUVBQS9CLEtBQStCO0FBQUEsZ0JBQXhCRSxjQUF3Qix1RUFBUCxLQUFPOztBQUNyRCxnQkFBSUMsUUFBUSxJQUFaO0FBQ0EsZ0JBQUluRixjQUFjLGlFQUFBcEMsQ0FBUW1NLFVBQVIsRUFBbEI7QUFDQSxnQkFBSXpDLFdBQVduQyxNQUFNK0gsS0FBTixDQUFZLGlFQUFBdFAsQ0FBUTJILGdCQUFSLEdBQTJCdEQsU0FBdkMsQ0FBZjs7QUFFQSxnQkFBSUUsVUFBVSxpRUFBQXZFLENBQVEySCxnQkFBUixHQUEyQnBELE9BQXpDOztBQUVBLGdCQUFJLENBQUN4QixFQUFFdUgsUUFBRixDQUFXLENBQUMsVUFBRCxFQUFhLFFBQWIsRUFBdUIsT0FBdkIsRUFBZ0MsV0FBaEMsRUFBNkMsUUFBN0MsRUFBdUQsZ0JBQXZELEVBQXlFLFFBQXpFLENBQVgsRUFBK0YvRixPQUEvRixDQUFMLEVBQThHO0FBQzFHQSwwQkFBVSxVQUFWO0FBQ0g7O0FBRUQsZ0JBQUltTCx5QkFBeUIsRUFBN0I7QUFDQSxnQkFBSUMsMEJBQTBCLEVBQTlCO0FBQ0EsZ0JBQUlDLDRCQUE0QixFQUFoQztBQUNBLGdCQUFHO0FBQ0NGLHlDQUF5QjlMLGdCQUFnQjhDLFlBQWhCLENBQTZCbUosT0FBN0IsQ0FBcUN0TCxPQUFyQyxFQUE4Q08sSUFBdkU7QUFDQTZLLDBDQUEwQi9MLGdCQUFnQjhDLFlBQWhCLENBQTZCbUosT0FBN0IsQ0FBcUN0TCxPQUFyQyxFQUE4Q3VMLEtBQXhFO0FBQ0FGLDRDQUE0QmhNLGdCQUFnQjhDLFlBQWhCLENBQTZCbUosT0FBN0IsQ0FBcUN0TCxPQUFyQyxFQUE4Q2tCLE9BQTFFO0FBQ0gsYUFKRCxDQUtBLE9BQU0xRCxHQUFOLEVBQVcsQ0FFVjs7QUFFRDJILHVCQUFXQSxTQUNOQyxPQURNLENBQ0Usa0JBREYsRUFDc0IrRixzQkFEdEIsRUFFTi9GLE9BRk0sQ0FFRSxtQkFGRixFQUV1QmdHLHVCQUZ2QixFQUdOaEcsT0FITSxDQUdFLHFCQUhGLEVBR3lCaUcseUJBSHpCLENBQVg7O0FBTUEsZ0JBQUlHLFVBQVUvTyxFQUFFMEksUUFBRixDQUFkO0FBQ0EsZ0JBQUkrQyxnQkFBZ0JzRCxRQUFRMU8sSUFBUixDQUFhLElBQWIsQ0FBcEI7O0FBRUEsZ0JBQUlpRyxrQkFBa0IsS0FBS21JLGVBQUwsQ0FBcUJwTyxJQUFyQixDQUEwQixtQkFBMUIsRUFBK0NaLE1BQS9DLEdBQXdELENBQTlFLEVBQWlGO0FBQzdFZ00sZ0NBQWdCLEtBQUtnRCxlQUFMLENBQXFCcE8sSUFBckIsQ0FBMEIsbUJBQTFCLENBQWhCO0FBQ0g7O0FBRUQsZ0JBQUkwQixFQUFFOEcsSUFBRixDQUFPdkcsS0FBSzBNLE9BQVosSUFBdUIsQ0FBdkIsSUFBNEJqTixFQUFFOEcsSUFBRixDQUFPdkcsS0FBS3VGLEtBQVosSUFBcUIsQ0FBckQsRUFBd0Q7QUFDcEQ3SCxrQkFBRSxpQkFBRixFQUFxQkMsUUFBckIsQ0FBOEIsV0FBOUI7QUFDSCxhQUZELE1BRU87QUFDSEQsa0JBQUUsaUJBQUYsRUFBcUJJLFdBQXJCLENBQWlDLFdBQWpDO0FBQ0g7O0FBRUQyQixjQUFFa04sT0FBRixDQUFVM00sS0FBSzBNLE9BQWYsRUFBd0IsVUFBVS9NLEtBQVYsRUFBaUJPLEtBQWpCLEVBQXdCO0FBQzVDLG9CQUFJc0QsT0FBT1MsTUFBTVQsSUFBTixDQUFXLGlFQUFBOUcsQ0FBUTJILGdCQUFSLEdBQTJCdEQsU0FBdEMsQ0FBWDtBQUNBeUMsdUJBQU9BLEtBQ0Y2QyxPQURFLENBQ00sWUFETixFQUNvQixRQURwQixFQUVGQSxPQUZFLENBRU0sVUFGTixFQUVrQjFHLE1BQU1KLEVBRnhCLEVBR0Y4RyxPQUhFLENBR00sWUFITixFQUdvQjFHLE1BQU04QixJQUFOLElBQWMsRUFIbEMsRUFJRjRFLE9BSkUsQ0FJTSxZQUpOLEVBSW9CLEVBSnBCLEVBS0ZBLE9BTEUsQ0FLTSxZQUxOLEVBS29CMUcsTUFBTWlOLFVBQU4sSUFBb0IsRUFMeEMsRUFNRnZHLE9BTkUsQ0FNTSxhQU5OLEVBTXFCLGdDQU5yQixDQUFQO0FBT0Esb0JBQUlnRCxRQUFRM0wsRUFBRThGLElBQUYsQ0FBWjtBQUNBL0Qsa0JBQUVrTixPQUFGLENBQVVoTixLQUFWLEVBQWlCLFVBQVV5SSxHQUFWLEVBQWVsSSxLQUFmLEVBQXNCO0FBQ25DbUosMEJBQU1ySixJQUFOLENBQVdFLEtBQVgsRUFBa0JrSSxHQUFsQjtBQUNILGlCQUZEO0FBR0FpQixzQkFBTXJKLElBQU4sQ0FBVyxXQUFYLEVBQXdCLElBQXhCO0FBQ0FxSixzQkFBTXJKLElBQU4sQ0FBVyxNQUFYLEVBQW1CbEIsWUFBWXNDLEtBQVosQ0FBa0JDLE1BQXJDO0FBQ0E4SCw4QkFBY3ZMLE1BQWQsQ0FBcUJ5TCxLQUFyQjtBQUNILGFBaEJEOztBQWtCQTVKLGNBQUVrTixPQUFGLENBQVUzTSxLQUFLdUYsS0FBZixFQUFzQixVQUFVNUYsS0FBVixFQUFpQjtBQUNuQyxvQkFBSTZELE9BQU9TLE1BQU1ULElBQU4sQ0FBVyxpRUFBQTlHLENBQVEySCxnQkFBUixHQUEyQnRELFNBQXRDLENBQVg7QUFDQXlDLHVCQUFPQSxLQUNGNkMsT0FERSxDQUNNLFlBRE4sRUFDb0IsTUFEcEIsRUFFRkEsT0FGRSxDQUVNLFVBRk4sRUFFa0IxRyxNQUFNSixFQUZ4QixFQUdGOEcsT0FIRSxDQUdNLFlBSE4sRUFHb0IxRyxNQUFNOEIsSUFBTixJQUFjLEVBSGxDLEVBSUY0RSxPQUpFLENBSU0sWUFKTixFQUlvQjFHLE1BQU00RyxJQUFOLElBQWMsRUFKbEMsRUFLRkYsT0FMRSxDQUtNLFlBTE4sRUFLb0IxRyxNQUFNaU4sVUFBTixJQUFvQixFQUx4QyxDQUFQO0FBTUEsb0JBQUksaUVBQUFsUSxDQUFRMkgsZ0JBQVIsR0FBMkJ0RCxTQUEzQixLQUF5QyxNQUE3QyxFQUFxRDtBQUNqRHlDLDJCQUFPQSxLQUNGNkMsT0FERSxDQUNNLGFBRE4sRUFDcUIsZUFBZTFHLE1BQU02QixJQUFyQixHQUE0QixRQURqRCxDQUFQO0FBRUgsaUJBSEQsTUFHTztBQUNILDRCQUFRN0IsTUFBTW1KLFNBQWQ7QUFDSSw2QkFBSyxTQUFMO0FBQ0l0RixtQ0FBT0EsS0FDRjZDLE9BREUsQ0FDTSxhQUROLEVBQ3FCLGVBQWUxRyxNQUFNaEIsT0FBTixDQUFja08sS0FBN0IsR0FBcUMsU0FBckMsR0FBaURsTixNQUFNOEIsSUFBdkQsR0FBOEQsSUFEbkYsQ0FBUDtBQUVBO0FBQ0o7QUFDSStCLG1DQUFPQSxLQUNGNkMsT0FERSxDQUNNLGFBRE4sRUFDcUIxRyxNQUFNa04sS0FBTixHQUFjLGVBQWVsTixNQUFNa04sS0FBckIsR0FBNkIsR0FBN0IsR0FBbUMsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQW5DLEdBQTBELFNBQTFELEdBQXNFcE4sTUFBTThCLElBQTVFLEdBQW1GLElBQWpHLEdBQXdHLGVBQWU5QixNQUFNNkIsSUFBckIsR0FBNEIsUUFEekosQ0FBUDtBQUVBO0FBUlI7QUFVSDtBQUNELG9CQUFJNkgsUUFBUTNMLEVBQUU4RixJQUFGLENBQVo7QUFDQTZGLHNCQUFNckosSUFBTixDQUFXLFdBQVgsRUFBd0IsS0FBeEI7QUFDQVAsa0JBQUVrTixPQUFGLENBQVVoTixLQUFWLEVBQWlCLFVBQVV5SSxHQUFWLEVBQWVsSSxLQUFmLEVBQXNCO0FBQ25DbUosMEJBQU1ySixJQUFOLENBQVdFLEtBQVgsRUFBa0JrSSxHQUFsQjtBQUNILGlCQUZEO0FBR0FlLDhCQUFjdkwsTUFBZCxDQUFxQnlMLEtBQXJCO0FBQ0gsYUE3QkQ7QUE4QkEsZ0JBQUl2RixXQUFXLEtBQWYsRUFBc0I7QUFDbEJHLHNCQUFNa0ksZUFBTixDQUFzQi9DLEtBQXRCO0FBQ0g7O0FBRUQsZ0JBQUlwRixrQkFBa0IsS0FBS21JLGVBQUwsQ0FBcUJwTyxJQUFyQixDQUEwQixtQkFBMUIsRUFBK0NaLE1BQS9DLEdBQXdELENBQTlFLEVBQWlGLENBRWhGLENBRkQsTUFFTztBQUNIOEcsc0JBQU1rSSxlQUFOLENBQXNCdk8sTUFBdEIsQ0FBNkI2TyxPQUE3QjtBQUNIO0FBQ0R4SSxrQkFBTWtJLGVBQU4sQ0FBc0JwTyxJQUF0QixDQUEyQixrQkFBM0IsRUFBK0NDLE1BQS9DO0FBQ0FxSCxZQUFBLGdGQUFBQSxDQUFlMkgsY0FBZjs7QUFFQTtBQUNBdFAsY0FBRSxrQ0FBa0NzQyxLQUFLZixnQkFBdkMsR0FBMEQsR0FBNUQsRUFBaUUwSSxPQUFqRSxDQUF5RSxPQUF6RTtBQUNIO0FBdEhMOztBQUFBO0FBQUEsSTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7SUFFTXNGLGU7QUFDRiwrQkFBYztBQUFBOztBQUNWLGFBQUt2SixZQUFMLEdBQW9CLElBQUksZ0ZBQUosRUFBcEI7QUFDQSxhQUFLd0osYUFBTCxHQUFxQixJQUFJLGtGQUFKLEVBQXJCO0FBQ0EsYUFBS0MsYUFBTCxHQUFxQixJQUFJLGtGQUFKLEVBQXJCOztBQUVBLFlBQUkseUZBQUo7O0FBRUEsYUFBS3ZDLEtBQUwsR0FBYWxOLEVBQUUsTUFBRixDQUFiO0FBQ0g7Ozs7K0JBRU07QUFDSGhCLFlBQUEscUVBQUFBLENBQVF1TSxlQUFSO0FBQ0EsaUJBQUttRSxXQUFMOztBQUVBLGlCQUFLQyxlQUFMO0FBQ0EsaUJBQUtDLGNBQUw7QUFDQSxpQkFBS0MsWUFBTDtBQUNBLGlCQUFLeFEsTUFBTDtBQUNBLGlCQUFLeVEsYUFBTDs7QUFFQSxpQkFBS04sYUFBTCxDQUFtQk8sSUFBbkI7O0FBRUEsaUJBQUtDLFlBQUw7QUFDQSxpQkFBS0MsYUFBTDs7QUFFQTtBQUNBN08sWUFBQSw0RUFBQUEsQ0FBWUMsY0FBWixDQUEyQmhDLE1BQTNCLEdBQW9DLEVBQXBDOztBQUVBVyxjQUFFLG1EQUFtRCxxRUFBQWhCLENBQVEySCxnQkFBUixHQUEyQnRELFNBQTlFLEdBQTBGLElBQTVGLEVBQWtHNEcsT0FBbEcsQ0FBMEcsT0FBMUc7QUFFSDs7O3NDQUVhO0FBQ1Y7OztBQUdBLGdCQUFJaUcsa0JBQWtCbFEsRUFBRSxnRUFBZ0UscUVBQUFoQixDQUFRMkgsZ0JBQVIsR0FBMkJyRCxNQUEzRixHQUFvRyxJQUF0RyxDQUF0Qjs7QUFFQTRNLDRCQUFnQnhOLE9BQWhCLENBQXdCLElBQXhCLEVBQ0t6QyxRQURMLENBQ2MsUUFEZCxFQUVLeUMsT0FGTCxDQUVhLFdBRmIsRUFFMEJyQyxJQUYxQixDQUUrQiw2QkFGL0IsRUFFOERGLElBRjlELENBRW1FLE1BQU0rUCxnQkFBZ0IvUCxJQUFoQixFQUFOLEdBQStCLEdBRmxHOztBQUlBLGdCQUFJZ1EsbUJBQW1CblEsRUFBRSxpRUFBaUUscUVBQUFoQixDQUFRMkgsZ0JBQVIsR0FBMkJwRCxPQUE1RixHQUFzRyxJQUF4RyxDQUF2Qjs7QUFFQTRNLDZCQUFpQnpOLE9BQWpCLENBQXlCLElBQXpCLEVBQ0t6QyxRQURMLENBQ2MsUUFEZCxFQUVLeUMsT0FGTCxDQUVhLFdBRmIsRUFFMEJyQyxJQUYxQixDQUUrQiw2QkFGL0IsRUFFOERGLElBRjlELENBRW1FLE1BQU1nUSxpQkFBaUJoUSxJQUFqQixFQUFOLEdBQWdDLEdBRm5HOztBQUlBLGdCQUFJLHFFQUFBbkIsQ0FBUW9SLFlBQVIsRUFBSixFQUE0QjtBQUN4QnBRLGtCQUFFLGtCQUFGLEVBQXNCSSxXQUF0QixDQUFrQyxRQUFsQztBQUNIOztBQUVEOzs7QUFHQUosY0FBRSxpRUFBaUUscUVBQUFoQixDQUFRMkgsZ0JBQVIsR0FBMkJuRCxPQUE1RixHQUFzRyxJQUF4RyxFQUNLZCxPQURMLENBQ2EsSUFEYixFQUVLekMsUUFGTCxDQUVjLFFBRmQ7O0FBSUE7OztBQUdBLGdCQUFJb1Esd0JBQXdCclEsRUFBRSx5QkFBRixDQUE1QjtBQUNBcVEsa0NBQXNCdEYsSUFBdEIsQ0FBMkIsU0FBM0IsRUFBc0MsNEVBQUEzSixDQUFZcUMsaUJBQVosSUFBaUMsS0FBdkU7QUFDQTZNLHVCQUFXLFlBQU07QUFDYnRRLGtCQUFFLG1CQUFGLEVBQXVCSSxXQUF2QixDQUFtQyxRQUFuQztBQUNILGFBRkQsRUFFRyxHQUZIO0FBR0FpUSxrQ0FBc0JyQyxFQUF0QixDQUF5QixRQUF6QixFQUFtQyxVQUFVVCxLQUFWLEVBQWlCO0FBQ2hEQSxzQkFBTUMsY0FBTjtBQUNBcE0sZ0JBQUEsNEVBQUFBLENBQVlxQyxpQkFBWixHQUFnQ3pELEVBQUUsSUFBRixFQUFRdVEsRUFBUixDQUFXLFVBQVgsQ0FBaEM7QUFDQXZSLGdCQUFBLHFFQUFBQSxDQUFRME8sV0FBUjtBQUNILGFBSkQ7O0FBTUExTixjQUFFK04sUUFBRixFQUFZQyxFQUFaLENBQWUsT0FBZixFQUF3Qiw0QkFBeEIsRUFBc0QsWUFBVztBQUM3RCxvQkFBSXpELFFBQVF2SyxFQUFFLElBQUYsRUFBUXNDLElBQVIsQ0FBYSxlQUFiLENBQVo7QUFDQXRDLGtCQUFFdUssS0FBRixFQUFTQSxLQUFULENBQWUsTUFBZjtBQUNILGFBSEQ7QUFJSDs7OzBDQUVpQjtBQUNkLGdCQUFJaEUsUUFBUSxJQUFaOztBQUVBO0FBQ0EsZ0JBQUlpSyxXQUFXLEtBQWY7O0FBRUE7QUFDQSxnQkFBSUMsV0FBVyxLQUFmOztBQUVBO0FBQ0EsZ0JBQUlDLFlBQVksS0FBaEI7O0FBRUExUSxjQUFFK04sUUFBRixFQUFZQyxFQUFaLENBQWUsZUFBZixFQUFnQyxVQUFVMkMsQ0FBVixFQUFhO0FBQ3pDO0FBQ0FILDJCQUFXRyxFQUFFQyxPQUFiO0FBQ0E7QUFDQUgsMkJBQVdFLEVBQUVFLE9BQWI7QUFDQTtBQUNBSCw0QkFBWUMsRUFBRUcsUUFBZDtBQUNILGFBUEQ7O0FBU0F2SyxrQkFBTTJHLEtBQU4sQ0FDS2MsRUFETCxDQUNRLE9BRFIsRUFDaUIsc0JBRGpCLEVBQ3lDLFVBQVVULEtBQVYsRUFBaUI7QUFDbERBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUlDLFdBQVd6TixFQUFFLElBQUYsQ0FBZjs7QUFFQSxvQkFBSTBRLFNBQUosRUFBZTtBQUNYLHdCQUFJL0QsWUFBWTVLLEVBQUU2SyxLQUFGLENBQVEscUVBQUE1TixDQUFRb0ssZ0JBQVIsRUFBUixDQUFoQjtBQUNBLHdCQUFJdUQsU0FBSixFQUFlO0FBQ1gsNEJBQUlvRSxhQUFhcEUsVUFBVXBLLFNBQTNCO0FBQ0EsNEJBQUl5TyxlQUFldkQsU0FBU2pMLEtBQVQsRUFBbkI7QUFDQXhDLDBCQUFFLG9CQUFGLEVBQXdCZ0MsSUFBeEIsQ0FBNkIsVUFBVVEsS0FBVixFQUFpQjtBQUMxQyxnQ0FBSUEsUUFBUXVPLFVBQVIsSUFBc0J2TyxTQUFTd08sWUFBbkMsRUFBaUQ7QUFDN0NoUixrQ0FBRSxJQUFGLEVBQVFLLElBQVIsQ0FBYSxzQkFBYixFQUFxQzBLLElBQXJDLENBQTBDLFNBQTFDLEVBQXFELElBQXJEO0FBQ0g7QUFDSix5QkFKRDtBQUtIO0FBQ0osaUJBWEQsTUFXTztBQUNILHdCQUFJLENBQUN5RixRQUFELElBQWEsQ0FBQ0MsUUFBbEIsRUFBNEI7QUFDeEJoRCxpQ0FBUy9LLE9BQVQsQ0FBaUIsaUJBQWpCLEVBQW9DckMsSUFBcEMsQ0FBeUMsc0JBQXpDLEVBQWlFMEssSUFBakUsQ0FBc0UsU0FBdEUsRUFBaUYsS0FBakY7QUFDSDtBQUNKOztBQUVELG9CQUFJa0csZ0JBQWdCeEQsU0FBU3BOLElBQVQsQ0FBYyxzQkFBZCxDQUFwQjtBQUNBNFEsOEJBQWNsRyxJQUFkLENBQW1CLFNBQW5CLEVBQThCLElBQTlCO0FBQ0FwRCxnQkFBQSxvRkFBQUEsQ0FBZTJILGNBQWY7O0FBRUEvSSxzQkFBTVAsWUFBTixDQUFtQlEsY0FBbkIsQ0FBa0NpSCxTQUFTbkwsSUFBVCxFQUFsQztBQUNILGFBM0JMLEVBNEJLMEwsRUE1QkwsQ0E0QlEsVUE1QlIsRUE0Qm9CLHNCQTVCcEIsRUE0QjRDLFVBQVVULEtBQVYsRUFBaUI7QUFDckRBLHNCQUFNQyxjQUFOOztBQUVBLG9CQUFJbEwsT0FBT3RDLEVBQUUsSUFBRixFQUFRc0MsSUFBUixFQUFYO0FBQ0Esb0JBQUlBLEtBQUsrSCxTQUFMLEtBQW1CLElBQXZCLEVBQTZCOztBQUV6QjtBQUNBakosb0JBQUEsNEVBQUFBLENBQVlDLGNBQVosQ0FBMkJoQyxNQUEzQixHQUFvQyxFQUFwQztBQUNBVyxzQkFBRSwyQkFBRixFQUErQjBLLEdBQS9CLENBQW1DLEVBQW5DOztBQUVBMUwsb0JBQUEscUVBQUFBLENBQVF1TSxlQUFSO0FBQ0FoRiwwQkFBTWtKLGFBQU4sQ0FBb0J5QixZQUFwQixDQUFpQzVPLEtBQUtULEVBQXRDO0FBQ0gsaUJBUkQsTUFRTztBQUNILHdCQUFJLENBQUMscUVBQUE3QyxDQUFRb1IsWUFBUixFQUFMLEVBQTZCO0FBQ3pCekksd0JBQUEsb0ZBQUFBLENBQWU4QyxhQUFmO0FBQ0gscUJBRkQsTUFFTyxJQUFJLHFFQUFBekwsQ0FBUW1NLFVBQVIsR0FBcUI5SixjQUFyQixDQUFvQ2tDLE9BQXBDLEtBQWdELE9BQXBELEVBQTZEO0FBQ2hFLDRCQUFJNkksZ0JBQWdCLHFFQUFBcE4sQ0FBUXFLLGdCQUFSLEVBQXBCO0FBQ0EsNEJBQUl0SCxFQUFFOEcsSUFBRixDQUFPdUQsYUFBUCxJQUF3QixDQUE1QixFQUErQjtBQUMzQkksNEJBQUEseURBQUFBLENBQWMyQixnQkFBZCxDQUErQi9CLGFBQS9CO0FBQ0g7QUFDSjtBQUNKO0FBQ0osYUFsREwsRUFtREs0QixFQW5ETCxDQW1EUSxVQW5EUixFQW1Eb0Isa0JBbkRwQixFQW1Ed0MsVUFBVVQsS0FBVixFQUFpQjtBQUNqREEsc0JBQU1DLGNBQU47QUFDQSxvQkFBSXRDLFFBQVFsTCxFQUFFLHFDQUFGLEVBQXlDUCxNQUFyRDtBQUNBTyxrQkFBRSxvREFBb0RrTCxRQUFRLENBQTVELElBQWlFLEtBQW5FLEVBQTBFakIsT0FBMUUsQ0FBa0YsT0FBbEY7QUFDSCxhQXZETCxFQXdESytELEVBeERMLENBd0RRLGFBeERSLEVBd0R1QixrQkF4RHZCLEVBd0QyQyxVQUFVMkMsQ0FBVixFQUFhO0FBQ2hELG9CQUFJLENBQUMzUSxFQUFFLElBQUYsRUFBUUssSUFBUixDQUFhLHNCQUFiLEVBQXFDa1EsRUFBckMsQ0FBd0MsVUFBeEMsQ0FBTCxFQUEwRDtBQUN0RHZRLHNCQUFFLElBQUYsRUFBUWlLLE9BQVIsQ0FBZ0IsT0FBaEI7QUFDSDtBQUNKLGFBNURMLEVBNkRLK0QsRUE3REwsQ0E2RFEsbUJBN0RSLEVBNkQ2QixpQkE3RDdCLEVBNkRnRCxVQUFVMkMsQ0FBVixFQUFhO0FBQ3JELG9CQUFJLENBQUM1TyxFQUFFOEcsSUFBRixDQUFPOEgsRUFBRVEsTUFBRixDQUFTek8sT0FBVCxDQUFpQixrQkFBakIsQ0FBUCxDQUFMLEVBQW1EO0FBQy9DMUMsc0JBQUUsd0NBQUYsRUFBNEMrSyxJQUE1QyxDQUFpRCxTQUFqRCxFQUE0RCxLQUE1RDtBQUNBL0ssc0JBQUUsc0JBQUYsRUFBMEJDLFFBQTFCLENBQW1DLFVBQW5DO0FBQ0FzRywwQkFBTVAsWUFBTixDQUFtQlEsY0FBbkIsQ0FBa0M7QUFDOUIxQyw4QkFBTSxpQkFEd0I7QUFFOUIyQywwQ0FBa0I7QUFGWSxxQkFBbEM7QUFJSDtBQUNKLGFBdEVMO0FBd0VIOzs7eUNBRWdCO0FBQ2IsZ0JBQUlGLFFBQVEsSUFBWjtBQUNBQSxrQkFBTTJHLEtBQU4sQ0FBWWMsRUFBWixDQUFlLE9BQWYsRUFBd0Isb0NBQXhCLEVBQThELFVBQVVULEtBQVYsRUFBaUI7QUFDM0VBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUlDLFdBQVd6TixFQUFFLElBQUYsQ0FBZjtBQUNBLG9CQUFJeU4sU0FBU2xOLFFBQVQsQ0FBa0IsUUFBbEIsQ0FBSixFQUFpQztBQUM3QjtBQUNIO0FBQ0RrTix5QkFBUy9LLE9BQVQsQ0FBaUIsK0JBQWpCLEVBQWtEckMsSUFBbEQsQ0FBdUQsTUFBdkQsRUFBK0RELFdBQS9ELENBQTJFLFFBQTNFO0FBQ0FxTix5QkFBU3hOLFFBQVQsQ0FBa0IsUUFBbEI7O0FBRUFtQixnQkFBQSw0RUFBQUEsQ0FBWUMsY0FBWixDQUEyQmdDLFNBQTNCLEdBQXVDb0ssU0FBU25MLElBQVQsQ0FBYyxNQUFkLENBQXZDOztBQUVBLG9CQUFJbUwsU0FBU25MLElBQVQsQ0FBYyxNQUFkLE1BQTBCLE9BQTlCLEVBQXVDO0FBQ25DdEMsc0JBQUUrTixRQUFGLEVBQVkxTixJQUFaLENBQWlCLHNCQUFqQixFQUF5QzBLLElBQXpDLENBQThDLFVBQTlDLEVBQTBELElBQTFEO0FBQ0gsaUJBRkQsTUFFTztBQUNIL0ssc0JBQUUrTixRQUFGLEVBQVkxTixJQUFaLENBQWlCLHNCQUFqQixFQUF5QzBLLElBQXpDLENBQThDLFVBQTlDLEVBQTBELEtBQTFEO0FBQ0g7O0FBRUQvTCxnQkFBQSxxRUFBQUEsQ0FBUTBPLFdBQVI7O0FBRUFuSCxzQkFBTVAsWUFBTixDQUFtQm9MLFFBQW5CLENBQTRCLElBQTVCLEVBQWtDLElBQWxDO0FBQ0gsYUFwQkQ7QUFxQkEsaUJBQUtDLHdCQUFMO0FBQ0g7Ozt1Q0FHYztBQUNYLGdCQUFJOUssUUFBUSxJQUFaO0FBQ0FBLGtCQUFNMkcsS0FBTixDQUFZYyxFQUFaLENBQWUsT0FBZixFQUF3Qiw0QkFBeEIsRUFBc0QsVUFBVVQsS0FBVixFQUFpQjtBQUNuRUEsc0JBQU1DLGNBQU47QUFDQSxvQkFBSSxDQUFDLHFFQUFBeE8sQ0FBUXNTLGVBQVIsRUFBTCxFQUFnQztBQUM1Qix3QkFBSTdELFdBQVd6TixFQUFFLElBQUYsQ0FBZjtBQUNBLHdCQUFJdVIsVUFBVTlELFNBQVMvSyxPQUFULENBQWlCLElBQWpCLENBQWQ7QUFDQSx3QkFBSUosT0FBT21MLFNBQVNuTCxJQUFULEVBQVg7QUFDQWxCLG9CQUFBLDRFQUFBQSxDQUFZQyxjQUFaLENBQTJCaUIsS0FBS2tDLElBQWhDLElBQXdDbEMsS0FBS0wsS0FBN0M7O0FBRUEsd0JBQUlLLEtBQUtrQyxJQUFMLEtBQWMsU0FBbEIsRUFBNkI7QUFDekJwRCx3QkFBQSw0RUFBQUEsQ0FBWUMsY0FBWixDQUEyQjZCLFNBQTNCLEdBQXVDLENBQXZDO0FBQ0EsNEJBQUlaLEtBQUtMLEtBQUwsS0FBZSxPQUFuQixFQUE0QjtBQUN4QmpDLDhCQUFFK04sUUFBRixFQUFZMU4sSUFBWixDQUFpQixzQkFBakIsRUFBeUMwSyxJQUF6QyxDQUE4QyxVQUE5QyxFQUEwRCxJQUExRDtBQUNILHlCQUZELE1BRU87QUFDSC9LLDhCQUFFK04sUUFBRixFQUFZMU4sSUFBWixDQUFpQixzQkFBakIsRUFBeUMwSyxJQUF6QyxDQUE4QyxVQUE5QyxFQUEwRCxLQUExRDtBQUNIO0FBQ0o7O0FBRUQwQyw2QkFBUy9LLE9BQVQsQ0FBaUIsV0FBakIsRUFBOEJyQyxJQUE5QixDQUFtQyw2QkFBbkMsRUFBa0VGLElBQWxFLENBQXVFLE1BQU1zTixTQUFTdE4sSUFBVCxFQUFOLEdBQXdCLEdBQS9GOztBQUVBbkIsb0JBQUEscUVBQUFBLENBQVEwTyxXQUFSO0FBQ0ExSCxvQkFBQSxnRkFBQUEsQ0FBYTBCLGFBQWI7O0FBRUExSSxvQkFBQSxxRUFBQUEsQ0FBUXVNLGVBQVI7QUFDQWhGLDBCQUFNUCxZQUFOLENBQW1Cb0wsUUFBbkIsQ0FBNEIsSUFBNUI7O0FBRUFHLDRCQUFRbFIsSUFBUixDQUFhLE1BQWIsRUFBcUJELFdBQXJCLENBQWlDLFFBQWpDO0FBQ0FxTiw2QkFBUy9LLE9BQVQsQ0FBaUIsSUFBakIsRUFBdUJ6QyxRQUF2QixDQUFnQyxRQUFoQztBQUNIO0FBQ0osYUE1QkQ7QUE2Qkg7OztpQ0FFUTs7QUFFTCxnQkFBSXNHLFFBQVEsSUFBWjs7QUFFQSxnQkFBSWlMLGVBQWUsU0FBZkEsWUFBZSxHQUFZOztBQUUzQjtBQUNBLG9CQUFJLDRFQUFBcFEsQ0FBWUMsY0FBWixDQUEyQjZCLFNBQTNCLEtBQXlDLENBQUMsQ0FBOUMsRUFBaUQ7O0FBRTdDLHdCQUFJLFFBQU8sNEVBQUE5QixDQUFZQyxjQUFaLENBQTJCaEMsTUFBbEMsS0FBNEN3SCxTQUE1QyxJQUF5RCxDQUFDLDRFQUFBekYsQ0FBWUMsY0FBWixDQUEyQmhDLE1BQXpGLEVBQWlHO0FBQzdGLDRCQUFJb1MsZUFBZSxxRUFBQXpTLENBQVEwUyxlQUFSLEVBQW5CO0FBQ0F0USx3QkFBQSw0RUFBQUEsQ0FBWUMsY0FBWixDQUEyQjZCLFNBQTNCLEdBQXVDdU8sZ0JBQWdCLENBQUMsQ0FBakIsR0FBcUJBLFlBQXJCLEdBQWtDLENBQXpFO0FBQ0g7QUFDSixpQkFORCxNQU9LO0FBQ0Q7QUFDQXpTLG9CQUFBLHFFQUFBQSxDQUFRMlMsZUFBUixDQUF3Qiw0RUFBQXZRLENBQVlDLGNBQVosQ0FBMkI2QixTQUFuRDtBQUNIOztBQUVEO0FBQ0E5QixnQkFBQSw0RUFBQUEsQ0FBWUMsY0FBWixDQUEyQmhDLE1BQTNCLEdBQW9DVyxFQUFFLDJCQUFGLEVBQStCMEssR0FBL0IsRUFBcEMsQ0FoQjJCLENBZ0IrQztBQUMxRSxvQkFBSSw0RUFBQXRKLENBQVlDLGNBQVosQ0FBMkJoQyxNQUEvQixFQUF1QztBQUNuQytCLG9CQUFBLDRFQUFBQSxDQUFZQyxjQUFaLENBQTJCNkIsU0FBM0IsR0FBdUMsQ0FBQyxDQUF4QztBQUNILGlCQUZELE1BR0s7QUFDRDtBQUNBLHdCQUFJLDRFQUFBOUIsQ0FBWUMsY0FBWixDQUEyQjZCLFNBQTNCLElBQXdDLENBQUMsQ0FBN0MsRUFBZ0Q7QUFDNUM5Qix3QkFBQSw0RUFBQUEsQ0FBWUMsY0FBWixDQUEyQjZCLFNBQTNCLEdBQXVDLENBQXZDO0FBQ0g7QUFDSjtBQUNKLGFBMUJEOztBQTRCQXNPOztBQUVBLGdCQUFJSSxlQUFnQixZQUFVO0FBQ3RCLG9CQUFJQyxRQUFRLENBQVo7QUFDQSx1QkFBTyxVQUFTekgsUUFBVCxFQUFtQjBILEVBQW5CLEVBQXNCO0FBQ3pCQyxpQ0FBY0YsS0FBZDtBQUNBQSw0QkFBUXZCLFdBQVdsRyxRQUFYLEVBQXFCMEgsRUFBckIsQ0FBUjtBQUNILGlCQUhEO0FBSUgsYUFOYyxFQUFuQjtBQUFBLGdCQU9JRSxVQUFVLDJCQVBkO0FBQUEsZ0JBUUlDLGlCQUFpQmpTLEVBQUVnUyxPQUFGLENBUnJCO0FBQUEsZ0JBU0lFLGFBQWEseUJBVGpCO0FBQUEsZ0JBWUlDLFlBQVksU0FBWkEsU0FBWSxDQUFVQyxPQUFWLEVBQW1CO0FBQzNCaFIsZ0JBQUEsNEVBQUFBLENBQVlDLGNBQVosQ0FBMkJoQyxNQUEzQixHQUFvQytTLE9BQXBDO0FBQ0FaO0FBQ0F4UyxnQkFBQSxxRUFBQUEsQ0FBUTBPLFdBQVI7QUFDQTFPLGdCQUFBLHFFQUFBQSxDQUFRdU0sZUFBUjtBQUNBaEYsc0JBQU1QLFlBQU4sQ0FBbUJvTCxRQUFuQixDQUE0QixJQUE1QjtBQUNILGFBbEJMOztBQW9CQTtBQUNBLGdCQUFJLENBQUVhLGVBQWV2SCxHQUFmLEVBQU4sRUFBNEI7QUFDeEIxSyxrQkFBRWtTLFVBQUYsRUFBY2pTLFFBQWQsQ0FBdUIsUUFBdkI7QUFDSDs7QUFFRDtBQUNBRCxjQUFFLGtCQUFGLEVBQXNCa08sR0FBdEIsQ0FBMEIsT0FBMUIsRUFBbUNnRSxVQUFuQyxFQUErQ2xFLEVBQS9DLENBQWtELE9BQWxELEVBQTJEa0UsVUFBM0QsRUFBdUUsWUFBWTtBQUMvRUQsK0JBQWV2SCxHQUFmLENBQW1CLEVBQW5CO0FBQ0F5SCwwQkFBVSxFQUFWO0FBQ0gsYUFIRDs7QUFNQW5TLGNBQUUsd0NBQUYsRUFBNENrTyxHQUE1QyxDQUFnRCxPQUFoRCxFQUF5RDhELE9BQXpELEVBQWtFaEUsRUFBbEUsQ0FBcUUsT0FBckUsRUFBOEVnRSxPQUE5RSxFQUF1RixVQUFTckIsQ0FBVCxFQUFXO0FBQzlGQSxrQkFBRW5ELGNBQUY7QUFDQSxvQkFBSTZFLFFBQVFyUyxFQUFFLElBQUYsQ0FBWjtBQUNBLG9CQUFJc1MsVUFBVXRTLEVBQUUyUSxFQUFFNEIsYUFBSixDQUFkO0FBQ0Esb0JBQUlDLGFBQWFGLFFBQVE1UCxPQUFSLENBQWdCLHVCQUFoQixFQUF5Q3JDLElBQXpDLENBQThDNlIsVUFBOUMsQ0FBakI7QUFDQSxvQkFBR00sV0FBVy9TLE1BQVgsR0FBb0IsQ0FBdkIsRUFBeUI7QUFDckIsd0JBQUc2UyxRQUFRNUgsR0FBUixNQUFpQixFQUFwQixFQUF1QjtBQUNuQiw0QkFBRzhILFdBQVdqUyxRQUFYLENBQW9CLFFBQXBCLENBQUgsRUFBaUM7QUFDN0JpUyx1Q0FBV3BTLFdBQVgsQ0FBdUIsUUFBdkI7QUFDSDtBQUNKLHFCQUpELE1BS0k7QUFDQSw0QkFBRyxDQUFDb1MsV0FBV2pTLFFBQVgsQ0FBb0IsUUFBcEIsQ0FBSixFQUFrQztBQUM5QmlTLHVDQUFXdlMsUUFBWCxDQUFvQixRQUFwQjtBQUNIO0FBQ0o7QUFDRDJSLGlDQUFhLFlBQVU7QUFDbkJPLGtDQUFVRSxNQUFNM0gsR0FBTixFQUFWO0FBQ0gscUJBRkQsRUFFRyxHQUZIO0FBR0g7QUFDSixhQXBCRDs7QUFzQkE7QUFDQW5FLGtCQUFNMkcsS0FBTixDQUFZYyxFQUFaLENBQWUsUUFBZixFQUF5Qix1QkFBekIsRUFBa0QsVUFBVVQsS0FBVixFQUFpQjtBQUMvREEsc0JBQU1DLGNBQU47QUFDQSx1QkFBTyxLQUFQO0FBQ0gsYUFIRDtBQUlIOzs7d0NBRWU7QUFDWixnQkFBSWpILFFBQVEsSUFBWjs7QUFFQUEsa0JBQU0yRyxLQUFOLENBQ0tjLEVBREwsQ0FDUSxPQURSLEVBQ2lCLDBEQURqQixFQUM2RSxVQUFVVCxLQUFWLEVBQWlCO0FBQ3RGQSxzQkFBTUMsY0FBTjs7QUFFQXhPLGdCQUFBLHFFQUFBQSxDQUFRdU0sZUFBUjs7QUFFQSxvQkFBSW9DLGNBQWMsT0FBT3hPLE9BQU82QixPQUFQLENBQWVxTSxHQUF0QixLQUE4QixXQUE5QixHQUE0Q2xPLE9BQU82QixPQUFQLENBQWVxTSxHQUFmLENBQW1CL0ssSUFBbkIsQ0FBd0IsVUFBeEIsQ0FBNUMsR0FBa0Z1RSxTQUFwRztBQUNBLG9CQUFJLE9BQU84RyxXQUFQLEtBQXVCLFdBQXZCLElBQXNDQSxZQUFZbE8sTUFBWixHQUFxQixDQUEzRCxJQUFnRSxPQUFPa08sWUFBWSxDQUFaLEVBQWVwTSxnQkFBdEIsS0FBMkMsV0FBL0csRUFBNEg7QUFDeEhnRiwwQkFBTVAsWUFBTixDQUFtQm9MLFFBQW5CLENBQTRCLElBQTVCLEVBQWtDLElBQWxDO0FBQ0gsaUJBRkQsTUFHSTdLLE1BQU1QLFlBQU4sQ0FBbUJvTCxRQUFuQixDQUE0QixJQUE1QixFQUFrQyxLQUFsQztBQUNQLGFBWEwsRUFZS3BELEVBWkwsQ0FZUSxPQVpSLEVBWWlCLDZCQVpqQixFQVlnRCxVQUFVVCxLQUFWLEVBQWlCO0FBQ3pEQSxzQkFBTUMsY0FBTjtBQUNBeE4sa0JBQUUsNkVBQUYsRUFBaUZpSyxPQUFqRixDQUF5RixPQUF6RjtBQUNILGFBZkwsRUFnQksrRCxFQWhCTCxDQWdCUSxRQWhCUixFQWdCa0Isa0JBaEJsQixFQWdCc0MsVUFBVVQsS0FBVixFQUFpQjtBQUMvQ0Esc0JBQU1DLGNBQU47QUFDQSxvQkFBSWlGLFNBQVN6UyxFQUFFLElBQUYsRUFBUUssSUFBUixDQUFhLGtCQUFiLENBQWI7QUFDQSxvQkFBSXFTLGFBQWFELE9BQU8vSCxHQUFQLEVBQWpCO0FBQ0FuRSxzQkFBTWtKLGFBQU4sQ0FBb0JrRCxNQUFwQixDQUEyQkQsVUFBM0I7QUFDQUQsdUJBQU8vSCxHQUFQLENBQVcsRUFBWDtBQUNILGFBdEJMLEVBdUJLc0QsRUF2QkwsQ0F1QlEsT0F2QlIsRUF1QmlCLG1CQXZCakIsRUF1QnNDLFVBQVVULEtBQVYsRUFBaUI7QUFDL0NBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUlvRixXQUFXLENBQUMsNEVBQUF4UixDQUFZQyxjQUFaLENBQTJCaEMsTUFBNUIsR0FBcUNXLEVBQUUsSUFBRixFQUFRc0MsSUFBUixDQUFhLFFBQWIsQ0FBckMsR0FBOEQsQ0FBQyxDQUE5RTtBQUNBdEQsZ0JBQUEscUVBQUFBLENBQVF1TSxlQUFSO0FBQ0FoRixzQkFBTWtKLGFBQU4sQ0FBb0J5QixZQUFwQixDQUFpQzBCLFFBQWpDO0FBQ0gsYUE1QkwsRUE2Qks1RSxFQTdCTCxDQTZCUSxPQTdCUixFQTZCaUIsa0JBN0JqQixFQTZCcUMsVUFBVVQsS0FBVixFQUFpQjtBQUM5Q0Esc0JBQU1DLGNBQU47QUFDQTdGLGdCQUFBLG9GQUFBQSxDQUFla0wsa0JBQWYsQ0FBa0M3UyxFQUFFLElBQUYsRUFBUXNDLElBQVIsQ0FBYSxRQUFiLENBQWxDLEVBQTBELFVBQVUrRSxHQUFWLEVBQWU7QUFDckVySSxvQkFBQSxxRUFBQUEsQ0FBUXVNLGVBQVI7QUFDQWhGLDBCQUFNUCxZQUFOLENBQW1Cb0wsUUFBbkIsQ0FBNEIsSUFBNUI7QUFDSCxpQkFIRDtBQUlILGFBbkNMO0FBcUNIOzs7dUNBRWM7QUFDWCxnQkFBSTdLLFFBQVEsSUFBWjtBQUNBO0FBQ0FBLGtCQUFNMkcsS0FBTixDQUFZYyxFQUFaLENBQWUsZUFBZixFQUFnQyxxQkFBaEMsRUFBdUQsVUFBVVQsS0FBVixFQUFpQjtBQUNwRTVGLGdCQUFBLG9GQUFBQSxDQUFlbUwsaUJBQWY7QUFDSCxhQUZEO0FBR0F2TSxrQkFBTTJHLEtBQU4sQ0FBWWMsRUFBWixDQUFlLFFBQWYsRUFBeUIsa0NBQXpCLEVBQTZELFVBQVVULEtBQVYsRUFBaUI7QUFDMUVBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUlwTCxRQUFRLEVBQVo7QUFDQSxvQkFBSTJRLFFBQVEvUyxFQUFFLElBQUYsQ0FBWjs7QUFFQUEsa0JBQUUsbUNBQUYsRUFBdUNnQyxJQUF2QyxDQUE0QyxZQUFZO0FBQ3BELHdCQUFJeUwsV0FBV3pOLEVBQUUsSUFBRixDQUFmO0FBQ0Esd0JBQUlzQyxPQUFPbUwsU0FBUy9LLE9BQVQsQ0FBaUIsYUFBakIsRUFBZ0NKLElBQWhDLEVBQVg7QUFDQUEseUJBQUt5QixJQUFMLEdBQVkwSixTQUFTL0MsR0FBVCxFQUFaO0FBQ0F0SSwwQkFBTUQsSUFBTixDQUFXRyxJQUFYO0FBQ0gsaUJBTEQ7O0FBT0FxRixnQkFBQSxvRkFBQUEsQ0FBZTBELGFBQWYsQ0FBNkI7QUFDekJySCw0QkFBUStPLE1BQU16USxJQUFOLENBQVcsUUFBWCxDQURpQjtBQUV6QkcsOEJBQVVMO0FBRmUsaUJBQTdCLEVBR0csVUFBVWlGLEdBQVYsRUFBZTtBQUNkLHdCQUFJLENBQUNBLElBQUlXLEtBQVQsRUFBZ0I7QUFDWitLLDhCQUFNclEsT0FBTixDQUFjLFFBQWQsRUFBd0I2SCxLQUF4QixDQUE4QixNQUE5QjtBQUNBaEUsOEJBQU1QLFlBQU4sQ0FBbUJvTCxRQUFuQixDQUE0QixJQUE1QjtBQUNILHFCQUhELE1BR087QUFDSHBSLDBCQUFFLGlDQUFGLEVBQXFDZ0MsSUFBckMsQ0FBMEMsWUFBWTtBQUNsRCxnQ0FBSXlMLFdBQVd6TixFQUFFLElBQUYsQ0FBZjtBQUNBLGdDQUFJK0IsRUFBRXVILFFBQUYsQ0FBV2pDLElBQUkvRSxJQUFmLEVBQXFCbUwsU0FBU25MLElBQVQsQ0FBYyxJQUFkLENBQXJCLENBQUosRUFBK0M7QUFDM0NtTCx5Q0FBU3hOLFFBQVQsQ0FBa0IsV0FBbEI7QUFDSCw2QkFGRCxNQUVPO0FBQ0h3Tix5Q0FBU3JOLFdBQVQsQ0FBcUIsV0FBckI7QUFDSDtBQUNKLHlCQVBEO0FBUUg7QUFDSixpQkFqQkQ7QUFrQkgsYUE5QkQ7O0FBZ0NBO0FBQ0FtRyxrQkFBTTJHLEtBQU4sQ0FBWWMsRUFBWixDQUFlLFFBQWYsRUFBeUIsb0JBQXpCLEVBQStDLFVBQVVULEtBQVYsRUFBaUI7QUFDNURBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUlwTCxRQUFRLEVBQVo7QUFDQSxvQkFBSTJRLFFBQVEvUyxFQUFFLElBQUYsQ0FBWjs7QUFFQStCLGtCQUFFQyxJQUFGLENBQU8scUVBQUFoRCxDQUFRb0ssZ0JBQVIsRUFBUCxFQUFtQyxVQUFVbkgsS0FBVixFQUFpQjtBQUNoREcsMEJBQU1ELElBQU4sQ0FBVztBQUNQTiw0QkFBSUksTUFBTUosRUFESDtBQUVQd0ksbUNBQVdwSSxNQUFNb0k7QUFGVixxQkFBWDtBQUlILGlCQUxEOztBQU9BMUMsZ0JBQUEsb0ZBQUFBLENBQWUwRCxhQUFmLENBQTZCO0FBQ3pCckgsNEJBQVErTyxNQUFNelEsSUFBTixDQUFXLFFBQVgsQ0FEaUI7QUFFekJHLDhCQUFVTDtBQUZlLGlCQUE3QixFQUdHLFVBQVVpRixHQUFWLEVBQWU7QUFDZDBMLDBCQUFNclEsT0FBTixDQUFjLFFBQWQsRUFBd0I2SCxLQUF4QixDQUE4QixNQUE5QjtBQUNBLHdCQUFJLENBQUNsRCxJQUFJVyxLQUFULEVBQWdCO0FBQ1p6Qiw4QkFBTVAsWUFBTixDQUFtQm9MLFFBQW5CLENBQTRCLElBQTVCO0FBQ0g7QUFDSixpQkFSRDtBQVNILGFBckJEOztBQXVCQTtBQUNBN0ssa0JBQU0yRyxLQUFOLENBQVljLEVBQVosQ0FBZSxRQUFmLEVBQXlCLDZCQUF6QixFQUF3RCxVQUFVVCxLQUFWLEVBQWlCO0FBQ3JFQSxzQkFBTUMsY0FBTjtBQUNBLG9CQUFJdUYsUUFBUS9TLEVBQUUsSUFBRixDQUFaOztBQUVBMkgsZ0JBQUEsb0ZBQUFBLENBQWUwRCxhQUFmLENBQTZCO0FBQ3pCckgsNEJBQVErTyxNQUFNelEsSUFBTixDQUFXLFFBQVg7QUFEaUIsaUJBQTdCLEVBRUcsVUFBVStFLEdBQVYsRUFBZTtBQUNkMEwsMEJBQU1yUSxPQUFOLENBQWMsUUFBZCxFQUF3QjZILEtBQXhCLENBQThCLE1BQTlCO0FBQ0FoRSwwQkFBTVAsWUFBTixDQUFtQm9MLFFBQW5CLENBQTRCLElBQTVCO0FBQ0gsaUJBTEQ7QUFNSCxhQVZEOztBQVlBO0FBQ0EsZ0JBQUk0QixRQUFRLEVBQVo7QUFDQSxnQkFBSUMsZUFBZWpULEVBQUUsZUFBRixDQUFuQjtBQUNBLGdCQUFJa1QsZ0JBQWdCbFQsRUFBRSxpQkFBRixDQUFwQjtBQUNBaVQseUJBQWFqRixFQUFiLENBQWdCLFFBQWhCLEVBQTBCLFVBQVVULEtBQVYsRUFBaUI7QUFDdkNBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUl4TixFQUFFLElBQUYsRUFBUTBLLEdBQVIsT0FBa0IsTUFBdEIsRUFBOEI7QUFDMUJ3SSxrQ0FBY3hRLE9BQWQsQ0FBc0IsYUFBdEIsRUFBcUN0QyxXQUFyQyxDQUFpRCxRQUFqRDtBQUNILGlCQUZELE1BRU87QUFDSDhTLGtDQUFjeFEsT0FBZCxDQUFzQixhQUF0QixFQUFxQ3pDLFFBQXJDLENBQThDLFFBQTlDO0FBQ0g7QUFDSixhQVBELEVBT0dnSyxPQVBILENBT1csUUFQWDtBQVFBMUQsa0JBQU0yRyxLQUFOLENBQ0tjLEVBREwsQ0FDUSxlQURSLEVBQ3lCLG9CQUR6QixFQUMrQyxVQUFVVCxLQUFWLEVBQWlCO0FBQ3hEMEYsNkJBQWF2SSxHQUFiLENBQWlCLFVBQWpCLEVBQTZCVCxPQUE3QixDQUFxQyxRQUFyQztBQUNBaUosOEJBQWN4SSxHQUFkLENBQWtCLEVBQWxCOztBQUVBLG9CQUFJeUksZ0JBQWdCLHFFQUFBblUsQ0FBUW9LLGdCQUFSLEVBQXBCOztBQUVBLG9CQUFJckgsRUFBRThHLElBQUYsQ0FBT3NLLGFBQVAsTUFBMEIsQ0FBOUIsRUFBaUM7O0FBRTdCLHdCQUFJQyxZQUFZLElBQWhCO0FBQ0FwVCxzQkFBRWdDLElBQUYsQ0FBT21SLGFBQVAsRUFBc0IsVUFBVTNRLEtBQVYsRUFBaUJvRCxFQUFqQixFQUFxQjtBQUN2Qyw0QkFBSUEsR0FBR3dOLFNBQUgsSUFBZ0IsQ0FBcEIsRUFBdUI7QUFDbkJBLHdDQUFZLEtBQVo7QUFDSDtBQUNKLHFCQUpEOztBQU1BLHdCQUFJQSxTQUFKLEVBQWU7QUFDWEgscUNBQWF2SSxHQUFiLENBQWlCLFVBQWpCLEVBQTZCVCxPQUE3QixDQUFxQyxRQUFyQztBQUNILHFCQUZELE1BRU87QUFDSGpLLDBCQUFFK0csSUFBRixDQUFPO0FBQ0g3SCxpQ0FBS1UsYUFBYXlULFNBRGY7QUFFSDdPLGtDQUFNLEtBRkg7QUFHSHlDLHNDQUFVLE1BSFA7QUFJSEcscUNBQVMsaUJBQVVDLEdBQVYsRUFBZTtBQUNwQixvQ0FBSSxDQUFDQSxJQUFJVyxLQUFULEVBQWdCO0FBQ1prTCxrREFBYy9TLElBQWQsQ0FBbUIsRUFBbkI7QUFDQTZTLDRDQUFRM0wsSUFBSS9FLElBQVo7QUFDQVAsc0NBQUVDLElBQUYsQ0FBT2dSLEtBQVAsRUFBYyxVQUFVL1EsS0FBVixFQUFpQjtBQUMzQiw0Q0FBSXFSLFNBQVMsb0JBQW9CclIsTUFBTUosRUFBMUIsR0FBK0IsSUFBL0IsR0FBc0NJLE1BQU04QixJQUE1QyxHQUFtRCxXQUFoRTtBQUNBbVAsc0RBQWNoVCxNQUFkLENBQXFCb1QsTUFBckI7QUFDSCxxQ0FIRDtBQUlILGlDQVBELE1BT087QUFDSC9PLG9DQUFBLG9GQUFBQSxDQUFla0IsV0FBZixDQUEyQixPQUEzQixFQUFvQzRCLElBQUk1QyxPQUF4QyxFQUFpRDdCLGdCQUFnQjhDLFlBQWhCLENBQTZCakIsT0FBN0IsQ0FBcUNrQixZQUF0RjtBQUNIO0FBQ0osNkJBZkU7QUFnQkhxQyxtQ0FBTyxlQUFVMUYsSUFBVixFQUFnQjtBQUNuQmlDLGdDQUFBLG9GQUFBQSxDQUFlMEQsV0FBZixDQUEyQjNGLElBQTNCO0FBQ0g7QUFsQkUseUJBQVA7QUFvQkg7QUFDSixpQkFqQ0QsTUFpQ087QUFDSCx3QkFBSWlSLGVBQWV4UixFQUFFNkssS0FBRixDQUFRdUcsYUFBUixDQUFuQjs7QUFFQSx3QkFBSUksYUFBYUgsU0FBakIsRUFBNEI7QUFDeEJILHFDQUFhdkksR0FBYixDQUFpQixVQUFqQixFQUE2QlQsT0FBN0IsQ0FBcUMsUUFBckM7QUFDSCxxQkFGRCxNQUVPO0FBQ0hqSywwQkFBRStHLElBQUYsQ0FBTztBQUNIN0gsaUNBQUtVLGFBQWE0VCxnQkFEZjtBQUVIaFAsa0NBQU0sS0FGSDtBQUdIbEMsa0NBQU07QUFDRm1SLDBDQUFVRixhQUFhMVIsRUFEckI7QUFFRndJLDJDQUFXa0osYUFBYWxKO0FBRnRCLDZCQUhIO0FBT0hwRCxzQ0FBVSxNQVBQO0FBUUhHLHFDQUFTLGlCQUFVQyxHQUFWLEVBQWU7QUFDcEIsb0NBQUksQ0FBQ0EsSUFBSVcsS0FBVCxFQUFnQjtBQUNaa0wsa0RBQWMvUyxJQUFkLENBQW1CLEVBQW5CO0FBQ0E2Uyw0Q0FBUTNMLElBQUkvRSxJQUFKLENBQVMwUSxLQUFqQjtBQUNBLHdDQUFJVSxnQkFBZ0IsQ0FBcEI7QUFDQTNSLHNDQUFFQyxJQUFGLENBQU9nUixLQUFQLEVBQWMsVUFBVS9RLEtBQVYsRUFBaUI7QUFDM0IsNENBQUkwUixhQUFhMVIsTUFBTTJSLFdBQXZCO0FBQ0EsNENBQUlELFVBQUosRUFBZ0I7QUFDWkQ7QUFDSDtBQUNELDRDQUFJSixTQUFTLG9CQUFvQnJSLE1BQU1KLEVBQTFCLEdBQStCLElBQS9CLElBQXVDOFIsYUFBYSxVQUFiLEdBQTBCLEVBQWpFLElBQXVFLEdBQXZFLEdBQTZFMVIsTUFBTThCLElBQW5GLEdBQTBGLFdBQXZHO0FBQ0FtUCxzREFBY2hULE1BQWQsQ0FBcUJvVCxNQUFyQjtBQUNILHFDQVBEO0FBUUEsd0NBQUlJLGdCQUFnQixDQUFwQixFQUF1QjtBQUNuQlQscURBQWF2SSxHQUFiLENBQWlCLE1BQWpCLEVBQXlCVCxPQUF6QixDQUFpQyxRQUFqQztBQUNIO0FBQ0osaUNBZkQsTUFlTztBQUNIMUYsb0NBQUEsb0ZBQUFBLENBQWVrQixXQUFmLENBQTJCLE9BQTNCLEVBQW9DNEIsSUFBSTVDLE9BQXhDLEVBQWlEN0IsZ0JBQWdCOEMsWUFBaEIsQ0FBNkJqQixPQUE3QixDQUFxQ2tCLFlBQXRGO0FBQ0g7QUFDSiw2QkEzQkU7QUE0QkhxQyxtQ0FBTyxlQUFVMUYsSUFBVixFQUFnQjtBQUNuQmlDLGdDQUFBLG9GQUFBQSxDQUFlMEQsV0FBZixDQUEyQjNGLElBQTNCO0FBQ0g7QUE5QkUseUJBQVA7QUFnQ0g7QUFDSjtBQUNKLGFBaEZMLEVBaUZLMEwsRUFqRkwsQ0FpRlEsUUFqRlIsRUFpRmtCLDZCQWpGbEIsRUFpRmlELFVBQVVULEtBQVYsRUFBaUI7QUFDMURBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUl1RixRQUFRL1MsRUFBRSxJQUFGLENBQVo7O0FBRUEsb0JBQUlvQyxRQUFRLEVBQVo7QUFDQUwsa0JBQUVDLElBQUYsQ0FBTyxxRUFBQWhELENBQVFvSyxnQkFBUixFQUFQLEVBQW1DLFVBQVVuSCxLQUFWLEVBQWlCO0FBQ2hERywwQkFBTUQsSUFBTixDQUFXO0FBQ1BOLDRCQUFJSSxNQUFNSixFQURIO0FBRVB3SSxtQ0FBV3BJLE1BQU1vSTtBQUZWLHFCQUFYO0FBSUgsaUJBTEQ7O0FBT0ExQyxnQkFBQSxvRkFBQUEsQ0FBZTBELGFBQWYsQ0FBNkI7QUFDekJySCw0QkFBUStPLE1BQU16USxJQUFOLENBQVcsUUFBWCxDQURpQjtBQUV6QkcsOEJBQVVMLEtBRmU7QUFHekJ5UixrQ0FBY1osYUFBYXZJLEdBQWIsRUFIVztBQUl6QnNJLDJCQUFPRSxjQUFjeEksR0FBZDtBQUprQixpQkFBN0IsRUFLRyxVQUFVckQsR0FBVixFQUFlO0FBQ2QwTCwwQkFBTXJRLE9BQU4sQ0FBYyxRQUFkLEVBQXdCNkgsS0FBeEIsQ0FBOEIsTUFBOUI7QUFDQWhFLDBCQUFNUCxZQUFOLENBQW1Cb0wsUUFBbkIsQ0FBNEIsSUFBNUI7QUFDSCxpQkFSRDtBQVNILGFBdEdMLEVBdUdLcEQsRUF2R0wsQ0F1R1EsUUF2R1IsRUF1R2tCLGlDQXZHbEIsRUF1R3FELFVBQVVULEtBQVYsRUFBaUI7QUFDOURBLHNCQUFNQyxjQUFOO0FBQ0Esb0JBQUl1RixRQUFRL1MsRUFBRSxJQUFGLENBQVo7O0FBRUEsb0JBQUlvQyxRQUFRLEVBQVo7QUFDQSxvQkFBSTBSLGlCQUFpQixxRUFBQTlVLENBQVFvSyxnQkFBUixFQUFyQjtBQUNBckgsa0JBQUVDLElBQUYsQ0FBTzhSLGNBQVAsRUFBdUIsVUFBVTdSLEtBQVYsRUFBaUI7QUFDcENHLDBCQUFNRCxJQUFOLENBQVc7QUFDUE4sNEJBQUlJLE1BQU1KLEVBREg7QUFFUHdJLG1DQUFXcEksTUFBTW9JO0FBRlYscUJBQVg7QUFJSCxpQkFMRDs7QUFPQTFDLGdCQUFBLG9GQUFBQSxDQUFlMEQsYUFBZixDQUE2QjtBQUN6QnJILDRCQUFRK08sTUFBTXpRLElBQU4sQ0FBVyxRQUFYLENBRGlCO0FBRXpCRyw4QkFBVUwsS0FGZTtBQUd6QndJLG9DQUFnQjVLLEVBQUUsd0JBQUYsRUFBNEIwSyxHQUE1QixFQUhTO0FBSXpCRyxxQ0FBaUI3SyxFQUFFLHVCQUFGLEVBQTJCMEssR0FBM0IsRUFKUTtBQUt6QkksZ0NBQVk5SyxFQUFFLDBCQUFGLEVBQThCMEssR0FBOUI7QUFMYSxpQkFBN0IsRUFNRyxVQUFVckQsR0FBVixFQUFlO0FBQ2QwTCwwQkFBTXJRLE9BQU4sQ0FBYyxRQUFkLEVBQXdCNkgsS0FBeEIsQ0FBOEIsTUFBOUI7QUFDQXhJLHNCQUFFQyxJQUFGLENBQU84UixjQUFQLEVBQXVCLFVBQVU3UixLQUFWLEVBQWlCO0FBQ3BDLDRCQUFJQSxNQUFNSixFQUFOLEtBQWF3RixJQUFJL0UsSUFBSixDQUFTVCxFQUExQixFQUE4QjtBQUMxQjdCLDhCQUFFLGtDQUFrQ2lDLE1BQU1KLEVBQXhDLEdBQTZDLEdBQS9DLEVBQW9EUyxJQUFwRCxDQUF5RCtFLElBQUkvRSxJQUE3RDtBQUNIO0FBQ0oscUJBSkQ7QUFLSCxpQkFiRDtBQWNILGFBbElMOztBQW9JQSxnQkFBSSw0RUFBQWxCLENBQVlDLGNBQVosQ0FBMkJrQyxPQUEzQixLQUF1QyxPQUEzQyxFQUFvRDtBQUNoRHZELGtCQUFFK04sUUFBRixFQUFZMU4sSUFBWixDQUFpQixzQkFBakIsRUFBeUMwSyxJQUF6QyxDQUE4QyxVQUE5QyxFQUEwRCxJQUExRDtBQUNILGFBRkQsTUFFTztBQUNIL0ssa0JBQUUrTixRQUFGLEVBQVkxTixJQUFaLENBQWlCLHNCQUFqQixFQUF5QzBLLElBQXpDLENBQThDLFVBQTlDLEVBQTBELEtBQTFEO0FBQ0g7O0FBRUQsaUJBQUtzRyx3QkFBTDtBQUNIOzs7NENBRW1CakYsYSxFQUFlO0FBQy9CLGdCQUFJLE9BQU9qTixPQUFPNkIsT0FBUCxDQUFlcU0sR0FBdEIsS0FBOEIsV0FBbEMsRUFBK0M7QUFDM0Msb0JBQUlWLFlBQVk1SyxFQUFFNkssS0FBRixDQUFRUixhQUFSLENBQWhCO0FBQ0Esb0JBQUl1QixjQUFjeE8sT0FBTzZCLE9BQVAsQ0FBZXFNLEdBQWYsQ0FBbUIvSyxJQUFuQixDQUF3QixVQUF4QixDQUFsQjtBQUNBLG9CQUFJLE9BQU9xTCxXQUFQLEtBQXVCLFdBQXZCLElBQXNDLE9BQU9BLFlBQVksQ0FBWixDQUFQLEtBQTBCLFdBQWhFLElBQStFLE9BQU9BLFlBQVksQ0FBWixFQUFlb0csU0FBdEIsS0FBb0MsV0FBbkgsSUFBa0lwSCxjQUFjLFdBQWhKLElBQ0dBLFVBQVVuSSxJQUFWLEtBQW1CLFdBRDFCLEVBQ3VDO0FBQ25DLHdCQUFJLENBQUNtSixZQUFZLENBQVosRUFBZW9HLFNBQWYsQ0FBeUJ2VSxLQUF6QixDQUErQm1OLFVBQVVuSSxJQUF6QyxDQUFMLEVBQXFEO0FBQ2pELCtCQUFPLEtBQVA7QUFDSCxxQkFGRCxNQUVPO0FBQ0gsNEJBQUssT0FBT21KLFlBQVksQ0FBWixFQUFlcUcsV0FBdEIsS0FBc0MsV0FBdEMsSUFBcURoVSxFQUFFaVUsT0FBRixDQUFVdEcsWUFBWSxDQUFaLEVBQWVxRyxXQUF6QixDQUExRCxFQUFpRztBQUM3RixnQ0FBSWhVLEVBQUVrVSxPQUFGLENBQVV2SCxVQUFVdkIsU0FBcEIsRUFBK0J1QyxZQUFZLENBQVosRUFBZXFHLFdBQTlDLEtBQThELENBQUMsQ0FBbkUsRUFBc0U7QUFDbEUsdUNBQU8sS0FBUDtBQUNIO0FBQ0o7QUFDSjtBQUNKO0FBQ0o7QUFDRCxtQkFBTyxJQUFQO0FBQ0g7OzttREFFMEI7QUFDdkIsZ0JBQUlHLGNBQWNuVSxFQUFFLGlCQUFGLENBQWxCO0FBQ0EsZ0JBQUl1RyxRQUFRLElBQVo7QUFDQTROLHdCQUFZakcsR0FBWixDQUFnQixPQUFoQixFQUF5QixzQkFBekIsRUFBaURGLEVBQWpELENBQW9ELE9BQXBELEVBQTZELHNCQUE3RCxFQUFxRixVQUFVVCxLQUFWLEVBQWlCO0FBQ2xHQSxzQkFBTUMsY0FBTjtBQUNBLG9CQUFJcEIsZ0JBQWdCLHFFQUFBcE4sQ0FBUXFLLGdCQUFSLEVBQXBCO0FBQ0Esb0JBQUl0SCxFQUFFOEcsSUFBRixDQUFPdUQsYUFBUCxJQUF3QixDQUE1QixFQUErQjtBQUMzQmpOLDJCQUFPNkIsT0FBUCxDQUFlQyxPQUFmLENBQXVCNkYsYUFBdkIsQ0FBcUNzRixhQUFyQyxFQUFvRGpOLE9BQU82QixPQUFQLENBQWVxTSxHQUFuRTtBQUNBLHdCQUFJOUcsTUFBTTZOLG1CQUFOLENBQTBCaEksYUFBMUIsQ0FBSixFQUE4QztBQUMxQytILG9DQUFZOVQsSUFBWixDQUFpQixRQUFqQixFQUEyQjRKLE9BQTNCLENBQW1DLE9BQW5DO0FBQ0g7QUFDSjtBQUNKLGFBVEQ7O0FBV0FrSyx3QkFBWWpHLEdBQVosQ0FBZ0IsVUFBaEIsRUFBNEIsc0JBQTVCLEVBQW9ERixFQUFwRCxDQUF1RCxVQUF2RCxFQUFtRSxzQkFBbkUsRUFBMkYsVUFBVVQsS0FBVixFQUFpQjtBQUN4R0Esc0JBQU1DLGNBQU47QUFDQTs7QUFFQSxvQkFBSSxxRUFBQXhPLENBQVFtTSxVQUFSLEdBQXFCOUosY0FBckIsQ0FBb0NrQyxPQUFwQyxLQUFnRCxPQUFwRCxFQUE2RDtBQUN6RCx3QkFBSTZJLGdCQUFnQixxRUFBQXBOLENBQVFxSyxnQkFBUixFQUFwQjtBQUNBLHdCQUFJdEgsRUFBRThHLElBQUYsQ0FBT3VELGFBQVAsSUFBd0IsQ0FBNUIsRUFBK0I7QUFDM0JqTiwrQkFBTzZCLE9BQVAsQ0FBZUMsT0FBZixDQUF1QjZGLGFBQXZCLENBQXFDc0YsYUFBckMsRUFBb0RqTixPQUFPNkIsT0FBUCxDQUFlcU0sR0FBbkU7QUFDQSw0QkFBSTlHLE1BQU02TixtQkFBTixDQUEwQmhJLGFBQTFCLENBQUosRUFBOEM7QUFDMUMrSCx3Q0FBWTlULElBQVosQ0FBaUIsUUFBakIsRUFBMkI0SixPQUEzQixDQUFtQyxPQUFuQztBQUNIO0FBQ0o7QUFDSixpQkFSRCxNQVFPO0FBQ0h0QyxvQkFBQSxvRkFBQUEsQ0FBZThDLGFBQWY7QUFDSDtBQUNKLGFBZkQ7QUFnQkg7Ozs7O0FBWUQ7d0NBQ2dCO0FBQ1osZ0JBQUlsRSxRQUFRLElBQVo7QUFDQXZHLGNBQUUsZ0NBQUYsRUFBb0NxVSxJQUFwQyxDQUF5QywyQkFBekMsRUFBc0UsVUFBVTFELENBQVYsRUFBYTtBQUMvRSxvQkFBSUEsRUFBRTJELGFBQUYsQ0FBZ0JDLE1BQWhCLEdBQXlCLENBQXpCLElBQThCNUQsRUFBRTJELGFBQUYsQ0FBZ0JFLFVBQWhCLEdBQTZCLENBQS9ELEVBQWtFO0FBQzlELHdCQUFJQyxhQUFhLEtBQWpCO0FBQ0Esd0JBQUl6VSxFQUFFLElBQUYsRUFBUTBDLE9BQVIsQ0FBZ0IsY0FBaEIsRUFBZ0NqRCxNQUFoQyxHQUF5QyxDQUE3QyxFQUFnRDtBQUM1Q2dWLHFDQUFhelUsRUFBRSxJQUFGLEVBQVEwVSxTQUFSLEtBQXNCMVUsRUFBRSxJQUFGLEVBQVEyVSxXQUFSLEtBQXVCLENBQTdDLElBQWtEM1UsRUFBRSxJQUFGLEVBQVEsQ0FBUixFQUFXNFUsWUFBWCxHQUEwQixHQUF6RjtBQUNILHFCQUZELE1BRU87QUFDSEgscUNBQWF6VSxFQUFFLElBQUYsRUFBUTBVLFNBQVIsS0FBc0IxVSxFQUFFLElBQUYsRUFBUTJVLFdBQVIsRUFBdEIsSUFBK0MzVSxFQUFFLElBQUYsRUFBUSxDQUFSLEVBQVc0VSxZQUFYLEdBQTBCLEdBQXRGO0FBQ0g7O0FBRUQsd0JBQUlILFVBQUosRUFBZ0I7QUFDWiw0QkFBSSxPQUFPN1IsZ0JBQWdCQyxVQUF2QixJQUFxQyxXQUFyQyxJQUFvREQsZ0JBQWdCQyxVQUFoQixDQUEyQkksUUFBbkYsRUFBNkY7QUFDekZzRCxrQ0FBTVAsWUFBTixDQUFtQm9MLFFBQW5CLENBQTRCLEtBQTVCLEVBQW1DLEtBQW5DLEVBQTBDLElBQTFDO0FBQ0gseUJBRkQsTUFFTztBQUNIO0FBQ0g7QUFDSjtBQUNKO0FBQ0osYUFqQkQ7QUFrQkg7Ozt3Q0EvQnNCO0FBQ25CcFIsY0FBRTZVLFNBQUYsQ0FBWTtBQUNSQyx5QkFBUztBQUNMLG9DQUFnQjlVLEVBQUUseUJBQUYsRUFBNkI0SSxJQUE3QixDQUFrQyxTQUFsQztBQURYO0FBREQsYUFBWjtBQUtIOzs7Ozs7QUE0Qkw1SSxFQUFFK04sUUFBRixFQUFZZ0gsS0FBWixDQUFrQixZQUFZO0FBQzFCNVYsV0FBTzZCLE9BQVAsR0FBaUI3QixPQUFPNkIsT0FBUCxJQUFrQixFQUFuQzs7QUFFQXVPLG9CQUFnQnlGLGFBQWhCO0FBQ0EsUUFBSXpGLGVBQUosR0FBc0JRLElBQXRCOztBQUVBL1AsTUFBRSxxQkFBRixFQUF5QmtPLEdBQXpCLENBQTZCLFVBQTdCLEVBQXlDLE9BQXpDLEVBQWtERixFQUFsRCxDQUFxRCxVQUFyRCxFQUFpRSxPQUFqRSxFQUEwRSxVQUFVMkMsQ0FBVixFQUFhO0FBQ25GLFlBQUkxTyxRQUFRME8sRUFBRTlLLEdBQWQ7QUFDQSxZQUFJb1AsVUFBVSxvQkFBZDtBQUNBLFlBQUksQ0FBQ0EsUUFBUUMsSUFBUixDQUFhalQsS0FBYixDQUFMLEVBQTBCO0FBQ3RCLG1CQUFPLEtBQVA7QUFDSDtBQUNKLEtBTkQ7O0FBUUFqQyxNQUFFLHFCQUFGLEVBQXlCa08sR0FBekIsQ0FBNkIsT0FBN0IsRUFBc0MsNkJBQXRDLEVBQXFFRixFQUFyRSxDQUF3RSxPQUF4RSxFQUFpRiw2QkFBakYsRUFBZ0gsVUFBVTJDLENBQVYsRUFBYTtBQUN6SCxZQUFJMkIsVUFBVXRTLEVBQUUyUSxFQUFFNEIsYUFBSixDQUFkO0FBQ0EsWUFBSTRDLFlBQVk3QyxRQUFRNVAsT0FBUixDQUFnQixNQUFoQixFQUF3QnJDLElBQXhCLENBQTZCLE9BQTdCLENBQWhCO0FBQ0EsWUFBSThVLFVBQVUxVixNQUFWLEdBQW1CLENBQXZCLEVBQTBCO0FBQ3RCLGdCQUFJMlYsYUFBYSxJQUFqQjtBQUNBcFYsY0FBRWdDLElBQUYsQ0FBT21ULFNBQVAsRUFBa0IsVUFBVTNTLEtBQVYsRUFBaUJzRCxJQUFqQixFQUF1QjtBQUNyQ0EsdUJBQU85RixFQUFFOEYsSUFBRixDQUFQO0FBQ0Esb0JBQUdBLEtBQUs0RSxHQUFMLE1BQWMsRUFBZCxJQUFvQjFLLEVBQUVxVixJQUFGLENBQU92UCxLQUFLNEUsR0FBTCxFQUFQLEtBQXNCLEVBQTdDLEVBQWlEO0FBQzdDMEssaUNBQWEsS0FBYjtBQUNBdFAseUJBQUs3RixRQUFMLENBQWMsZUFBZDtBQUNBNkYseUJBQUt3RSxLQUFMO0FBQ0gsaUJBSkQsTUFJTztBQUNIeEUseUJBQUsxRixXQUFMLENBQWlCLGVBQWpCO0FBQ0g7QUFDSixhQVREO0FBVUEsZ0JBQUksQ0FBQ2dWLFVBQUwsRUFBaUI7QUFDYix1QkFBTyxLQUFQO0FBQ0g7QUFDSjtBQUNKLEtBbkJEO0FBc0JILENBcENELEU7Ozs7Ozs7Ozs7Ozs7O0FDeHJCQTtBQUNBOztBQUVBLElBQWFuTSxrQkFBYjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsc0NBQ3lCO0FBQ2pCLGdCQUFJcU0sU0FBU0MsV0FBYixFQUEwQjtBQUN0QnZWLGtCQUFFdVYsV0FBRixDQUFjO0FBQ1Z0SSw4QkFBVSx1Q0FEQTtBQUVWdUksMkJBQU8sZUFBVXpWLFFBQVYsRUFBb0J3TixLQUFwQixFQUEyQjtBQUM5QiwrQkFBTztBQUNIbkwsbUNBQU82RyxtQkFBbUJ3TSxnQkFBbkI7QUFESix5QkFBUDtBQUdIO0FBTlMsaUJBQWQ7O0FBU0F6VixrQkFBRXVWLFdBQUYsQ0FBYztBQUNWdEksOEJBQVUseUNBREE7QUFFVnVJLDJCQUFPLGVBQVV6VixRQUFWLEVBQW9Cd04sS0FBcEIsRUFBMkI7QUFDOUIsK0JBQU87QUFDSG5MLG1DQUFPNkcsbUJBQW1CeU0sa0JBQW5CO0FBREoseUJBQVA7QUFHSDtBQU5TLGlCQUFkO0FBUUg7QUFDSjtBQXJCTDtBQUFBO0FBQUEsMkNBdUI4QjtBQUN0QixnQkFBSXRULFFBQVE7QUFDUnVULHlCQUFTO0FBQ0w1UiwwQkFBTSxTQUREO0FBRUxELDBCQUFNLGNBQVU4UixHQUFWLEVBQWVDLFlBQWYsRUFBNkJDLE9BQTdCLEVBQXNDaFEsSUFBdEMsRUFBNEM7QUFDOUMrUCxxQ0FBYTFWLElBQWIsQ0FBa0Isa0RBQWtEMkYsS0FBSy9CLElBQXpFOztBQUVBLCtCQUFPLDJCQUFQO0FBQ0gscUJBTkk7QUFPTHFHLDhCQUFVLGtCQUFVdkUsR0FBVixFQUFlK1AsR0FBZixFQUFvQjtBQUMxQmpPLHdCQUFBLHVFQUFBQSxDQUFlOEMsYUFBZjtBQUNIO0FBVEksaUJBREQ7QUFZUnNMLGlDQUFpQjtBQUNiaFMsMEJBQU0saUJBRE87QUFFYkQsMEJBQU0sY0FBVThSLEdBQVYsRUFBZUMsWUFBZixFQUE2QkMsT0FBN0IsRUFBc0NoUSxJQUF0QyxFQUE0QztBQUM5QytQLHFDQUFhMVYsSUFBYixDQUFrQiwyREFBMkQyRixLQUFLL0IsSUFBbEY7O0FBRUEsK0JBQU8sMkJBQVA7QUFDSCxxQkFOWTtBQU9icUcsOEJBQVUsa0JBQVV2RSxHQUFWLEVBQWUrUCxHQUFmLEVBQW9CO0FBQzFCNVYsMEJBQUUsaURBQUYsRUFBcURpSyxPQUFyRCxDQUE2RCxPQUE3RDtBQUNIO0FBVFk7QUFaVCxhQUFaOztBQXlCQWxJLGNBQUVDLElBQUYsQ0FBTyxpRUFBQWhELENBQVFtTSxVQUFSLEdBQXFCdkgsWUFBNUIsRUFBMEMsVUFBVW9TLFdBQVYsRUFBdUJuUSxHQUF2QixFQUE0QjtBQUNsRTlELGtCQUFFQyxJQUFGLENBQU9nVSxXQUFQLEVBQW9CLFVBQVUvVCxLQUFWLEVBQWlCO0FBQ2pDRywwQkFBTUgsTUFBTStCLE1BQVosSUFBc0I7QUFDbEJELDhCQUFNOUIsTUFBTThCLElBRE07QUFFbEJELDhCQUFNLGNBQVU4UixHQUFWLEVBQWVDLFlBQWYsRUFBNkJDLE9BQTdCLEVBQXNDaFEsSUFBdEMsRUFBNEM7QUFDOUMrUCx5Q0FBYTFWLElBQWIsQ0FBa0IsZUFBZThCLE1BQU02QixJQUFyQixHQUE0Qiw0QkFBNUIsSUFBNERsQixnQkFBZ0I4QyxZQUFoQixDQUE2QjlCLFlBQTdCLENBQTBDaUMsR0FBMUMsRUFBK0M1RCxNQUFNK0IsTUFBckQsS0FBZ0U4QixLQUFLL0IsSUFBakksQ0FBbEI7O0FBRUEsbUNBQU8sMkJBQVA7QUFDSCx5QkFOaUI7QUFPbEJxRyxrQ0FBVSxrQkFBVXZFLEdBQVYsRUFBZStQLEdBQWYsRUFBb0I7QUFDMUI1Viw4QkFBRSxtQ0FBbUNpQyxNQUFNK0IsTUFBekMsR0FBa0QsSUFBcEQsRUFBMERpRyxPQUExRCxDQUFrRSxPQUFsRTtBQUNIO0FBVGlCLHFCQUF0QjtBQVdILGlCQVpEO0FBYUgsYUFkRDs7QUFnQkEsZ0JBQUlnTSxTQUFTLEVBQWI7O0FBRUEsb0JBQVEsaUVBQUFqWCxDQUFRMkgsZ0JBQVIsR0FBMkJwRCxPQUFuQztBQUNJLHFCQUFLLFVBQUw7QUFDSTBTLDZCQUFTLENBQUMsaUJBQUQsRUFBb0IsUUFBcEIsRUFBOEIsU0FBOUIsRUFBeUMsY0FBekMsQ0FBVDtBQUNBO0FBQ0oscUJBQUssUUFBTDtBQUNJQSw2QkFBUyxDQUFDLGlCQUFELEVBQW9CLFFBQXBCLEVBQThCLFNBQTlCLEVBQXlDLGNBQXpDLENBQVQ7QUFDQTtBQUNKLHFCQUFLLFFBQUw7QUFDSUEsNkJBQVMsQ0FBQyxXQUFELEVBQWMsaUJBQWQsRUFBaUMsUUFBakMsRUFBMkMsU0FBM0MsRUFBc0QsY0FBdEQsQ0FBVDtBQUNBO0FBQ0oscUJBQUssZ0JBQUw7QUFDSUEsNkJBQVMsQ0FBQyxPQUFELEVBQVUsaUJBQVYsRUFBNkIsUUFBN0IsRUFBdUMsU0FBdkMsRUFBa0QsV0FBbEQsQ0FBVDtBQUNBO0FBQ0oscUJBQUssUUFBTDtBQUNJQSw2QkFBUyxDQUFDLGlCQUFELEVBQW9CLFFBQXBCLEVBQThCLFNBQTlCLEVBQXlDLGNBQXpDLEVBQXlELFdBQXpELENBQVQ7QUFDQTtBQUNKLHFCQUFLLFdBQUw7QUFDSUEsNkJBQVMsQ0FBQyxVQUFELEVBQWEsUUFBYixFQUF1QixTQUF2QixFQUFrQyxjQUFsQyxFQUFrRCxXQUFsRCxDQUFUO0FBQ0E7QUFDSixxQkFBSyxPQUFMO0FBQ0k3VCw0QkFBUTtBQUNKdVQsaUNBQVN2VCxNQUFNdVQsT0FEWDtBQUVKTyxnQ0FBUTlULE1BQU04VCxNQUZWO0FBR0pqTCxrQ0FBVTdJLE1BQU02SSxRQUhaO0FBSUprTCxnQ0FBUS9ULE1BQU0rVCxNQUpWO0FBS0pDLGlDQUFTaFUsTUFBTWdVO0FBTFgscUJBQVI7QUFPQTtBQTNCUjs7QUE4QkFyVSxjQUFFQyxJQUFGLENBQU9pVSxNQUFQLEVBQWUsVUFBVWhVLEtBQVYsRUFBaUI7QUFDNUJHLHNCQUFNSCxLQUFOLElBQWU0RSxTQUFmO0FBQ0gsYUFGRDs7QUFJQSxnQkFBSSxpRUFBQTdILENBQVFvSyxnQkFBUixHQUEyQjNKLE1BQTNCLEdBQW9DLENBQXhDLEVBQTJDO0FBQ3ZDMkMsc0JBQU0yVCxlQUFOLEdBQXdCbFAsU0FBeEI7QUFDSDs7QUFFRCxnQkFBSStFLG9CQUFvQixpRUFBQTVNLENBQVE2TSxpQkFBUixHQUE0QnBNLE1BQTVCLEdBQXFDLENBQTdEOztBQUVBLGdCQUFJbU0saUJBQUosRUFBdUI7QUFDbkJ4SixzQkFBTXVULE9BQU4sR0FBZ0I5TyxTQUFoQjtBQUNBekUsc0JBQU0yVCxlQUFOLEdBQXdCbFAsU0FBeEI7QUFDQXpFLHNCQUFNaVUsU0FBTixHQUFrQnhQLFNBQWxCOztBQUVBLG9CQUFJLENBQUM5RSxFQUFFdUgsUUFBRixDQUFXMUcsZ0JBQWdCdUosV0FBM0IsRUFBd0MsZ0JBQXhDLENBQUwsRUFBZ0U7QUFDNUQvSiwwQkFBTWtVLFNBQU4sR0FBa0J6UCxTQUFsQjtBQUNIOztBQUVELG9CQUFJLENBQUM5RSxFQUFFdUgsUUFBRixDQUFXMUcsZ0JBQWdCdUosV0FBM0IsRUFBd0MsY0FBeEMsQ0FBTCxFQUE4RDtBQUMxRC9KLDBCQUFNOFQsTUFBTixHQUFlclAsU0FBZjtBQUNBekUsMEJBQU1tVSxLQUFOLEdBQWMxUCxTQUFkO0FBQ0F6RSwwQkFBTW9VLFlBQU4sR0FBcUIzUCxTQUFyQjtBQUNBekUsMEJBQU1xVSxRQUFOLEdBQWlCNVAsU0FBakI7QUFDSDs7QUFFRCxvQkFBSSxDQUFDOUUsRUFBRXVILFFBQUYsQ0FBVzFHLGdCQUFnQnVKLFdBQTNCLEVBQXdDLGVBQXhDLENBQUwsRUFBK0Q7QUFDM0QvSiwwQkFBTXNVLEtBQU4sR0FBYzdQLFNBQWQ7QUFDQXpFLDBCQUFNZ1UsT0FBTixHQUFnQnZQLFNBQWhCO0FBQ0g7O0FBRUQsb0JBQUksQ0FBQzlFLEVBQUV1SCxRQUFGLENBQVcxRyxnQkFBZ0J1SixXQUEzQixFQUF3QyxnQkFBeEMsQ0FBTCxFQUFnRTtBQUM1RC9KLDBCQUFNK1QsTUFBTixHQUFldFAsU0FBZjtBQUNIO0FBQ0o7O0FBRUQsZ0JBQUl1RixnQkFBZ0IsaUVBQUFwTixDQUFRcUssZ0JBQVIsRUFBcEI7QUFDQSxnQkFBSStDLGNBQWMzTSxNQUFkLEdBQXVCLENBQXZCLElBQTRCMk0sY0FBYyxDQUFkLEVBQWlCNUgsSUFBakIsS0FBMEIsT0FBMUQsRUFBbUU7QUFDL0RwQyxzQkFBTTJULGVBQU4sR0FBd0JsUCxTQUF4QjtBQUNIOztBQUVELGdCQUFJdUYsY0FBYzNNLE1BQWQsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDMUIsb0JBQUksQ0FBQ3NDLEVBQUV1SCxRQUFGLENBQVcxRyxnQkFBZ0J1SixXQUEzQixFQUF3QyxjQUF4QyxDQUFMLEVBQThEO0FBQzFEL0osMEJBQU1rVSxTQUFOLEdBQWtCelAsU0FBbEI7QUFDSDs7QUFFRCxvQkFBSSxDQUFDOUUsRUFBRXVILFFBQUYsQ0FBVzFHLGdCQUFnQnVKLFdBQTNCLEVBQXdDLFlBQXhDLENBQUwsRUFBNEQ7QUFDeEQvSiwwQkFBTThULE1BQU4sR0FBZXJQLFNBQWY7QUFDQXpFLDBCQUFNMlQsZUFBTixHQUF3QmxQLFNBQXhCO0FBQ0F6RSwwQkFBTW1VLEtBQU4sR0FBYzFQLFNBQWQ7QUFDQXpFLDBCQUFNb1UsWUFBTixHQUFxQjNQLFNBQXJCO0FBQ0F6RSwwQkFBTXFVLFFBQU4sR0FBaUI1UCxTQUFqQjtBQUNIOztBQUVELG9CQUFJLENBQUM5RSxFQUFFdUgsUUFBRixDQUFXMUcsZ0JBQWdCdUosV0FBM0IsRUFBd0MsYUFBeEMsQ0FBTCxFQUE2RDtBQUN6RC9KLDBCQUFNc1UsS0FBTixHQUFjN1AsU0FBZDtBQUNBekUsMEJBQU1nVSxPQUFOLEdBQWdCdlAsU0FBaEI7QUFDSDs7QUFFRCxvQkFBSSxDQUFDOUUsRUFBRXVILFFBQUYsQ0FBVzFHLGdCQUFnQnVKLFdBQTNCLEVBQXdDLGNBQXhDLENBQUwsRUFBOEQ7QUFDMUQvSiwwQkFBTStULE1BQU4sR0FBZXRQLFNBQWY7QUFDSDtBQUNKOztBQUVELGdCQUFJd0YsY0FBYyxLQUFsQjtBQUNBdEssY0FBRUMsSUFBRixDQUFPb0ssYUFBUCxFQUFzQixVQUFVbkssS0FBVixFQUFpQjtBQUNuQyxvQkFBSUYsRUFBRXVILFFBQUYsQ0FBVyxDQUFDLE9BQUQsRUFBVSxTQUFWLEVBQXFCLEtBQXJCLEVBQTRCLE1BQTVCLEVBQW9DLE9BQXBDLENBQVgsRUFBeURySCxNQUFNdUMsSUFBL0QsQ0FBSixFQUEwRTtBQUN0RTZILGtDQUFjLElBQWQ7QUFDSDtBQUNKLGFBSkQ7O0FBTUEsZ0JBQUksQ0FBQ0EsV0FBTCxFQUFrQjtBQUNkakssc0JBQU11VCxPQUFOLEdBQWdCOU8sU0FBaEI7QUFDSDs7QUFFRCxnQkFBSWpFLGdCQUFnQjBKLElBQWhCLEtBQXlCLFFBQTdCLEVBQXVDO0FBQ25DbEssc0JBQU1tVSxLQUFOLEdBQWMxUCxTQUFkO0FBQ0F6RSxzQkFBTXFVLFFBQU4sR0FBaUI1UCxTQUFqQjtBQUNBekUsc0JBQU1vVSxZQUFOLEdBQXFCM1AsU0FBckI7QUFDSDs7QUFFRCxtQkFBT3pFLEtBQVA7QUFDSDtBQW5MTDtBQUFBO0FBQUEsNkNBcUxnQztBQUN4QixnQkFBSUEsUUFBUTZHLG1CQUFtQndNLGdCQUFuQixFQUFaOztBQUVBclQsa0JBQU11VCxPQUFOLEdBQWdCOU8sU0FBaEI7QUFDQXpFLGtCQUFNMlQsZUFBTixHQUF3QmxQLFNBQXhCO0FBQ0F6RSxrQkFBTWlVLFNBQU4sR0FBa0J4UCxTQUFsQjs7QUFFQSxtQkFBT3pFLEtBQVA7QUFDSDtBQTdMTDtBQUFBO0FBQUEseUNBK0w0QjtBQUNwQixnQkFBSWtULFNBQVNDLFdBQWIsRUFBMEI7QUFDdEJ2VixrQkFBRXVWLFdBQUYsQ0FBYyxTQUFkO0FBQ0g7QUFDSjtBQW5NTDs7QUFBQTtBQUFBLEk7Ozs7Ozs7Ozs7Ozs7QUNIQTs7QUFFQSxJQUFhclAsWUFBYjtBQUNJLDRCQUFjO0FBQUE7O0FBQ1YsYUFBS3lRLGVBQUwsR0FBdUIzVyxFQUFFLGtDQUFGLENBQXZCOztBQUVBLGFBQUs0Vyx1QkFBTCxHQUErQiwwREFBL0I7O0FBRUEsYUFBS0MsVUFBTCxHQUFrQixDQUNkLE1BRGMsRUFFZCxVQUZjLEVBR2QsTUFIYyxFQUlkLFdBSmMsRUFLZCxZQUxjLEVBTWQsWUFOYyxFQU9kLGtCQVBjLENBQWxCOztBQVVBLGFBQUtDLGFBQUwsR0FBcUIsQ0FDakIsU0FEaUIsRUFFakIsT0FGaUIsRUFHakIsVUFIaUIsRUFJakIsYUFKaUIsRUFLakIsTUFMaUIsRUFNakIsV0FOaUIsQ0FBckI7QUFRSDs7QUF4Qkw7QUFBQTtBQUFBLG1DQTBCZXhVLElBMUJmLEVBMEJxQjtBQUNiLGdCQUFJaUUsUUFBUSxJQUFaO0FBQ0EsZ0JBQUk0SSxRQUFRN00sS0FBS2tDLElBQUwsS0FBYyxPQUFkLEdBQXdCLGVBQWVsQyxLQUFLdUgsUUFBcEIsR0FBK0IsU0FBL0IsR0FBMkN2SCxLQUFLeUIsSUFBaEQsR0FBdUQsSUFBL0UsR0FBc0Z6QixLQUFLOEksU0FBTCxLQUFtQixTQUFuQixHQUErQixlQUFlOUksS0FBS3JCLE9BQUwsQ0FBYWtPLEtBQTVCLEdBQW9DLFNBQXBDLEdBQWdEN00sS0FBS3lCLElBQXJELEdBQTRELElBQTNGLEdBQWtHLGVBQWV6QixLQUFLd0IsSUFBcEIsR0FBMkIsUUFBL047QUFDQSxnQkFBSWlULGNBQWMsRUFBbEI7QUFDQSxnQkFBSUMsZUFBZSxLQUFuQjtBQUNBalYsY0FBRWtOLE9BQUYsQ0FBVTNNLElBQVYsRUFBZ0IsVUFBVW9JLEdBQVYsRUFBZWxJLEtBQWYsRUFBc0I7QUFDbEMsb0JBQUlULEVBQUV1SCxRQUFGLENBQVcvQyxNQUFNc1EsVUFBakIsRUFBNkJyVSxLQUE3QixDQUFKLEVBQXlDO0FBQ3JDLHdCQUFLLENBQUNULEVBQUV1SCxRQUFGLENBQVcvQyxNQUFNdVEsYUFBakIsRUFBZ0N4VSxLQUFLa0MsSUFBckMsQ0FBRixJQUFrRHpDLEVBQUV1SCxRQUFGLENBQVcvQyxNQUFNdVEsYUFBakIsRUFBZ0N4VSxLQUFLa0MsSUFBckMsS0FBOEMsQ0FBQ3pDLEVBQUV1SCxRQUFGLENBQVcsQ0FBQyxNQUFELEVBQVMsV0FBVCxDQUFYLEVBQWtDOUcsS0FBbEMsQ0FBckcsRUFBZ0o7QUFDNUl1VSx1Q0FBZXhRLE1BQU1xUSx1QkFBTixDQUNWak8sT0FEVSxDQUNGLGFBREUsRUFDYS9GLGdCQUFnQjhDLFlBQWhCLENBQTZCbEQsS0FBN0IsQ0FEYixFQUVWbUcsT0FGVSxDQUVGLFdBRkUsRUFFVytCLE1BQU1sSSxVQUFVLFVBQVYsR0FBdUIsOEVBQThFa0ksR0FBOUUsR0FBb0YscU9BQXBGLEdBQTRULGlFQUFBMUwsQ0FBUWlZLEtBQVIsQ0FBYyxpQ0FBZCxDQUE1VCxHQUErVyw2REFBdFksR0FBc2Msa0JBQWtCdk0sR0FBbEIsR0FBd0IsSUFBeEIsR0FBK0JBLEdBQS9CLEdBQXFDLFNBQWpmLEdBQTZmLEVBRnhnQixDQUFmO0FBR0EsNEJBQUlsSSxVQUFVLFVBQWQsRUFBMEI7QUFDdEJ3VSwyQ0FBZSxJQUFmO0FBQ0g7QUFDSjtBQUNKO0FBQ0osYUFYRDtBQVlBelEsa0JBQU1vUSxlQUFOLENBQXNCdFcsSUFBdEIsQ0FBMkIscUJBQTNCLEVBQWtERixJQUFsRCxDQUF1RGdQLEtBQXZEO0FBQ0E1SSxrQkFBTW9RLGVBQU4sQ0FBc0J0VyxJQUF0QixDQUEyQix1QkFBM0IsRUFBb0RGLElBQXBELENBQXlENFcsV0FBekQ7QUFDQSxnQkFBSUMsWUFBSixFQUFrQjtBQUNkLG9CQUFJOU0sWUFBWSxJQUFJSCxTQUFKLENBQWMsMkJBQWQsQ0FBaEI7QUFDQS9KLGtCQUFFLDJCQUFGLEVBQStCa1gsT0FBL0IsR0FBeUNsSixFQUF6QyxDQUE0QyxZQUE1QyxFQUEwRCxVQUFVVCxLQUFWLEVBQWlCO0FBQ3ZFdk4sc0JBQUUsSUFBRixFQUFRa1gsT0FBUixDQUFnQixNQUFoQjtBQUNILGlCQUZEO0FBR0g7QUFDSjtBQW5ETDs7QUFBQTtBQUFBLEk7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFhekgsYUFBYjtBQUNJLDZCQUFjO0FBQUE7O0FBQ1YsYUFBS3hKLFNBQUwsR0FBaUIsSUFBSSxtRUFBSixFQUFqQjtBQUNBLGFBQUtELFlBQUwsR0FBb0IsSUFBSSxtRUFBSixFQUFwQjs7QUFFQWhHLFVBQUUsTUFBRixFQUFVZ08sRUFBVixDQUFhLGdCQUFiLEVBQStCLG1CQUEvQixFQUFvRCxZQUFZO0FBQzVEaE8sY0FBRSxJQUFGLEVBQVFLLElBQVIsQ0FBYSxtQ0FBYixFQUFrRGlLLEtBQWxEO0FBQ0gsU0FGRDtBQUdIOztBQVJMO0FBQUE7QUFBQSwrQkFVV29JLFVBVlgsRUFVdUI7QUFDZixnQkFBSW5NLFFBQVEsSUFBWjtBQUNBdkcsY0FBRStHLElBQUYsQ0FBTztBQUNIN0gscUJBQUtVLGFBQWF1WCxhQURmO0FBRUgzUyxzQkFBTSxNQUZIO0FBR0hsQyxzQkFBTTtBQUNGOFUsK0JBQVcsaUVBQUFwWSxDQUFRMkgsZ0JBQVIsR0FBMkJ6RCxTQURwQztBQUVGYSwwQkFBTTJPO0FBRkosaUJBSEg7QUFPSHpMLDBCQUFVLE1BUFA7QUFRSEMsNEJBQVksc0JBQVk7QUFDcEJsSSxvQkFBQSxpRUFBQUEsQ0FBUW1JLGVBQVI7QUFDSCxpQkFWRTtBQVdIQyx5QkFBUyxpQkFBVUMsR0FBVixFQUFlO0FBQ3BCLHdCQUFJQSxJQUFJVyxLQUFSLEVBQWU7QUFDWHpELHdCQUFBLGdGQUFBQSxDQUFla0IsV0FBZixDQUEyQixPQUEzQixFQUFvQzRCLElBQUk1QyxPQUF4QyxFQUFpRDdCLGdCQUFnQjhDLFlBQWhCLENBQTZCakIsT0FBN0IsQ0FBcUNrQixZQUF0RjtBQUNILHFCQUZELE1BRU87QUFDSHBCLHdCQUFBLGdGQUFBQSxDQUFla0IsV0FBZixDQUEyQixTQUEzQixFQUFzQzRCLElBQUk1QyxPQUExQyxFQUFtRDdCLGdCQUFnQjhDLFlBQWhCLENBQTZCakIsT0FBN0IsQ0FBcUMwRixjQUF4RjtBQUNBbkwsd0JBQUEsaUVBQUFBLENBQVF1TSxlQUFSO0FBQ0FoRiw4QkFBTVAsWUFBTixDQUFtQm9MLFFBQW5CLENBQTRCLElBQTVCO0FBQ0EzQixzQ0FBYzRILFVBQWQ7QUFDSDtBQUNKLGlCQXBCRTtBQXFCSHZQLDBCQUFVLGtCQUFVeEYsSUFBVixFQUFnQjtBQUN0QnRELG9CQUFBLGlFQUFBQSxDQUFRK0ksZUFBUjtBQUNILGlCQXZCRTtBQXdCSEMsdUJBQU8sZUFBVTFGLElBQVYsRUFBZ0I7QUFDbkJpQyxvQkFBQSxnRkFBQUEsQ0FBZTBELFdBQWYsQ0FBMkIzRixJQUEzQjtBQUNIO0FBMUJFLGFBQVA7QUE0Qkg7QUF4Q0w7QUFBQTtBQUFBLHFDQTBDaUJzUSxRQTFDakIsRUEwQzJCO0FBQ25CeFIsWUFBQSx3RUFBQUEsQ0FBWUMsY0FBWixDQUEyQjZCLFNBQTNCLEdBQXVDMFAsUUFBdkM7QUFDQTVULFlBQUEsaUVBQUFBLENBQVEyUyxlQUFSLENBQXdCaUIsUUFBeEI7QUFDQTVULFlBQUEsaUVBQUFBLENBQVEwTyxXQUFSO0FBQ0EsaUJBQUsxSCxZQUFMLENBQWtCb0wsUUFBbEIsQ0FBMkIsSUFBM0I7QUFDSDtBQS9DTDtBQUFBO0FBQUEscUNBaUR3QjtBQUNoQnBSLGNBQUUsbUJBQUYsRUFBdUJ1SyxLQUF2QixDQUE2QixNQUE3QjtBQUNIO0FBbkRMOztBQUFBO0FBQUEsSTs7Ozs7Ozs7Ozs7Ozs7QUNOQTtBQUNBOztBQUVBLElBQWFpRixhQUFiO0FBQ0ksNkJBQWM7QUFBQTs7QUFDVixhQUFLdEMsS0FBTCxHQUFhbE4sRUFBRSxNQUFGLENBQWI7O0FBRUEsYUFBS3NYLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUEsYUFBS0MsU0FBTCxHQUFpQjNYLGFBQWE0WCxXQUE5Qjs7QUFFQSxhQUFLQyxpQkFBTCxHQUF5QnpYLEVBQUUscUJBQUYsQ0FBekI7O0FBRUEsYUFBSzBYLHVCQUFMLEdBQStCMVgsRUFBRSwrQ0FBRixDQUEvQjs7QUFFQSxhQUFLMlgsc0JBQUwsR0FBOEIzWCxFQUFFLGdDQUFGLEVBQW9DRyxJQUFwQyxFQUE5Qjs7QUFFQSxhQUFLeVgsV0FBTCxHQUFtQixDQUFuQjs7QUFFQSxhQUFLNVIsWUFBTCxHQUFvQixJQUFJLDRFQUFKLEVBQXBCOztBQUVBLGFBQUs2UixVQUFMLEdBQWtCLENBQWxCO0FBQ0g7O0FBbkJMO0FBQUE7QUFBQSwrQkFxQlc7QUFDSCxnQkFBSTlWLEVBQUV1SCxRQUFGLENBQVcxRyxnQkFBZ0J1SixXQUEzQixFQUF3QyxjQUF4QyxLQUEyRG5NLEVBQUUsaUJBQUYsRUFBcUJQLE1BQXJCLEdBQThCLENBQTdGLEVBQWdHO0FBQzVGLHFCQUFLcVksYUFBTDtBQUNIO0FBQ0QsaUJBQUtDLFlBQUw7QUFDSDtBQTFCTDtBQUFBO0FBQUEsd0NBNEJvQjtBQUNaLGdCQUFJeFIsUUFBUSxJQUFaO0FBQ0FBLGtCQUFNK1EsUUFBTixHQUFpQixJQUFJVSxRQUFKLENBQWFqSyxTQUFTa0ssYUFBVCxDQUF1QixpQkFBdkIsQ0FBYixFQUF3RDtBQUNyRS9ZLHFCQUFLcUgsTUFBTWdSLFNBRDBEO0FBRXJFVyxnQ0FBZ0IsS0FGcUQ7QUFHckVDLGlDQUFpQixLQUhvRDtBQUlyRUMsaUNBQWlCLENBSm9EO0FBS3JFQywyQkFBVyxJQUwwRDtBQU1yRUMsMkJBQVcscUJBTjBEO0FBT3JFQyxpQ0FBaUIsS0FQb0Q7QUFRckVDLG1DQUFtQixLQVJrRDtBQVNyRUMsZ0NBQWdCLElBVHFEO0FBVXJFQyx5QkFBUyxpQkFBVXZVLElBQVYsRUFBZ0J3VSxHQUFoQixFQUFxQkMsUUFBckIsRUFBK0I7QUFDcENBLDZCQUFTMVksTUFBVCxDQUFnQixRQUFoQixFQUEwQkYsRUFBRSx5QkFBRixFQUE2QjRJLElBQTdCLENBQWtDLFNBQWxDLENBQTFCO0FBQ0FnUSw2QkFBUzFZLE1BQVQsQ0FBZ0IsV0FBaEIsRUFBNkIsaUVBQUFsQixDQUFRMkgsZ0JBQVIsR0FBMkJ6RCxTQUF4RDtBQUNBMFYsNkJBQVMxWSxNQUFULENBQWdCLFNBQWhCLEVBQTJCLGlFQUFBbEIsQ0FBUTJILGdCQUFSLEdBQTJCcEQsT0FBdEQ7QUFDSDtBQWRvRSxhQUF4RCxDQUFqQjs7QUFpQkFnRCxrQkFBTStRLFFBQU4sQ0FBZXRKLEVBQWYsQ0FBa0IsV0FBbEIsRUFBK0IsZ0JBQVE7QUFDbkM3SixxQkFBSzNCLEtBQUwsR0FBYStELE1BQU1xUixXQUFuQjtBQUNBclIsc0JBQU1xUixXQUFOO0FBQ0gsYUFIRDs7QUFLQXJSLGtCQUFNK1EsUUFBTixDQUFldEosRUFBZixDQUFrQixTQUFsQixFQUE2QixnQkFBUTtBQUNqQ3pILHNCQUFNc1MsWUFBTixDQUFtQjFVLEtBQUtKLElBQXhCLEVBQThCSSxLQUFLMEUsSUFBbkM7QUFDSCxhQUZEOztBQUlBdEMsa0JBQU0rUSxRQUFOLENBQWV0SixFQUFmLENBQWtCLFNBQWxCLEVBQTZCLGdCQUFRLENBRXBDLENBRkQ7O0FBSUF6SCxrQkFBTStRLFFBQU4sQ0FBZXRKLEVBQWYsQ0FBa0IsVUFBbEIsRUFBOEIsZ0JBQVE7QUFDbEN6SCxzQkFBTXVTLG9CQUFOLENBQTJCM1UsSUFBM0I7QUFDSCxhQUZEOztBQUlBb0Msa0JBQU0rUSxRQUFOLENBQWV0SixFQUFmLENBQWtCLGVBQWxCLEVBQW1DLFlBQU07QUFDckNoUCxnQkFBQSxpRUFBQUEsQ0FBUXVNLGVBQVI7QUFDQWhGLHNCQUFNUCxZQUFOLENBQW1Cb0wsUUFBbkIsQ0FBNEIsSUFBNUI7QUFDQSxvQkFBSTdLLE1BQU1zUixVQUFOLEtBQXFCLENBQXpCLEVBQTRCO0FBQ3hCdkgsK0JBQVcsWUFBWTtBQUNuQnRRLDBCQUFFLGlDQUFGLEVBQXFDaUssT0FBckMsQ0FBNkMsT0FBN0M7QUFDSCxxQkFGRCxFQUVHLElBRkg7QUFHSDtBQUNKLGFBUkQ7QUFTSDtBQXpFTDtBQUFBO0FBQUEsdUNBMkVtQjtBQUNYLGdCQUFJMUQsUUFBUSxJQUFaO0FBQ0E7OztBQUdBQSxrQkFBTTJHLEtBQU4sQ0FBWWMsRUFBWixDQUFlLE9BQWYsRUFBd0IsaUNBQXhCLEVBQTJELFVBQVVULEtBQVYsRUFBaUI7QUFDeEVBLHNCQUFNQyxjQUFOO0FBQ0F4TixrQkFBRSxxQkFBRixFQUF5QkMsUUFBekIsQ0FBa0MsZUFBbEM7QUFDQXNHLHNCQUFNc1IsVUFBTixHQUFtQixDQUFuQjtBQUNBdkgsMkJBQVcsWUFBTTtBQUNidFEsc0JBQUUsd0JBQUYsRUFBNEJNLE1BQTVCO0FBQ0FpRywwQkFBTXFSLFdBQU4sR0FBb0IsQ0FBcEI7QUFDSCxpQkFIRCxFQUdHLEdBSEg7QUFJSCxhQVJEO0FBU0g7QUF6Rkw7QUFBQTtBQUFBLHFDQTJGaUJtQixTQTNGakIsRUEyRjRCQyxTQTNGNUIsRUEyRnVDO0FBQy9CLGdCQUFJdFEsV0FBVyxLQUFLaVAsc0JBQUwsQ0FDTmhQLE9BRE0sQ0FDRSxnQkFERixFQUNvQm9RLFNBRHBCLEVBRU5wUSxPQUZNLENBRUUsZ0JBRkYsRUFFb0I2RyxjQUFjeUosY0FBZCxDQUE2QkQsU0FBN0IsQ0FGcEIsRUFHTnJRLE9BSE0sQ0FHRSxjQUhGLEVBR2tCLFNBSGxCLEVBSU5BLE9BSk0sQ0FJRSxlQUpGLEVBSW1CLFdBSm5CLENBQWY7QUFNQSxpQkFBSytPLHVCQUFMLENBQTZCeFgsTUFBN0IsQ0FBb0N3SSxRQUFwQztBQUNBLGlCQUFLK08saUJBQUwsQ0FBdUJyWCxXQUF2QixDQUFtQyxlQUFuQztBQUNBLGlCQUFLcVgsaUJBQUwsQ0FBdUJwWCxJQUF2QixDQUE0QixhQUE1QixFQUNLNlksT0FETCxDQUNhLEVBQUN4RSxXQUFXLEtBQUtnRCx1QkFBTCxDQUE2QnlCLE1BQTdCLEVBQVosRUFEYixFQUNpRSxHQURqRTtBQUVIO0FBdEdMO0FBQUE7QUFBQSw2Q0F3R3lCaFYsSUF4R3pCLEVBd0crQjtBQUN2QixnQkFBSW9DLFFBQVEsSUFBWjtBQUNBLGdCQUFJNlMsZ0JBQWdCN1MsTUFBTW1SLHVCQUFOLENBQThCclgsSUFBOUIsQ0FBbUMsa0JBQW1COEQsS0FBSzNCLEtBQXhCLEdBQWlDLEdBQXBFLENBQXBCO0FBQ0EsZ0JBQUk2VyxTQUFTRCxjQUFjL1ksSUFBZCxDQUFtQixRQUFuQixDQUFiO0FBQ0FnWixtQkFBT2paLFdBQVAsQ0FBbUIsMENBQW5COztBQUVBLGdCQUFJa1osV0FBVyxpRUFBQXRhLENBQVF1YSxVQUFSLENBQW1CcFYsS0FBS3dVLEdBQUwsQ0FBU2EsWUFBVCxJQUF5QixFQUE1QyxFQUFnRCxFQUFoRCxDQUFmOztBQUVBalQsa0JBQU1zUixVQUFOLEdBQW1CdFIsTUFBTXNSLFVBQU4sSUFBb0J5QixTQUFTdFIsS0FBVCxLQUFtQixJQUFuQixJQUEyQjdELEtBQUtzVixNQUFMLEtBQWdCLE9BQTNDLEdBQXFELENBQXJELEdBQXlELENBQTdFLENBQW5COztBQUVBSixtQkFBT3BaLFFBQVAsQ0FBZ0JxWixTQUFTdFIsS0FBVCxLQUFtQixJQUFuQixJQUEyQjdELEtBQUtzVixNQUFMLEtBQWdCLE9BQTNDLEdBQXFELGNBQXJELEdBQXNFLGVBQXRGO0FBQ0FKLG1CQUFPbFosSUFBUCxDQUFZbVosU0FBU3RSLEtBQVQsS0FBbUIsSUFBbkIsSUFBMkI3RCxLQUFLc1YsTUFBTCxLQUFnQixPQUEzQyxHQUFxRCxPQUFyRCxHQUErRCxVQUEzRTtBQUNBLGdCQUFJdFYsS0FBS3NWLE1BQUwsS0FBZ0IsT0FBcEIsRUFBNkI7QUFDekIsb0JBQUl0VixLQUFLd1UsR0FBTCxDQUFTYyxNQUFULEtBQW9CLEdBQXhCLEVBQTZCO0FBQ3pCLHdCQUFJQyxhQUFhLEVBQWpCO0FBQ0ExWixzQkFBRWdDLElBQUYsQ0FBT3NYLFFBQVAsRUFBaUIsVUFBVXpULEdBQVYsRUFBZUMsSUFBZixFQUFxQjtBQUNsQzRULHNDQUFjLCtCQUErQjVULElBQS9CLEdBQXNDLGFBQXBEO0FBQ0gscUJBRkQ7QUFHQXNULGtDQUFjL1ksSUFBZCxDQUFtQixhQUFuQixFQUFrQ0YsSUFBbEMsQ0FBdUN1WixVQUF2QztBQUNILGlCQU5ELE1BTU8sSUFBSXZWLEtBQUt3VSxHQUFMLENBQVNjLE1BQVQsS0FBb0IsR0FBeEIsRUFBNkI7QUFDaENMLGtDQUFjL1ksSUFBZCxDQUFtQixhQUFuQixFQUFrQ0YsSUFBbEMsQ0FBdUMsK0JBQStCZ0UsS0FBS3dVLEdBQUwsQ0FBUzVTLFVBQXhDLEdBQXFELFNBQTVGO0FBQ0g7QUFDSixhQVZELE1BVU8sSUFBSXVULFNBQVN0UixLQUFiLEVBQW9CO0FBQ3ZCb1IsOEJBQWMvWSxJQUFkLENBQW1CLGFBQW5CLEVBQWtDRixJQUFsQyxDQUF1QywrQkFBK0JtWixTQUFTN1UsT0FBeEMsR0FBa0QsU0FBekY7QUFDSCxhQUZNLE1BRUE7QUFDSHpGLGdCQUFBLGlFQUFBQSxDQUFRMmEsV0FBUixDQUFvQkwsU0FBU2hYLElBQVQsQ0FBY1QsRUFBbEM7QUFDQTdDLGdCQUFBLGlFQUFBQSxDQUFRNGEsZUFBUixDQUF3Qk4sU0FBU2hYLElBQVQsQ0FBY1QsRUFBdEM7QUFDSDtBQUNKO0FBcElMO0FBQUE7QUFBQSx1Q0FzSTBCZ1ksS0F0STFCLEVBc0k2QztBQUFBLGdCQUFaQyxFQUFZLHVFQUFQLEtBQU87O0FBQ3JDLGdCQUFJQyxTQUFTRCxLQUFLLElBQUwsR0FBWSxJQUF6QjtBQUNBLGdCQUFJRSxLQUFLQyxHQUFMLENBQVNKLEtBQVQsSUFBa0JFLE1BQXRCLEVBQThCO0FBQzFCLHVCQUFPRixRQUFRLElBQWY7QUFDSDtBQUNELGdCQUFJSyxRQUFRLENBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxJQUFiLEVBQW1CLElBQW5CLEVBQXlCLElBQXpCLEVBQStCLElBQS9CLEVBQXFDLElBQXJDLEVBQTJDLElBQTNDLENBQVo7QUFDQSxnQkFBSUMsSUFBSSxDQUFDLENBQVQ7QUFDQSxlQUFHO0FBQ0NOLHlCQUFTRSxNQUFUO0FBQ0Esa0JBQUVJLENBQUY7QUFDSCxhQUhELFFBR1NILEtBQUtDLEdBQUwsQ0FBU0osS0FBVCxLQUFtQkUsTUFBbkIsSUFBNkJJLElBQUlELE1BQU16YSxNQUFOLEdBQWUsQ0FIekQ7QUFJQSxtQkFBT29hLE1BQU1PLE9BQU4sQ0FBYyxDQUFkLElBQW1CLEdBQW5CLEdBQXlCRixNQUFNQyxDQUFOLENBQWhDO0FBQ0g7QUFsSkw7O0FBQUE7QUFBQSxJOzs7Ozs7Ozs7OztBQ0hBOztBQUVBLElBQWFFLGdCQUFiLEdBQ0ksNEJBQWM7QUFBQTs7QUFDVixRQUFJLHlEQUFKO0FBQ0gsQ0FITCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FDRkE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBYUMsT0FBYjtBQUNJLHVCQUFjO0FBQUE7O0FBQ1YsYUFBS3RVLFlBQUwsR0FBb0IsSUFBSSw0RUFBSixFQUFwQjs7QUFFQSxhQUFLa0gsS0FBTCxHQUFhbE4sRUFBRSxNQUFGLENBQWI7O0FBRUEsYUFBS3VhLE1BQUwsR0FBY3ZhLEVBQUUseUJBQUYsQ0FBZDs7QUFFQSxZQUFJdUcsUUFBUSxJQUFaOztBQUVBLFlBQUk7QUFDQSxpQkFBS2lVLFVBQUwsQ0FBZ0I1WCxnQkFBZ0I4QyxZQUFoQixDQUE2QitVLFFBQTdCLENBQXNDQyxPQUF0QyxDQUE4Q0MsWUFBOUQ7QUFDSCxTQUZELENBR0EsT0FBTTVaLEdBQU4sRUFBVyxDQUVWOztBQUVELGFBQUt3WixNQUFMLENBQVl2TSxFQUFaLENBQWUsaUJBQWYsRUFBa0MsWUFBTTtBQUNwQyxnQkFBSTtBQUNKekgsc0JBQU1pVSxVQUFOLENBQWlCNVgsZ0JBQWdCOEMsWUFBaEIsQ0FBNkIrVSxRQUE3QixDQUFzQ0MsT0FBdEMsQ0FBOENDLFlBQS9EO0FBQ0MsYUFGRCxDQUdBLE9BQU01WixHQUFOLEVBQVcsQ0FDVjtBQUNKLFNBTkQ7O0FBUUEsYUFBS21NLEtBQUwsQ0FBV2MsRUFBWCxDQUFjLE9BQWQsRUFBdUIsaURBQXZCLEVBQTBFLFVBQVVULEtBQVYsRUFBaUI7QUFDdkZBLGtCQUFNQyxjQUFOOztBQUVBakgsa0JBQU1xVSxpQkFBTixDQUF3QjVhLEVBQUUsSUFBRixFQUFRMEMsT0FBUixDQUFnQix5QkFBaEIsRUFBMkNyQyxJQUEzQyxDQUFnRCxpQkFBaEQsQ0FBeEI7QUFDSCxTQUpEO0FBS0g7O0FBOUJMO0FBQUE7QUFBQSxtQ0F1RGV3YSxHQXZEZixFQXVEb0I7QUFDWixpQkFBS04sTUFBTCxDQUFZbGEsSUFBWixDQUFpQixlQUFqQixFQUFrQ0YsSUFBbEMsQ0FBdUMwYSxHQUF2QztBQUNIO0FBekRMO0FBQUE7QUFBQSwwQ0EyRHNCcEksTUEzRHRCLEVBMkQ4QjtBQUN0QixnQkFBSWxNLFFBQVEsSUFBWjtBQUNBLGdCQUFJLENBQUMrVCxRQUFRUSxtQkFBUixDQUE0QnJJLE9BQU8vSCxHQUFQLEVBQTVCLENBQUQsSUFBOEMsQ0FBQyw0RkFBQXFRLENBQXNCTCxPQUF0QixDQUE4Qk0sT0FBakYsRUFBMEY7QUFDdEYsb0JBQUksNEZBQUFELENBQXNCTCxPQUF0QixDQUE4Qk0sT0FBbEMsRUFBMkM7QUFDdkMsd0JBQUk7QUFDQXpVLDhCQUFNaVUsVUFBTixDQUFpQjVYLGdCQUFnQjhDLFlBQWhCLENBQTZCK1UsUUFBN0IsQ0FBc0NDLE9BQXRDLENBQThDTyxlQUEvRDtBQUNILHFCQUZELENBR0EsT0FBTWxhLEdBQU4sRUFBVyxDQUNWO0FBQ0osaUJBTkQsTUFNTztBQUNILHdCQUFJO0FBQ0F3Riw4QkFBTWlVLFVBQU4sQ0FBaUI1WCxnQkFBZ0I4QyxZQUFoQixDQUE2QitVLFFBQTdCLENBQXNDQyxPQUF0QyxDQUE4Q1EsY0FBL0Q7QUFDSCxxQkFGRCxDQUdBLE9BQU1uYSxHQUFOLEVBQVcsQ0FDVjtBQUNKO0FBQ0osYUFkRCxNQWNPO0FBQ0gsb0JBQUlvYSxZQUFZYixRQUFRYyxZQUFSLENBQXFCM0ksT0FBTy9ILEdBQVAsRUFBckIsQ0FBaEI7QUFDQSxvQkFBSTJRLGFBQWEscURBQXFERixTQUF0RTtBQUNBLG9CQUFJRyxhQUFhL1UsTUFBTWdVLE1BQU4sQ0FBYWxhLElBQWIsQ0FBa0IseUNBQWxCLEVBQTZEa1EsRUFBN0QsQ0FBZ0UsVUFBaEUsQ0FBakI7O0FBRUEsb0JBQUkrSyxVQUFKLEVBQWdCO0FBQ1pILGdDQUFZYixRQUFRaUIsb0JBQVIsQ0FBNkI5SSxPQUFPL0gsR0FBUCxFQUE3QixDQUFaO0FBQ0EyUSxpQ0FBYSxvRUFBb0VGLFNBQWpGO0FBQ0g7O0FBRURuYixrQkFBRStHLElBQUYsQ0FBTztBQUNIN0gseUJBQUttYyxhQUFhLE9BQWIsR0FBdUIsNEZBQUFOLENBQXNCTCxPQUF0QixDQUE4Qk0sT0FBckQsR0FBK0QsZUFEakU7QUFFSHhXLDBCQUFNLEtBRkg7QUFHSDRDLDZCQUFTLGlCQUFVOUUsSUFBVixFQUFnQjtBQUNyQiw0QkFBSWdaLFVBQUosRUFBZ0I7QUFDWkUsa0RBQXNCbFosSUFBdEIsRUFBNEJtUSxPQUFPL0gsR0FBUCxFQUE1QjtBQUNILHlCQUZELE1BRU87QUFDSCtRLGdEQUFvQm5aLElBQXBCLEVBQTBCbVEsT0FBTy9ILEdBQVAsRUFBMUI7QUFDSDtBQUNKLHFCQVRFO0FBVUgxQywyQkFBTyxlQUFVMUYsSUFBVixFQUFnQjtBQUNuQiw0QkFBSTtBQUNBaUUsa0NBQU1pVSxVQUFOLENBQWlCNVgsZ0JBQWdCOEMsWUFBaEIsQ0FBNkIrVSxRQUE3QixDQUFzQ0MsT0FBdEMsQ0FBOENnQixTQUEvRDtBQUNILHlCQUZELENBR0EsT0FBTTNhLEdBQU4sRUFBVyxDQUNWO0FBQ0o7QUFoQkUsaUJBQVA7QUFrQkg7O0FBRUQscUJBQVMwYSxtQkFBVCxDQUE2Qm5aLElBQTdCLEVBQW1DcEQsR0FBbkMsRUFBd0M7QUFDcENjLGtCQUFFK0csSUFBRixDQUFPO0FBQ0g3SCx5QkFBS1UsYUFBYStiLG9CQURmO0FBRUhuWCwwQkFBTSxNQUZIO0FBR0h5Qyw4QkFBVSxNQUhQO0FBSUgzRSwwQkFBTTtBQUNGa0MsOEJBQU0sU0FESjtBQUVGVCw4QkFBTXpCLEtBQUtGLEtBQUwsQ0FBVyxDQUFYLEVBQWN3WixPQUFkLENBQXNCOU0sS0FGMUI7QUFHRjVMLG1DQUFXLGlFQUFBbEUsQ0FBUTJILGdCQUFSLEdBQTJCekQsU0FIcEM7QUFJRmhFLDZCQUFLQSxHQUpIO0FBS0YrQixpQ0FBUztBQUNMa08sbUNBQU8sZ0NBQWdDN00sS0FBS0YsS0FBTCxDQUFXLENBQVgsRUFBY1AsRUFBOUMsR0FBbUQ7QUFEckQ7QUFMUCxxQkFKSDtBQWFIdUYsNkJBQVMsaUJBQVVDLEdBQVYsRUFBZTtBQUNwQiw0QkFBSUEsSUFBSVcsS0FBUixFQUFlO0FBQ1h6RCw0QkFBQSxnRkFBQUEsQ0FBZWtCLFdBQWYsQ0FBMkIsT0FBM0IsRUFBb0M0QixJQUFJNUMsT0FBeEMsRUFBaUQ3QixnQkFBZ0I4QyxZQUFoQixDQUE2QmpCLE9BQTdCLENBQXFDa0IsWUFBdEY7QUFDSCx5QkFGRCxNQUVPO0FBQ0hwQiw0QkFBQSxnRkFBQUEsQ0FBZWtCLFdBQWYsQ0FBMkIsU0FBM0IsRUFBc0M0QixJQUFJNUMsT0FBMUMsRUFBbUQ3QixnQkFBZ0I4QyxZQUFoQixDQUE2QmpCLE9BQTdCLENBQXFDMEYsY0FBeEY7QUFDQTVELGtDQUFNUCxZQUFOLENBQW1Cb0wsUUFBbkIsQ0FBNEIsSUFBNUI7QUFDSDtBQUNKLHFCQXBCRTtBQXFCSHBKLDJCQUFPLGVBQVUxRixJQUFWLEVBQWdCO0FBQ25CaUMsd0JBQUEsZ0ZBQUFBLENBQWUwRCxXQUFmLENBQTJCM0YsSUFBM0I7QUFDSDtBQXZCRSxpQkFBUDtBQXlCQWlFLHNCQUFNZ1UsTUFBTixDQUFhaFEsS0FBYixDQUFtQixNQUFuQjtBQUNIOztBQUVELHFCQUFTaVIscUJBQVQsQ0FBK0JsWixJQUEvQixFQUFxQ3BELEdBQXJDLEVBQTBDO0FBQ3RDcUgsc0JBQU1nVSxNQUFOLENBQWFoUSxLQUFiLENBQW1CLE1BQW5CO0FBQ0g7QUFDSjtBQXpJTDtBQUFBO0FBQUEsNENBZ0MrQnJMLEdBaEMvQixFQWdDb0M7QUFDNUIsZ0JBQUkyYyxJQUFJLCtFQUFSO0FBQ0EsbUJBQVEzYyxJQUFJTSxLQUFKLENBQVVxYyxDQUFWLENBQUQsR0FBaUJ0YyxPQUFPdWMsRUFBeEIsR0FBNkIsS0FBcEM7QUFDSDtBQW5DTDtBQUFBO0FBQUEscUNBcUN3QjVjLEdBckN4QixFQXFDNkI7QUFDckIsZ0JBQUk2YyxTQUFTLGlFQUFiO0FBQ0EsZ0JBQUl2YyxRQUFRTixJQUFJTSxLQUFKLENBQVV1YyxNQUFWLENBQVo7QUFDQSxnQkFBSXZjLFNBQVNBLE1BQU0sQ0FBTixFQUFTQyxNQUFULEtBQW9CLEVBQWpDLEVBQXFDO0FBQ2pDLHVCQUFPRCxNQUFNLENBQU4sQ0FBUDtBQUNIO0FBQ0QsbUJBQU8sSUFBUDtBQUNIO0FBNUNMO0FBQUE7QUFBQSw2Q0E4Q2dDTixHQTlDaEMsRUE4Q3FDO0FBQzdCLGdCQUFJNmMsU0FBUyx1RUFBYjtBQUNBLGdCQUFJdmMsUUFBUU4sSUFBSU0sS0FBSixDQUFVdWMsTUFBVixDQUFaO0FBQ0EsZ0JBQUl2YyxLQUFKLEVBQVc7QUFDUCx1QkFBT0EsTUFBTSxDQUFOLENBQVA7QUFDSDtBQUNELG1CQUFPLElBQVA7QUFDSDtBQXJETDs7QUFBQTtBQUFBLEk7Ozs7Ozs7QUNMQTtBQUFBLElBQUl1Yix3QkFBd0I7QUFDeEJMLGFBQVM7QUFDTE0saUJBQVM7QUFESjtBQURlLENBQTVCOzs7Ozs7OztBQ0FBLHlDIiwiZmlsZSI6Ii9qcy9tZWRpYS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDcpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDBkMTM3MmJiYjA2NDc5ZGJlYzdlIiwiaW1wb3J0IHtNZWRpYUNvbmZpZywgUmVjZW50SXRlbXN9IGZyb20gJy4uL0NvbmZpZy9NZWRpYUNvbmZpZyc7XG5cbmV4cG9ydCBjbGFzcyBIZWxwZXJzIHtcbiAgICBzdGF0aWMgZ2V0VXJsUGFyYW0ocGFyYW1OYW1lLCB1cmwgPSBudWxsKSB7XG4gICAgICAgIGlmICghdXJsKSB7XG4gICAgICAgICAgICB1cmwgPSB3aW5kb3cubG9jYXRpb24uc2VhcmNoO1xuICAgICAgICB9XG4gICAgICAgIGxldCByZVBhcmFtID0gbmV3IFJlZ0V4cCgnKD86W1xcPyZdfCYpJyArIHBhcmFtTmFtZSArICc9KFteJl0rKScsICdpJyk7XG4gICAgICAgIGxldCBtYXRjaCA9IHVybC5tYXRjaChyZVBhcmFtKTtcbiAgICAgICAgcmV0dXJuICggbWF0Y2ggJiYgbWF0Y2gubGVuZ3RoID4gMSApID8gbWF0Y2hbMV0gOiBudWxsO1xuICAgIH1cblxuICAgIHN0YXRpYyBhc3NldCh1cmwpIHtcbiAgICAgICAgaWYgKHVybC5zdWJzdHJpbmcoMCwgMikgPT09ICcvLycgfHwgdXJsLnN1YnN0cmluZygwLCA3KSA9PT0gJ2h0dHA6Ly8nIHx8IHVybC5zdWJzdHJpbmcoMCwgOCkgPT09ICdodHRwczovLycpIHtcbiAgICAgICAgICAgIHJldHVybiB1cmw7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgYmFzZVVybCA9IFJWX01FRElBX1VSTC5iYXNlX3VybC5zdWJzdHIoLTEsIDEpICE9PSAnLycgPyBSVl9NRURJQV9VUkwuYmFzZV91cmwgKyAnLycgOiBSVl9NRURJQV9VUkwuYmFzZV91cmw7XG5cbiAgICAgICAgaWYgKHVybC5zdWJzdHJpbmcoMCwgMSkgPT09ICcvJykge1xuICAgICAgICAgICAgcmV0dXJuIGJhc2VVcmwgKyB1cmwuc3Vic3RyaW5nKDEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBiYXNlVXJsICsgdXJsO1xuICAgIH1cblxuICAgIHN0YXRpYyBzaG93QWpheExvYWRpbmcoJGVsZW1lbnQgPSAkKCcucnYtbWVkaWEtbWFpbicpKSB7XG4gICAgICAgICRlbGVtZW50XG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ29uLWxvYWRpbmcnKVxuICAgICAgICAgICAgLmFwcGVuZCgkKCcjcnZfbWVkaWFfbG9hZGluZycpLmh0bWwoKSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGhpZGVBamF4TG9hZGluZygkZWxlbWVudCA9ICQoJy5ydi1tZWRpYS1tYWluJykpIHtcbiAgICAgICAgJGVsZW1lbnRcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnb24tbG9hZGluZycpXG4gICAgICAgICAgICAuZmluZCgnLmxvYWRpbmctd3JhcHBlcicpLnJlbW92ZSgpO1xuICAgIH1cblxuICAgIHN0YXRpYyBpc09uQWpheExvYWRpbmcoJGVsZW1lbnQgPSAkKCcucnYtbWVkaWEtaXRlbXMnKSkge1xuICAgICAgICByZXR1cm4gJGVsZW1lbnQuaGFzQ2xhc3MoJ29uLWxvYWRpbmcnKTtcbiAgICB9XG5cbiAgICBzdGF0aWMganNvbkVuY29kZShvYmplY3QpIHtcbiAgICAgICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgICAgIGlmICh0eXBlb2Ygb2JqZWN0ID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgb2JqZWN0ID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkob2JqZWN0KTtcbiAgICB9O1xuXG4gICAgc3RhdGljIGpzb25EZWNvZGUoanNvblN0cmluZywgZGVmYXVsdFZhbHVlKSB7XG4gICAgICAgIFwidXNlIHN0cmljdFwiO1xuICAgICAgICBpZiAoIWpzb25TdHJpbmcpIHtcbiAgICAgICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGVvZiBqc29uU3RyaW5nID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgbGV0IHJlc3VsdDtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gJC5wYXJzZUpTT04oanNvblN0cmluZyk7XG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSBkZWZhdWx0VmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBqc29uU3RyaW5nO1xuICAgIH07XG5cbiAgICBzdGF0aWMgZ2V0UmVxdWVzdFBhcmFtcygpIHtcbiAgICAgICAgaWYgKHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMgJiYgd2luZG93LnJ2TWVkaWEub3B0aW9ucy5vcGVuX2luID09PSAnbW9kYWwnKSB7XG4gICAgICAgICAgICByZXR1cm4gJC5leHRlbmQodHJ1ZSwgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMsIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMgfHwge30pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcztcbiAgICB9XG5cbiAgICBzdGF0aWMgc2V0U2VsZWN0ZWRGaWxlKCRmaWxlX2lkKSB7XG4gICAgICAgIGlmICh0eXBlb2Ygd2luZG93LnJ2TWVkaWEub3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuc2VsZWN0ZWRfZmlsZV9pZCA9ICRmaWxlX2lkO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuc2VsZWN0ZWRfZmlsZV9pZCA9ICRmaWxlX2lkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc3RhdGljIGdldENvbmZpZ3MoKSB7XG4gICAgICAgIHJldHVybiBNZWRpYUNvbmZpZztcbiAgICB9XG5cbiAgICBzdGF0aWMgc3RvcmVDb25maWcoKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdNZWRpYUNvbmZpZycsIEhlbHBlcnMuanNvbkVuY29kZShNZWRpYUNvbmZpZykpO1xuICAgIH1cblxuICAgIHN0YXRpYyBzdG9yZVJlY2VudEl0ZW1zKCkge1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnUmVjZW50SXRlbXMnLCBIZWxwZXJzLmpzb25FbmNvZGUoUmVjZW50SXRlbXMpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBXZSBjdXJyZW50bHkgYWxsb3cgc2VhcmNoaW5nIGFsbCBwbGFjZSBzbyBuZWVkIHRvIEtlZXAgcHJldiBmb2xkZXIgd2hlbiByZW1vdmUga2V5d29yZC4gSW4gdGhpcyBjYXNlXG4gICAgICogdGhlIHNlbGVjdGVkIGZvbGRlciB3aWxsIGJlIHJlLWFzc2lnbmVkXG4gICAgICogKi9cbiAgICBzdGF0aWMgc2V0UHJldkZvbGRlcklEKGZvbGRlcklEKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdwcmV2Rm9sZGVyJywgZm9sZGVySUQpO1xuICAgIH1cblxuICAgIHN0YXRpYyBnZXRQcmV2Rm9sZGVySUQoKSB7XG4gICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgncHJldkZvbGRlcicpO1xuICAgIH1cblxuICAgIHN0YXRpYyBhZGRUb1JlY2VudChpZCkge1xuICAgICAgICBpZiAoaWQgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgICAgXy5lYWNoKGlkLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICBSZWNlbnRJdGVtcy5wdXNoKHZhbHVlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgUmVjZW50SXRlbXMucHVzaChpZCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0SXRlbXMoKSB7XG4gICAgICAgIGxldCBpdGVtcyA9IFtdO1xuICAgICAgICAkKCcuanMtbWVkaWEtbGlzdC10aXRsZScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgbGV0ICRib3ggPSAkKHRoaXMpO1xuICAgICAgICAgICAgbGV0IGRhdGEgPSAkYm94LmRhdGEoKSB8fCB7fTtcbiAgICAgICAgICAgIGRhdGEuaW5kZXhfa2V5ID0gJGJveC5pbmRleCgpO1xuICAgICAgICAgICAgaXRlbXMucHVzaChkYXRhKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBpdGVtcztcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0U2VsZWN0ZWRJdGVtcygpIHtcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gW107XG4gICAgICAgICQoJy5qcy1tZWRpYS1saXN0LXRpdGxlIGlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGxldCAkYm94ID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtbWVkaWEtbGlzdC10aXRsZScpO1xuICAgICAgICAgICAgbGV0IGRhdGEgPSAkYm94LmRhdGEoKSB8fCB7fTtcbiAgICAgICAgICAgIGRhdGEuaW5kZXhfa2V5ID0gJGJveC5pbmRleCgpO1xuICAgICAgICAgICAgc2VsZWN0ZWQucHVzaChkYXRhKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBzZWxlY3RlZDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0U2VsZWN0ZWRGaWxlcygpIHtcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gW107XG4gICAgICAgICQoJy5qcy1tZWRpYS1saXN0LXRpdGxlW2RhdGEtY29udGV4dD1maWxlXSBpbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBsZXQgJGJveCA9ICQodGhpcykuY2xvc2VzdCgnLmpzLW1lZGlhLWxpc3QtdGl0bGUnKTtcbiAgICAgICAgICAgIGxldCBkYXRhID0gJGJveC5kYXRhKCkgfHwge307XG4gICAgICAgICAgICBkYXRhLmluZGV4X2tleSA9ICRib3guaW5kZXgoKTtcbiAgICAgICAgICAgIHNlbGVjdGVkLnB1c2goZGF0YSk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gc2VsZWN0ZWQ7XG4gICAgfVxuXG4gICAgc3RhdGljIGdldFNlbGVjdGVkRm9sZGVyKCkge1xuICAgICAgICBsZXQgc2VsZWN0ZWQgPSBbXTtcbiAgICAgICAgJCgnLmpzLW1lZGlhLWxpc3QtdGl0bGVbZGF0YS1jb250ZXh0PWZvbGRlcl0gaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgbGV0ICRib3ggPSAkKHRoaXMpLmNsb3Nlc3QoJy5qcy1tZWRpYS1saXN0LXRpdGxlJyk7XG4gICAgICAgICAgICBsZXQgZGF0YSA9ICRib3guZGF0YSgpIHx8IHt9O1xuICAgICAgICAgICAgZGF0YS5pbmRleF9rZXkgPSAkYm94LmluZGV4KCk7XG4gICAgICAgICAgICBzZWxlY3RlZC5wdXNoKGRhdGEpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHNlbGVjdGVkO1xuICAgIH1cblxuICAgIHN0YXRpYyBpc1VzZUluTW9kYWwoKSB7XG4gICAgICAgIHJldHVybiBIZWxwZXJzLmdldFVybFBhcmFtKCdtZWRpYS1hY3Rpb24nKSA9PT0gJ3NlbGVjdC1maWxlcycgfHwgKHdpbmRvdy5ydk1lZGlhICYmIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMgJiYgd2luZG93LnJ2TWVkaWEub3B0aW9ucy5vcGVuX2luID09PSAnbW9kYWwnKTtcbiAgICB9XG5cbiAgICBzdGF0aWMgcmVzZXRQYWdpbmF0aW9uKCkge1xuICAgICAgICBSVl9NRURJQV9DT05GSUcucGFnaW5hdGlvbiA9IHsgcGFnZWQ6IDEsIHBvc3RzX3Blcl9wYWdlOiA0MCwgaW5fcHJvY2Vzc19nZXRfbWVkaWE6IGZhbHNlLCBoYXNfbW9yZTogdHJ1ZSwgZm9sZGVyX2lkOiAwfTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9IZWxwZXJzL0hlbHBlcnMuanMiLCJsZXQgTWVkaWFDb25maWcgPSAkLnBhcnNlSlNPTihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnTWVkaWFDb25maWcnKSkgfHwge307XG5cbmxldCBkZWZhdWx0Q29uZmlnID0ge1xuICAgIGFwcF9rZXk6ICc0ODNhMHh5enl0ejEyNDJjMGQ1MjA0MjZlOGJhMzY2YzUzMGMzZDlkeHh4JyxcbiAgICByZXF1ZXN0X3BhcmFtczoge1xuICAgICAgICB2aWV3X3R5cGU6ICd0aWxlcycsXG4gICAgICAgIGZpbHRlcjogJ2V2ZXJ5dGhpbmcnLFxuICAgICAgICB2aWV3X2luOiAnbXlfbWVkaWEnLFxuICAgICAgICBzZWFyY2g6ICcnLFxuICAgICAgICBzb3J0X2J5OiAnY3JlYXRlZF9hdC1kZXNjJyxcbiAgICAgICAgZm9sZGVyX2lkOiAwLFxuICAgIH0sXG4gICAgaGlkZV9kZXRhaWxzX3BhbmU6IGZhbHNlLFxuICAgIGljb25zOiB7XG4gICAgICAgIGZvbGRlcjogJ2ZhIGZhLWZvbGRlci1vJyxcbiAgICB9LFxuICAgIGFjdGlvbnNfbGlzdDoge1xuICAgICAgICBiYXNpYzogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1leWUnLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdQcmV2aWV3JyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdwcmV2aWV3JyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMCxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1wcmV2aWV3JyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICAgIGZpbGU6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtbGluaycsXG4gICAgICAgICAgICAgICAgbmFtZTogJ0NvcHkgbGluaycsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnY29weV9saW5rJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMCxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1jb3B5LWxpbmsnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtcGVuY2lsJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnUmVuYW1lJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZW5hbWUnLFxuICAgICAgICAgICAgICAgIG9yZGVyOiAxLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncnYtYWN0aW9uLXJlbmFtZScsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1jb3B5JyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnTWFrZSBhIGNvcHknLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ21ha2VfY29weScsXG4gICAgICAgICAgICAgICAgb3JkZXI6IDIsXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdydi1hY3Rpb24tbWFrZS1jb3B5JyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWRvdC1jaXJjbGUtbycsXG4gICAgICAgICAgICAgICAgbmFtZTogJ1NldCBmb2N1cyBwb2ludCcsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc2V0X2ZvY3VzX3BvaW50JyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMyxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1zZXQtZm9jdXMtcG9pbnQnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgICAgdXNlcjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1zaGFyZS1hbHQnLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdTaGFyZScsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAnc2hhcmUnLFxuICAgICAgICAgICAgICAgIG9yZGVyOiAwLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncnYtYWN0aW9uLXNoYXJlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWJhbicsXG4gICAgICAgICAgICAgICAgbmFtZTogJ1JlbW92ZSBzaGFyZScsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiAncmVtb3ZlX3NoYXJlJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMSxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1yZW1vdmUtc2hhcmUnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtc3RhcicsXG4gICAgICAgICAgICAgICAgbmFtZTogJ0Zhdm9yaXRlJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdmYXZvcml0ZScsXG4gICAgICAgICAgICAgICAgb3JkZXI6IDIsXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdydi1hY3Rpb24tZmF2b3JpdGUnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtc3Rhci1vJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnUmVtb3ZlIGZhdm9yaXRlJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZW1vdmVfZmF2b3JpdGUnLFxuICAgICAgICAgICAgICAgIG9yZGVyOiAzLFxuICAgICAgICAgICAgICAgIGNsYXNzOiAncnYtYWN0aW9uLWZhdm9yaXRlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICAgIG90aGVyOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWRvd25sb2FkJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnRG93bmxvYWQnLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2Rvd25sb2FkJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMCxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1kb3dubG9hZCcsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS10cmFzaCcsXG4gICAgICAgICAgICAgICAgbmFtZTogJ01vdmUgdG8gdHJhc2gnLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ3RyYXNoJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMSxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi10cmFzaCcsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGljb246ICdmYSBmYS1lcmFzZXInLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdEZWxldGUgcGVybWFuZW50bHknLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogJ2RlbGV0ZScsXG4gICAgICAgICAgICAgICAgb3JkZXI6IDIsXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdydi1hY3Rpb24tZGVsZXRlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLXVuZG8nLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdSZXN0b3JlJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246ICdyZXN0b3JlJyxcbiAgICAgICAgICAgICAgICBvcmRlcjogMyxcbiAgICAgICAgICAgICAgICBjbGFzczogJ3J2LWFjdGlvbi1yZXN0b3JlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgfSxcbiAgICBkZW5pZWRfZG93bmxvYWQ6IFtcbiAgICAgICAgJ3lvdXR1YmUnLFxuICAgIF0sXG59O1xuXG5pZiAoIU1lZGlhQ29uZmlnLmFwcF9rZXkgfHwgTWVkaWFDb25maWcuYXBwX2tleSAhPT0gZGVmYXVsdENvbmZpZy5hcHBfa2V5KSB7XG4gICAgTWVkaWFDb25maWcgPSBkZWZhdWx0Q29uZmlnO1xufVxuXG5sZXQgUmVjZW50SXRlbXMgPSAkLnBhcnNlSlNPTihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnUmVjZW50SXRlbXMnKSkgfHwgW107XG5cbmV4cG9ydCB7TWVkaWFDb25maWcsIFJlY2VudEl0ZW1zfTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL0NvbmZpZy9NZWRpYUNvbmZpZy5qcyIsImV4cG9ydCBjbGFzcyBNZXNzYWdlU2VydmljZSB7XG4gICAgc3RhdGljIHNob3dNZXNzYWdlKHR5cGUsIG1lc3NhZ2UsIG1lc3NhZ2VIZWFkZXIpIHtcbiAgICAgICAgdG9hc3RyLm9wdGlvbnMgPSB7XG4gICAgICAgICAgICBjbG9zZUJ1dHRvbjogdHJ1ZSxcbiAgICAgICAgICAgIHByb2dyZXNzQmFyOiB0cnVlLFxuICAgICAgICAgICAgcG9zaXRpb25DbGFzczogJ3RvYXN0LWJvdHRvbS1yaWdodCcsXG4gICAgICAgICAgICBvbmNsaWNrOiBudWxsLFxuICAgICAgICAgICAgc2hvd0R1cmF0aW9uOiAxMDAwLFxuICAgICAgICAgICAgaGlkZUR1cmF0aW9uOiAxMDAwLFxuICAgICAgICAgICAgdGltZU91dDogMTAwMDAsXG4gICAgICAgICAgICBleHRlbmRlZFRpbWVPdXQ6IDEwMDAsXG4gICAgICAgICAgICBzaG93RWFzaW5nOiAnc3dpbmcnLFxuICAgICAgICAgICAgaGlkZUVhc2luZzogJ2xpbmVhcicsXG4gICAgICAgICAgICBzaG93TWV0aG9kOiAnZmFkZUluJyxcbiAgICAgICAgICAgIGhpZGVNZXRob2Q6ICdmYWRlT3V0J1xuICAgICAgICB9O1xuICAgICAgICB0b2FzdHJbdHlwZV0obWVzc2FnZSwgbWVzc2FnZUhlYWRlcik7XG4gICAgfVxuXG4gICAgc3RhdGljIGhhbmRsZUVycm9yKGRhdGEpIHtcbiAgICAgICAgaWYgKHR5cGVvZiAoZGF0YS5yZXNwb25zZUpTT04pICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiAoZGF0YS5yZXNwb25zZUpTT04ubWVzc2FnZSkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgTWVzc2FnZVNlcnZpY2Uuc2hvd01lc3NhZ2UoJ2Vycm9yJywgZGF0YS5yZXNwb25zZUpTT04ubWVzc2FnZSwgUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5tZXNzYWdlLmVycm9yX2hlYWRlcik7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChkYXRhLnJlc3BvbnNlSlNPTiwgZnVuY3Rpb24gKGluZGV4LCBlbCkge1xuICAgICAgICAgICAgICAgICAgICAkLmVhY2goZWwsIGZ1bmN0aW9uIChrZXksIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdlcnJvcicsIGl0ZW0sIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5lcnJvcl9oZWFkZXIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdlcnJvcicsIGRhdGEuc3RhdHVzVGV4dCwgUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5tZXNzYWdlLmVycm9yX2hlYWRlcik7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvU2VydmljZXMvTWVzc2FnZVNlcnZpY2UuanMiLCJpbXBvcnQge1JlY2VudEl0ZW1zfSBmcm9tICcuLi9Db25maWcvTWVkaWFDb25maWcnO1xuaW1wb3J0IHtIZWxwZXJzfSBmcm9tICcuLi9IZWxwZXJzL0hlbHBlcnMnO1xuaW1wb3J0IHtNZXNzYWdlU2VydmljZX0gZnJvbSAnLi4vU2VydmljZXMvTWVzc2FnZVNlcnZpY2UnO1xuaW1wb3J0IHtBY3Rpb25zU2VydmljZX0gZnJvbSAnLi4vU2VydmljZXMvQWN0aW9uc1NlcnZpY2UnO1xuaW1wb3J0IHtDb250ZXh0TWVudVNlcnZpY2V9IGZyb20gJy4uL1NlcnZpY2VzL0NvbnRleHRNZW51U2VydmljZSc7XG5pbXBvcnQge01lZGlhTGlzdH0gZnJvbSAnLi4vVmlld3MvTWVkaWFMaXN0JztcbmltcG9ydCB7TWVkaWFEZXRhaWxzfSBmcm9tICcuLi9WaWV3cy9NZWRpYURldGFpbHMnO1xuXG5leHBvcnQgY2xhc3MgTWVkaWFTZXJ2aWNlIHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5NZWRpYUxpc3QgPSBuZXcgTWVkaWFMaXN0KCk7XG4gICAgICAgIHRoaXMuTWVkaWFEZXRhaWxzID0gbmV3IE1lZGlhRGV0YWlscygpO1xuICAgICAgICB0aGlzLmJyZWFkY3J1bWJUZW1wbGF0ZSA9ICQoJyNydl9tZWRpYV9icmVhZGNydW1iX2l0ZW0nKS5odG1sKCk7XG4gICAgfVxuXG4gICAgZ2V0TWVkaWEocmVsb2FkID0gZmFsc2UsIGlzX3BvcHVwID0gZmFsc2UsIGxvYWRfbW9yZV9maWxlID0gZmFsc2UpIHtcbiAgICAgICAgaWYodHlwZW9mIFJWX01FRElBX0NPTkZJRy5wYWdpbmF0aW9uICE9ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICBpZiAoUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24uaW5fcHJvY2Vzc19nZXRfbWVkaWEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIFJWX01FRElBX0NPTkZJRy5wYWdpbmF0aW9uLmluX3Byb2Nlc3NfZ2V0X21lZGlhID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgX3NlbGYuZ2V0RmlsZURldGFpbHMoe1xuICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLXBpY3R1cmUtbycsXG4gICAgICAgICAgICBub3RoaW5nX3NlbGVjdGVkOiAnJyxcbiAgICAgICAgfSk7XG5cbiAgICAgICAgbGV0IHBhcmFtcyA9IEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpO1xuXG4gICAgICAgIGlmIChwYXJhbXMudmlld19pbiA9PT0gJ3JlY2VudCcpIHtcbiAgICAgICAgICAgIHBhcmFtcy5yZWNlbnRfaXRlbXMgPSBSZWNlbnRJdGVtcztcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChpc19wb3B1cCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgcGFyYW1zLmlzX3BvcHVwID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNle1xuICAgICAgICAgICAgcGFyYW1zLmlzX3BvcHVwID0gdW5kZWZpbmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgcGFyYW1zLm9uU2VsZWN0RmlsZXMgPSB1bmRlZmluZWQ7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBwYXJhbXMuc2VhcmNoICE9ICd1bmRlZmluZWQnICYmIHBhcmFtcy5zZWFyY2ggIT0gJycgJiYgdHlwZW9mIHBhcmFtcy5zZWxlY3RlZF9maWxlX2lkICE9ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICBwYXJhbXMuc2VsZWN0ZWRfZmlsZV9pZCA9IHVuZGVmaW5lZFxuICAgICAgICB9XG5cbiAgICAgICAgcGFyYW1zLmxvYWRfbW9yZV9maWxlID0gbG9hZF9tb3JlX2ZpbGU7XG4gICAgICAgIGlmICh0eXBlb2YgUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24gIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIHBhcmFtcy5wYWdlZCA9IFJWX01FRElBX0NPTkZJRy5wYWdpbmF0aW9uLnBhZ2VkO1xuICAgICAgICAgICAgcGFyYW1zLnBvc3RzX3Blcl9wYWdlID0gUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24ucG9zdHNfcGVyX3BhZ2U7XG4gICAgICAgIH1cbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHVybDogUlZfTUVESUFfVVJMLmdldF9tZWRpYSxcbiAgICAgICAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgICAgICAgZGF0YTogcGFyYW1zLFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgIGJlZm9yZVNlbmQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBIZWxwZXJzLnNob3dBamF4TG9hZGluZygpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXMpIHtcbiAgICAgICAgICAgICAgICBfc2VsZi5NZWRpYUxpc3QucmVuZGVyRGF0YShyZXMuZGF0YSwgcmVsb2FkLCBsb2FkX21vcmVfZmlsZSk7XG4gICAgICAgICAgICAgICAgX3NlbGYuZmV0Y2hRdW90YSgpO1xuICAgICAgICAgICAgICAgIF9zZWxmLnJlbmRlckJyZWFkY3J1bWJzKHJlcy5kYXRhLmJyZWFkY3J1bWJzKTtcbiAgICAgICAgICAgICAgICBNZWRpYVNlcnZpY2UucmVmcmVzaEZpbHRlcigpO1xuICAgICAgICAgICAgICAgIEFjdGlvbnNTZXJ2aWNlLnJlbmRlckFjdGlvbnMoKTtcblxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24gIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBSVl9NRURJQV9DT05GSUcucGFnaW5hdGlvbi5wYWdlZCAhPSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24ucGFnZWQgKz0gMTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24uaW5fcHJvY2Vzc19nZXRfbWVkaWEgIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFJWX01FRElBX0NPTkZJRy5wYWdpbmF0aW9uLmluX3Byb2Nlc3NfZ2V0X21lZGlhID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIFJWX01FRElBX0NPTkZJRy5wYWdpbmF0aW9uLnBvc3RzX3Blcl9wYWdlICE9ICd1bmRlZmluZWQnICYmIHJlcy5kYXRhLmZpbGVzLmxlbmd0aCA8IFJWX01FRElBX0NPTkZJRy5wYWdpbmF0aW9uLnBvc3RzX3Blcl9wYWdlICYmIHR5cGVvZiBSVl9NRURJQV9DT05GSUcucGFnaW5hdGlvbi5oYXNfbW9yZSAhPSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgUlZfTUVESUFfQ09ORklHLnBhZ2luYXRpb24uaGFzX21vcmUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICBIZWxwZXJzLmhpZGVBamF4TG9hZGluZygpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLmhhbmRsZUVycm9yKGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBnZXRGaWxlRGV0YWlscyhkYXRhKSB7XG4gICAgICAgIHRoaXMuTWVkaWFEZXRhaWxzLnJlbmRlckRhdGEoZGF0YSk7XG4gICAgfVxuXG4gICAgZmV0Y2hRdW90YSgpIHtcbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHVybDogUlZfTUVESUFfVVJMLmdldF9xdW90YSxcbiAgICAgICAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgIGJlZm9yZVNlbmQ6IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXMpIHtcbiAgICAgICAgICAgICAgICBsZXQgZGF0YSA9IHJlcy5kYXRhO1xuXG4gICAgICAgICAgICAgICAgJCgnLnJ2LW1lZGlhLWFzaWRlLWJvdHRvbSAudXNlZC1hbmFseXRpY3Mgc3BhbicpLmh0bWwoZGF0YS51c2VkICsgJyAvICcgKyBkYXRhLnF1b3RhKTtcbiAgICAgICAgICAgICAgICAkKCcucnYtbWVkaWEtYXNpZGUtYm90dG9tIC5wcm9ncmVzcy1iYXInKS5jc3Moe1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogZGF0YS5wZXJjZW50ICsgJyUnLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLmhhbmRsZUVycm9yKGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICByZW5kZXJCcmVhZGNydW1icyhicmVhZGNydW1iSXRlbXMpIHtcbiAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcbiAgICAgICAgbGV0ICRicmVhZGNydW1iQ29udGFpbmVyID0gJCgnLnJ2LW1lZGlhLWJyZWFkY3J1bWIgLmJyZWFkY3J1bWInKTtcbiAgICAgICAgJGJyZWFkY3J1bWJDb250YWluZXIuZmluZCgnbGknKS5yZW1vdmUoKTtcblxuICAgICAgICBfLmVhY2goYnJlYWRjcnVtYkl0ZW1zLCBmdW5jdGlvbiAodmFsdWUsIGluZGV4KSB7XG4gICAgICAgICAgICBsZXQgdGVtcGxhdGUgPSBfc2VsZi5icmVhZGNydW1iVGVtcGxhdGU7XG4gICAgICAgICAgICB0ZW1wbGF0ZSA9IHRlbXBsYXRlXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fbmFtZV9fL2dpLCB2YWx1ZS5uYW1lIHx8ICcnKVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX2ljb25fXy9naSwgdmFsdWUuaWNvbiA/ICc8aSBjbGFzcz1cIicgKyB2YWx1ZS5pY29uICsgJ1wiPjwvaT4nIDogJycpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fZm9sZGVySWRfXy9naSwgdmFsdWUuaWQgfHwgMCk7XG4gICAgICAgICAgICAkYnJlYWRjcnVtYkNvbnRhaW5lci5hcHBlbmQoJCh0ZW1wbGF0ZSkpO1xuICAgICAgICB9KTtcbiAgICAgICAgJCgnLnJ2LW1lZGlhLWNvbnRhaW5lcicpLmF0dHIoJ2RhdGEtYnJlYWRjcnVtYi1jb3VudCcsIF8uc2l6ZShicmVhZGNydW1iSXRlbXMpKTtcbiAgICB9XG5cbiAgICBzdGF0aWMgcmVmcmVzaEZpbHRlcigpIHtcbiAgICAgICAgbGV0ICRydk1lZGlhQ29udGFpbmVyID0gJCgnLnJ2LW1lZGlhLWNvbnRhaW5lcicpO1xuICAgICAgICBsZXQgdmlld19pbiA9IEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLnZpZXdfaW47XG4gICAgICAgIGlmICh2aWV3X2luICE9PSAnbXlfbWVkaWEnICYmICgodmlld19pbiAhPT0gJ3NoYXJlZCcgJiYgdmlld19pbiAhPT0gJ3NoYXJlZF93aXRoX21lJyAmJiB2aWV3X2luICE9PSAncHVibGljJykgfHwgSGVscGVycy5nZXRSZXF1ZXN0UGFyYW1zKCkuZm9sZGVyX2lkID09IDApKSB7XG4gICAgICAgICAgICAkKCcucnYtbWVkaWEtYWN0aW9ucyAuYnRuOm5vdChbZGF0YS10eXBlPVwicmVmcmVzaFwiXSk6bm90KGxhYmVsKScpLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgICAgICAgJHJ2TWVkaWFDb250YWluZXIuYXR0cignZGF0YS1hbGxvdy11cGxvYWQnLCAnZmFsc2UnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQoJy5ydi1tZWRpYS1hY3Rpb25zIC5idG46bm90KFtkYXRhLXR5cGU9XCJyZWZyZXNoXCJdKTpub3QobGFiZWwpJykucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICAkcnZNZWRpYUNvbnRhaW5lci5hdHRyKCdkYXRhLWFsbG93LXVwbG9hZCcsICd0cnVlJyk7XG4gICAgICAgIH1cblxuICAgICAgICAkKCcucnYtbWVkaWEtYWN0aW9ucyAuYnRuLmpzLXJ2LW1lZGlhLWNoYW5nZS1maWx0ZXItZ3JvdXAnKS5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcblxuICAgICAgICBsZXQgJGVtcHR5X3RyYXNoX2J0biA9ICQoJy5ydi1tZWRpYS1hY3Rpb25zIC5idG5bZGF0YS1hY3Rpb249XCJlbXB0eV90cmFzaFwiXScpO1xuICAgICAgICBpZiAodmlld19pbiA9PT0gJ3RyYXNoJykge1xuICAgICAgICAgICAgJGVtcHR5X3RyYXNoX2J0bi5yZW1vdmVDbGFzcygnaGlkZGVuJykucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICBpZiAoIV8uc2l6ZShIZWxwZXJzLmdldEl0ZW1zKCkpKSB7XG4gICAgICAgICAgICAgICAgJGVtcHR5X3RyYXNoX2J0bi5hZGRDbGFzcygnaGlkZGVuJykuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkZW1wdHlfdHJhc2hfYnRuLmFkZENsYXNzKCdoaWRkZW4nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIENvbnRleHRNZW51U2VydmljZS5kZXN0cm95Q29udGV4dCgpO1xuICAgICAgICBDb250ZXh0TWVudVNlcnZpY2UuaW5pdENvbnRleHQoKTtcblxuICAgICAgICAkcnZNZWRpYUNvbnRhaW5lci5hdHRyKCdkYXRhLXZpZXctaW4nLCB2aWV3X2luKTtcbiAgICB9XG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvU2VydmljZXMvTWVkaWFTZXJ2aWNlLmpzIiwiaW1wb3J0IHtSZWNlbnRJdGVtc30gZnJvbSAnLi4vQ29uZmlnL01lZGlhQ29uZmlnJztcbmltcG9ydCB7SGVscGVyc30gZnJvbSAnLi4vSGVscGVycy9IZWxwZXJzJztcbmltcG9ydCB7TWVzc2FnZVNlcnZpY2V9IGZyb20gJy4uL1NlcnZpY2VzL01lc3NhZ2VTZXJ2aWNlJztcblxuZXhwb3J0IGNsYXNzIEFjdGlvbnNTZXJ2aWNlIHtcbiAgICBzdGF0aWMgaGFuZGxlRHJvcGRvd24oKSB7XG4gICAgICAgIGxldCBzZWxlY3RlZCA9IF8uc2l6ZShIZWxwZXJzLmdldFNlbGVjdGVkSXRlbXMoKSk7XG5cbiAgICAgICAgQWN0aW9uc1NlcnZpY2UucmVuZGVyQWN0aW9ucygpO1xuXG4gICAgICAgIGlmIChzZWxlY3RlZCA+IDApIHtcbiAgICAgICAgICAgICQoJy5ydi1kcm9wZG93bi1hY3Rpb25zJykucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKCcucnYtZHJvcGRvd24tYWN0aW9ucycpLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc3RhdGljIGhhbmRsZVByZXZpZXcoKSB7XG4gICAgICAgIGxldCBzZWxlY3RlZCA9IFtdO1xuXG4gICAgICAgIF8uZWFjaChIZWxwZXJzLmdldFNlbGVjdGVkRmlsZXMoKSwgZnVuY3Rpb24gKHZhbHVlLCBpbmRleCkge1xuICAgICAgICAgICAgaWYgKF8uY29udGFpbnMoWydpbWFnZScsICd5b3V0dWJlJywgJ3BkZicsICd0ZXh0JywgJ3ZpZGVvJ10sIHZhbHVlLnR5cGUpKSB7XG4gICAgICAgICAgICAgICAgc2VsZWN0ZWQucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgIHNyYzogdmFsdWUudXJsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgUmVjZW50SXRlbXMucHVzaCh2YWx1ZS5pZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChfLnNpemUoc2VsZWN0ZWQpID4gMCkge1xuICAgICAgICAgICAgJC5mYW5jeWJveC5vcGVuKHNlbGVjdGVkKTtcbiAgICAgICAgICAgIEhlbHBlcnMuc3RvcmVSZWNlbnRJdGVtcygpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy90dXJuIG9mZiBhdXRvIGRvd25sb2FkIGZpbGUvZm9sZGVyIGZ1bmMgd2hlbiBkb3VibGUgY2xpY2sgcHJldmlldyBmaWxlL2ZvbGRlclxuICAgICAgICAgICAgLy90aGlzLmhhbmRsZUdsb2JhbEFjdGlvbignZG93bmxvYWQnKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHN0YXRpYyBoYW5kbGVDb3B5TGluaygpIHtcbiAgICAgICAgbGV0IGxpbmtzID0gJyc7XG4gICAgICAgIF8uZWFjaChIZWxwZXJzLmdldFNlbGVjdGVkRmlsZXMoKSwgZnVuY3Rpb24gKHZhbHVlLCBpbmRleCkge1xuICAgICAgICAgICAgaWYgKCFfLmlzRW1wdHkobGlua3MpKSB7XG4gICAgICAgICAgICAgICAgbGlua3MgKz0gJ1xcbic7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBsaW5rcyArPSB2YWx1ZS5mdWxsX3VybDtcbiAgICAgICAgfSk7XG4gICAgICAgIGxldCAkY2xpcGJvYXJkVGVtcCA9ICQoJy5qcy1ydi1jbGlwYm9hcmQtdGVtcCcpO1xuICAgICAgICAkY2xpcGJvYXJkVGVtcC5kYXRhKCdjbGlwYm9hcmQtdGV4dCcsIGxpbmtzKTtcbiAgICAgICAgbmV3IENsaXBib2FyZCgnLmpzLXJ2LWNsaXBib2FyZC10ZW1wJywge1xuICAgICAgICAgICAgdGV4dDogZnVuY3Rpb24gKHRyaWdnZXIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbGlua3M7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBNZXNzYWdlU2VydmljZS5zaG93TWVzc2FnZSgnc3VjY2VzcycsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMuY2xpcGJvYXJkLnN1Y2Nlc3MsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5zdWNjZXNzX2hlYWRlcik7XG4gICAgICAgICRjbGlwYm9hcmRUZW1wLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgfVxuXG4gICAgc3RhdGljIGhhbmRsZUdsb2JhbEFjdGlvbih0eXBlLCBjYWxsYmFjaykge1xuICAgICAgICBsZXQgc2VsZWN0ZWQgPSBbXTtcbiAgICAgICAgXy5lYWNoKEhlbHBlcnMuZ2V0U2VsZWN0ZWRJdGVtcygpLCBmdW5jdGlvbiAodmFsdWUsIGluZGV4KSB7XG4gICAgICAgICAgICBzZWxlY3RlZC5wdXNoKHtcbiAgICAgICAgICAgICAgICBpc19mb2xkZXI6IHZhbHVlLmlzX2ZvbGRlcixcbiAgICAgICAgICAgICAgICBpZDogdmFsdWUuaWQsXG4gICAgICAgICAgICAgICAgZnVsbF91cmw6IHZhbHVlLmZ1bGxfdXJsLFxuICAgICAgICAgICAgICAgIGZvY3VzOiB2YWx1ZS5mb2N1cyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgJ3JlbmFtZSc6XG4gICAgICAgICAgICAgICAgJCgnI21vZGFsX3JlbmFtZV9pdGVtcycpLm1vZGFsKCdzaG93JykuZmluZCgnZm9ybS5ydi1mb3JtJykuZGF0YSgnYWN0aW9uJywgdHlwZSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdjb3B5X2xpbmsnOlxuICAgICAgICAgICAgICAgIEFjdGlvbnNTZXJ2aWNlLmhhbmRsZUNvcHlMaW5rKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdwcmV2aWV3JzpcbiAgICAgICAgICAgICAgICBBY3Rpb25zU2VydmljZS5oYW5kbGVQcmV2aWV3KCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdzZXRfZm9jdXNfcG9pbnQnOlxuICAgICAgICAgICAgICAgIGxldCBtb2RhbCA9ICQoJyNtb2RhbF9zZXRfZm9jdXNfcG9pbnQnKTtcbiAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRbMF0uZm9jdXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZGFsLmZpbmQoJy5oZWxwZXItdG9vbC1kYXRhLWF0dHInKS52YWwoJycpO1xuICAgICAgICAgICAgICAgICAgICBtb2RhbC5maW5kKCcuaGVscGVyLXRvb2wtY3NzMy12YWwnKS52YWwoJycpO1xuICAgICAgICAgICAgICAgICAgICBtb2RhbC5maW5kKCcuaGVscGVyLXRvb2wtcmV0aWNsZS1jc3MnKS52YWwoJycpO1xuICAgICAgICAgICAgICAgICAgICAkKCcucmV0aWNsZScpLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbW9kYWwuZmluZCgnLmhlbHBlci10b29sLWRhdGEtYXR0cicpLnZhbChzZWxlY3RlZFswXS5mb2N1cy5kYXRhX2F0dHJpYnV0ZSk7XG4gICAgICAgICAgICAgICAgICAgIG1vZGFsLmZpbmQoJy5oZWxwZXItdG9vbC1jc3MzLXZhbCcpLnZhbChzZWxlY3RlZFswXS5mb2N1cy5jc3NfYmdfcG9zaXRpb24pO1xuICAgICAgICAgICAgICAgICAgICBtb2RhbC5maW5kKCcuaGVscGVyLXRvb2wtcmV0aWNsZS1jc3MnKS52YWwoc2VsZWN0ZWRbMF0uZm9jdXMucmV0aWNlX2Nzcyk7XG4gICAgICAgICAgICAgICAgICAgICQoJy5yZXRpY2xlJykucHJvcCgnc3R5bGUnLCBzZWxlY3RlZFswXS5mb2N1cy5yZXRpY2VfY3NzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbW9kYWwubW9kYWwoJ3Nob3cnKS5maW5kKCdmb3JtLnJ2LWZvcm0nKS5kYXRhKCdhY3Rpb24nLCB0eXBlKS5kYXRhKCdpbWFnZScsIHNlbGVjdGVkWzBdLmZ1bGxfdXJsKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ3RyYXNoJzpcbiAgICAgICAgICAgICAgICAkKCcjbW9kYWxfdHJhc2hfaXRlbXMnKS5tb2RhbCgnc2hvdycpLmZpbmQoJ2Zvcm0ucnYtZm9ybScpLmRhdGEoJ2FjdGlvbicsIHR5cGUpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAnZGVsZXRlJzpcbiAgICAgICAgICAgICAgICAkKCcjbW9kYWxfZGVsZXRlX2l0ZW1zJykubW9kYWwoJ3Nob3cnKS5maW5kKCdmb3JtLnJ2LWZvcm0nKS5kYXRhKCdhY3Rpb24nLCB0eXBlKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ3NoYXJlJzpcbiAgICAgICAgICAgICAgICAkKCcjbW9kYWxfc2hhcmVfaXRlbXMnKS5tb2RhbCgnc2hvdycpLmZpbmQoJ2Zvcm0ucnYtZm9ybScpLmRhdGEoJ2FjdGlvbicsIHR5cGUpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAnZW1wdHlfdHJhc2gnOlxuICAgICAgICAgICAgICAgICQoJyNtb2RhbF9lbXB0eV90cmFzaCcpLm1vZGFsKCdzaG93JykuZmluZCgnZm9ybS5ydi1mb3JtJykuZGF0YSgnYWN0aW9uJywgdHlwZSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdkb3dubG9hZCc6XG4gICAgICAgICAgICAgICAgbGV0IGRvd25sb2FkTGluayA9IFJWX01FRElBX1VSTC5kb3dubG9hZDtcbiAgICAgICAgICAgICAgICBsZXQgY291bnQgPSAwO1xuICAgICAgICAgICAgICAgIF8uZWFjaChIZWxwZXJzLmdldFNlbGVjdGVkSXRlbXMoKSwgZnVuY3Rpb24gKHZhbHVlLCBpbmRleCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoSGVscGVycy5nZXRDb25maWdzKCkuZGVuaWVkX2Rvd25sb2FkLCB2YWx1ZS5taW1lX3R5cGUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkb3dubG9hZExpbmsgKz0gKGNvdW50ID09PSAwID8gJz8nIDogJyYnKSArICdzZWxlY3RlZFsnICsgY291bnQgKyAnXVtpc19mb2xkZXJdPScgKyB2YWx1ZS5pc19mb2xkZXIgKyAnJnNlbGVjdGVkWycgKyBjb3VudCArICddW2lkXT0nICsgdmFsdWUuaWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb3VudCsrO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgaWYgKGRvd25sb2FkTGluayAhPT0gUlZfTUVESUFfVVJMLmRvd25sb2FkKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5vcGVuKGRvd25sb2FkTGluaywgJ19ibGFuaycpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdlcnJvcicsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMuZG93bmxvYWQuZXJyb3IsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5lcnJvcl9oZWFkZXIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgQWN0aW9uc1NlcnZpY2UucHJvY2Vzc0FjdGlvbih7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBzZWxlY3RlZCxcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiB0eXBlXG4gICAgICAgICAgICAgICAgfSwgY2FsbGJhY2spO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc3RhdGljIHByb2Nlc3NBY3Rpb24oZGF0YSwgY2FsbGJhY2sgPSBudWxsKSB7XG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB1cmw6IFJWX01FRElBX1VSTC5nbG9iYWxfYWN0aW9ucyxcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcbiAgICAgICAgICAgIGRhdGE6IGRhdGEsXG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgICAgYmVmb3JlU2VuZDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIEhlbHBlcnMuc2hvd0FqYXhMb2FkaW5nKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlcykge1xuICAgICAgICAgICAgICAgIEhlbHBlcnMucmVzZXRQYWdpbmF0aW9uKCk7XG4gICAgICAgICAgICAgICAgaWYgKCFyZXMuZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgTWVzc2FnZVNlcnZpY2Uuc2hvd01lc3NhZ2UoJ3N1Y2Nlc3MnLCByZXMubWVzc2FnZSwgUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5tZXNzYWdlLnN1Y2Nlc3NfaGVhZGVyKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBNZXNzYWdlU2VydmljZS5zaG93TWVzc2FnZSgnZXJyb3InLCByZXMubWVzc2FnZSwgUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5tZXNzYWdlLmVycm9yX2hlYWRlcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhyZXMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICBIZWxwZXJzLmhpZGVBamF4TG9hZGluZygpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLmhhbmRsZUVycm9yKGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzdGF0aWMgcmVuZGVyUmVuYW1lSXRlbXMoKSB7XG4gICAgICAgIGxldCBWSUVXID0gJCgnI3J2X21lZGlhX3JlbmFtZV9pdGVtJykuaHRtbCgpO1xuICAgICAgICBsZXQgJGl0ZW1zV3JhcHBlciA9ICQoJyNtb2RhbF9yZW5hbWVfaXRlbXMgLnJlbmFtZS1pdGVtcycpLmVtcHR5KCk7XG5cbiAgICAgICAgXy5lYWNoKEhlbHBlcnMuZ2V0U2VsZWN0ZWRJdGVtcygpLCBmdW5jdGlvbiAodmFsdWUsIGluZGV4KSB7XG4gICAgICAgICAgICBsZXQgaXRlbSA9IFZJRVdcbiAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19faWNvbl9fL2dpLCB2YWx1ZS5pY29uIHx8ICdmYSBmYS1maWxlLW8nKVxuICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvX19wbGFjZWhvbGRlcl9fL2dpLCAnSW5wdXQgZmlsZSBuYW1lJylcbiAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fdmFsdWVfXy9naSwgdmFsdWUubmFtZSlcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICBsZXQgJGl0ZW0gPSAkKGl0ZW0pO1xuICAgICAgICAgICAgJGl0ZW0uZGF0YSgnaWQnLCB2YWx1ZS5pZCk7XG4gICAgICAgICAgICAkaXRlbS5kYXRhKCdpc19mb2xkZXInLCB2YWx1ZS5pc19mb2xkZXIpO1xuICAgICAgICAgICAgJGl0ZW0uZGF0YSgnbmFtZScsIHZhbHVlLm5hbWUpO1xuICAgICAgICAgICAgJGl0ZW1zV3JhcHBlci5hcHBlbmQoJGl0ZW0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzdGF0aWMgcmVuZGVyQWN0aW9ucygpIHtcbiAgICAgICAgbGV0IGhhc0ZvbGRlclNlbGVjdGVkID0gSGVscGVycy5nZXRTZWxlY3RlZEZvbGRlcigpLmxlbmd0aCA+IDA7XG5cbiAgICAgICAgbGV0IEFDVElPTl9URU1QTEFURSA9ICQoJyNydl9hY3Rpb25faXRlbScpLmh0bWwoKTtcbiAgICAgICAgbGV0IGluaXRpYWxpemVkX2l0ZW0gPSAwO1xuICAgICAgICBsZXQgJGRyb3Bkb3duQWN0aW9ucyA9ICQoJy5ydi1kcm9wZG93bi1hY3Rpb25zIC5kcm9wZG93bi1tZW51Jyk7XG4gICAgICAgICRkcm9wZG93bkFjdGlvbnMuZW1wdHkoKTtcblxuICAgICAgICBsZXQgYWN0aW9uc0xpc3QgPSAkLmV4dGVuZCh7fSwgdHJ1ZSwgSGVscGVycy5nZXRDb25maWdzKCkuYWN0aW9uc19saXN0KTtcblxuICAgICAgICBpZiAoaGFzRm9sZGVyU2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIGFjdGlvbnNMaXN0LmJhc2ljID0gXy5yZWplY3QoYWN0aW9uc0xpc3QuYmFzaWMsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0uYWN0aW9uID09PSAncHJldmlldyc7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGFjdGlvbnNMaXN0LmZpbGUgPSBfLnJlamVjdChhY3Rpb25zTGlzdC5maWxlLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpdGVtLmFjdGlvbiA9PT0gJ2NvcHlfbGluayc7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGFjdGlvbnNMaXN0LmZpbGUgPSBfLnJlamVjdChhY3Rpb25zTGlzdC5maWxlLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpdGVtLmFjdGlvbiA9PT0gJ3NldF9mb2N1c19wb2ludCc7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZvbGRlcnMuY3JlYXRlJykpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25zTGlzdC5maWxlID0gXy5yZWplY3QoYWN0aW9uc0xpc3QuZmlsZSwgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0uYWN0aW9uID09PSAnbWFrZV9jb3B5JztcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZvbGRlcnMuZWRpdCcpKSB7XG4gICAgICAgICAgICAgICAgYWN0aW9uc0xpc3QuZmlsZSA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0LmZpbGUsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfLmNvbnRhaW5zKFsncmVuYW1lJ10sIGl0ZW0uYWN0aW9uKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIGFjdGlvbnNMaXN0LnVzZXIgPSBfLnJlamVjdChhY3Rpb25zTGlzdC51c2VyLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXy5jb250YWlucyhbJ3JlbmFtZScsICdzaGFyZScsICdyZW1vdmVfc2hhcmUnLCAndW5fc2hhcmUnXSwgaXRlbS5hY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZm9sZGVycy50cmFzaCcpKSB7XG4gICAgICAgICAgICAgICAgYWN0aW9uc0xpc3Qub3RoZXIgPSBfLnJlamVjdChhY3Rpb25zTGlzdC5vdGhlciwgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF8uY29udGFpbnMoWyd0cmFzaCcsICdyZXN0b3JlJ10sIGl0ZW0uYWN0aW9uKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZvbGRlcnMuZGVsZXRlJykpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25zTGlzdC5vdGhlciA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0Lm90aGVyLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXy5jb250YWlucyhbJ2RlbGV0ZSddLCBpdGVtLmFjdGlvbik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgc2VsZWN0ZWRGaWxlcyA9IEhlbHBlcnMuZ2V0U2VsZWN0ZWRGaWxlcygpO1xuICAgICAgICBpZiAoc2VsZWN0ZWRGaWxlcy5sZW5ndGggPiAwICYmIHNlbGVjdGVkRmlsZXNbMF0udHlwZSAhPT0gJ2ltYWdlJykge1xuICAgICAgICAgICAgYWN0aW9uc0xpc3QuZmlsZSA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0LmZpbGUsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0uYWN0aW9uID09PSAnc2V0X2ZvY3VzX3BvaW50JztcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGNhbl9wcmV2aWV3ID0gZmFsc2U7XG4gICAgICAgIF8uZWFjaChzZWxlY3RlZEZpbGVzLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgIGlmIChfLmNvbnRhaW5zKFsnaW1hZ2UnLCAneW91dHViZScsICdwZGYnLCAndGV4dCcsICd2aWRlbyddLCB2YWx1ZS50eXBlKSkge1xuICAgICAgICAgICAgICAgIGNhbl9wcmV2aWV3ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKCFjYW5fcHJldmlldykge1xuICAgICAgICAgICAgYWN0aW9uc0xpc3QuYmFzaWMgPSBfLnJlamVjdChhY3Rpb25zTGlzdC5iYXNpYywgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaXRlbS5hY3Rpb24gPT09ICdwcmV2aWV3JztcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKFJWX01FRElBX0NPTkZJRy5tb2RlID09PSAnc2ltcGxlJykge1xuICAgICAgICAgICAgYWN0aW9uc0xpc3QudXNlciA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0LnVzZXIsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIF8uY29udGFpbnMoWydzaGFyZScsICdyZW1vdmVfc2hhcmUnLCAndW5fc2hhcmUnXSwgaXRlbS5hY3Rpb24pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc2VsZWN0ZWRGaWxlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZmlsZXMuY3JlYXRlJykpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25zTGlzdC5maWxlID0gXy5yZWplY3QoYWN0aW9uc0xpc3QuZmlsZSwgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0uYWN0aW9uID09PSAnbWFrZV9jb3B5JztcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZpbGVzLmVkaXQnKSkge1xuICAgICAgICAgICAgICAgIGFjdGlvbnNMaXN0LmZpbGUgPSBfLnJlamVjdChhY3Rpb25zTGlzdC5maWxlLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXy5jb250YWlucyhbJ3JlbmFtZScsICdzZXRfZm9jdXNfcG9pbnQnXSwgaXRlbS5hY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgYWN0aW9uc0xpc3QudXNlciA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0LnVzZXIsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfLmNvbnRhaW5zKFsnc2hhcmUnLCAncmVtb3ZlX3NoYXJlJywgJ3VuX3NoYXJlJ10sIGl0ZW0uYWN0aW9uKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZpbGVzLnRyYXNoJykpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25zTGlzdC5vdGhlciA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0Lm90aGVyLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXy5jb250YWlucyhbJ3RyYXNoJywgJ3Jlc3RvcmUnXSwgaXRlbS5hY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZmlsZXMuZGVsZXRlJykpIHtcbiAgICAgICAgICAgICAgICBhY3Rpb25zTGlzdC5vdGhlciA9IF8ucmVqZWN0KGFjdGlvbnNMaXN0Lm90aGVyLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXy5jb250YWlucyhbJ2RlbGV0ZSddLCBpdGVtLmFjdGlvbik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBfLmVhY2goYWN0aW9uc0xpc3QsIGZ1bmN0aW9uIChhY3Rpb24sIGtleSkge1xuICAgICAgICAgICAgXy5lYWNoKGFjdGlvbiwgZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XG4gICAgICAgICAgICAgICAgbGV0IGlzX2JyZWFrID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChIZWxwZXJzLmdldFJlcXVlc3RQYXJhbXMoKS52aWV3X2luKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ215X21lZGlhJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLmNvbnRhaW5zKFsncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ3JlbW92ZV9zaGFyZSddLCBpdGVtLmFjdGlvbikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc19icmVhayA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnc2hhcmVkJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLmNvbnRhaW5zKFsncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ21ha2VfY29weScsICdyZW1vdmVfc2hhcmUnXSwgaXRlbS5hY3Rpb24pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNfYnJlYWsgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3NoYXJlZF93aXRoX21lJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLmNvbnRhaW5zKFsncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ21ha2VfY29weScsICdzaGFyZSddLCBpdGVtLmFjdGlvbikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc19icmVhayA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncHVibGljJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLmNvbnRhaW5zKFsncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ21ha2VfY29weScsICdzaGFyZScsICdyZW1vdmVfc2hhcmUnXSwgaXRlbS5hY3Rpb24pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNfYnJlYWsgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3JlY2VudCc6XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXy5jb250YWlucyhbJ3JlbW92ZV9mYXZvcml0ZScsICdkZWxldGUnLCAncmVzdG9yZScsICdtYWtlX2NvcHknLCAncmVtb3ZlX3NoYXJlJ10sIGl0ZW0uYWN0aW9uKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzX2JyZWFrID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdmYXZvcml0ZXMnOlxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKF8uY29udGFpbnMoWydmYXZvcml0ZScsICdkZWxldGUnLCAncmVzdG9yZScsICdtYWtlX2NvcHknLCAncmVtb3ZlX3NoYXJlJ10sIGl0ZW0uYWN0aW9uKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzX2JyZWFrID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICd0cmFzaCc6XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoWydwcmV2aWV3JywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ3JlbmFtZScsICdkb3dubG9hZCddLCBpdGVtLmFjdGlvbikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc19icmVhayA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKCFpc19icmVhaykge1xuICAgICAgICAgICAgICAgICAgICBsZXQgYWN0aW9uc19saXN0ID0gJyc7XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb25zX2xpc3QgPSBSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLmFjdGlvbnNfbGlzdFtrZXldW2l0ZW0uYWN0aW9uXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYXRjaChlcnIpIHtcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGxldCB0ZW1wbGF0ZSA9IEFDVElPTl9URU1QTEFURVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fYWN0aW9uX18vZ2ksIGl0ZW0uYWN0aW9uIHx8ICcnKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19faWNvbl9fL2dpLCBpdGVtLmljb24gfHwgJycpXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvX19uYW1lX18vZ2ksIGFjdGlvbnNfbGlzdCB8fCBpdGVtLm5hbWUpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWluZGV4ICYmIGluaXRpYWxpemVkX2l0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlID0gJzxsaSByb2xlPVwic2VwYXJhdG9yXCIgY2xhc3M9XCJkaXZpZGVyXCI+PC9saT4nICsgdGVtcGxhdGU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgJGRyb3Bkb3duQWN0aW9ucy5hcHBlbmQodGVtcGxhdGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKGFjdGlvbi5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgaW5pdGlhbGl6ZWRfaXRlbSsrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9TZXJ2aWNlcy9BY3Rpb25zU2VydmljZS5qcyIsImltcG9ydCB7SGVscGVyc30gZnJvbSAnLi9BcHAvSGVscGVycy9IZWxwZXJzJztcbmltcG9ydCB7TWVkaWFDb25maWd9IGZyb20gJy4vQXBwL0NvbmZpZy9NZWRpYUNvbmZpZyc7XG5cbmV4cG9ydCBjbGFzcyBFZGl0b3JTZXJ2aWNlIHtcbiAgICBzdGF0aWMgZWRpdG9yU2VsZWN0RmlsZShzZWxlY3RlZEZpbGVzKSB7XG5cbiAgICAgICAgbGV0IGlzX2NrZWRpdG9yID0gSGVscGVycy5nZXRVcmxQYXJhbSgnQ0tFZGl0b3InKSB8fCBIZWxwZXJzLmdldFVybFBhcmFtKCdDS0VkaXRvckZ1bmNOdW0nKTtcblxuICAgICAgICBpZiAod2luZG93Lm9wZW5lciAmJiBpc19ja2VkaXRvcikge1xuICAgICAgICAgICAgbGV0IGZpcnN0SXRlbSA9IF8uZmlyc3Qoc2VsZWN0ZWRGaWxlcyk7XG5cbiAgICAgICAgICAgIHdpbmRvdy5vcGVuZXIuQ0tFRElUT1IudG9vbHMuY2FsbEZ1bmN0aW9uKEhlbHBlcnMuZ2V0VXJsUGFyYW0oJ0NLRWRpdG9yRnVuY051bScpLCBmaXJzdEl0ZW0udXJsKTtcblxuICAgICAgICAgICAgaWYgKHdpbmRvdy5vcGVuZXIpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cuY2xvc2UoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIE5vIFdZU0lXWUcgZWRpdG9yIGZvdW5kLCB1c2UgY3VzdG9tIG1ldGhvZC5cbiAgICAgICAgfVxuICAgIH1cbn1cblxuY2xhc3MgcnZNZWRpYSB7XG4gICAgY29uc3RydWN0b3Ioc2VsZWN0b3IsIG9wdGlvbnMpIHtcbiAgICAgICAgd2luZG93LnJ2TWVkaWEgPSB3aW5kb3cucnZNZWRpYSB8fCB7fTtcblxuICAgICAgICBsZXQgJGJvZHkgPSAkKCdib2R5Jyk7XG5cbiAgICAgICAgbGV0IGRlZmF1bHRPcHRpb25zID0ge1xuICAgICAgICAgICAgbXVsdGlwbGU6IHRydWUsXG4gICAgICAgICAgICB0eXBlOiAnKicsXG4gICAgICAgICAgICBvblNlbGVjdEZpbGVzOiBmdW5jdGlvbiAoZmlsZXMsICRlbCkge1xuXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcblxuICAgICAgICBsZXQgY2xpY2tDYWxsYmFjayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGxldCAkY3VycmVudCA9ICQodGhpcyk7XG4gICAgICAgICAgICAkKCcjcnZfbWVkaWFfbW9kYWwnKS5tb2RhbCgpO1xuXG4gICAgICAgICAgICB3aW5kb3cucnZNZWRpYS5vcHRpb25zID0gb3B0aW9ucztcbiAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMub3Blbl9pbiA9ICdtb2RhbCc7XG5cbiAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLiRlbCA9ICRjdXJyZW50O1xuXG4gICAgICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5maWx0ZXIgPSAnZXZlcnl0aGluZyc7XG4gICAgICAgICAgICBIZWxwZXJzLnN0b3JlQ29uZmlnKCk7XG5cbiAgICAgICAgICAgIGxldCBlbGVfb3B0aW9ucyA9IHdpbmRvdy5ydk1lZGlhLiRlbC5kYXRhKCdydi1tZWRpYScpO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBlbGVfb3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcgJiYgZWxlX29wdGlvbnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIGVsZV9vcHRpb25zID0gZWxlX29wdGlvbnNbMF07XG4gICAgICAgICAgICAgICAgd2luZG93LnJ2TWVkaWEub3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMsIGVsZV9vcHRpb25zIHx8IHt9KTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGVsZV9vcHRpb25zLnNlbGVjdGVkX2ZpbGVfaWQgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuaXNfcG9wdXAgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuaXNfcG9wdXAgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMuaXNfcG9wdXAgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoJCgnI3J2X21lZGlhX2JvZHkgLnJ2LW1lZGlhLWNvbnRhaW5lcicpLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICQoJyNydl9tZWRpYV9ib2R5JykubG9hZChSVl9NRURJQV9VUkwucG9wdXAsIGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChkYXRhLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICQoJyNydl9tZWRpYV9ib2R5JylcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnbWVkaWEtbW9kYWwtbG9hZGluZycpXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2xvc2VzdCgnLm1vZGFsLWNvbnRlbnQnKS5yZW1vdmVDbGFzcygnYmItbG9hZGluZycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKGRvY3VtZW50KS5maW5kKCcucnYtbWVkaWEtY29udGFpbmVyIC5qcy1jaGFuZ2UtYWN0aW9uW2RhdGEtdHlwZT1yZWZyZXNoXScpLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHR5cGVvZiBzZWxlY3RvciA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICRib2R5Lm9uKCdjbGljaycsIHNlbGVjdG9yLCBjbGlja0NhbGxiYWNrKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHNlbGVjdG9yLm9uKCdjbGljaycsIGNsaWNrQ2FsbGJhY2spO1xuICAgICAgICB9XG4gICAgfVxufVxuXG53aW5kb3cuUnZNZWRpYVN0YW5kQWxvbmUgPSBydk1lZGlhO1xuXG4kKCcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLm9mZignY2xpY2snKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGxldCBzZWxlY3RlZEZpbGVzID0gSGVscGVycy5nZXRTZWxlY3RlZEZpbGVzKCk7XG4gICAgaWYgKF8uc2l6ZShzZWxlY3RlZEZpbGVzKSA+IDApIHtcbiAgICAgICAgRWRpdG9yU2VydmljZS5lZGl0b3JTZWxlY3RGaWxlKHNlbGVjdGVkRmlsZXMpO1xuICAgIH1cbn0pO1xuXG4kLmZuLnJ2TWVkaWEgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgIGxldCAkc2VsZWN0b3IgPSAkKHRoaXMpO1xuXG4gICAgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuZmlsdGVyID0gJ2V2ZXJ5dGhpbmcnO1xuICAgIGlmIChNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy52aWV3X2luID09PSAndHJhc2gnKSB7XG4gICAgICAgICQoZG9jdW1lbnQpLmZpbmQoJy5qcy1pbnNlcnQtdG8tZWRpdG9yJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKGRvY3VtZW50KS5maW5kKCcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIH1cbiAgICBIZWxwZXJzLnN0b3JlQ29uZmlnKCk7XG5cbiAgICBuZXcgcnZNZWRpYSgkc2VsZWN0b3IsIG9wdGlvbnMpO1xufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvaW50ZWdyYXRlLmpzIiwiaW1wb3J0IHtIZWxwZXJzfSBmcm9tICcuLi9IZWxwZXJzL0hlbHBlcnMnO1xuaW1wb3J0IHtBY3Rpb25zU2VydmljZX0gZnJvbSAnLi4vU2VydmljZXMvQWN0aW9uc1NlcnZpY2UnO1xuXG5leHBvcnQgY2xhc3MgTWVkaWFMaXN0IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5ncm91cCA9IHt9O1xuICAgICAgICB0aGlzLmdyb3VwLmxpc3QgPSAkKCcjcnZfbWVkaWFfaXRlbXNfbGlzdCcpLmh0bWwoKTtcbiAgICAgICAgdGhpcy5ncm91cC50aWxlcyA9ICQoJyNydl9tZWRpYV9pdGVtc190aWxlcycpLmh0bWwoKTtcblxuICAgICAgICB0aGlzLml0ZW0gPSB7fTtcbiAgICAgICAgdGhpcy5pdGVtLmxpc3QgPSAkKCcjcnZfbWVkaWFfaXRlbXNfbGlzdF9lbGVtZW50JykuaHRtbCgpO1xuICAgICAgICB0aGlzLml0ZW0udGlsZXMgPSAkKCcjcnZfbWVkaWFfaXRlbXNfdGlsZXNfZWxlbWVudCcpLmh0bWwoKTtcblxuICAgICAgICB0aGlzLiRncm91cENvbnRhaW5lciA9ICQoJy5ydi1tZWRpYS1pdGVtcycpO1xuICAgIH1cblxuXG4gICAgcmVuZGVyRGF0YShkYXRhLCByZWxvYWQgPSBmYWxzZSwgbG9hZF9tb3JlX2ZpbGUgPSBmYWxzZSkge1xuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuICAgICAgICBsZXQgTWVkaWFDb25maWcgPSBIZWxwZXJzLmdldENvbmZpZ3MoKTtcbiAgICAgICAgbGV0IHRlbXBsYXRlID0gX3NlbGYuZ3JvdXBbSGVscGVycy5nZXRSZXF1ZXN0UGFyYW1zKCkudmlld190eXBlXTtcblxuICAgICAgICBsZXQgdmlld19pbiA9IEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLnZpZXdfaW47XG5cbiAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFsnbXlfbWVkaWEnLCAncHVibGljJywgJ3RyYXNoJywgJ2Zhdm9yaXRlcycsICdzaGFyZWQnLCAnc2hhcmVkX3dpdGhfbWUnLCAncmVjZW50J10sIHZpZXdfaW4pKSB7XG4gICAgICAgICAgICB2aWV3X2luID0gJ215X21lZGlhJztcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCB0cmFuc2xhdGlvbnNfdmlld19pY29uID0gJyc7XG4gICAgICAgIGxldCB0cmFuc2xhdGlvbnNfdmlld190aXRsZSA9ICcnO1xuICAgICAgICBsZXQgdHJhbnNsYXRpb25zX3ZpZXdfbWVzc2FnZSA9ICcnO1xuICAgICAgICB0cnl7XG4gICAgICAgICAgICB0cmFuc2xhdGlvbnNfdmlld19pY29uID0gUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5ub19pdGVtW3ZpZXdfaW5dLmljb247XG4gICAgICAgICAgICB0cmFuc2xhdGlvbnNfdmlld190aXRsZSA9IFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubm9faXRlbVt2aWV3X2luXS50aXRsZTtcbiAgICAgICAgICAgIHRyYW5zbGF0aW9uc192aWV3X21lc3NhZ2UgPSBSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLm5vX2l0ZW1bdmlld19pbl0ubWVzc2FnZTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaChlcnIpIHtcblxuICAgICAgICB9XG5cbiAgICAgICAgdGVtcGxhdGUgPSB0ZW1wbGF0ZVxuICAgICAgICAgICAgLnJlcGxhY2UoL19fbm9JdGVtSWNvbl9fL2dpLCB0cmFuc2xhdGlvbnNfdmlld19pY29uKVxuICAgICAgICAgICAgLnJlcGxhY2UoL19fbm9JdGVtVGl0bGVfXy9naSwgdHJhbnNsYXRpb25zX3ZpZXdfdGl0bGUpXG4gICAgICAgICAgICAucmVwbGFjZSgvX19ub0l0ZW1NZXNzYWdlX18vZ2ksIHRyYW5zbGF0aW9uc192aWV3X21lc3NhZ2UpO1xuXG5cbiAgICAgICAgbGV0ICRyZXN1bHQgPSAkKHRlbXBsYXRlKTtcbiAgICAgICAgbGV0ICRpdGVtc1dyYXBwZXIgPSAkcmVzdWx0LmZpbmQoJ3VsJyk7XG5cbiAgICAgICAgaWYgKGxvYWRfbW9yZV9maWxlICYmIHRoaXMuJGdyb3VwQ29udGFpbmVyLmZpbmQoJy5ydi1tZWRpYS1ncmlkIHVsJykubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgJGl0ZW1zV3JhcHBlciA9IHRoaXMuJGdyb3VwQ29udGFpbmVyLmZpbmQoJy5ydi1tZWRpYS1ncmlkIHVsJylcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChfLnNpemUoZGF0YS5mb2xkZXJzKSA+IDAgfHwgXy5zaXplKGRhdGEuZmlsZXMpID4gMCkge1xuICAgICAgICAgICAgJCgnLnJ2LW1lZGlhLWl0ZW1zJykuYWRkQ2xhc3MoJ2hhcy1pdGVtcycpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCgnLnJ2LW1lZGlhLWl0ZW1zJykucmVtb3ZlQ2xhc3MoJ2hhcy1pdGVtcycpO1xuICAgICAgICB9XG5cbiAgICAgICAgXy5mb3JFYWNoKGRhdGEuZm9sZGVycywgZnVuY3Rpb24gKHZhbHVlLCBpbmRleCkge1xuICAgICAgICAgICAgbGV0IGl0ZW0gPSBfc2VsZi5pdGVtW0hlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLnZpZXdfdHlwZV07XG4gICAgICAgICAgICBpdGVtID0gaXRlbVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX3R5cGVfXy9naSwgJ2ZvbGRlcicpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19faWRfXy9naSwgdmFsdWUuaWQpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fbmFtZV9fL2dpLCB2YWx1ZS5uYW1lIHx8ICcnKVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX3NpemVfXy9naSwgJycpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fZGF0ZV9fL2dpLCB2YWx1ZS5jcmVhdGVkX2F0IHx8ICcnKVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX3RodW1iX18vZ2ksICc8aSBjbGFzcz1cImZhIGZhLWZvbGRlci1vXCI+PC9pPicpO1xuICAgICAgICAgICAgbGV0ICRpdGVtID0gJChpdGVtKTtcbiAgICAgICAgICAgIF8uZm9yRWFjaCh2YWx1ZSwgZnVuY3Rpb24gKHZhbCwgaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAkaXRlbS5kYXRhKGluZGV4LCB2YWwpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkaXRlbS5kYXRhKCdpc19mb2xkZXInLCB0cnVlKTtcbiAgICAgICAgICAgICRpdGVtLmRhdGEoJ2ljb24nLCBNZWRpYUNvbmZpZy5pY29ucy5mb2xkZXIpO1xuICAgICAgICAgICAgJGl0ZW1zV3JhcHBlci5hcHBlbmQoJGl0ZW0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBfLmZvckVhY2goZGF0YS5maWxlcywgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICBsZXQgaXRlbSA9IF9zZWxmLml0ZW1bSGVscGVycy5nZXRSZXF1ZXN0UGFyYW1zKCkudmlld190eXBlXTtcbiAgICAgICAgICAgIGl0ZW0gPSBpdGVtXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fdHlwZV9fL2dpLCAnZmlsZScpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19faWRfXy9naSwgdmFsdWUuaWQpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fbmFtZV9fL2dpLCB2YWx1ZS5uYW1lIHx8ICcnKVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX3NpemVfXy9naSwgdmFsdWUuc2l6ZSB8fCAnJylcbiAgICAgICAgICAgICAgICAucmVwbGFjZSgvX19kYXRlX18vZ2ksIHZhbHVlLmNyZWF0ZWRfYXQgfHwgJycpO1xuICAgICAgICAgICAgaWYgKEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLnZpZXdfdHlwZSA9PT0gJ2xpc3QnKSB7XG4gICAgICAgICAgICAgICAgaXRlbSA9IGl0ZW1cbiAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fdGh1bWJfXy9naSwgJzxpIGNsYXNzPVwiJyArIHZhbHVlLmljb24gKyAnXCI+PC9pPicpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKHZhbHVlLm1pbWVfdHlwZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlICd5b3V0dWJlJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBpdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fdGh1bWJfXy9naSwgJzxpbWcgc3JjPVwiJyArIHZhbHVlLm9wdGlvbnMudGh1bWIgKyAnXCIgYWx0PVwiJyArIHZhbHVlLm5hbWUgKyAnXCI+Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBpdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fdGh1bWJfXy9naSwgdmFsdWUudGh1bWIgPyAnPGltZyBzcmM9XCInICsgdmFsdWUudGh1bWIgKyAnPycgKyBuZXcgRGF0ZSgpLmdldFRpbWUoKSArICdcIiBhbHQ9XCInICsgdmFsdWUubmFtZSArICdcIj4nIDogJzxpIGNsYXNzPVwiJyArIHZhbHVlLmljb24gKyAnXCI+PC9pPicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbGV0ICRpdGVtID0gJChpdGVtKTtcbiAgICAgICAgICAgICRpdGVtLmRhdGEoJ2lzX2ZvbGRlcicsIGZhbHNlKTtcbiAgICAgICAgICAgIF8uZm9yRWFjaCh2YWx1ZSwgZnVuY3Rpb24gKHZhbCwgaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAkaXRlbS5kYXRhKGluZGV4LCB2YWwpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkaXRlbXNXcmFwcGVyLmFwcGVuZCgkaXRlbSk7XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAocmVsb2FkICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgX3NlbGYuJGdyb3VwQ29udGFpbmVyLmVtcHR5KCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAobG9hZF9tb3JlX2ZpbGUgJiYgdGhpcy4kZ3JvdXBDb250YWluZXIuZmluZCgnLnJ2LW1lZGlhLWdyaWQgdWwnKS5sZW5ndGggPiAwKSB7XG5cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIF9zZWxmLiRncm91cENvbnRhaW5lci5hcHBlbmQoJHJlc3VsdCk7XG4gICAgICAgIH1cbiAgICAgICAgX3NlbGYuJGdyb3VwQ29udGFpbmVyLmZpbmQoJy5sb2FkaW5nLXdyYXBwZXInKS5yZW1vdmUoKTtcbiAgICAgICAgQWN0aW9uc1NlcnZpY2UuaGFuZGxlRHJvcGRvd24oKTtcblxuICAgICAgICAvL3RyaWdnZXIgZXZlbnQgY2xpY2sgZm9yIGZpbGUgc2VsZWN0ZWRcbiAgICAgICAgJCgnLmpzLW1lZGlhLWxpc3QtdGl0bGVbZGF0YS1pZD0nICsgZGF0YS5zZWxlY3RlZF9maWxlX2lkICsgJ10nKS50cmlnZ2VyKCdjbGljaycpO1xuICAgIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL1ZpZXdzL01lZGlhTGlzdC5qcyIsImltcG9ydCB7TWVkaWFDb25maWd9IGZyb20gJy4vQXBwL0NvbmZpZy9NZWRpYUNvbmZpZyc7XG5pbXBvcnQge0hlbHBlcnN9IGZyb20gJy4vQXBwL0hlbHBlcnMvSGVscGVycyc7XG5pbXBvcnQge01lZGlhU2VydmljZX0gZnJvbSAnLi9BcHAvU2VydmljZXMvTWVkaWFTZXJ2aWNlJztcbmltcG9ydCB7TWVzc2FnZVNlcnZpY2V9IGZyb20gJy4vQXBwL1NlcnZpY2VzL01lc3NhZ2VTZXJ2aWNlJztcbmltcG9ydCB7Rm9sZGVyU2VydmljZX0gZnJvbSAnLi9BcHAvU2VydmljZXMvRm9sZGVyU2VydmljZSc7XG5pbXBvcnQge1VwbG9hZFNlcnZpY2V9IGZyb20gJy4vQXBwL1NlcnZpY2VzL1VwbG9hZFNlcnZpY2UnO1xuaW1wb3J0IHtBY3Rpb25zU2VydmljZX0gZnJvbSAnLi9BcHAvU2VydmljZXMvQWN0aW9uc1NlcnZpY2UnO1xuaW1wb3J0IHtFeHRlcm5hbFNlcnZpY2VzfSBmcm9tICcuL0FwcC9FeHRlcm5hbHMvRXh0ZXJuYWxTZXJ2aWNlcyc7XG5pbXBvcnQge0VkaXRvclNlcnZpY2V9IGZyb20gJy4vaW50ZWdyYXRlJztcblxuY2xhc3MgTWVkaWFNYW5hZ2VtZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5NZWRpYVNlcnZpY2UgPSBuZXcgTWVkaWFTZXJ2aWNlKCk7XG4gICAgICAgIHRoaXMuVXBsb2FkU2VydmljZSA9IG5ldyBVcGxvYWRTZXJ2aWNlKCk7XG4gICAgICAgIHRoaXMuRm9sZGVyU2VydmljZSA9IG5ldyBGb2xkZXJTZXJ2aWNlKCk7XG5cbiAgICAgICAgbmV3IEV4dGVybmFsU2VydmljZXMoKTtcblxuICAgICAgICB0aGlzLiRib2R5ID0gJCgnYm9keScpO1xuICAgIH1cblxuICAgIGluaXQoKSB7XG4gICAgICAgIEhlbHBlcnMucmVzZXRQYWdpbmF0aW9uKCk7XG4gICAgICAgIHRoaXMuc2V0dXBMYXlvdXQoKTtcblxuICAgICAgICB0aGlzLmhhbmRsZU1lZGlhTGlzdCgpO1xuICAgICAgICB0aGlzLmNoYW5nZVZpZXdUeXBlKCk7XG4gICAgICAgIHRoaXMuY2hhbmdlRmlsdGVyKCk7XG4gICAgICAgIHRoaXMuc2VhcmNoKCk7XG4gICAgICAgIHRoaXMuaGFuZGxlQWN0aW9ucygpO1xuXG4gICAgICAgIHRoaXMuVXBsb2FkU2VydmljZS5pbml0KCk7XG5cbiAgICAgICAgdGhpcy5oYW5kbGVNb2RhbHMoKTtcbiAgICAgICAgdGhpcy5zY3JvbGxHZXRNb3JlKCk7XG5cbiAgICAgICAgLy8gcmVtb3ZlIGtleXdvcmQgb25jZSByZWZyZXNoaW5nXG4gICAgICAgIE1lZGlhQ29uZmlnLnJlcXVlc3RfcGFyYW1zLnNlYXJjaCA9ICcnO1xuXG4gICAgICAgICQoJy5qcy1ydi1tZWRpYS1jaGFuZ2Utdmlldy10eXBlIC5idG5bZGF0YS10eXBlPVwiJyArIEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLnZpZXdfdHlwZSArICdcIl0nKS50cmlnZ2VyKCdjbGljaycpO1xuXG4gICAgfVxuXG4gICAgc2V0dXBMYXlvdXQoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBTaWRlYmFyXG4gICAgICAgICAqL1xuICAgICAgICBsZXQgJGN1cnJlbnRfZmlsdGVyID0gJCgnLmpzLXJ2LW1lZGlhLWNoYW5nZS1maWx0ZXJbZGF0YS10eXBlPVwiZmlsdGVyXCJdW2RhdGEtdmFsdWU9XCInICsgSGVscGVycy5nZXRSZXF1ZXN0UGFyYW1zKCkuZmlsdGVyICsgJ1wiXScpO1xuXG4gICAgICAgICRjdXJyZW50X2ZpbHRlci5jbG9zZXN0KCdsaScpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICAuY2xvc2VzdCgnLmRyb3Bkb3duJykuZmluZCgnLmpzLXJ2LW1lZGlhLWZpbHRlci1jdXJyZW50JykuaHRtbCgnKCcgKyAkY3VycmVudF9maWx0ZXIuaHRtbCgpICsgJyknKTtcblxuICAgICAgICBsZXQgJGN1cnJlbnRfdmlld19pbiA9ICQoJy5qcy1ydi1tZWRpYS1jaGFuZ2UtZmlsdGVyW2RhdGEtdHlwZT1cInZpZXdfaW5cIl1bZGF0YS12YWx1ZT1cIicgKyBIZWxwZXJzLmdldFJlcXVlc3RQYXJhbXMoKS52aWV3X2luICsgJ1wiXScpO1xuXG4gICAgICAgICRjdXJyZW50X3ZpZXdfaW4uY2xvc2VzdCgnbGknKVxuICAgICAgICAgICAgLmFkZENsYXNzKCdhY3RpdmUnKVxuICAgICAgICAgICAgLmNsb3Nlc3QoJy5kcm9wZG93bicpLmZpbmQoJy5qcy1ydi1tZWRpYS1maWx0ZXItY3VycmVudCcpLmh0bWwoJygnICsgJGN1cnJlbnRfdmlld19pbi5odG1sKCkgKyAnKScpO1xuXG4gICAgICAgIGlmIChIZWxwZXJzLmlzVXNlSW5Nb2RhbCgpKSB7XG4gICAgICAgICAgICAkKCcucnYtbWVkaWEtZm9vdGVyJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNvcnRcbiAgICAgICAgICovXG4gICAgICAgICQoJy5qcy1ydi1tZWRpYS1jaGFuZ2UtZmlsdGVyW2RhdGEtdHlwZT1cInNvcnRfYnlcIl1bZGF0YS12YWx1ZT1cIicgKyBIZWxwZXJzLmdldFJlcXVlc3RQYXJhbXMoKS5zb3J0X2J5ICsgJ1wiXScpXG4gICAgICAgICAgICAuY2xvc2VzdCgnbGknKVxuICAgICAgICAgICAgLmFkZENsYXNzKCdhY3RpdmUnKTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGV0YWlscyBwYW5lXG4gICAgICAgICAqL1xuICAgICAgICBsZXQgJG1lZGlhRGV0YWlsc0NoZWNrYm94ID0gJCgnI21lZGlhX2RldGFpbHNfY29sbGFwc2UnKTtcbiAgICAgICAgJG1lZGlhRGV0YWlsc0NoZWNrYm94LnByb3AoJ2NoZWNrZWQnLCBNZWRpYUNvbmZpZy5oaWRlX2RldGFpbHNfcGFuZSB8fCBmYWxzZSk7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgJCgnLnJ2LW1lZGlhLWRldGFpbHMnKS5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG4gICAgICAgIH0sIDMwMCk7XG4gICAgICAgICRtZWRpYURldGFpbHNDaGVja2JveC5vbignY2hhbmdlJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgTWVkaWFDb25maWcuaGlkZV9kZXRhaWxzX3BhbmUgPSAkKHRoaXMpLmlzKCc6Y2hlY2tlZCcpO1xuICAgICAgICAgICAgSGVscGVycy5zdG9yZUNvbmZpZygpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnYnV0dG9uW2RhdGEtZGlzbWlzcy1tb2RhbF0nLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGxldCBtb2RhbCA9ICQodGhpcykuZGF0YSgnZGlzbWlzcy1tb2RhbCcpO1xuICAgICAgICAgICAgJChtb2RhbCkubW9kYWwoJ2hpZGUnKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaGFuZGxlTWVkaWFMaXN0KCkge1xuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuXG4gICAgICAgIC8qQ3RybCBrZXkgaW4gV2luZG93cyovXG4gICAgICAgIGxldCBjdHJsX2tleSA9IGZhbHNlO1xuXG4gICAgICAgIC8qQ29tbWFuZCBrZXkgaW4gTUFDKi9cbiAgICAgICAgbGV0IG1ldGFfa2V5ID0gZmFsc2U7XG5cbiAgICAgICAgLypTaGlmdCBrZXkqL1xuICAgICAgICBsZXQgc2hpZnRfa2V5ID0gZmFsc2U7XG5cbiAgICAgICAgJChkb2N1bWVudCkub24oJ2tleXVwIGtleWRvd24nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgLypVc2VyIGhvbGQgY3RybCBrZXkqL1xuICAgICAgICAgICAgY3RybF9rZXkgPSBlLmN0cmxLZXk7XG4gICAgICAgICAgICAvKlVzZXIgaG9sZCBjb21tYW5kIGtleSovXG4gICAgICAgICAgICBtZXRhX2tleSA9IGUubWV0YUtleTtcbiAgICAgICAgICAgIC8qVXNlciBob2xkIHNoaWZ0IGtleSovXG4gICAgICAgICAgICBzaGlmdF9rZXkgPSBlLnNoaWZ0S2V5O1xuICAgICAgICB9KTtcblxuICAgICAgICBfc2VsZi4kYm9keVxuICAgICAgICAgICAgLm9uKCdjbGljaycsICcuanMtbWVkaWEtbGlzdC10aXRsZScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgbGV0ICRjdXJyZW50ID0gJCh0aGlzKTtcblxuICAgICAgICAgICAgICAgIGlmIChzaGlmdF9rZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGZpcnN0SXRlbSA9IF8uZmlyc3QoSGVscGVycy5nZXRTZWxlY3RlZEl0ZW1zKCkpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZmlyc3RJdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgZmlyc3RJbmRleCA9IGZpcnN0SXRlbS5pbmRleF9rZXk7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgY3VycmVudEluZGV4ID0gJGN1cnJlbnQuaW5kZXgoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5ydi1tZWRpYS1pdGVtcyBsaScpLmVhY2goZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGluZGV4ID4gZmlyc3RJbmRleCAmJiBpbmRleCA8PSBjdXJyZW50SW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XScpLnByb3AoJ2NoZWNrZWQnLCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghY3RybF9rZXkgJiYgIW1ldGFfa2V5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkY3VycmVudC5jbG9zZXN0KCcucnYtbWVkaWEtaXRlbXMnKS5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XScpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBsZXQgJGxpbmVDaGVja0JveCA9ICRjdXJyZW50LmZpbmQoJ2lucHV0W3R5cGU9Y2hlY2tib3hdJyk7XG4gICAgICAgICAgICAgICAgJGxpbmVDaGVja0JveC5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgQWN0aW9uc1NlcnZpY2UuaGFuZGxlRHJvcGRvd24oKTtcblxuICAgICAgICAgICAgICAgIF9zZWxmLk1lZGlhU2VydmljZS5nZXRGaWxlRGV0YWlscygkY3VycmVudC5kYXRhKCkpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vbignZGJsY2xpY2snLCAnLmpzLW1lZGlhLWxpc3QtdGl0bGUnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAgICAgbGV0IGRhdGEgPSAkKHRoaXMpLmRhdGEoKTtcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5pc19mb2xkZXIgPT09IHRydWUpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUga2V5d29yZCBzZWFyY2ggdG8gbG9hZCBmaWxlIGZvciB0aGUgY3VycmVudCBmb2xkZXJcbiAgICAgICAgICAgICAgICAgICAgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuc2VhcmNoID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICQoJyNydi1tZWRpYS10ZXh0LWJveC1zZWFyY2gnKS52YWwoJycpO1xuXG4gICAgICAgICAgICAgICAgICAgIEhlbHBlcnMucmVzZXRQYWdpbmF0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLkZvbGRlclNlcnZpY2UuY2hhbmdlRm9sZGVyKGRhdGEuaWQpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghSGVscGVycy5pc1VzZUluTW9kYWwoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgQWN0aW9uc1NlcnZpY2UuaGFuZGxlUHJldmlldygpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKEhlbHBlcnMuZ2V0Q29uZmlncygpLnJlcXVlc3RfcGFyYW1zLnZpZXdfaW4gIT09ICd0cmFzaCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzZWxlY3RlZEZpbGVzID0gSGVscGVycy5nZXRTZWxlY3RlZEZpbGVzKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXy5zaXplKHNlbGVjdGVkRmlsZXMpID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIEVkaXRvclNlcnZpY2UuZWRpdG9yU2VsZWN0RmlsZShzZWxlY3RlZEZpbGVzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ2RibGNsaWNrJywgJy5qcy11cC1vbmUtbGV2ZWwnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGxldCBjb3VudCA9ICQoJy5ydi1tZWRpYS1icmVhZGNydW1iIC5icmVhZGNydW1iIGxpJykubGVuZ3RoO1xuICAgICAgICAgICAgICAgICQoJy5ydi1tZWRpYS1icmVhZGNydW1iIC5icmVhZGNydW1iIGxpOm50aC1jaGlsZCgnICsgKGNvdW50IC0gMSkgKyAnKSBhJykudHJpZ2dlcignY2xpY2snKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ2NvbnRleHRtZW51JywgJy5qcy1jb250ZXh0LW1lbnUnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgIGlmICghJCh0aGlzKS5maW5kKCdpbnB1dFt0eXBlPWNoZWNrYm94XScpLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICQodGhpcykudHJpZ2dlcignY2xpY2snKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKCdjbGljayBjb250ZXh0bWVudScsICcucnYtbWVkaWEtaXRlbXMnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgIGlmICghXy5zaXplKGUudGFyZ2V0LmNsb3Nlc3QoJy5qcy1jb250ZXh0LW1lbnUnKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnLnJ2LW1lZGlhLWl0ZW1zIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXScpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgICQoJy5ydi1kcm9wZG93bi1hY3Rpb25zJykuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLk1lZGlhU2VydmljZS5nZXRGaWxlRGV0YWlscyh7XG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiAnZmEgZmEtcGljdHVyZS1vJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vdGhpbmdfc2VsZWN0ZWQ6ICcnLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICA7XG4gICAgfVxuXG4gICAgY2hhbmdlVmlld1R5cGUoKSB7XG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XG4gICAgICAgIF9zZWxmLiRib2R5Lm9uKCdjbGljaycsICcuanMtcnYtbWVkaWEtY2hhbmdlLXZpZXctdHlwZSAuYnRuJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgbGV0ICRjdXJyZW50ID0gJCh0aGlzKTtcbiAgICAgICAgICAgIGlmICgkY3VycmVudC5oYXNDbGFzcygnYWN0aXZlJykpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkY3VycmVudC5jbG9zZXN0KCcuanMtcnYtbWVkaWEtY2hhbmdlLXZpZXctdHlwZScpLmZpbmQoJy5idG4nKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAkY3VycmVudC5hZGRDbGFzcygnYWN0aXZlJyk7XG5cbiAgICAgICAgICAgIE1lZGlhQ29uZmlnLnJlcXVlc3RfcGFyYW1zLnZpZXdfdHlwZSA9ICRjdXJyZW50LmRhdGEoJ3R5cGUnKTtcblxuICAgICAgICAgICAgaWYgKCRjdXJyZW50LmRhdGEoJ3R5cGUnKSA9PT0gJ3RyYXNoJykge1xuICAgICAgICAgICAgICAgICQoZG9jdW1lbnQpLmZpbmQoJy5qcy1pbnNlcnQtdG8tZWRpdG9yJykucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJChkb2N1bWVudCkuZmluZCgnLmpzLWluc2VydC10by1lZGl0b3InKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgSGVscGVycy5zdG9yZUNvbmZpZygpO1xuXG4gICAgICAgICAgICBfc2VsZi5NZWRpYVNlcnZpY2UuZ2V0TWVkaWEodHJ1ZSwgdHJ1ZSk7XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmJpbmRJbnRlZ3JhdGVNb2RhbEV2ZW50cygpO1xuICAgIH1cblxuXG4gICAgY2hhbmdlRmlsdGVyKCkge1xuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuICAgICAgICBfc2VsZi4kYm9keS5vbignY2xpY2snLCAnLmpzLXJ2LW1lZGlhLWNoYW5nZS1maWx0ZXInLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBpZiAoIUhlbHBlcnMuaXNPbkFqYXhMb2FkaW5nKCkpIHtcbiAgICAgICAgICAgICAgICBsZXQgJGN1cnJlbnQgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgIGxldCAkcGFyZW50ID0gJGN1cnJlbnQuY2xvc2VzdCgndWwnKTtcbiAgICAgICAgICAgICAgICBsZXQgZGF0YSA9ICRjdXJyZW50LmRhdGEoKTtcbiAgICAgICAgICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtc1tkYXRhLnR5cGVdID0gZGF0YS52YWx1ZTtcblxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnR5cGUgPT09ICd2aWV3X2luJykge1xuICAgICAgICAgICAgICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5mb2xkZXJfaWQgPSAwO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS52YWx1ZSA9PT0gJ3RyYXNoJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgJChkb2N1bWVudCkuZmluZCgnLmpzLWluc2VydC10by1lZGl0b3InKS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJChkb2N1bWVudCkuZmluZCgnLmpzLWluc2VydC10by1lZGl0b3InKS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICRjdXJyZW50LmNsb3Nlc3QoJy5kcm9wZG93bicpLmZpbmQoJy5qcy1ydi1tZWRpYS1maWx0ZXItY3VycmVudCcpLmh0bWwoJygnICsgJGN1cnJlbnQuaHRtbCgpICsgJyknKTtcblxuICAgICAgICAgICAgICAgIEhlbHBlcnMuc3RvcmVDb25maWcoKTtcbiAgICAgICAgICAgICAgICBNZWRpYVNlcnZpY2UucmVmcmVzaEZpbHRlcigpO1xuXG4gICAgICAgICAgICAgICAgSGVscGVycy5yZXNldFBhZ2luYXRpb24oKTtcbiAgICAgICAgICAgICAgICBfc2VsZi5NZWRpYVNlcnZpY2UuZ2V0TWVkaWEodHJ1ZSk7XG5cbiAgICAgICAgICAgICAgICAkcGFyZW50LmZpbmQoJz4gbGknKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgJGN1cnJlbnQuY2xvc2VzdCgnbGknKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHNlYXJjaCgpIHtcblxuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuXG4gICAgICAgIHZhciBfcmVzZXRGb2xkZXIgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgIC8vIE9uY2UgcmVmcmVzaGluZyBkYXRhXG4gICAgICAgICAgICBpZiAoTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuZm9sZGVyX2lkID09PSAtMSkge1xuXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5zZWFyY2ggPT0gdW5kZWZpbmVkIHx8ICFNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5zZWFyY2gpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHByZXZGb2xkZXJJRCA9IEhlbHBlcnMuZ2V0UHJldkZvbGRlcklEKCk7XG4gICAgICAgICAgICAgICAgICAgIE1lZGlhQ29uZmlnLnJlcXVlc3RfcGFyYW1zLmZvbGRlcl9pZCA9IHByZXZGb2xkZXJJRCAhPSAtMSA/IHByZXZGb2xkZXJJRDowO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIEtlZXAgdGhlIHByZXYgZm9sZGVyIElEXG4gICAgICAgICAgICAgICAgSGVscGVycy5zZXRQcmV2Rm9sZGVySUQoTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuZm9sZGVyX2lkKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gQWxsb3cgc2VhcmNoaW5nIGFsbCBmb2xkZXJzXG4gICAgICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5zZWFyY2ggPSAkKCcjcnYtbWVkaWEtdGV4dC1ib3gtc2VhcmNoJykudmFsKCk7IC8vIHJlLWFzc2lnbiBrZXl3b3JkIHRvIHJlc2V0IHZhbHVlIGlmIHRoaXMgdmFsdWUgaXMgc3RvcmluZyBpbiBsb2NhbCBzdG9yYWdlXG4gICAgICAgICAgICBpZiAoTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuc2VhcmNoKSB7XG4gICAgICAgICAgICAgICAgTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuZm9sZGVyX2lkID0gLTE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBBdm9pZCBmaW5kIGFsbCBmb2xkZXJzIGFuZCBmaWxlcyB3aGVuIGxlYXZlIHRvIGFub3RoZXIgcGFnZSBhbmQgZ2V0IGJhY2sgYWZ0ZXJcbiAgICAgICAgICAgICAgICBpZiAoTWVkaWFDb25maWcucmVxdWVzdF9wYXJhbXMuZm9sZGVyX2lkID09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIE1lZGlhQ29uZmlnLnJlcXVlc3RfcGFyYW1zLmZvbGRlcl9pZCA9IDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIF9yZXNldEZvbGRlcigpO1xuXG4gICAgICAgIHZhciBfZGVsYXlTZWFyY2ggPSAoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICB2YXIgdGltZXIgPSAwO1xuICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihjYWxsYmFjaywgbXMpe1xuICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQgKHRpbWVyKTtcbiAgICAgICAgICAgICAgICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KGNhbGxiYWNrLCBtcyk7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0pKCksXG4gICAgICAgICAgICB0ZXh0Qm94ID0gJyNydi1tZWRpYS10ZXh0LWJveC1zZWFyY2gnLFxuICAgICAgICAgICAgJHNlYXJjaFRleHRib3ggPSAkKHRleHRCb3gpLFxuICAgICAgICAgICAgZGVsZXRlSWNvbiA9ICcjcnYtaWNvbi1ybS1zZWFyY2gtdGV4dCcsXG5cblxuICAgICAgICAgICAgX3NlYXJjaEZuID0gZnVuY3Rpb24gKGtleXdvcmQpIHtcbiAgICAgICAgICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5zZWFyY2ggPSBrZXl3b3JkO1xuICAgICAgICAgICAgICAgIF9yZXNldEZvbGRlcigpO1xuICAgICAgICAgICAgICAgIEhlbHBlcnMuc3RvcmVDb25maWcoKTtcbiAgICAgICAgICAgICAgICBIZWxwZXJzLnJlc2V0UGFnaW5hdGlvbigpO1xuICAgICAgICAgICAgICAgIF9zZWxmLk1lZGlhU2VydmljZS5nZXRNZWRpYSh0cnVlKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgLy8gSGlkZSB0aGUgcmVtb3ZlIGljb24gb25jZSB0aGUgdGV4dCBpcyBlbXB0eVxuICAgICAgICBpZiAoISAkc2VhcmNoVGV4dGJveC52YWwoKSkge1xuICAgICAgICAgICAgJChkZWxldGVJY29uKS5hZGRDbGFzcygnaGlkZGVuJyk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyByZW1vdmUga2V5d29yZFxuICAgICAgICAkKCcucnYtbWVkaWEtc2VhcmNoJykub2ZmKCdjbGljaycsIGRlbGV0ZUljb24pLm9uKCdjbGljaycsIGRlbGV0ZUljb24sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICRzZWFyY2hUZXh0Ym94LnZhbCgnJyk7XG4gICAgICAgICAgICBfc2VhcmNoRm4oJycpO1xuICAgICAgICB9KTtcblxuXG4gICAgICAgICQoJy5ydi1tZWRpYS1zZWFyY2ggLmlucHV0LXNlYXJjaC13cmFwcGVyJykub2ZmKCdrZXl1cCcsIHRleHRCb3gpLm9uKCdrZXl1cCcsIHRleHRCb3gsIGZ1bmN0aW9uKGUpe1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgICAgIHZhciBjdXJyZW50ID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgICAgICAgdmFyIGljb25SZW1vdmUgPSBjdXJyZW50LmNsb3Nlc3QoJy5zZWFyY2gtaW5wdXQtd3JhcHBlcicpLmZpbmQoZGVsZXRlSWNvbik7XG4gICAgICAgICAgICBpZihpY29uUmVtb3ZlLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgIGlmKGN1cnJlbnQudmFsKCkgIT0gJycpe1xuICAgICAgICAgICAgICAgICAgICBpZihpY29uUmVtb3ZlLmhhc0NsYXNzKCdoaWRkZW4nKSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uUmVtb3ZlLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgICAgICAgICBpZighaWNvblJlbW92ZS5oYXNDbGFzcygnaGlkZGVuJykpe1xuICAgICAgICAgICAgICAgICAgICAgICAgaWNvblJlbW92ZS5hZGRDbGFzcygnaGlkZGVuJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgX2RlbGF5U2VhcmNoKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgIF9zZWFyY2hGbigkdGhpcy52YWwoKSk7XG4gICAgICAgICAgICAgICAgfSwgNTAwKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gUHJldmVudCBzdWJtaXQgZm9ybVxuICAgICAgICBfc2VsZi4kYm9keS5vbignc3VibWl0JywgJy5pbnB1dC1zZWFyY2gtd3JhcHBlcicsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaGFuZGxlQWN0aW9ucygpIHtcbiAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcblxuICAgICAgICBfc2VsZi4kYm9keVxuICAgICAgICAgICAgLm9uKCdjbGljaycsICcucnYtbWVkaWEtYWN0aW9ucyAuanMtY2hhbmdlLWFjdGlvbltkYXRhLXR5cGU9XCJyZWZyZXNoXCJdJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgICAgIEhlbHBlcnMucmVzZXRQYWdpbmF0aW9uKCk7XG5cbiAgICAgICAgICAgICAgICBsZXQgZWxlX29wdGlvbnMgPSB0eXBlb2Ygd2luZG93LnJ2TWVkaWEuJGVsICE9PSAndW5kZWZpbmVkJyA/IHdpbmRvdy5ydk1lZGlhLiRlbC5kYXRhKCdydi1tZWRpYScpIDogdW5kZWZpbmVkO1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZWxlX29wdGlvbnMgIT09ICd1bmRlZmluZWQnICYmIGVsZV9vcHRpb25zLmxlbmd0aCA+IDAgJiYgdHlwZW9mIGVsZV9vcHRpb25zWzBdLnNlbGVjdGVkX2ZpbGVfaWQgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLk1lZGlhU2VydmljZS5nZXRNZWRpYSh0cnVlLCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKHRydWUsIGZhbHNlKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJy5ydi1tZWRpYS1pdGVtcyBsaS5uby1pdGVtcycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgJCgnLnJ2LW1lZGlhLWhlYWRlciAucnYtbWVkaWEtdG9wLWhlYWRlciAucnYtbWVkaWEtYWN0aW9ucyAuanMtZHJvcHpvbmUtdXBsb2FkJykudHJpZ2dlcignY2xpY2snKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ3N1Ym1pdCcsICcuZm9ybS1hZGQtZm9sZGVyJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBsZXQgJGlucHV0ID0gJCh0aGlzKS5maW5kKCdpbnB1dFt0eXBlPXRleHRdJyk7XG4gICAgICAgICAgICAgICAgbGV0IGZvbGRlck5hbWUgPSAkaW5wdXQudmFsKCk7XG4gICAgICAgICAgICAgICAgX3NlbGYuRm9sZGVyU2VydmljZS5jcmVhdGUoZm9sZGVyTmFtZSk7XG4gICAgICAgICAgICAgICAgJGlucHV0LnZhbCgnJyk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKCdjbGljaycsICcuanMtY2hhbmdlLWZvbGRlcicsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgbGV0IGZvbGRlcklkID0gIU1lZGlhQ29uZmlnLnJlcXVlc3RfcGFyYW1zLnNlYXJjaCA/ICQodGhpcykuZGF0YSgnZm9sZGVyJykgOiAtMTtcbiAgICAgICAgICAgICAgICBIZWxwZXJzLnJlc2V0UGFnaW5hdGlvbigpO1xuICAgICAgICAgICAgICAgIF9zZWxmLkZvbGRlclNlcnZpY2UuY2hhbmdlRm9sZGVyKGZvbGRlcklkKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJy5qcy1maWxlcy1hY3Rpb24nLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIEFjdGlvbnNTZXJ2aWNlLmhhbmRsZUdsb2JhbEFjdGlvbigkKHRoaXMpLmRhdGEoJ2FjdGlvbicpLCBmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgICAgICAgICAgIEhlbHBlcnMucmVzZXRQYWdpbmF0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLk1lZGlhU2VydmljZS5nZXRNZWRpYSh0cnVlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgIDtcbiAgICB9XG5cbiAgICBoYW5kbGVNb2RhbHMoKSB7XG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XG4gICAgICAgIC8qUmVuYW1lIGZpbGVzKi9cbiAgICAgICAgX3NlbGYuJGJvZHkub24oJ3Nob3cuYnMubW9kYWwnLCAnI21vZGFsX3JlbmFtZV9pdGVtcycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgQWN0aW9uc1NlcnZpY2UucmVuZGVyUmVuYW1lSXRlbXMoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIF9zZWxmLiRib2R5Lm9uKCdzdWJtaXQnLCAnI21vZGFsX3JlbmFtZV9pdGVtcyAuZm9ybS1yZW5hbWUnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBsZXQgaXRlbXMgPSBbXTtcbiAgICAgICAgICAgIGxldCAkZm9ybSA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgICQoJyNtb2RhbF9yZW5hbWVfaXRlbXMgLmZvcm0tY29udHJvbCcpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGxldCAkY3VycmVudCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgbGV0IGRhdGEgPSAkY3VycmVudC5jbG9zZXN0KCcuZm9ybS1ncm91cCcpLmRhdGEoKTtcbiAgICAgICAgICAgICAgICBkYXRhLm5hbWUgPSAkY3VycmVudC52YWwoKTtcbiAgICAgICAgICAgICAgICBpdGVtcy5wdXNoKGRhdGEpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIEFjdGlvbnNTZXJ2aWNlLnByb2Nlc3NBY3Rpb24oe1xuICAgICAgICAgICAgICAgIGFjdGlvbjogJGZvcm0uZGF0YSgnYWN0aW9uJyksXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWQ6IGl0ZW1zXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFyZXMuZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgJGZvcm0uY2xvc2VzdCgnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKHRydWUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNtb2RhbF9yZW5hbWVfaXRlbXMgLmZvcm0tZ3JvdXAnKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCAkY3VycmVudCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXy5jb250YWlucyhyZXMuZGF0YSwgJGN1cnJlbnQuZGF0YSgnaWQnKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY3VycmVudC5hZGRDbGFzcygnaGFzLWVycm9yJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjdXJyZW50LnJlbW92ZUNsYXNzKCdoYXMtZXJyb3InKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8qRGVsZXRlIGZpbGVzKi9cbiAgICAgICAgX3NlbGYuJGJvZHkub24oJ3N1Ym1pdCcsICcuZm9ybS1kZWxldGUtaXRlbXMnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBsZXQgaXRlbXMgPSBbXTtcbiAgICAgICAgICAgIGxldCAkZm9ybSA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgIF8uZWFjaChIZWxwZXJzLmdldFNlbGVjdGVkSXRlbXMoKSwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgIGlkOiB2YWx1ZS5pZCxcbiAgICAgICAgICAgICAgICAgICAgaXNfZm9sZGVyOiB2YWx1ZS5pc19mb2xkZXIsXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBBY3Rpb25zU2VydmljZS5wcm9jZXNzQWN0aW9uKHtcbiAgICAgICAgICAgICAgICBhY3Rpb246ICRmb3JtLmRhdGEoJ2FjdGlvbicpLFxuICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBpdGVtc1xuICAgICAgICAgICAgfSwgZnVuY3Rpb24gKHJlcykge1xuICAgICAgICAgICAgICAgICRmb3JtLmNsb3Nlc3QoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgaWYgKCFyZXMuZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKHRydWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAvKkVtcHR5IHRyYXNoKi9cbiAgICAgICAgX3NlbGYuJGJvZHkub24oJ3N1Ym1pdCcsICcjbW9kYWxfZW1wdHlfdHJhc2ggLnJ2LWZvcm0nLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBsZXQgJGZvcm0gPSAkKHRoaXMpO1xuXG4gICAgICAgICAgICBBY3Rpb25zU2VydmljZS5wcm9jZXNzQWN0aW9uKHtcbiAgICAgICAgICAgICAgICBhY3Rpb246ICRmb3JtLmRhdGEoJ2FjdGlvbicpXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgICAgICAgJGZvcm0uY2xvc2VzdCgnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICBfc2VsZi5NZWRpYVNlcnZpY2UuZ2V0TWVkaWEodHJ1ZSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLypTaGFyZSBmaWxlcyovXG4gICAgICAgIGxldCB1c2VycyA9IFtdO1xuICAgICAgICBsZXQgJHNoYXJlT3B0aW9uID0gJCgnI3NoYXJlX29wdGlvbicpO1xuICAgICAgICBsZXQgJHNoYXJlVG9Vc2VycyA9ICQoJyNzaGFyZV90b191c2VycycpO1xuICAgICAgICAkc2hhcmVPcHRpb24ub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGlmICgkKHRoaXMpLnZhbCgpID09PSAndXNlcicpIHtcbiAgICAgICAgICAgICAgICAkc2hhcmVUb1VzZXJzLmNsb3Nlc3QoJy5mb3JtLWdyb3VwJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkc2hhcmVUb1VzZXJzLmNsb3Nlc3QoJy5mb3JtLWdyb3VwJykuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KS50cmlnZ2VyKCdjaGFuZ2UnKTtcbiAgICAgICAgX3NlbGYuJGJvZHlcbiAgICAgICAgICAgIC5vbignc2hvdy5icy5tb2RhbCcsICcjbW9kYWxfc2hhcmVfaXRlbXMnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAkc2hhcmVPcHRpb24udmFsKCdub19zaGFyZScpLnRyaWdnZXIoJ2NoYW5nZScpO1xuICAgICAgICAgICAgICAgICRzaGFyZVRvVXNlcnMudmFsKCcnKTtcblxuICAgICAgICAgICAgICAgIGxldCBzZWxlY3RlZEl0ZW1zID0gSGVscGVycy5nZXRTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICAgICAgICAgICAgICBpZiAoXy5zaXplKHNlbGVjdGVkSXRlbXMpICE9PSAxKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgbGV0IGlzX3B1YmxpYyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICQuZWFjaChzZWxlY3RlZEl0ZW1zLCBmdW5jdGlvbiAoaW5kZXgsIGVsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZWwuaXNfcHVibGljID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc19wdWJsaWMgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKGlzX3B1YmxpYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNoYXJlT3B0aW9uLnZhbCgnZXZlcnlvbmUnKS50cmlnZ2VyKCdjaGFuZ2UnKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiBSVl9NRURJQV9VUkwuZ2V0X3VzZXJzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXJlcy5lcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNoYXJlVG9Vc2Vycy5odG1sKCcnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJzID0gcmVzLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmVhY2godXNlcnMsIGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBvcHRpb24gPSAnPG9wdGlvbiB2YWx1ZT1cIicgKyB2YWx1ZS5pZCArICdcIj4nICsgdmFsdWUubmFtZSArICc8L29wdGlvbj4nO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzaGFyZVRvVXNlcnMuYXBwZW5kKG9wdGlvbik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdlcnJvcicsIHJlcy5tZXNzYWdlLCBSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLm1lc3NhZ2UuZXJyb3JfaGVhZGVyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLmhhbmRsZUVycm9yKGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHNlbGVjdGVkSXRlbSA9IF8uZmlyc3Qoc2VsZWN0ZWRJdGVtcyk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkSXRlbS5pc19wdWJsaWMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzaGFyZU9wdGlvbi52YWwoJ2V2ZXJ5b25lJykudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogUlZfTUVESUFfVVJMLmdldF9zaGFyZWRfdXNlcnMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaGFyZV9pZDogc2VsZWN0ZWRJdGVtLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc19mb2xkZXI6IHNlbGVjdGVkSXRlbS5pc19mb2xkZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFyZXMuZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzaGFyZVRvVXNlcnMuaHRtbCgnJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VycyA9IHJlcy5kYXRhLnVzZXJzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRvdGFsU2VsZWN0ZWQgPSAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5lYWNoKHVzZXJzLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaXNTZWxlY3RlZCA9IHZhbHVlLmlzX3NlbGVjdGVkO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc1NlbGVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsU2VsZWN0ZWQrKztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG9wdGlvbiA9ICc8b3B0aW9uIHZhbHVlPVwiJyArIHZhbHVlLmlkICsgJ1wiICcgKyAoaXNTZWxlY3RlZCA/ICdzZWxlY3RlZCcgOiAnJykgKyAnPicgKyB2YWx1ZS5uYW1lICsgJzwvb3B0aW9uPic7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNoYXJlVG9Vc2Vycy5hcHBlbmQob3B0aW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRvdGFsU2VsZWN0ZWQgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNoYXJlT3B0aW9uLnZhbCgndXNlcicpLnRyaWdnZXIoJ2NoYW5nZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTWVzc2FnZVNlcnZpY2Uuc2hvd01lc3NhZ2UoJ2Vycm9yJywgcmVzLm1lc3NhZ2UsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5lcnJvcl9oZWFkZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTWVzc2FnZVNlcnZpY2UuaGFuZGxlRXJyb3IoZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKCdzdWJtaXQnLCAnI21vZGFsX3NoYXJlX2l0ZW1zIC5ydi1mb3JtJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBsZXQgJGZvcm0gPSAkKHRoaXMpO1xuXG4gICAgICAgICAgICAgICAgbGV0IGl0ZW1zID0gW107XG4gICAgICAgICAgICAgICAgXy5lYWNoKEhlbHBlcnMuZ2V0U2VsZWN0ZWRJdGVtcygpLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgaXRlbXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogdmFsdWUuaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICBpc19mb2xkZXI6IHZhbHVlLmlzX2ZvbGRlcixcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIEFjdGlvbnNTZXJ2aWNlLnByb2Nlc3NBY3Rpb24oe1xuICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICRmb3JtLmRhdGEoJ2FjdGlvbicpLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZDogaXRlbXMsXG4gICAgICAgICAgICAgICAgICAgIHNoYXJlX29wdGlvbjogJHNoYXJlT3B0aW9uLnZhbCgpLFxuICAgICAgICAgICAgICAgICAgICB1c2VyczogJHNoYXJlVG9Vc2Vycy52YWwoKVxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChyZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgJGZvcm0uY2xvc2VzdCgnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKHRydWUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vbignc3VibWl0JywgJyNtb2RhbF9zZXRfZm9jdXNfcG9pbnQgLnJ2LWZvcm0nLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGxldCAkZm9ybSA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgICAgICBsZXQgaXRlbXMgPSBbXTtcbiAgICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRfaXRlbXMgPSBIZWxwZXJzLmdldFNlbGVjdGVkSXRlbXMoKTtcbiAgICAgICAgICAgICAgICBfLmVhY2goc2VsZWN0ZWRfaXRlbXMsIGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICBpdGVtcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiB2YWx1ZS5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzX2ZvbGRlcjogdmFsdWUuaXNfZm9sZGVyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgQWN0aW9uc1NlcnZpY2UucHJvY2Vzc0FjdGlvbih7XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJGZvcm0uZGF0YSgnYWN0aW9uJyksXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBpdGVtcyxcbiAgICAgICAgICAgICAgICAgICAgZGF0YV9hdHRyaWJ1dGU6ICQoJy5oZWxwZXItdG9vbC1kYXRhLWF0dHInKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgY3NzX2JnX3Bvc2l0aW9uOiAkKCcuaGVscGVyLXRvb2wtY3NzMy12YWwnKS52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgcmV0aWNlX2NzczogJCgnLmhlbHBlci10b29sLXJldGljbGUtY3NzJykudmFsKClcbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgICAgICAgICAgICRmb3JtLmNsb3Nlc3QoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgICAgIF8uZWFjaChzZWxlY3RlZF9pdGVtcywgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUuaWQgPT09IHJlcy5kYXRhLmlkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnLmpzLW1lZGlhLWxpc3QtdGl0bGVbZGF0YS1pZD0nICsgdmFsdWUuaWQgKyAnXScpLmRhdGEocmVzLmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy52aWV3X2luID09PSAndHJhc2gnKSB7XG4gICAgICAgICAgICAkKGRvY3VtZW50KS5maW5kKCcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKGRvY3VtZW50KS5maW5kKCcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5iaW5kSW50ZWdyYXRlTW9kYWxFdmVudHMoKTtcbiAgICB9XG5cbiAgICBjaGVja0ZpbGVUeXBlU2VsZWN0KHNlbGVjdGVkRmlsZXMpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB3aW5kb3cucnZNZWRpYS4kZWwgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICBsZXQgZmlyc3RJdGVtID0gXy5maXJzdChzZWxlY3RlZEZpbGVzKTtcbiAgICAgICAgICAgIGxldCBlbGVfb3B0aW9ucyA9IHdpbmRvdy5ydk1lZGlhLiRlbC5kYXRhKCdydi1tZWRpYScpO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBlbGVfb3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGVsZV9vcHRpb25zWzBdICE9PSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgZWxlX29wdGlvbnNbMF0uZmlsZV90eXBlICE9PSAndW5kZWZpbmVkJyAmJiBmaXJzdEl0ZW0gIT09ICd1bmRlZmluZWQnXG4gICAgICAgICAgICAgICAgJiYgZmlyc3RJdGVtLnR5cGUgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFlbGVfb3B0aW9uc1swXS5maWxlX3R5cGUubWF0Y2goZmlyc3RJdGVtLnR5cGUpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIHR5cGVvZiBlbGVfb3B0aW9uc1swXS5leHRfYWxsb3dlZCAhPT0gJ3VuZGVmaW5lZCcgJiYgJC5pc0FycmF5KGVsZV9vcHRpb25zWzBdLmV4dF9hbGxvd2VkKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQuaW5BcnJheShmaXJzdEl0ZW0ubWltZV90eXBlLCBlbGVfb3B0aW9uc1swXS5leHRfYWxsb3dlZCkgPT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgYmluZEludGVncmF0ZU1vZGFsRXZlbnRzKCkge1xuICAgICAgICBsZXQgJG1haW5fbW9kYWwgPSAkKCcjcnZfbWVkaWFfbW9kYWwnKTtcbiAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcbiAgICAgICAgJG1haW5fbW9kYWwub2ZmKCdjbGljaycsICcuanMtaW5zZXJ0LXRvLWVkaXRvcicpLm9uKCdjbGljaycsICcuanMtaW5zZXJ0LXRvLWVkaXRvcicsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGxldCBzZWxlY3RlZEZpbGVzID0gSGVscGVycy5nZXRTZWxlY3RlZEZpbGVzKCk7XG4gICAgICAgICAgICBpZiAoXy5zaXplKHNlbGVjdGVkRmlsZXMpID4gMCkge1xuICAgICAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMub25TZWxlY3RGaWxlcyhzZWxlY3RlZEZpbGVzLCB3aW5kb3cucnZNZWRpYS4kZWwpO1xuICAgICAgICAgICAgICAgIGlmIChfc2VsZi5jaGVja0ZpbGVUeXBlU2VsZWN0KHNlbGVjdGVkRmlsZXMpKSB7XG4gICAgICAgICAgICAgICAgICAgICRtYWluX21vZGFsLmZpbmQoJy5jbG9zZScpLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkbWFpbl9tb2RhbC5vZmYoJ2RibGNsaWNrJywgJy5qcy1tZWRpYS1saXN0LXRpdGxlJykub24oJ2RibGNsaWNrJywgJy5qcy1tZWRpYS1saXN0LXRpdGxlJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgZGVidWdnZXJcblxuICAgICAgICAgICAgaWYgKEhlbHBlcnMuZ2V0Q29uZmlncygpLnJlcXVlc3RfcGFyYW1zLnZpZXdfaW4gIT09ICd0cmFzaCcpIHtcbiAgICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRGaWxlcyA9IEhlbHBlcnMuZ2V0U2VsZWN0ZWRGaWxlcygpO1xuICAgICAgICAgICAgICAgIGlmIChfLnNpemUoc2VsZWN0ZWRGaWxlcykgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5ydk1lZGlhLm9wdGlvbnMub25TZWxlY3RGaWxlcyhzZWxlY3RlZEZpbGVzLCB3aW5kb3cucnZNZWRpYS4kZWwpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoX3NlbGYuY2hlY2tGaWxlVHlwZVNlbGVjdChzZWxlY3RlZEZpbGVzKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJG1haW5fbW9kYWwuZmluZCgnLmNsb3NlJykudHJpZ2dlcignY2xpY2snKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgQWN0aW9uc1NlcnZpY2UuaGFuZGxlUHJldmlldygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzdGF0aWMgc2V0dXBTZWN1cml0eSgpIHtcbiAgICAgICAgJC5hamF4U2V0dXAoe1xuICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICdYLUNTUkYtVE9LRU4nOiAkKCdtZXRhW25hbWU9XCJjc3JmLXRva2VuXCJdJykuYXR0cignY29udGVudCcpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuXG5cbiAgICAvL3Njcm9sbCBnZXQgbW9yZSBtZWRpYVxuICAgIHNjcm9sbEdldE1vcmUoKSB7XG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XG4gICAgICAgICQoJy5ydi1tZWRpYS1tYWluIC5ydi1tZWRpYS1pdGVtcycpLmJpbmQoJ0RPTU1vdXNlU2Nyb2xsIG1vdXNld2hlZWwnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgaWYgKGUub3JpZ2luYWxFdmVudC5kZXRhaWwgPiAwIHx8IGUub3JpZ2luYWxFdmVudC53aGVlbERlbHRhIDwgMCkge1xuICAgICAgICAgICAgICAgIGxldCAkbG9hZF9tb3JlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgaWYgKCQodGhpcykuY2xvc2VzdCgnLm1lZGlhLW1vZGFsJykubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAkbG9hZF9tb3JlID0gJCh0aGlzKS5zY3JvbGxUb3AoKSArICQodGhpcykuaW5uZXJIZWlnaHQoKSAvMiA+PSAkKHRoaXMpWzBdLnNjcm9sbEhlaWdodCAtIDQ1MFxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICRsb2FkX21vcmUgPSAkKHRoaXMpLnNjcm9sbFRvcCgpICsgJCh0aGlzKS5pbm5lckhlaWdodCgpID49ICQodGhpcylbMF0uc2Nyb2xsSGVpZ2h0IC0gMTUwXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKCRsb2FkX21vcmUpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBSVl9NRURJQV9DT05GSUcucGFnaW5hdGlvbiAhPSAndW5kZWZpbmVkJyAmJiBSVl9NRURJQV9DT05GSUcucGFnaW5hdGlvbi5oYXNfbW9yZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgX3NlbGYuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKGZhbHNlLCBmYWxzZSwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgIHdpbmRvdy5ydk1lZGlhID0gd2luZG93LnJ2TWVkaWEgfHwge307XG5cbiAgICBNZWRpYU1hbmFnZW1lbnQuc2V0dXBTZWN1cml0eSgpO1xuICAgIG5ldyBNZWRpYU1hbmFnZW1lbnQoKS5pbml0KCk7XG5cbiAgICAkKCcjbW9kYWxfcmVuYW1lX2l0ZW1zJykub2ZmKCdrZXlwcmVzcycsICdpbnB1dCcpLm9uKCdrZXlwcmVzcycsICdpbnB1dCcsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHZhciB2YWx1ZSA9IGUua2V5O1xuICAgICAgICB2YXIgcGF0dGVybiA9IC9eW0EtWmEtejAtOSAnLi1dKyQvO1xuICAgICAgICBpZiAoIXBhdHRlcm4udGVzdCh2YWx1ZSkpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgJCgnI21vZGFsX3JlbmFtZV9pdGVtcycpLm9mZignY2xpY2snLCAnLm1lZGlhLXJlbmFtZS1jdXN0b20tYWN0aW9uJykub24oJ2NsaWNrJywgJy5tZWRpYS1yZW5hbWUtY3VzdG9tLWFjdGlvbicsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHZhciBjdXJyZW50ID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgICB2YXIgbGlzdElucHV0ID0gY3VycmVudC5jbG9zZXN0KCdmb3JtJykuZmluZCgnaW5wdXQnKTtcbiAgICAgICAgaWYgKGxpc3RJbnB1dC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB2YXIgY2hlY2tWYWx1ZSA9IHRydWU7XG4gICAgICAgICAgICAkLmVhY2gobGlzdElucHV0LCBmdW5jdGlvbiAoaW5kZXgsIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICBpdGVtID0gJChpdGVtKTtcbiAgICAgICAgICAgICAgICBpZihpdGVtLnZhbCgpID09ICcnIHx8ICQudHJpbShpdGVtLnZhbCgpKSA9PSAnJykge1xuICAgICAgICAgICAgICAgICAgICBjaGVja1ZhbHVlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0uYWRkQ2xhc3MoJ25hbWUtcmVxdWlyZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5mb2N1cygpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0ucmVtb3ZlQ2xhc3MoJ25hbWUtcmVxdWlyZWQnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGlmICghY2hlY2tWYWx1ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xuXG5cbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvbWVkaWEuanMiLCJpbXBvcnQge0FjdGlvbnNTZXJ2aWNlfSBmcm9tICcuL0FjdGlvbnNTZXJ2aWNlJztcbmltcG9ydCB7SGVscGVyc30gZnJvbSAnLi4vSGVscGVycy9IZWxwZXJzJztcblxuZXhwb3J0IGNsYXNzIENvbnRleHRNZW51U2VydmljZSB7XG4gICAgc3RhdGljIGluaXRDb250ZXh0KCkge1xuICAgICAgICBpZiAoalF1ZXJ5KCkuY29udGV4dE1lbnUpIHtcbiAgICAgICAgICAgICQuY29udGV4dE1lbnUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdG9yOiAnLmpzLWNvbnRleHQtbWVudVtkYXRhLWNvbnRleHQ9XCJmaWxlXCJdJyxcbiAgICAgICAgICAgICAgICBidWlsZDogZnVuY3Rpb24gKCRlbGVtZW50LCBldmVudCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbXM6IENvbnRleHRNZW51U2VydmljZS5fZmlsZUNvbnRleHRNZW51KCksXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkLmNvbnRleHRNZW51KHtcbiAgICAgICAgICAgICAgICBzZWxlY3RvcjogJy5qcy1jb250ZXh0LW1lbnVbZGF0YS1jb250ZXh0PVwiZm9sZGVyXCJdJyxcbiAgICAgICAgICAgICAgICBidWlsZDogZnVuY3Rpb24gKCRlbGVtZW50LCBldmVudCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbXM6IENvbnRleHRNZW51U2VydmljZS5fZm9sZGVyQ29udGV4dE1lbnUoKSxcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGF0aWMgX2ZpbGVDb250ZXh0TWVudSgpIHtcbiAgICAgICAgbGV0IGl0ZW1zID0ge1xuICAgICAgICAgICAgcHJldmlldzoge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdQcmV2aWV3JyxcbiAgICAgICAgICAgICAgICBpY29uOiBmdW5jdGlvbiAob3B0LCAkaXRlbUVsZW1lbnQsIGl0ZW1LZXksIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgJGl0ZW1FbGVtZW50Lmh0bWwoJzxpIGNsYXNzPVwiZmEgZmEtZXllXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPiAnICsgaXRlbS5uYW1lKTtcblxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ2NvbnRleHQtbWVudS1pY29uLXVwZGF0ZWQnO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uIChrZXksIG9wdCkge1xuICAgICAgICAgICAgICAgICAgICBBY3Rpb25zU2VydmljZS5oYW5kbGVQcmV2aWV3KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHNldF9mb2N1c19wb2ludDoge1xuICAgICAgICAgICAgICAgIG5hbWU6IFwiU2V0IGZvY3VzIHBvaW50XCIsXG4gICAgICAgICAgICAgICAgaWNvbjogZnVuY3Rpb24gKG9wdCwgJGl0ZW1FbGVtZW50LCBpdGVtS2V5LCBpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICRpdGVtRWxlbWVudC5odG1sKCc8aSBjbGFzcz1cImZhIGZhLWRvdC1jaXJjbGUtb1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT4gJyArIGl0ZW0ubmFtZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdjb250ZXh0LW1lbnUtaWNvbi11cGRhdGVkJztcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoa2V5LCBvcHQpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnLmpzLWZpbGVzLWFjdGlvbltkYXRhLWFjdGlvbj1cInNldF9mb2N1c19wb2ludFwiXScpLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcblxuICAgICAgICBfLmVhY2goSGVscGVycy5nZXRDb25maWdzKCkuYWN0aW9uc19saXN0LCBmdW5jdGlvbiAoYWN0aW9uR3JvdXAsIGtleSkge1xuICAgICAgICAgICAgXy5lYWNoKGFjdGlvbkdyb3VwLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICBpdGVtc1t2YWx1ZS5hY3Rpb25dID0ge1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiB2YWx1ZS5uYW1lLFxuICAgICAgICAgICAgICAgICAgICBpY29uOiBmdW5jdGlvbiAob3B0LCAkaXRlbUVsZW1lbnQsIGl0ZW1LZXksIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRpdGVtRWxlbWVudC5odG1sKCc8aSBjbGFzcz1cIicgKyB2YWx1ZS5pY29uICsgJ1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT4gJyArIChSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLmFjdGlvbnNfbGlzdFtrZXldW3ZhbHVlLmFjdGlvbl0gfHwgaXRlbS5uYW1lKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnY29udGV4dC1tZW51LWljb24tdXBkYXRlZCc7XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoa2V5LCBvcHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5qcy1maWxlcy1hY3Rpb25bZGF0YS1hY3Rpb249XCInICsgdmFsdWUuYWN0aW9uICsgJ1wiXScpLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfSk7XG5cbiAgICAgICAgbGV0IGV4Y2VwdCA9IFtdO1xuXG4gICAgICAgIHN3aXRjaCAoSGVscGVycy5nZXRSZXF1ZXN0UGFyYW1zKCkudmlld19pbikge1xuICAgICAgICAgICAgY2FzZSAnbXlfbWVkaWEnOlxuICAgICAgICAgICAgICAgIGV4Y2VwdCA9IFsncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ3JlbW92ZV9zaGFyZSddO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAncHVibGljJzpcbiAgICAgICAgICAgICAgICBleGNlcHQgPSBbJ3JlbW92ZV9mYXZvcml0ZScsICdkZWxldGUnLCAncmVzdG9yZScsICdyZW1vdmVfc2hhcmUnXTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ3NoYXJlZCc6XG4gICAgICAgICAgICAgICAgZXhjZXB0ID0gWydtYWtlX2NvcHknLCAncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ3JlbW92ZV9zaGFyZSddO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAnc2hhcmVkX3dpdGhfbWUnOlxuICAgICAgICAgICAgICAgIGV4Y2VwdCA9IFsnc2hhcmUnLCAncmVtb3ZlX2Zhdm9yaXRlJywgJ2RlbGV0ZScsICdyZXN0b3JlJywgJ21ha2VfY29weSddO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAncmVjZW50JzpcbiAgICAgICAgICAgICAgICBleGNlcHQgPSBbJ3JlbW92ZV9mYXZvcml0ZScsICdkZWxldGUnLCAncmVzdG9yZScsICdyZW1vdmVfc2hhcmUnLCAnbWFrZV9jb3B5J107XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdmYXZvcml0ZXMnOlxuICAgICAgICAgICAgICAgIGV4Y2VwdCA9IFsnZmF2b3JpdGUnLCAnZGVsZXRlJywgJ3Jlc3RvcmUnLCAncmVtb3ZlX3NoYXJlJywgJ21ha2VfY29weSddO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAndHJhc2gnOlxuICAgICAgICAgICAgICAgIGl0ZW1zID0ge1xuICAgICAgICAgICAgICAgICAgICBwcmV2aWV3OiBpdGVtcy5wcmV2aWV3LFxuICAgICAgICAgICAgICAgICAgICByZW5hbWU6IGl0ZW1zLnJlbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgZG93bmxvYWQ6IGl0ZW1zLmRvd25sb2FkLFxuICAgICAgICAgICAgICAgICAgICBkZWxldGU6IGl0ZW1zLmRlbGV0ZSxcbiAgICAgICAgICAgICAgICAgICAgcmVzdG9yZTogaXRlbXMucmVzdG9yZSxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgXy5lYWNoKGV4Y2VwdCwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICBpdGVtc1t2YWx1ZV0gPSB1bmRlZmluZWQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChIZWxwZXJzLmdldFNlbGVjdGVkSXRlbXMoKS5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICBpdGVtcy5zZXRfZm9jdXNfcG9pbnQgPSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgaGFzRm9sZGVyU2VsZWN0ZWQgPSBIZWxwZXJzLmdldFNlbGVjdGVkRm9sZGVyKCkubGVuZ3RoID4gMDtcblxuICAgICAgICBpZiAoaGFzRm9sZGVyU2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIGl0ZW1zLnByZXZpZXcgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBpdGVtcy5zZXRfZm9jdXNfcG9pbnQgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBpdGVtcy5jb3B5X2xpbmsgPSB1bmRlZmluZWQ7XG5cbiAgICAgICAgICAgIGlmICghXy5jb250YWlucyhSVl9NRURJQV9DT05GSUcucGVybWlzc2lvbnMsICdmb2xkZXJzLmNyZWF0ZScpKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMubWFrZV9jb3B5ID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZm9sZGVycy5lZGl0JykpIHtcbiAgICAgICAgICAgICAgICBpdGVtcy5yZW5hbWUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgaXRlbXMuc2hhcmUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgaXRlbXMucmVtb3ZlX3NoYXJlID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgICAgIGl0ZW1zLnVuX3NoYXJlID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZm9sZGVycy50cmFzaCcpKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMudHJhc2ggPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgaXRlbXMucmVzdG9yZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZvbGRlcnMuZGVsZXRlJykpIHtcbiAgICAgICAgICAgICAgICBpdGVtcy5kZWxldGUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgc2VsZWN0ZWRGaWxlcyA9IEhlbHBlcnMuZ2V0U2VsZWN0ZWRGaWxlcygpO1xuICAgICAgICBpZiAoc2VsZWN0ZWRGaWxlcy5sZW5ndGggPiAwICYmIHNlbGVjdGVkRmlsZXNbMF0udHlwZSAhPT0gJ2ltYWdlJykge1xuICAgICAgICAgICAgaXRlbXMuc2V0X2ZvY3VzX3BvaW50ID0gdW5kZWZpbmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHNlbGVjdGVkRmlsZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZpbGVzLmNyZWF0ZScpKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMubWFrZV9jb3B5ID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIV8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZmlsZXMuZWRpdCcpKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMucmVuYW1lID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgICAgIGl0ZW1zLnNldF9mb2N1c19wb2ludCA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgICAgICBpdGVtcy5zaGFyZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgICAgICBpdGVtcy5yZW1vdmVfc2hhcmUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgaXRlbXMudW5fc2hhcmUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICghXy5jb250YWlucyhSVl9NRURJQV9DT05GSUcucGVybWlzc2lvbnMsICdmaWxlcy50cmFzaCcpKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMudHJhc2ggPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICAgICAgaXRlbXMucmVzdG9yZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFfLmNvbnRhaW5zKFJWX01FRElBX0NPTkZJRy5wZXJtaXNzaW9ucywgJ2ZpbGVzLmRlbGV0ZScpKSB7XG4gICAgICAgICAgICAgICAgaXRlbXMuZGVsZXRlID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGNhbl9wcmV2aWV3ID0gZmFsc2U7XG4gICAgICAgIF8uZWFjaChzZWxlY3RlZEZpbGVzLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgIGlmIChfLmNvbnRhaW5zKFsnaW1hZ2UnLCAneW91dHViZScsICdwZGYnLCAndGV4dCcsICd2aWRlbyddLCB2YWx1ZS50eXBlKSkge1xuICAgICAgICAgICAgICAgIGNhbl9wcmV2aWV3ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKCFjYW5fcHJldmlldykge1xuICAgICAgICAgICAgaXRlbXMucHJldmlldyA9IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChSVl9NRURJQV9DT05GSUcubW9kZSA9PT0gJ3NpbXBsZScpIHtcbiAgICAgICAgICAgIGl0ZW1zLnNoYXJlID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgaXRlbXMudW5fc2hhcmUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBpdGVtcy5yZW1vdmVfc2hhcmUgPSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gaXRlbXM7XG4gICAgfVxuXG4gICAgc3RhdGljIF9mb2xkZXJDb250ZXh0TWVudSgpIHtcbiAgICAgICAgbGV0IGl0ZW1zID0gQ29udGV4dE1lbnVTZXJ2aWNlLl9maWxlQ29udGV4dE1lbnUoKTtcblxuICAgICAgICBpdGVtcy5wcmV2aWV3ID0gdW5kZWZpbmVkO1xuICAgICAgICBpdGVtcy5zZXRfZm9jdXNfcG9pbnQgPSB1bmRlZmluZWQ7XG4gICAgICAgIGl0ZW1zLmNvcHlfbGluayA9IHVuZGVmaW5lZDtcblxuICAgICAgICByZXR1cm4gaXRlbXM7XG4gICAgfVxuXG4gICAgc3RhdGljIGRlc3Ryb3lDb250ZXh0KCkge1xuICAgICAgICBpZiAoalF1ZXJ5KCkuY29udGV4dE1lbnUpIHtcbiAgICAgICAgICAgICQuY29udGV4dE1lbnUoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL1NlcnZpY2VzL0NvbnRleHRNZW51U2VydmljZS5qcyIsImltcG9ydCB7SGVscGVyc30gZnJvbSAnLi4vSGVscGVycy9IZWxwZXJzJztcblxuZXhwb3J0IGNsYXNzIE1lZGlhRGV0YWlscyB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMuJGRldGFpbHNXcmFwcGVyID0gJCgnLnJ2LW1lZGlhLW1haW4gLnJ2LW1lZGlhLWRldGFpbHMnKTtcblxuICAgICAgICB0aGlzLmRlc2NyaXB0aW9uSXRlbVRlbXBsYXRlID0gJzxkaXYgY2xhc3M9XCJydi1tZWRpYS1uYW1lXCI+PHA+X190aXRsZV9fPC9wPl9fdXJsX188L2Rpdj4nO1xuXG4gICAgICAgIHRoaXMub25seUZpZWxkcyA9IFtcbiAgICAgICAgICAgICduYW1lJyxcbiAgICAgICAgICAgICdmdWxsX3VybCcsXG4gICAgICAgICAgICAnc2l6ZScsXG4gICAgICAgICAgICAnbWltZV90eXBlJyxcbiAgICAgICAgICAgICdjcmVhdGVkX2F0JyxcbiAgICAgICAgICAgICd1cGRhdGVkX2F0JyxcbiAgICAgICAgICAgICdub3RoaW5nX3NlbGVjdGVkJyxcbiAgICAgICAgXTtcblxuICAgICAgICB0aGlzLmV4dGVybmFsVHlwZXMgPSBbXG4gICAgICAgICAgICAneW91dHViZScsXG4gICAgICAgICAgICAndmltZW8nLFxuICAgICAgICAgICAgJ21ldGFjYWZlJyxcbiAgICAgICAgICAgICdkYWlseW1vdGlvbicsXG4gICAgICAgICAgICAndmluZScsXG4gICAgICAgICAgICAnaW5zdGFncmFtJyxcbiAgICAgICAgXTtcbiAgICB9XG5cbiAgICByZW5kZXJEYXRhKGRhdGEpIHtcbiAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcbiAgICAgICAgbGV0IHRodW1iID0gZGF0YS50eXBlID09PSAnaW1hZ2UnID8gJzxpbWcgc3JjPVwiJyArIGRhdGEuZnVsbF91cmwgKyAnXCIgYWx0PVwiJyArIGRhdGEubmFtZSArICdcIj4nIDogZGF0YS5taW1lX3R5cGUgPT09ICd5b3V0dWJlJyA/ICc8aW1nIHNyYz1cIicgKyBkYXRhLm9wdGlvbnMudGh1bWIgKyAnXCIgYWx0PVwiJyArIGRhdGEubmFtZSArICdcIj4nIDogJzxpIGNsYXNzPVwiJyArIGRhdGEuaWNvbiArICdcIj48L2k+JztcbiAgICAgICAgbGV0IGRlc2NyaXB0aW9uID0gJyc7XG4gICAgICAgIGxldCB1c2VDbGlwYm9hcmQgPSBmYWxzZTtcbiAgICAgICAgXy5mb3JFYWNoKGRhdGEsIGZ1bmN0aW9uICh2YWwsIGluZGV4KSB7XG4gICAgICAgICAgICBpZiAoXy5jb250YWlucyhfc2VsZi5vbmx5RmllbGRzLCBpbmRleCkpIHtcbiAgICAgICAgICAgICAgICBpZiAoKCFfLmNvbnRhaW5zKF9zZWxmLmV4dGVybmFsVHlwZXMsIGRhdGEudHlwZSkpIHx8IChfLmNvbnRhaW5zKF9zZWxmLmV4dGVybmFsVHlwZXMsIGRhdGEudHlwZSkgJiYgIV8uY29udGFpbnMoWydzaXplJywgJ21pbWVfdHlwZSddLCBpbmRleCkpKSB7XG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uICs9IF9zZWxmLmRlc2NyaXB0aW9uSXRlbVRlbXBsYXRlXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvX190aXRsZV9fL2dpLCBSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zW2luZGV4XSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX3VybF9fL2dpLCB2YWwgPyBpbmRleCA9PT0gJ2Z1bGxfdXJsJyA/ICc8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXBcIj48aW5wdXQgaWQ9XCJmaWxlX2RldGFpbHNfdXJsXCIgdHlwZT1cInRleHRcIiB2YWx1ZT1cIicgKyB2YWwgKyAnXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIj48c3BhbiBjbGFzcz1cImlucHV0LWdyb3VwLWJ0blwiPjxidXR0b24gY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQganMtYnRuLWNvcHktdG8tY2xpcGJvYXJkXCIgdHlwZT1cImJ1dHRvblwiIGRhdGEtY2xpcGJvYXJkLXRhcmdldD1cIiNmaWxlX2RldGFpbHNfdXJsXCIgdGl0bGU9XCJDb3BpZWRcIiBkYXRhLXRyaWdnZXI9XCJjbGlja1wiPjxpbWcgY2xhc3M9XCJjbGlwcHlcIiBzcmM9XCInICsgSGVscGVycy5hc3NldCgnL3ZlbmRvci9tZWRpYS9pbWFnZXMvY2xpcHB5LnN2ZycpICsgJ1wiIHdpZHRoPVwiMTNcIiBhbHQ9XCJDb3B5IHRvIGNsaXBib2FyZFwiPjwvYnV0dG9uPjwvc3Bhbj48L2Rpdj4nIDogJzxzcGFuIHRpdGxlPVwiJyArIHZhbCArICdcIj4nICsgdmFsICsgJzwvc3Bhbj4nIDogJycpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggPT09ICdmdWxsX3VybCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZUNsaXBib2FyZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBfc2VsZi4kZGV0YWlsc1dyYXBwZXIuZmluZCgnLnJ2LW1lZGlhLXRodW1ibmFpbCcpLmh0bWwodGh1bWIpO1xuICAgICAgICBfc2VsZi4kZGV0YWlsc1dyYXBwZXIuZmluZCgnLnJ2LW1lZGlhLWRlc2NyaXB0aW9uJykuaHRtbChkZXNjcmlwdGlvbik7XG4gICAgICAgIGlmICh1c2VDbGlwYm9hcmQpIHtcbiAgICAgICAgICAgIGxldCBjbGlwYm9hcmQgPSBuZXcgQ2xpcGJvYXJkKCcuanMtYnRuLWNvcHktdG8tY2xpcGJvYXJkJyk7XG4gICAgICAgICAgICAkKCcuanMtYnRuLWNvcHktdG8tY2xpcGJvYXJkJykudG9vbHRpcCgpLm9uKCdtb3VzZWxlYXZlJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS50b29sdGlwKCdoaWRlJyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL1ZpZXdzL01lZGlhRGV0YWlscy5qcyIsImltcG9ydCB7TWVkaWFMaXN0fSBmcm9tICcuLi9WaWV3cy9NZWRpYUxpc3QnO1xuaW1wb3J0IHtNZWRpYUNvbmZpZ30gZnJvbSAnLi4vQ29uZmlnL01lZGlhQ29uZmlnJztcbmltcG9ydCB7TWVkaWFTZXJ2aWNlfSBmcm9tICcuL01lZGlhU2VydmljZSc7XG5pbXBvcnQge01lc3NhZ2VTZXJ2aWNlfSBmcm9tICcuLi9TZXJ2aWNlcy9NZXNzYWdlU2VydmljZSc7XG5pbXBvcnQge0hlbHBlcnN9IGZyb20gJy4uL0hlbHBlcnMvSGVscGVycyc7XG5cbmV4cG9ydCBjbGFzcyBGb2xkZXJTZXJ2aWNlIHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5NZWRpYUxpc3QgPSBuZXcgTWVkaWFMaXN0KCk7XG4gICAgICAgIHRoaXMuTWVkaWFTZXJ2aWNlID0gbmV3IE1lZGlhU2VydmljZSgpO1xuXG4gICAgICAgICQoJ2JvZHknKS5vbignc2hvd24uYnMubW9kYWwnLCAnI21vZGFsX2FkZF9mb2xkZXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKHRoaXMpLmZpbmQoJy5mb3JtLWFkZC1mb2xkZXIgaW5wdXRbdHlwZT10ZXh0XScpLmZvY3VzKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNyZWF0ZShmb2xkZXJOYW1lKSB7XG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB1cmw6IFJWX01FRElBX1VSTC5jcmVhdGVfZm9sZGVyLFxuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxuICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgIHBhcmVudF9pZDogSGVscGVycy5nZXRSZXF1ZXN0UGFyYW1zKCkuZm9sZGVyX2lkLFxuICAgICAgICAgICAgICAgIG5hbWU6IGZvbGRlck5hbWVcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgICAgYmVmb3JlU2VuZDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIEhlbHBlcnMuc2hvd0FqYXhMb2FkaW5nKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlcykge1xuICAgICAgICAgICAgICAgIGlmIChyZXMuZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgTWVzc2FnZVNlcnZpY2Uuc2hvd01lc3NhZ2UoJ2Vycm9yJywgcmVzLm1lc3NhZ2UsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5lcnJvcl9oZWFkZXIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdzdWNjZXNzJywgcmVzLm1lc3NhZ2UsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5zdWNjZXNzX2hlYWRlcik7XG4gICAgICAgICAgICAgICAgICAgIEhlbHBlcnMucmVzZXRQYWdpbmF0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLk1lZGlhU2VydmljZS5nZXRNZWRpYSh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgRm9sZGVyU2VydmljZS5jbG9zZU1vZGFsKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNvbXBsZXRlOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIEhlbHBlcnMuaGlkZUFqYXhMb2FkaW5nKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgTWVzc2FnZVNlcnZpY2UuaGFuZGxlRXJyb3IoZGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNoYW5nZUZvbGRlcihmb2xkZXJJZCkge1xuICAgICAgICBNZWRpYUNvbmZpZy5yZXF1ZXN0X3BhcmFtcy5mb2xkZXJfaWQgPSBmb2xkZXJJZDtcbiAgICAgICAgSGVscGVycy5zZXRQcmV2Rm9sZGVySUQoZm9sZGVySWQpO1xuICAgICAgICBIZWxwZXJzLnN0b3JlQ29uZmlnKCk7XG4gICAgICAgIHRoaXMuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKHRydWUpO1xuICAgIH1cblxuICAgIHN0YXRpYyBjbG9zZU1vZGFsKCkge1xuICAgICAgICAkKCcjbW9kYWxfYWRkX2ZvbGRlcicpLm1vZGFsKCdoaWRlJyk7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvU2VydmljZXMvRm9sZGVyU2VydmljZS5qcyIsImltcG9ydCB7TWVkaWFTZXJ2aWNlfSBmcm9tICcuLi9TZXJ2aWNlcy9NZWRpYVNlcnZpY2UnO1xuaW1wb3J0IHtIZWxwZXJzfSBmcm9tICcuLi9IZWxwZXJzL0hlbHBlcnMnO1xuXG5leHBvcnQgY2xhc3MgVXBsb2FkU2VydmljZSB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMuJGJvZHkgPSAkKCdib2R5Jyk7XG5cbiAgICAgICAgdGhpcy5kcm9wWm9uZSA9IG51bGw7XG5cbiAgICAgICAgdGhpcy51cGxvYWRVcmwgPSBSVl9NRURJQV9VUkwudXBsb2FkX2ZpbGU7XG5cbiAgICAgICAgdGhpcy51cGxvYWRQcm9ncmVzc0JveCA9ICQoJy5ydi11cGxvYWQtcHJvZ3Jlc3MnKTtcblxuICAgICAgICB0aGlzLnVwbG9hZFByb2dyZXNzQ29udGFpbmVyID0gJCgnLnJ2LXVwbG9hZC1wcm9ncmVzcyAucnYtdXBsb2FkLXByb2dyZXNzLXRhYmxlJyk7XG5cbiAgICAgICAgdGhpcy51cGxvYWRQcm9ncmVzc1RlbXBsYXRlID0gJCgnI3J2X21lZGlhX3VwbG9hZF9wcm9ncmVzc19pdGVtJykuaHRtbCgpO1xuXG4gICAgICAgIHRoaXMudG90YWxRdWV1ZWQgPSAxO1xuXG4gICAgICAgIHRoaXMuTWVkaWFTZXJ2aWNlID0gbmV3IE1lZGlhU2VydmljZSgpO1xuXG4gICAgICAgIHRoaXMudG90YWxFcnJvciA9IDA7XG4gICAgfVxuXG4gICAgaW5pdCgpIHtcbiAgICAgICAgaWYgKF8uY29udGFpbnMoUlZfTUVESUFfQ09ORklHLnBlcm1pc3Npb25zLCAnZmlsZXMuY3JlYXRlJykgJiYgJCgnLnJ2LW1lZGlhLWl0ZW1zJykubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgdGhpcy5zZXR1cERyb3Bab25lKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5oYW5kbGVFdmVudHMoKTtcbiAgICB9XG5cbiAgICBzZXR1cERyb3Bab25lKCkge1xuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuICAgICAgICBfc2VsZi5kcm9wWm9uZSA9IG5ldyBEcm9wem9uZShkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucnYtbWVkaWEtaXRlbXMnKSwge1xuICAgICAgICAgICAgdXJsOiBfc2VsZi51cGxvYWRVcmwsXG4gICAgICAgICAgICB0aHVtYm5haWxXaWR0aDogZmFsc2UsXG4gICAgICAgICAgICB0aHVtYm5haWxIZWlnaHQ6IGZhbHNlLFxuICAgICAgICAgICAgcGFyYWxsZWxVcGxvYWRzOiAxLFxuICAgICAgICAgICAgYXV0b1F1ZXVlOiB0cnVlLFxuICAgICAgICAgICAgY2xpY2thYmxlOiAnLmpzLWRyb3B6b25lLXVwbG9hZCcsXG4gICAgICAgICAgICBwcmV2aWV3VGVtcGxhdGU6IGZhbHNlLFxuICAgICAgICAgICAgcHJldmlld3NDb250YWluZXI6IGZhbHNlLFxuICAgICAgICAgICAgdXBsb2FkTXVsdGlwbGU6IHRydWUsXG4gICAgICAgICAgICBzZW5kaW5nOiBmdW5jdGlvbiAoZmlsZSwgeGhyLCBmb3JtRGF0YSkge1xuICAgICAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZCgnX3Rva2VuJywgJCgnbWV0YVtuYW1lPVwiY3NyZi10b2tlblwiXScpLmF0dHIoJ2NvbnRlbnQnKSk7XG4gICAgICAgICAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdmb2xkZXJfaWQnLCBIZWxwZXJzLmdldFJlcXVlc3RQYXJhbXMoKS5mb2xkZXJfaWQpO1xuICAgICAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZCgndmlld19pbicsIEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLnZpZXdfaW4pO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgfSk7XG5cbiAgICAgICAgX3NlbGYuZHJvcFpvbmUub24oJ2FkZGVkZmlsZScsIGZpbGUgPT4ge1xuICAgICAgICAgICAgZmlsZS5pbmRleCA9IF9zZWxmLnRvdGFsUXVldWVkO1xuICAgICAgICAgICAgX3NlbGYudG90YWxRdWV1ZWQrKztcbiAgICAgICAgfSk7XG5cbiAgICAgICAgX3NlbGYuZHJvcFpvbmUub24oJ3NlbmRpbmcnLCBmaWxlID0+IHtcbiAgICAgICAgICAgIF9zZWxmLmluaXRQcm9ncmVzcyhmaWxlLm5hbWUsIGZpbGUuc2l6ZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIF9zZWxmLmRyb3Bab25lLm9uKCdzdWNjZXNzJywgZmlsZSA9PiB7XG5cbiAgICAgICAgfSk7XG5cbiAgICAgICAgX3NlbGYuZHJvcFpvbmUub24oJ2NvbXBsZXRlJywgZmlsZSA9PiB7XG4gICAgICAgICAgICBfc2VsZi5jaGFuZ2VQcm9ncmVzc1N0YXR1cyhmaWxlKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgX3NlbGYuZHJvcFpvbmUub24oJ3F1ZXVlY29tcGxldGUnLCAoKSA9PiB7XG4gICAgICAgICAgICBIZWxwZXJzLnJlc2V0UGFnaW5hdGlvbigpO1xuICAgICAgICAgICAgX3NlbGYuTWVkaWFTZXJ2aWNlLmdldE1lZGlhKHRydWUpO1xuICAgICAgICAgICAgaWYgKF9zZWxmLnRvdGFsRXJyb3IgPT09IDApIHtcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnLnJ2LXVwbG9hZC1wcm9ncmVzcyAuY2xvc2UtcGFuZScpLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICAgICAgfSwgNTAwMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGhhbmRsZUV2ZW50cygpIHtcbiAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcbiAgICAgICAgLyoqXG4gICAgICAgICAqIENsb3NlIHVwbG9hZCBwcm9ncmVzcyBwYW5lXG4gICAgICAgICAqL1xuICAgICAgICBfc2VsZi4kYm9keS5vbignY2xpY2snLCAnLnJ2LXVwbG9hZC1wcm9ncmVzcyAuY2xvc2UtcGFuZScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICQoJy5ydi11cGxvYWQtcHJvZ3Jlc3MnKS5hZGRDbGFzcygnaGlkZS10aGUtcGFuZScpO1xuICAgICAgICAgICAgX3NlbGYudG90YWxFcnJvciA9IDA7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAkKCcucnYtdXBsb2FkLXByb2dyZXNzIGxpJykucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgX3NlbGYudG90YWxRdWV1ZWQgPSAxO1xuICAgICAgICAgICAgfSwgMzAwKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaW5pdFByb2dyZXNzKCRmaWxlTmFtZSwgJGZpbGVTaXplKSB7XG4gICAgICAgIGxldCB0ZW1wbGF0ZSA9IHRoaXMudXBsb2FkUHJvZ3Jlc3NUZW1wbGF0ZVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9fX2ZpbGVOYW1lX18vZ2ksICRmaWxlTmFtZSlcbiAgICAgICAgICAgICAgICAucmVwbGFjZSgvX19maWxlU2l6ZV9fL2dpLCBVcGxvYWRTZXJ2aWNlLmZvcm1hdEZpbGVTaXplKCRmaWxlU2l6ZSkpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL19fc3RhdHVzX18vZ2ksICd3YXJuaW5nJylcbiAgICAgICAgICAgICAgICAucmVwbGFjZSgvX19tZXNzYWdlX18vZ2ksICdVcGxvYWRpbmcnKVxuICAgICAgICAgICAgO1xuICAgICAgICB0aGlzLnVwbG9hZFByb2dyZXNzQ29udGFpbmVyLmFwcGVuZCh0ZW1wbGF0ZSk7XG4gICAgICAgIHRoaXMudXBsb2FkUHJvZ3Jlc3NCb3gucmVtb3ZlQ2xhc3MoJ2hpZGUtdGhlLXBhbmUnKTtcbiAgICAgICAgdGhpcy51cGxvYWRQcm9ncmVzc0JveC5maW5kKCcucGFuZWwtYm9keScpXG4gICAgICAgICAgICAuYW5pbWF0ZSh7c2Nyb2xsVG9wOiB0aGlzLnVwbG9hZFByb2dyZXNzQ29udGFpbmVyLmhlaWdodCgpfSwgMTUwKTtcbiAgICB9XG5cbiAgICBjaGFuZ2VQcm9ncmVzc1N0YXR1cyhmaWxlKSB7XG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XG4gICAgICAgIGxldCAkcHJvZ3Jlc3NMaW5lID0gX3NlbGYudXBsb2FkUHJvZ3Jlc3NDb250YWluZXIuZmluZCgnbGk6bnRoLWNoaWxkKCcgKyAoZmlsZS5pbmRleCkgKyAnKScpO1xuICAgICAgICBsZXQgJGxhYmVsID0gJHByb2dyZXNzTGluZS5maW5kKCcubGFiZWwnKTtcbiAgICAgICAgJGxhYmVsLnJlbW92ZUNsYXNzKCdsYWJlbC1zdWNjZXNzIGxhYmVsLWRhbmdlciBsYWJlbC13YXJuaW5nJyk7XG5cbiAgICAgICAgbGV0IHJlc3BvbnNlID0gSGVscGVycy5qc29uRGVjb2RlKGZpbGUueGhyLnJlc3BvbnNlVGV4dCB8fCAnJywge30pO1xuXG4gICAgICAgIF9zZWxmLnRvdGFsRXJyb3IgPSBfc2VsZi50b3RhbEVycm9yICsgKHJlc3BvbnNlLmVycm9yID09PSB0cnVlIHx8IGZpbGUuc3RhdHVzID09PSAnZXJyb3InID8gMSA6IDApO1xuXG4gICAgICAgICRsYWJlbC5hZGRDbGFzcyhyZXNwb25zZS5lcnJvciA9PT0gdHJ1ZSB8fCBmaWxlLnN0YXR1cyA9PT0gJ2Vycm9yJyA/ICdsYWJlbC1kYW5nZXInIDogJ2xhYmVsLXN1Y2Nlc3MnKTtcbiAgICAgICAgJGxhYmVsLmh0bWwocmVzcG9uc2UuZXJyb3IgPT09IHRydWUgfHwgZmlsZS5zdGF0dXMgPT09ICdlcnJvcicgPyAnRXJyb3InIDogJ1VwbG9hZGVkJyk7XG4gICAgICAgIGlmIChmaWxlLnN0YXR1cyA9PT0gJ2Vycm9yJykge1xuICAgICAgICAgICAgaWYgKGZpbGUueGhyLnN0YXR1cyA9PT0gNDIyKSB7XG4gICAgICAgICAgICAgICAgbGV0IGVycm9yX2h0bWwgPSAnJztcbiAgICAgICAgICAgICAgICAkLmVhY2gocmVzcG9uc2UsIGZ1bmN0aW9uIChrZXksIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JfaHRtbCArPSAnPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPicgKyBpdGVtICsgJzwvc3Bhbj48YnI+JztcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAkcHJvZ3Jlc3NMaW5lLmZpbmQoJy5maWxlLWVycm9yJykuaHRtbChlcnJvcl9odG1sKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZS54aHIuc3RhdHVzID09PSA1MDApIHtcbiAgICAgICAgICAgICAgICAkcHJvZ3Jlc3NMaW5lLmZpbmQoJy5maWxlLWVycm9yJykuaHRtbCgnPHNwYW4gY2xhc3M9XCJ0ZXh0LWRhbmdlclwiPicgKyBmaWxlLnhoci5zdGF0dXNUZXh0ICsgJzwvc3Bhbj4nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZS5lcnJvcikge1xuICAgICAgICAgICAgJHByb2dyZXNzTGluZS5maW5kKCcuZmlsZS1lcnJvcicpLmh0bWwoJzxzcGFuIGNsYXNzPVwidGV4dC1kYW5nZXJcIj4nICsgcmVzcG9uc2UubWVzc2FnZSArICc8L3NwYW4+Jyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBIZWxwZXJzLmFkZFRvUmVjZW50KHJlc3BvbnNlLmRhdGEuaWQpO1xuICAgICAgICAgICAgSGVscGVycy5zZXRTZWxlY3RlZEZpbGUocmVzcG9uc2UuZGF0YS5pZCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGF0aWMgZm9ybWF0RmlsZVNpemUoYnl0ZXMsIHNpID0gZmFsc2UpIHtcbiAgICAgICAgbGV0IHRocmVzaCA9IHNpID8gMTAwMCA6IDEwMjQ7XG4gICAgICAgIGlmIChNYXRoLmFicyhieXRlcykgPCB0aHJlc2gpIHtcbiAgICAgICAgICAgIHJldHVybiBieXRlcyArICcgQic7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHVuaXRzID0gWydLQicsICdNQicsICdHQicsICdUQicsICdQQicsICdFQicsICdaQicsICdZQiddO1xuICAgICAgICBsZXQgdSA9IC0xO1xuICAgICAgICBkbyB7XG4gICAgICAgICAgICBieXRlcyAvPSB0aHJlc2g7XG4gICAgICAgICAgICArK3U7XG4gICAgICAgIH0gd2hpbGUgKE1hdGguYWJzKGJ5dGVzKSA+PSB0aHJlc2ggJiYgdSA8IHVuaXRzLmxlbmd0aCAtIDEpO1xuICAgICAgICByZXR1cm4gYnl0ZXMudG9GaXhlZCgxKSArICcgJyArIHVuaXRzW3VdO1xuICAgIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvQXBwL1NlcnZpY2VzL1VwbG9hZFNlcnZpY2UuanMiLCJpbXBvcnQge1lvdXR1YmV9IGZyb20gJy4vWW91dHViZSc7XG5cbmV4cG9ydCBjbGFzcyBFeHRlcm5hbFNlcnZpY2VzIHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgbmV3IFlvdXR1YmUoKTtcbiAgICB9XG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvRXh0ZXJuYWxzL0V4dGVybmFsU2VydmljZXMuanMiLCJpbXBvcnQge0hlbHBlcnN9IGZyb20gJy4uL0hlbHBlcnMvSGVscGVycyc7XG5pbXBvcnQge0V4dGVybmFsU2VydmljZUNvbmZpZ30gZnJvbSAnLi4vQ29uZmlnL0V4dGVybmFsU2VydmljZUNvbmZpZyc7XG5pbXBvcnQge01lZGlhU2VydmljZX0gZnJvbSAnLi4vU2VydmljZXMvTWVkaWFTZXJ2aWNlJztcbmltcG9ydCB7TWVzc2FnZVNlcnZpY2V9IGZyb20gJy4uL1NlcnZpY2VzL01lc3NhZ2VTZXJ2aWNlJztcblxuZXhwb3J0IGNsYXNzIFlvdXR1YmUge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLk1lZGlhU2VydmljZSA9IG5ldyBNZWRpYVNlcnZpY2UoKTtcblxuICAgICAgICB0aGlzLiRib2R5ID0gJCgnYm9keScpO1xuXG4gICAgICAgIHRoaXMuJG1vZGFsID0gJCgnI21vZGFsX2FkZF9mcm9tX3lvdXR1YmUnKTtcblxuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLnNldE1lc3NhZ2UoUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5hZGRfZnJvbS55b3V0dWJlLm9yaWdpbmFsX21zZyk7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2goZXJyKSB7XG5cbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuJG1vZGFsLm9uKCdoaWRkZW4uYnMubW9kYWwnLCAoKSA9PiB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgX3NlbGYuc2V0TWVzc2FnZShSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLmFkZF9mcm9tLnlvdXR1YmUub3JpZ2luYWxfbXNnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhdGNoKGVycikge1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLiRib2R5Lm9uKCdjbGljaycsICcjbW9kYWxfYWRkX2Zyb21feW91dHViZSAucnYtYnRuLWFkZC15b3V0dWJlLXVybCcsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgX3NlbGYuY2hlY2tZb3VUdWJlVmlkZW8oJCh0aGlzKS5jbG9zZXN0KCcjbW9kYWxfYWRkX2Zyb21feW91dHViZScpLmZpbmQoJy5ydi15b3V0dWJlLXVybCcpKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIHZhbGlkYXRlWW91VHViZUxpbmsodXJsKSB7XG4gICAgICAgIGxldCBwID0gL14oPzpodHRwcz86XFwvXFwvKT8oPzp3d3dcXC4pP3lvdXR1YmVcXC5jb21cXC93YXRjaFxcPyg/PS4qdj0oKFxcd3wtKXsxMX0pKSg/OlxcUyspPyQvO1xuICAgICAgICByZXR1cm4gKHVybC5tYXRjaChwKSkgPyBSZWdFeHAuJDEgOiBmYWxzZTtcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0WW91VHViZUlkKHVybCkge1xuICAgICAgICBsZXQgcmVnRXhwID0gL14uKih5b3V0dS5iZVxcL3x2XFwvfHVcXC9cXHdcXC98ZW1iZWRcXC98d2F0Y2hcXD92PXxcXCZ2PSkoW14jXFwmXFw/XSopLiovO1xuICAgICAgICBsZXQgbWF0Y2ggPSB1cmwubWF0Y2gocmVnRXhwKTtcbiAgICAgICAgaWYgKG1hdGNoICYmIG1hdGNoWzJdLmxlbmd0aCA9PT0gMTEpIHtcbiAgICAgICAgICAgIHJldHVybiBtYXRjaFsyXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0WW91dHViZVBsYXlsaXN0SWQodXJsKSB7XG4gICAgICAgIGxldCByZWdFeHAgPSAvXi4qKHlvdXR1LmJlXFwvfHZcXC98dVxcL1xcd1xcL3xlbWJlZFxcL3x3YXRjaFxcP2xpc3Q9fFxcJmxpc3Q9KShbXiNcXCZcXD9dKikuKi87XG4gICAgICAgIGxldCBtYXRjaCA9IHVybC5tYXRjaChyZWdFeHApO1xuICAgICAgICBpZiAobWF0Y2gpIHtcbiAgICAgICAgICAgIHJldHVybiBtYXRjaFsyXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBzZXRNZXNzYWdlKG1zZykge1xuICAgICAgICB0aGlzLiRtb2RhbC5maW5kKCcubW9kYWwtbm90aWNlJykuaHRtbChtc2cpO1xuICAgIH1cblxuICAgIGNoZWNrWW91VHViZVZpZGVvKCRpbnB1dCkge1xuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xuICAgICAgICBpZiAoIVlvdXR1YmUudmFsaWRhdGVZb3VUdWJlTGluaygkaW5wdXQudmFsKCkpIHx8ICFFeHRlcm5hbFNlcnZpY2VDb25maWcueW91dHViZS5hcGlfa2V5KSB7XG4gICAgICAgICAgICBpZiAoRXh0ZXJuYWxTZXJ2aWNlQ29uZmlnLnlvdXR1YmUuYXBpX2tleSkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnNldE1lc3NhZ2UoUlZfTUVESUFfQ09ORklHLnRyYW5zbGF0aW9ucy5hZGRfZnJvbS55b3V0dWJlLmludmFsaWRfdXJsX21zZyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhdGNoKGVycikge1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuc2V0TWVzc2FnZShSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLmFkZF9mcm9tLnlvdXR1YmUubm9fYXBpX2tleV9tc2cpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjYXRjaChlcnIpIHtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsZXQgeW91dHViZUlkID0gWW91dHViZS5nZXRZb3VUdWJlSWQoJGlucHV0LnZhbCgpKTtcbiAgICAgICAgICAgIGxldCByZXF1ZXN0VXJsID0gJ2h0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3lvdXR1YmUvdjMvdmlkZW9zP2lkPScgKyB5b3V0dWJlSWQ7XG4gICAgICAgICAgICBsZXQgaXNQbGF5bGlzdCA9IF9zZWxmLiRtb2RhbC5maW5kKCcuY3VzdG9tLWNoZWNrYm94IGlucHV0W3R5cGU9XCJjaGVja2JveFwiXScpLmlzKCc6Y2hlY2tlZCcpO1xuXG4gICAgICAgICAgICBpZiAoaXNQbGF5bGlzdCkge1xuICAgICAgICAgICAgICAgIHlvdXR1YmVJZCA9IFlvdXR1YmUuZ2V0WW91dHViZVBsYXlsaXN0SWQoJGlucHV0LnZhbCgpKTtcbiAgICAgICAgICAgICAgICByZXF1ZXN0VXJsID0gJ2h0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3lvdXR1YmUvdjMvcGxheWxpc3RJdGVtcz9wbGF5bGlzdElkPScgKyB5b3V0dWJlSWQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgdXJsOiByZXF1ZXN0VXJsICsgJyZrZXk9JyArIEV4dGVybmFsU2VydmljZUNvbmZpZy55b3V0dWJlLmFwaV9rZXkgKyAnJnBhcnQ9c25pcHBldCcsXG4gICAgICAgICAgICAgICAgdHlwZTogXCJHRVRcIixcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaXNQbGF5bGlzdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGxheWxpc3RWaWRlb0NhbGxiYWNrKGRhdGEsICRpbnB1dC52YWwoKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaW5nbGVWaWRlb0NhbGxiYWNrKGRhdGEsICRpbnB1dC52YWwoKSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgX3NlbGYuc2V0TWVzc2FnZShSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLmFkZF9mcm9tLnlvdXR1YmUuZXJyb3JfbXNnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYXRjaChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gc2luZ2xlVmlkZW9DYWxsYmFjayhkYXRhLCB1cmwpIHtcbiAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgdXJsOiBSVl9NRURJQV9VUkwuYWRkX2V4dGVybmFsX3NlcnZpY2UsXG4gICAgICAgICAgICAgICAgdHlwZTogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICd5b3V0dWJlJyxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogZGF0YS5pdGVtc1swXS5zbmlwcGV0LnRpdGxlLFxuICAgICAgICAgICAgICAgICAgICBmb2xkZXJfaWQ6IEhlbHBlcnMuZ2V0UmVxdWVzdFBhcmFtcygpLmZvbGRlcl9pZCxcbiAgICAgICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRodW1iOiAnaHR0cHM6Ly9pbWcueW91dHViZS5jb20vdmkvJyArIGRhdGEuaXRlbXNbMF0uaWQgKyAnL21heHJlc2RlZmF1bHQuanBnJ1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMuZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdlcnJvcicsIHJlcy5tZXNzYWdlLCBSVl9NRURJQV9DT05GSUcudHJhbnNsYXRpb25zLm1lc3NhZ2UuZXJyb3JfaGVhZGVyKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIE1lc3NhZ2VTZXJ2aWNlLnNob3dNZXNzYWdlKCdzdWNjZXNzJywgcmVzLm1lc3NhZ2UsIFJWX01FRElBX0NPTkZJRy50cmFuc2xhdGlvbnMubWVzc2FnZS5zdWNjZXNzX2hlYWRlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICBfc2VsZi5NZWRpYVNlcnZpY2UuZ2V0TWVkaWEodHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBNZXNzYWdlU2VydmljZS5oYW5kbGVFcnJvcihkYXRhKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIF9zZWxmLiRtb2RhbC5tb2RhbCgnaGlkZScpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gcGxheWxpc3RWaWRlb0NhbGxiYWNrKGRhdGEsIHVybCkge1xuICAgICAgICAgICAgX3NlbGYuJG1vZGFsLm1vZGFsKCdoaWRlJyk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL0FwcC9FeHRlcm5hbHMvWW91dHViZS5qcyIsImxldCBFeHRlcm5hbFNlcnZpY2VDb25maWcgPSB7XG4gICAgeW91dHViZToge1xuICAgICAgICBhcGlfa2V5OiBcIkFJemFTeUNWNGZtZmRnc1ZhbEdOUjNzYy0wVzNjYnBFWjh1T2Q2MFwiXG4gICAgfVxufTtcblxuZXhwb3J0IHtFeHRlcm5hbFNlcnZpY2VDb25maWd9O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9BcHAvQ29uZmlnL0V4dGVybmFsU2VydmljZUNvbmZpZy5qcyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL3Nhc3MvbWVkaWEuc2Nzc1xuLy8gbW9kdWxlIGlkID0gMTZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sInNvdXJjZVJvb3QiOiIifQ==
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 20);
/******/ })
/************************************************************************/
/******/ ({

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(21);


/***/ }),

/***/ 21:
/***/ (function(module, exports) {

// Gets focus point coordinates from an image - adapt to suit your needs.

(function ($) {
    $(document).ready(function () {

        var defaultImage;
        var $dataAttrInput;
        var $cssAttrInput;
        var $focusPointContainers;
        var $focusPointImages;
        var $helperToolImage;

        //This stores focusPoint's data-attribute values
        var focusPointAttr = {
            x: 0,
            y: 0,
            w: 0,
            h: 0
        };

        //Initialize Helper Tool
        (function () {

            //Initialize Variables
            defaultImage = $('#modal_set_focus_point').find('form.rv-form').data('image');
            $dataAttrInput = $('.helper-tool-data-attr');
            $cssAttrInput = $('.helper-tool-css3-val');
            $helperToolImage = $('img.helper-tool-img, img.target-overlay');

            //Create Grid Elements
            for (var i = 1; i < 10; i++) {
                $('.focuspoint-frames').append('<div class="focuspoint focuspoint-frame' + i + '"><img/></div>');
            }
            //Store focus point containers
            $focusPointContainers = $('.focuspoint');
            $focusPointImages = $('.focuspoint img');

            //Set the default source image
            setImage(defaultImage);
        })();

        /*-----------------------------------------*/

        // function setImage(<URL>)
        // Set a new image to use in the demo, requires URI to an image

        /*-----------------------------------------*/

        function setImage(imgURL) {
            //Get the dimensions of the image by referencing an image stored in memory
            $("<img/>").attr("src", imgURL).load(function () {
                focusPointAttr.w = this.width;
                focusPointAttr.h = this.height;

                //Set src on the thumbnail used in the GUI
                $helperToolImage.attr('src', imgURL);

                //Set src on all .focuspoint images
                $focusPointImages.attr('src', imgURL);

                //Set up initial properties of .focuspoint containers

                /*-----------------------------------------*/
                // Note ---
                // Setting these up with attr doesn't really make a difference
                // added to demo only so changes are made visually in the dom
                // for users inspecting it. Because of how FocusPoint uses .data()
                // only the .data() assignments that follow are necessary.
                /*-----------------------------------------*/
                $focusPointContainers.attr({
                    'data-focus-x': focusPointAttr.x,
                    'data-focus-y': focusPointAttr.y,
                    'data-image-w': focusPointAttr.w,
                    'data-image-h': focusPointAttr.h
                });

                /*-----------------------------------------*/
                // These assignments using .data() are what counts.
                /*-----------------------------------------*/
                $focusPointContainers.data('focusX', focusPointAttr.x);
                $focusPointContainers.data('focusY', focusPointAttr.y);
                $focusPointContainers.data('imageW', focusPointAttr.w);
                $focusPointContainers.data('imageH', focusPointAttr.h);

                //Run FocusPoint for the first time.
                $('.focuspoint').focusPoint();
            });
        }

        /*-----------------------------------------*/

        // Update the data attributes shown to the user

        /*-----------------------------------------*/

        function printDataAttr() {
            $dataAttrInput.val('data-focus-x="' + focusPointAttr.x.toFixed(2) + '" data-focus-y="' + focusPointAttr.y.toFixed(2) + '" data-image-w="' + focusPointAttr.w + '" data-image-h="' + focusPointAttr.h + '"');
        }

        /*-----------------------------------------*/

        // Bind to helper image click event
        // Adjust focus on Click / provides focuspoint and CSS3 properties

        /*-----------------------------------------*/

        $helperToolImage.click(function (e) {

            var imageW = $(this).width();
            var imageH = $(this).height();

            //Calculate FocusPoint coordinates
            var offsetX = e.pageX - $(this).offset().left;
            var offsetY = e.pageY - $(this).offset().top;
            var focusX = (offsetX / imageW - .5) * 2;
            var focusY = (offsetY / imageH - .5) * -2;
            focusPointAttr.x = focusX;
            focusPointAttr.y = focusY;

            //Write values to input
            printDataAttr();

            //Update focus point
            updateFocusPoint();

            //Calculate CSS Percentages
            var percentageX = offsetX / imageW * 100;
            var percentageY = offsetY / imageH * 100;
            var backgroundPosition = percentageX.toFixed(0) + '% ' + percentageY.toFixed(0) + '%';
            var backgroundPositionCSS = 'background-position: ' + backgroundPosition + ';';
            $cssAttrInput.val(backgroundPositionCSS);

            //Leave a sweet target reticle at the focus point.
            $('.reticle').css({
                'top': percentageY + '%',
                'left': percentageX + '%'
            });

            $('.helper-tool-reticle-css').val('top: ' + percentageY + '%; left: ' + percentageX + '%');
        });

        /*-----------------------------------------*/

        // Change image on paste/blur
        // When you paste an image into the specified input, it will be used for the demo

        /*-----------------------------------------*/

        $('#modal_set_focus_point').on('shown.bs.modal', function (e) {
            setImage($('#modal_set_focus_point').find('form.rv-form').data('image'));
        });

        /*-----------------------------------------*/

        /* Update Helper */
        // This function is used to update the focuspoint

        /*-----------------------------------------*/

        function updateFocusPoint() {
            /*-----------------------------------------*/
            // See note in setImage() function regarding these attribute assignments.
            //TLDR - You don't need them for this to work.
            /*-----------------------------------------*/
            $focusPointContainers.attr({
                'data-focus-x': focusPointAttr.x,
                'data-focus-y': focusPointAttr.y
            });
            /*-----------------------------------------*/
            // These you DO need :)
            /*-----------------------------------------*/
            $focusPointContainers.data('focusX', focusPointAttr.x);
            $focusPointContainers.data('focusY', focusPointAttr.y);
            $focusPointContainers.adjustFocus();
        }
    });
})(jQuery);

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMGQxMzcyYmJiMDY0NzlkYmVjN2UiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9mb2N1cy5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImRlZmF1bHRJbWFnZSIsIiRkYXRhQXR0cklucHV0IiwiJGNzc0F0dHJJbnB1dCIsIiRmb2N1c1BvaW50Q29udGFpbmVycyIsIiRmb2N1c1BvaW50SW1hZ2VzIiwiJGhlbHBlclRvb2xJbWFnZSIsImZvY3VzUG9pbnRBdHRyIiwieCIsInkiLCJ3IiwiaCIsImZpbmQiLCJkYXRhIiwiaSIsImFwcGVuZCIsInNldEltYWdlIiwiaW1nVVJMIiwiYXR0ciIsImxvYWQiLCJ3aWR0aCIsImhlaWdodCIsImZvY3VzUG9pbnQiLCJwcmludERhdGFBdHRyIiwidmFsIiwidG9GaXhlZCIsImNsaWNrIiwiZSIsImltYWdlVyIsImltYWdlSCIsIm9mZnNldFgiLCJwYWdlWCIsIm9mZnNldCIsImxlZnQiLCJvZmZzZXRZIiwicGFnZVkiLCJ0b3AiLCJmb2N1c1giLCJmb2N1c1kiLCJ1cGRhdGVGb2N1c1BvaW50IiwicGVyY2VudGFnZVgiLCJwZXJjZW50YWdlWSIsImJhY2tncm91bmRQb3NpdGlvbiIsImJhY2tncm91bmRQb3NpdGlvbkNTUyIsImNzcyIsIm9uIiwiYWRqdXN0Rm9jdXMiLCJqUXVlcnkiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQzdEQTs7QUFFQyxXQUFVQSxDQUFWLEVBQWE7QUFDVkEsTUFBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVk7O0FBRTFCLFlBQUlDLFlBQUo7QUFDQSxZQUFJQyxjQUFKO0FBQ0EsWUFBSUMsYUFBSjtBQUNBLFlBQUlDLHFCQUFKO0FBQ0EsWUFBSUMsaUJBQUo7QUFDQSxZQUFJQyxnQkFBSjs7QUFFQTtBQUNBLFlBQUlDLGlCQUFpQjtBQUNqQkMsZUFBRyxDQURjO0FBRWpCQyxlQUFHLENBRmM7QUFHakJDLGVBQUcsQ0FIYztBQUlqQkMsZUFBRztBQUpjLFNBQXJCOztBQU9BO0FBQ0EsU0FBQyxZQUFZOztBQUVUO0FBQ0FWLDJCQUFlSCxFQUFFLHdCQUFGLEVBQTRCYyxJQUE1QixDQUFpQyxjQUFqQyxFQUFpREMsSUFBakQsQ0FBc0QsT0FBdEQsQ0FBZjtBQUNBWCw2QkFBaUJKLEVBQUUsd0JBQUYsQ0FBakI7QUFDQUssNEJBQWdCTCxFQUFFLHVCQUFGLENBQWhCO0FBQ0FRLCtCQUFtQlIsRUFBRSx5Q0FBRixDQUFuQjs7QUFFQTtBQUNBLGlCQUFLLElBQUlnQixJQUFJLENBQWIsRUFBZ0JBLElBQUksRUFBcEIsRUFBd0JBLEdBQXhCLEVBQTZCO0FBQ3pCaEIsa0JBQUUsb0JBQUYsRUFBd0JpQixNQUF4QixDQUErQiw0Q0FBNENELENBQTVDLEdBQWdELGdCQUEvRTtBQUNIO0FBQ0Q7QUFDQVYsb0NBQXdCTixFQUFFLGFBQUYsQ0FBeEI7QUFDQU8sZ0NBQW9CUCxFQUFFLGlCQUFGLENBQXBCOztBQUVBO0FBQ0FrQixxQkFBU2YsWUFBVDtBQUVILFNBbkJEOztBQXFCQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLGlCQUFTZSxRQUFULENBQWtCQyxNQUFsQixFQUEwQjtBQUN0QjtBQUNBbkIsY0FBRSxRQUFGLEVBQ0tvQixJQURMLENBQ1UsS0FEVixFQUNpQkQsTUFEakIsRUFFS0UsSUFGTCxDQUVVLFlBQVk7QUFDZFosK0JBQWVHLENBQWYsR0FBbUIsS0FBS1UsS0FBeEI7QUFDQWIsK0JBQWVJLENBQWYsR0FBbUIsS0FBS1UsTUFBeEI7O0FBRUE7QUFDQWYsaUNBQWlCWSxJQUFqQixDQUFzQixLQUF0QixFQUE2QkQsTUFBN0I7O0FBRUE7QUFDQVosa0NBQWtCYSxJQUFsQixDQUF1QixLQUF2QixFQUE4QkQsTUFBOUI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQWIsc0NBQXNCYyxJQUF0QixDQUEyQjtBQUN2QixvQ0FBZ0JYLGVBQWVDLENBRFI7QUFFdkIsb0NBQWdCRCxlQUFlRSxDQUZSO0FBR3ZCLG9DQUFnQkYsZUFBZUcsQ0FIUjtBQUl2QixvQ0FBZ0JILGVBQWVJO0FBSlIsaUJBQTNCOztBQU9BO0FBQ0E7QUFDQTtBQUNBUCxzQ0FBc0JTLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDTixlQUFlQyxDQUFwRDtBQUNBSixzQ0FBc0JTLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDTixlQUFlRSxDQUFwRDtBQUNBTCxzQ0FBc0JTLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDTixlQUFlRyxDQUFwRDtBQUNBTixzQ0FBc0JTLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDTixlQUFlSSxDQUFwRDs7QUFFQTtBQUNBYixrQkFBRSxhQUFGLEVBQWlCd0IsVUFBakI7QUFFSCxhQXZDTDtBQXdDSDs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQSxpQkFBU0MsYUFBVCxHQUF5QjtBQUNyQnJCLDJCQUFlc0IsR0FBZixDQUFtQixtQkFBbUJqQixlQUFlQyxDQUFmLENBQWlCaUIsT0FBakIsQ0FBeUIsQ0FBekIsQ0FBbkIsR0FBaUQsa0JBQWpELEdBQXNFbEIsZUFBZUUsQ0FBZixDQUFpQmdCLE9BQWpCLENBQXlCLENBQXpCLENBQXRFLEdBQW9HLGtCQUFwRyxHQUF5SGxCLGVBQWVHLENBQXhJLEdBQTRJLGtCQUE1SSxHQUFpS0gsZUFBZUksQ0FBaEwsR0FBb0wsR0FBdk07QUFDSDs7QUFFRDs7QUFFQTtBQUNBOztBQUVBOztBQUVBTCx5QkFBaUJvQixLQUFqQixDQUF1QixVQUFVQyxDQUFWLEVBQWE7O0FBRWhDLGdCQUFJQyxTQUFTOUIsRUFBRSxJQUFGLEVBQVFzQixLQUFSLEVBQWI7QUFDQSxnQkFBSVMsU0FBUy9CLEVBQUUsSUFBRixFQUFRdUIsTUFBUixFQUFiOztBQUVBO0FBQ0EsZ0JBQUlTLFVBQVVILEVBQUVJLEtBQUYsR0FBVWpDLEVBQUUsSUFBRixFQUFRa0MsTUFBUixHQUFpQkMsSUFBekM7QUFDQSxnQkFBSUMsVUFBVVAsRUFBRVEsS0FBRixHQUFVckMsRUFBRSxJQUFGLEVBQVFrQyxNQUFSLEdBQWlCSSxHQUF6QztBQUNBLGdCQUFJQyxTQUFTLENBQUNQLFVBQVVGLE1BQVYsR0FBbUIsRUFBcEIsSUFBMEIsQ0FBdkM7QUFDQSxnQkFBSVUsU0FBUyxDQUFDSixVQUFVTCxNQUFWLEdBQW1CLEVBQXBCLElBQTBCLENBQUMsQ0FBeEM7QUFDQXRCLDJCQUFlQyxDQUFmLEdBQW1CNkIsTUFBbkI7QUFDQTlCLDJCQUFlRSxDQUFmLEdBQW1CNkIsTUFBbkI7O0FBRUE7QUFDQWY7O0FBRUE7QUFDQWdCOztBQUVBO0FBQ0EsZ0JBQUlDLGNBQWVWLFVBQVVGLE1BQVgsR0FBcUIsR0FBdkM7QUFDQSxnQkFBSWEsY0FBZVAsVUFBVUwsTUFBWCxHQUFxQixHQUF2QztBQUNBLGdCQUFJYSxxQkFBcUJGLFlBQVlmLE9BQVosQ0FBb0IsQ0FBcEIsSUFBeUIsSUFBekIsR0FBZ0NnQixZQUFZaEIsT0FBWixDQUFvQixDQUFwQixDQUFoQyxHQUF5RCxHQUFsRjtBQUNBLGdCQUFJa0Isd0JBQXdCLDBCQUEwQkQsa0JBQTFCLEdBQStDLEdBQTNFO0FBQ0F2QywwQkFBY3FCLEdBQWQsQ0FBa0JtQixxQkFBbEI7O0FBRUE7QUFDQTdDLGNBQUUsVUFBRixFQUFjOEMsR0FBZCxDQUFrQjtBQUNkLHVCQUFPSCxjQUFjLEdBRFA7QUFFZCx3QkFBUUQsY0FBYztBQUZSLGFBQWxCOztBQUtBMUMsY0FBRSwwQkFBRixFQUE4QjBCLEdBQTlCLENBQWtDLFVBQVVpQixXQUFWLEdBQXdCLFdBQXhCLEdBQXNDRCxXQUF0QyxHQUFvRCxHQUF0RjtBQUNILFNBakNEOztBQW1DQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBMUMsVUFBRSx3QkFBRixFQUE0QitDLEVBQTVCLENBQStCLGdCQUEvQixFQUFpRCxVQUFVbEIsQ0FBVixFQUFhO0FBQzFEWCxxQkFBU2xCLEVBQUUsd0JBQUYsRUFBNEJjLElBQTVCLENBQWlDLGNBQWpDLEVBQWlEQyxJQUFqRCxDQUFzRCxPQUF0RCxDQUFUO0FBQ0gsU0FGRDs7QUFJQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLGlCQUFTMEIsZ0JBQVQsR0FBNEI7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQW5DLGtDQUFzQmMsSUFBdEIsQ0FBMkI7QUFDdkIsZ0NBQWdCWCxlQUFlQyxDQURSO0FBRXZCLGdDQUFnQkQsZUFBZUU7QUFGUixhQUEzQjtBQUlBO0FBQ0E7QUFDQTtBQUNBTCxrQ0FBc0JTLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDTixlQUFlQyxDQUFwRDtBQUNBSixrQ0FBc0JTLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDTixlQUFlRSxDQUFwRDtBQUNBTCxrQ0FBc0IwQyxXQUF0QjtBQUNIO0FBQ0osS0FoTEQ7QUFpTEgsQ0FsTEEsRUFrTENDLE1BbExELENBQUQsQyIsImZpbGUiOiIvanMvZm9jdXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAyMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgMGQxMzcyYmJiMDY0NzlkYmVjN2UiLCIvLyBHZXRzIGZvY3VzIHBvaW50IGNvb3JkaW5hdGVzIGZyb20gYW4gaW1hZ2UgLSBhZGFwdCB0byBzdWl0IHlvdXIgbmVlZHMuXG5cbihmdW5jdGlvbiAoJCkge1xuICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgICAgICB2YXIgZGVmYXVsdEltYWdlO1xuICAgICAgICB2YXIgJGRhdGFBdHRySW5wdXQ7XG4gICAgICAgIHZhciAkY3NzQXR0cklucHV0O1xuICAgICAgICB2YXIgJGZvY3VzUG9pbnRDb250YWluZXJzO1xuICAgICAgICB2YXIgJGZvY3VzUG9pbnRJbWFnZXM7XG4gICAgICAgIHZhciAkaGVscGVyVG9vbEltYWdlO1xuXG4gICAgICAgIC8vVGhpcyBzdG9yZXMgZm9jdXNQb2ludCdzIGRhdGEtYXR0cmlidXRlIHZhbHVlc1xuICAgICAgICB2YXIgZm9jdXNQb2ludEF0dHIgPSB7XG4gICAgICAgICAgICB4OiAwLFxuICAgICAgICAgICAgeTogMCxcbiAgICAgICAgICAgIHc6IDAsXG4gICAgICAgICAgICBoOiAwXG4gICAgICAgIH07XG5cbiAgICAgICAgLy9Jbml0aWFsaXplIEhlbHBlciBUb29sXG4gICAgICAgIChmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgIC8vSW5pdGlhbGl6ZSBWYXJpYWJsZXNcbiAgICAgICAgICAgIGRlZmF1bHRJbWFnZSA9ICQoJyNtb2RhbF9zZXRfZm9jdXNfcG9pbnQnKS5maW5kKCdmb3JtLnJ2LWZvcm0nKS5kYXRhKCdpbWFnZScpO1xuICAgICAgICAgICAgJGRhdGFBdHRySW5wdXQgPSAkKCcuaGVscGVyLXRvb2wtZGF0YS1hdHRyJyk7XG4gICAgICAgICAgICAkY3NzQXR0cklucHV0ID0gJCgnLmhlbHBlci10b29sLWNzczMtdmFsJyk7XG4gICAgICAgICAgICAkaGVscGVyVG9vbEltYWdlID0gJCgnaW1nLmhlbHBlci10b29sLWltZywgaW1nLnRhcmdldC1vdmVybGF5Jyk7XG5cbiAgICAgICAgICAgIC8vQ3JlYXRlIEdyaWQgRWxlbWVudHNcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgMTA7IGkrKykge1xuICAgICAgICAgICAgICAgICQoJy5mb2N1c3BvaW50LWZyYW1lcycpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImZvY3VzcG9pbnQgZm9jdXNwb2ludC1mcmFtZScgKyBpICsgJ1wiPjxpbWcvPjwvZGl2PicpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy9TdG9yZSBmb2N1cyBwb2ludCBjb250YWluZXJzXG4gICAgICAgICAgICAkZm9jdXNQb2ludENvbnRhaW5lcnMgPSAkKCcuZm9jdXNwb2ludCcpO1xuICAgICAgICAgICAgJGZvY3VzUG9pbnRJbWFnZXMgPSAkKCcuZm9jdXNwb2ludCBpbWcnKTtcblxuICAgICAgICAgICAgLy9TZXQgdGhlIGRlZmF1bHQgc291cmNlIGltYWdlXG4gICAgICAgICAgICBzZXRJbWFnZShkZWZhdWx0SW1hZ2UpO1xuXG4gICAgICAgIH0pKCk7XG5cbiAgICAgICAgLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG5cbiAgICAgICAgLy8gZnVuY3Rpb24gc2V0SW1hZ2UoPFVSTD4pXG4gICAgICAgIC8vIFNldCBhIG5ldyBpbWFnZSB0byB1c2UgaW4gdGhlIGRlbW8sIHJlcXVpcmVzIFVSSSB0byBhbiBpbWFnZVxuXG4gICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXG4gICAgICAgIGZ1bmN0aW9uIHNldEltYWdlKGltZ1VSTCkge1xuICAgICAgICAgICAgLy9HZXQgdGhlIGRpbWVuc2lvbnMgb2YgdGhlIGltYWdlIGJ5IHJlZmVyZW5jaW5nIGFuIGltYWdlIHN0b3JlZCBpbiBtZW1vcnlcbiAgICAgICAgICAgICQoXCI8aW1nLz5cIilcbiAgICAgICAgICAgICAgICAuYXR0cihcInNyY1wiLCBpbWdVUkwpXG4gICAgICAgICAgICAgICAgLmxvYWQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBmb2N1c1BvaW50QXR0ci53ID0gdGhpcy53aWR0aDtcbiAgICAgICAgICAgICAgICAgICAgZm9jdXNQb2ludEF0dHIuaCA9IHRoaXMuaGVpZ2h0O1xuXG4gICAgICAgICAgICAgICAgICAgIC8vU2V0IHNyYyBvbiB0aGUgdGh1bWJuYWlsIHVzZWQgaW4gdGhlIEdVSVxuICAgICAgICAgICAgICAgICAgICAkaGVscGVyVG9vbEltYWdlLmF0dHIoJ3NyYycsIGltZ1VSTCk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy9TZXQgc3JjIG9uIGFsbCAuZm9jdXNwb2ludCBpbWFnZXNcbiAgICAgICAgICAgICAgICAgICAgJGZvY3VzUG9pbnRJbWFnZXMuYXR0cignc3JjJywgaW1nVVJMKTtcblxuICAgICAgICAgICAgICAgICAgICAvL1NldCB1cCBpbml0aWFsIHByb3BlcnRpZXMgb2YgLmZvY3VzcG9pbnQgY29udGFpbmVyc1xuXG4gICAgICAgICAgICAgICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuICAgICAgICAgICAgICAgICAgICAvLyBOb3RlIC0tLVxuICAgICAgICAgICAgICAgICAgICAvLyBTZXR0aW5nIHRoZXNlIHVwIHdpdGggYXR0ciBkb2Vzbid0IHJlYWxseSBtYWtlIGEgZGlmZmVyZW5jZVxuICAgICAgICAgICAgICAgICAgICAvLyBhZGRlZCB0byBkZW1vIG9ubHkgc28gY2hhbmdlcyBhcmUgbWFkZSB2aXN1YWxseSBpbiB0aGUgZG9tXG4gICAgICAgICAgICAgICAgICAgIC8vIGZvciB1c2VycyBpbnNwZWN0aW5nIGl0LiBCZWNhdXNlIG9mIGhvdyBGb2N1c1BvaW50IHVzZXMgLmRhdGEoKVxuICAgICAgICAgICAgICAgICAgICAvLyBvbmx5IHRoZSAuZGF0YSgpIGFzc2lnbm1lbnRzIHRoYXQgZm9sbG93IGFyZSBuZWNlc3NhcnkuXG4gICAgICAgICAgICAgICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuICAgICAgICAgICAgICAgICAgICAkZm9jdXNQb2ludENvbnRhaW5lcnMuYXR0cih7XG4gICAgICAgICAgICAgICAgICAgICAgICAnZGF0YS1mb2N1cy14JzogZm9jdXNQb2ludEF0dHIueCxcbiAgICAgICAgICAgICAgICAgICAgICAgICdkYXRhLWZvY3VzLXknOiBmb2N1c1BvaW50QXR0ci55LFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2RhdGEtaW1hZ2Utdyc6IGZvY3VzUG9pbnRBdHRyLncsXG4gICAgICAgICAgICAgICAgICAgICAgICAnZGF0YS1pbWFnZS1oJzogZm9jdXNQb2ludEF0dHIuaFxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cbiAgICAgICAgICAgICAgICAgICAgLy8gVGhlc2UgYXNzaWdubWVudHMgdXNpbmcgLmRhdGEoKSBhcmUgd2hhdCBjb3VudHMuXG4gICAgICAgICAgICAgICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuICAgICAgICAgICAgICAgICAgICAkZm9jdXNQb2ludENvbnRhaW5lcnMuZGF0YSgnZm9jdXNYJywgZm9jdXNQb2ludEF0dHIueCk7XG4gICAgICAgICAgICAgICAgICAgICRmb2N1c1BvaW50Q29udGFpbmVycy5kYXRhKCdmb2N1c1knLCBmb2N1c1BvaW50QXR0ci55KTtcbiAgICAgICAgICAgICAgICAgICAgJGZvY3VzUG9pbnRDb250YWluZXJzLmRhdGEoJ2ltYWdlVycsIGZvY3VzUG9pbnRBdHRyLncpO1xuICAgICAgICAgICAgICAgICAgICAkZm9jdXNQb2ludENvbnRhaW5lcnMuZGF0YSgnaW1hZ2VIJywgZm9jdXNQb2ludEF0dHIuaCk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy9SdW4gRm9jdXNQb2ludCBmb3IgdGhlIGZpcnN0IHRpbWUuXG4gICAgICAgICAgICAgICAgICAgICQoJy5mb2N1c3BvaW50JykuZm9jdXNQb2ludCgpO1xuXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuICAgICAgICAvLyBVcGRhdGUgdGhlIGRhdGEgYXR0cmlidXRlcyBzaG93biB0byB0aGUgdXNlclxuXG4gICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXG4gICAgICAgIGZ1bmN0aW9uIHByaW50RGF0YUF0dHIoKSB7XG4gICAgICAgICAgICAkZGF0YUF0dHJJbnB1dC52YWwoJ2RhdGEtZm9jdXMteD1cIicgKyBmb2N1c1BvaW50QXR0ci54LnRvRml4ZWQoMikgKyAnXCIgZGF0YS1mb2N1cy15PVwiJyArIGZvY3VzUG9pbnRBdHRyLnkudG9GaXhlZCgyKSArICdcIiBkYXRhLWltYWdlLXc9XCInICsgZm9jdXNQb2ludEF0dHIudyArICdcIiBkYXRhLWltYWdlLWg9XCInICsgZm9jdXNQb2ludEF0dHIuaCArICdcIicpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG5cbiAgICAgICAgLy8gQmluZCB0byBoZWxwZXIgaW1hZ2UgY2xpY2sgZXZlbnRcbiAgICAgICAgLy8gQWRqdXN0IGZvY3VzIG9uIENsaWNrIC8gcHJvdmlkZXMgZm9jdXNwb2ludCBhbmQgQ1NTMyBwcm9wZXJ0aWVzXG5cbiAgICAgICAgLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG5cbiAgICAgICAgJGhlbHBlclRvb2xJbWFnZS5jbGljayhmdW5jdGlvbiAoZSkge1xuXG4gICAgICAgICAgICB2YXIgaW1hZ2VXID0gJCh0aGlzKS53aWR0aCgpO1xuICAgICAgICAgICAgdmFyIGltYWdlSCA9ICQodGhpcykuaGVpZ2h0KCk7XG5cbiAgICAgICAgICAgIC8vQ2FsY3VsYXRlIEZvY3VzUG9pbnQgY29vcmRpbmF0ZXNcbiAgICAgICAgICAgIHZhciBvZmZzZXRYID0gZS5wYWdlWCAtICQodGhpcykub2Zmc2V0KCkubGVmdDtcbiAgICAgICAgICAgIHZhciBvZmZzZXRZID0gZS5wYWdlWSAtICQodGhpcykub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgdmFyIGZvY3VzWCA9IChvZmZzZXRYIC8gaW1hZ2VXIC0gLjUpICogMjtcbiAgICAgICAgICAgIHZhciBmb2N1c1kgPSAob2Zmc2V0WSAvIGltYWdlSCAtIC41KSAqIC0yO1xuICAgICAgICAgICAgZm9jdXNQb2ludEF0dHIueCA9IGZvY3VzWDtcbiAgICAgICAgICAgIGZvY3VzUG9pbnRBdHRyLnkgPSBmb2N1c1k7XG5cbiAgICAgICAgICAgIC8vV3JpdGUgdmFsdWVzIHRvIGlucHV0XG4gICAgICAgICAgICBwcmludERhdGFBdHRyKCk7XG5cbiAgICAgICAgICAgIC8vVXBkYXRlIGZvY3VzIHBvaW50XG4gICAgICAgICAgICB1cGRhdGVGb2N1c1BvaW50KCk7XG5cbiAgICAgICAgICAgIC8vQ2FsY3VsYXRlIENTUyBQZXJjZW50YWdlc1xuICAgICAgICAgICAgdmFyIHBlcmNlbnRhZ2VYID0gKG9mZnNldFggLyBpbWFnZVcpICogMTAwO1xuICAgICAgICAgICAgdmFyIHBlcmNlbnRhZ2VZID0gKG9mZnNldFkgLyBpbWFnZUgpICogMTAwO1xuICAgICAgICAgICAgdmFyIGJhY2tncm91bmRQb3NpdGlvbiA9IHBlcmNlbnRhZ2VYLnRvRml4ZWQoMCkgKyAnJSAnICsgcGVyY2VudGFnZVkudG9GaXhlZCgwKSArICclJztcbiAgICAgICAgICAgIHZhciBiYWNrZ3JvdW5kUG9zaXRpb25DU1MgPSAnYmFja2dyb3VuZC1wb3NpdGlvbjogJyArIGJhY2tncm91bmRQb3NpdGlvbiArICc7JztcbiAgICAgICAgICAgICRjc3NBdHRySW5wdXQudmFsKGJhY2tncm91bmRQb3NpdGlvbkNTUyk7XG5cbiAgICAgICAgICAgIC8vTGVhdmUgYSBzd2VldCB0YXJnZXQgcmV0aWNsZSBhdCB0aGUgZm9jdXMgcG9pbnQuXG4gICAgICAgICAgICAkKCcucmV0aWNsZScpLmNzcyh7XG4gICAgICAgICAgICAgICAgJ3RvcCc6IHBlcmNlbnRhZ2VZICsgJyUnLFxuICAgICAgICAgICAgICAgICdsZWZ0JzogcGVyY2VudGFnZVggKyAnJSdcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkKCcuaGVscGVyLXRvb2wtcmV0aWNsZS1jc3MnKS52YWwoJ3RvcDogJyArIHBlcmNlbnRhZ2VZICsgJyU7IGxlZnQ6ICcgKyBwZXJjZW50YWdlWCArICclJyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXG4gICAgICAgIC8vIENoYW5nZSBpbWFnZSBvbiBwYXN0ZS9ibHVyXG4gICAgICAgIC8vIFdoZW4geW91IHBhc3RlIGFuIGltYWdlIGludG8gdGhlIHNwZWNpZmllZCBpbnB1dCwgaXQgd2lsbCBiZSB1c2VkIGZvciB0aGUgZGVtb1xuXG4gICAgICAgIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuXG4gICAgICAgICQoJyNtb2RhbF9zZXRfZm9jdXNfcG9pbnQnKS5vbignc2hvd24uYnMubW9kYWwnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgc2V0SW1hZ2UoJCgnI21vZGFsX3NldF9mb2N1c19wb2ludCcpLmZpbmQoJ2Zvcm0ucnYtZm9ybScpLmRhdGEoJ2ltYWdlJykpO1xuICAgICAgICB9KTtcblxuICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuICAgICAgICAvKiBVcGRhdGUgSGVscGVyICovXG4gICAgICAgIC8vIFRoaXMgZnVuY3Rpb24gaXMgdXNlZCB0byB1cGRhdGUgdGhlIGZvY3VzcG9pbnRcblxuICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuICAgICAgICBmdW5jdGlvbiB1cGRhdGVGb2N1c1BvaW50KCkge1xuICAgICAgICAgICAgLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG4gICAgICAgICAgICAvLyBTZWUgbm90ZSBpbiBzZXRJbWFnZSgpIGZ1bmN0aW9uIHJlZ2FyZGluZyB0aGVzZSBhdHRyaWJ1dGUgYXNzaWdubWVudHMuXG4gICAgICAgICAgICAvL1RMRFIgLSBZb3UgZG9uJ3QgbmVlZCB0aGVtIGZvciB0aGlzIHRvIHdvcmsuXG4gICAgICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cbiAgICAgICAgICAgICRmb2N1c1BvaW50Q29udGFpbmVycy5hdHRyKHtcbiAgICAgICAgICAgICAgICAnZGF0YS1mb2N1cy14JzogZm9jdXNQb2ludEF0dHIueCxcbiAgICAgICAgICAgICAgICAnZGF0YS1mb2N1cy15JzogZm9jdXNQb2ludEF0dHIueVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cbiAgICAgICAgICAgIC8vIFRoZXNlIHlvdSBETyBuZWVkIDopXG4gICAgICAgICAgICAvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cbiAgICAgICAgICAgICRmb2N1c1BvaW50Q29udGFpbmVycy5kYXRhKCdmb2N1c1gnLCBmb2N1c1BvaW50QXR0ci54KTtcbiAgICAgICAgICAgICRmb2N1c1BvaW50Q29udGFpbmVycy5kYXRhKCdmb2N1c1knLCBmb2N1c1BvaW50QXR0ci55KTtcbiAgICAgICAgICAgICRmb2N1c1BvaW50Q29udGFpbmVycy5hZGp1c3RGb2N1cygpO1xuICAgICAgICB9XG4gICAgfSk7XG59KGpRdWVyeSkpO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvZm9jdXMuanMiXSwic291cmNlUm9vdCI6IiJ9
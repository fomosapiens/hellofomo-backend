$(document).ready(function () {
    $('#selc1').on('change', function () {
        if ($(this).val() != null && $(this).val() != '') {
            $.ajax({
                url: '/log/api/getapiattributes',
                data: {
                    'url': $('#selc1').val()
                },
                type: 'GET',
                dataType: "json",
                success: function (response) {
                    var options = '<option value="">Select a field in Request\'s Body</option>';
                    $.each(response, function (value, text) {
                        options += '<option value="' + value + '">' + text + '</option>';
                    });
                    $('#selc2').html(options).prop('disabled', false);
                    $('#filter_search_value').prop('disabled', true);
                    gen_selc('selc2');
                }
            });
        } else {
            $('#selc2').prop('disabled', true);
            $('#filter_search_value').prop('disabled', true);
        }
    });

    $('#selc2').on('change', function () {
        if ($(this).val() != null && $(this).val() != '') {
            $('#filter_search_value').prop('disabled', false);
        } else {
            $('#filter_search_value').prop('disabled', true);
        }
    });

    $('#filter_start_date').on('change', function () {
        if ($(this).val() != null && $(this).val() != '') {
            $('#selc_0_1').prop('disabled', false);
        } else {
            $('#selc_0_1').prop('disabled', true);
        }
    });

    $('#filter_end_date').on('change', function () {
        if ($(this).val() != null && $(this).val() != '') {
            $('#selc_0_2').prop('disabled', false);
        } else {
            $('#selc_0_2').prop('disabled', true);
        }
    });

    $('.select-user').multiselect({
        enableFiltering: true,
        nonSelectedText: 'Modified by user'
    });
});
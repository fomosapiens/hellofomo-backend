<?php

/**
 * User
 */
Route::get('users/search', 'UserController@search');
Route::post('users/register', 'RegisterController@register')->name('userRegister');
Route::post('users/login', 'LoginController@attemptLogin')->name('userLogin');
Route::post('users/email/existed', 'UserController@checkEmailExisted')->name('emailExisted');
Route::put('user/finish-profile', 'UserController@finishProfile')->name('user.finishProfile');



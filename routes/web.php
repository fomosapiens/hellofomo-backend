<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin-show-login')->middleware(['locale']);
Route::post('admin/login', 'Admin\Auth\LoginController@login')->name('admin.login');
Route::get('admin/set-locale/{locale}', 'Admin\Auth\LoginController@setLocale')->name('admin.locale');

Route::get('admin/get-client', 'Admin\OAuthClientController@index')->middleware(['cus_auth_not_redirect']); // get client ID & client secret

Route::group([
    'prefix'    => 'admin',
    'namespace' => 'Admin',
    'as'        => 'admin.',
    'middleware' => ['acl.role', 'locale']
], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/profile', 'DashboardController@profile')->name('admin-profile');
    Route::get('/logout', 'Auth\LoginController@adminLogout')->name('logout');

    /**
     * User
     */
    Route::post('/users/more', 'UserController@more')->name('users.more');
    Route::get('/users/{user}/trash', 'UserController@trash')->name('users.trash');
    Route::get('/users/{user}/restore', 'UserController@restore')->name('users.restore');
    Route::post('/users/email/existed', 'UserController@checkEmailExist')->name('users.checkEmailExist');
    Route::post('/users/trash-multi', 'UserController@trashMultiple')->name('users.trash_multi');
    Route::post('/users/delete-multi', 'UserController@deleteMultiple')->name('users.delete_multi');
    Route::post('/users/restore-multi', 'UserController@restoreMultiple')->name('users.restore_multi');
    Route::get('/users/update-profile', 'UserController@getUpdateProfile')->name('users.updateProfile');
    Route::put('/users/update-profile', 'UserController@postUpdateProfile')->name('users.updateProfile');
    Route::resource('/users', 'UserController');

    /**
     * Author
     */
    Route::get('/authors/sorting', 'AuthorController@showSorting')->name('authors.showSorting');
    Route::post('/authors/update-sorting-all', 'AuthorController@updateSortingAll')->name('authors.updateSortingAll');
    Route::get('/authors/search/{q?}', 'AuthorController@search')->name('authors.search');
    Route::resource('/authors', 'AuthorController');
    Route::post('/authors/more', 'AuthorController@more')->name('authors.more');
    Route::get('/authors/{author}/trash', 'AuthorController@trash')->name('authors.trash');
    Route::get('/authors/{author}/restore', 'AuthorController@restore')->name('authors.restore');
    Route::post('/authors/bulk-trash-multi', 'AuthorController@trashMulti')->name('authors.bulk_trash_multi');
    Route::post('/authors/restore-multi', 'AuthorController@restoreMulti')->name('authors.restore_multi');
    Route::post('/authors/delete-multi', 'AuthorController@deleteMulti')->name('authors.delete_multi');
    Route::post('/authors/update-sorting', 'AuthorController@updateSorting')->name('authors.updateSorting');

    /**
     * Salutations
     */
    Route::resource('/salutations', 'SalutationController');
    Route::post('/salutations/more', 'SalutationController@more')->name('salutations.more');

    /**
     * Media
     */
    Route::get('/media', 'ComponentController@media')->name('media.index');

    /**
     * News
     */
    Route::post('/news/ajax-related-news-pagination', 'NewsController@moreRelatedNews')->name('news.moreRelatedNews');
    Route::get('/news/search/{q?}', 'NewsController@search')->name('news.search');
    Route::resource('/news', 'NewsController');
    Route::post('/news/more', 'NewsController@more')->name('news.more');
    Route::get('/news/{news}/trash', 'NewsController@trash')->name('news.trash');
    Route::get('/news/{news}/restore', 'NewsController@restore')->name('news.restore');
    Route::post('/news/apply', 'NewsController@apply')->name('news.apply');
    Route::post('/news/bulk-trash-multi', 'NewsController@trashMulti')->name('news.bulk_trash_multi');
    Route::post('/news/restore-multi', 'NewsController@restoreMulti')->name('news.restore_multi');
    Route::post('/news/delete-multi', 'NewsController@deleteMulti')->name('news.delete_multi');

    /**
     * Categories
     */
    Route::get('/categories/sorting', 'CategoryController@showSorting')->name('categories.showSorting');
    Route::post('/categories/update-sorting', 'CategoryController@updateSorting')->name('categories.updateSorting');
    Route::resource('/categories', 'CategoryController');
    Route::post('/categories/more', 'CategoryController@more')->name('categories.more');

    /**
     * Tags
     */
    Route::resource('/tags', 'TagController');
    Route::post('/tags/more', 'TagController@more')->name('tags.more');

    /**
     * General settings
     */
    Route::get('/settings', 'GeneralSettingController@edit')->name('settings.edit');
    Route::put('/settings', 'GeneralSettingController@update')->name('settings.update');

    /**
     * Email settings
     */
    Route::get('/settings/email', 'EmailSettingController@edit')->name('settings.email.edit');
    Route::put('/settings/email', 'EmailSettingController@update')->name('settings.email.update');

    /**
     * Security
     */
    Route::get('/security', 'SecurityController@index')->name('security.read');
    Route::post('/security', 'SecurityController@update')->name('security.update');
    Route::post('/security/more', 'SecurityController@more')->name('security.more');
    Route::get('/security/unblock', 'SecurityController@unblock')->name('security.unblock');

    /**
     * App
     */
    Route::get('/app/push-notification', 'PushNotificationController@getPushNotification')->name('app.push-notification');
    Route::post('/app/push-notification', 'PushNotificationController@postPushNotification')->name('app.send.push-notification');
    Route::get('/app/settings', 'AppSettingController@edit')->name('app.settings.edit');
    Route::put('/app/settings', 'AppSettingController@update')->name('app.settings.update');
    Route::get('/app/layout/settings/{key}', 'AppLayoutController@getLayoutSettings')->name('app.layout.settings');
    Route::post('/app/layout/settings/{key}', 'AppLayoutController@saveLayoutSettings')->name('app.layout.settings.save');
    Route::any('/app/layout/setting/{key}', 'AppLayoutController@settingContent')->name('app.layout.setting');

    /**
     * Custom translation
     */
    Route::post('/translation/export', 'TranslationController@exportTranslationToJsonFile')->name('export.translation');
});

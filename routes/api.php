<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * These are routes which are no need to authenticate
 */
Route::group(['prefix' => 'v1', 'namespace' => 'APIs\V1'], function() {
    /**
     * Mobile login
     */
    Route::post('auth', 'AuthController@login')->name('api.mobile.login');
    Route::post('auth/refresh-token', 'AuthController@refreshToken')->name('api.mobile.refresh-token');

    Route::group(['middleware'  => 'auth:api'], function() {
        /**
         * Font
         */
        Route::get('settings/font', "SettingController@getCurrentFont");

        /**
         * Layout Dashboard
         */
        Route::get('layout/dashboard', "SettingController@getCurrentLayoutDashboard");

        /**
         * Layout News Dashboard
         */
        Route::get('layout/news', "SettingController@getCurrentLayoutNewsDashboard");

        /**
         * Layout Category Dashboard
         */
        Route::get('layout/category', "SettingController@getCurrentLayoutCategoryDashboard");

        //Layout Contact
        Route::get('layout/contact', "LayoutController@getContentContact");

        //Layout Privacy
        Route::get('layout/privacy', "LayoutController@getContentPrivacy");

        //Layout Impressum
        Route::get('layout/impressum', "LayoutController@getContentImpressum");

        //Layout AGB
        Route::get('layout/agb', "LayoutController@getContentAGB");


        /**
         * News
         */
        Route::resource('news', 'NewsController');


        /**
         * News category
         */
        Route::resource('categories', 'CategoryController');
    });


});

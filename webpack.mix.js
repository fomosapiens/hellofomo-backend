const { mix } = require('laravel-mix');

/**
 * Fomo system
 */
mix.js([
    'resources/assets/admin/dev/js/table.js',
    'resources/assets/admin/dev/js/form-validation.js',
    'resources/assets/admin/dev/js/fomo-form.js',
    'resources/assets/admin/dev/js/news/fomo-news-form.js',
    'resources/assets/admin/dev/js/fomo-news-services.js',
    'resources/assets/admin/dev/js/page-bar.js',
    'resources/assets/admin/dev/js/user/form.js',
    'resources/assets/admin/dev/js/toastr.js',
    'resources/assets/admin/dev/js/user/update-profile.js',
    'resources/assets/admin/dev/js/fomo-link-marked-text.js',
    'resources/assets/admin/dev/js/app/fomo-app-dashboard-layout.js',
    'resources/assets/admin/dev/js/app/push-notification.js',
    'resources/assets/admin/dev/js/app/settings-general.js',
    'resources/assets/admin/dev/js/app/fomo-app-post-layout.js',
    'resources/assets/admin/dev/js/setting/fomo-security.js',
    'resources/assets/admin/dev/js/news/news-show.js',
    'resources/assets/admin/dev/js/author/fomo-author-sorting.js',
    'resources/assets/admin/dev/js/author/fomo-author-sorting-all.js',
    'resources/assets/admin/dev/js/init_ajax.js',
    'resources/assets/admin/dev/js/jquery.addMedia.js',
], 'public/vendor/metronic/internal/js/app.js');
mix.sass('resources/assets/admin/dev/scss/app.scss', 'public/vendor/metronic/internal/css');

/**
 * Files management
 * */
mix.sass('vendor/botble/media/resources/assets/sass/media.scss', 'public/vendor/media/css');
mix.js('vendor/botble/media/resources/assets/js/integrate.js', 'public/vendor/media/js');
mix.js('vendor/botble/media/resources/assets/js/media.js', 'public/vendor/media/js');
mix.js('vendor/botble/media/resources/assets/js/focus.js', 'public/vendor/media/js');

/**
 * Build for media package
 * */
mix
    .copyDirectory('public/vendor/media/css', './vendor/botble/media/public/assets/css')
    .copyDirectory('public/vendor/media/js', './vendor/botble/media/public/assets/js');

/**
 * ACL
 * */
mix.js('resources/assets/admin/vendor/acl/js/acl.js', 'public/vendor/acl/js/acl.js');

/**
 * Translation package
 */
mix.js('resources/assets/admin/vendor/translation/js/global.js', 'public/vendor/translation/js/global.js');

/**
 * Ajax datatable
 */
mix.copyDirectory('vendor/elidev/ajax-datatable/public', 'public/vendor/ajax-datatable');

if (mix.config.inProduction) {
    mix.version();
}

var FOMO = FOMO || {};

(function($) {
    $(function() {
        $.validator.addMethod("youtubeorVimeoVideo", function(value) {
            // not validate when user is not input the URL
            if ( value == '' ) {
                return true;
            }

            var youtubePattern = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
                vimeoPattern = /^.*(vimeo.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/;

            var isYoutube = youtubePattern.test(value),
                isVimeo   = vimeoPattern.test(value);

            return ( isYoutube || isVimeo ) ? true : false;
        }, "This field must be a valid Youtube or Vimeo video URL");

        $.validator.addMethod("lengthPerKeyword", function(value, element, param) {
            var keywords = value ? value.split(',') : '',
                totalKeywords = keywords.length,
                keyword = '';
            var success = true;
            for(var i=0; i < totalKeywords; i++) {
                keyword = keywords[i].trim();

                // Not check the last commas "Test, abc,"
                if (!keyword.length && totalKeywords && i == totalKeywords - 1) {
                    continue;
                }

                if (!keyword.length || keyword.length > param) {
                    success = false;
                }
            }

            return success;
        }, "The keyword is invalid. Each keyword is greater than zero and less or equal to 50 characters, separated by commas.");

        $.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "This field must have no space.");

        $.validator.addMethod("suppress_url", function(value, element) {
            var re = /^(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
            return value != "" && re.test(value) !== true;

        }, "This field does not allow a URL");

        $.validator.addMethod("noSpecialChars", function (value) {
            var allowCharsReg = /^[A-Za-z0-9-]*$/;
            return value != "" && allowCharsReg.test(value);
        }, "Please use only letters, numbers, and hyphen");

        $.validator.addMethod("customUrl", function(value, element, param) {
            // if no url, don't do anything
            if (value.length == 0) { return true; }

            // if user has not entered http:// https:// or ftp:// assume they mean http://
            if(!/^(https?|ftp):\/\//i.test(value)) {
                value = 'http://' + value; // set both the value
                //$(element).val(value); // also update the form element
            }

            // now check if valid url
            // http://docs.jquery.com/Plugins/Validation/Methods/url
            // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
            return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
        }, "Please enter a valid URL.");
    });
}) (jQuery)

/**
 * Handle app dashboard layout behaviours
 */
FomoAppDashboardLayout = function() {
    var form               = '#frm-new-section',
        originalSection    = '#new-section-box',
        appendDestination  = '#app-layout-dashboard-portlet-body form#frm-new-section',
        sectionLayout      = '.box-item-layout-section',
        sectionLayoutTitle = '.portlet-title .caption',
        btnAddSection      = '.group-button-action button.btn-block',
        btnDeleteSection   = '#app-layout-dashboard-wrapper .frm-section .delete-section',
        btnSectionCollapse = '#app-layout-dashboard-wrapper .frm-section .tools .collapse',
        btnSectionExpand   = '#app-layout-dashboard-wrapper .frm-section .tools .expand',
        btnSaveLayout      = '#fomo-save-app-dashboard-layout-btn',
        btnEachSectionSave = '.group-button-action .btn-each-section-save',
        selectDataType     = 'select[name="type[]"]',
        selectLayoutType   = '#app-layout-dashboard-wrapper select[name="layout[]"]',
        indexBox           = $('div[id^=frm-section]').length,
        originalForm       = null;

    /**
     * Handle add section button
     */
    function handleAddSectionButton() {
        $(document).on('click', btnAddSection, function(e) {
            e.preventDefault();
            cloneSection();
            toastr.info($(this).data().msg);
        });
    }

    /**
     * Handle delete section button
     */
    function handleDeleteSectionButton() {
        $(document).on('click', btnDeleteSection, function(e) {
            e.preventDefault();
            var itemDelete = $(this).closest('.frm-section');
            swal({
                    title: typeof alertTitle != undefined ? alertTitle : "Are you sure?",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: typeof alertText != undefined ? alertText : "Yes, delete it!",
                    closeOnConfirm: true,
                    showCancelButton: true
                },
                function() {
                    itemDelete.remove();
                });
        });
    }

    /**
     * Handle change background
     */
    function handleChangeBackground() {
        /**
         * Handle change background when collapse the section
         */
        $(document).on('click', btnSectionCollapse, function(e) {
            e.preventDefault();
            $(this).closest(sectionLayout).addClass('green').find(sectionLayoutTitle).removeClass('font-dark');
        });

        /**
         * Handle change background when expand the section
         */
        $(document).on('click', btnSectionExpand, function(e) {
            e.preventDefault();
            $(this).closest(sectionLayout).removeClass('green').find(sectionLayoutTitle).addClass('font-dark');
        });
    }

    /**
     * Handle save for "Save" button of each section in Left Panel
     */
    function handleSaveEachSectionInLeftPanel() {
        $(document).on('click', btnEachSectionSave, function(e) {
            e.preventDefault();
            $(btnSaveLayout).trigger('click');
        });
    }

    /**
     * Handle save settings
     */
    function handleSaveAppDashboardLayoutButton() {
        $(document).on('click', btnSaveLayout, function(e) {
            e.preventDefault();

            // send ajax to save layout settings
            let ajaxUrl = $(this).attr('data-ajax-url');
            let data = new FormData($(form)[0]);
            /** Layout post settings action */
            var current = $(e.currentTarget);
            var postForm = current.closest('body').find('#fomo-update-post-setting-display');
            if (postForm.length > 0) {
                var listItem = postForm.find('.fomo-post-sorting-list li');
                if (listItem.length > 0) {
                    $.each(listItem, function (index, item) {
                        var itemName = $(item).attr('data-id');
                        data.append(itemName, postForm.find('#'+itemName).is(':checked') ? 1 : 0);
                    });
                }
            } else {
                $("#frm-new-section > .frm-section ").each(function(index, item){
                    data.append("type_value_" + index, $(this).find('select[name="type_value[]"]:not(.hidden)').val());
                });
            }

            if ( typeof ajaxUrl != 'undefined' && ajaxUrl != '' ) {
                $.ajax({
                    url: ajaxUrl,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: data,
                    beforeSend: function () {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function( response ) {
                        $.unblockUI();
                        if ( response.errors == false ) {
                            toastr.success(response.data.message);

                            // after saved form -> update the original form
                            originalForm = $(form).serialize();
                        }
                    },
                    error: function(jqXHR, exception) {
                        $.unblockUI();
                        if ( jqXHR.status === 400 ) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    }
                });
            }
        });
    }

    /**
     * Handle select data type change
     */
    function handleSelectDataTypeChange() {
        $(selectDataType).on('change', function() {
            var divSelectDataTypeValue = $(this).parent().parent().siblings().closest('div.select-data-type-value'),
                lblDataTypeValue       = divSelectDataTypeValue.find('label[for="input-type-value"]'),
                selectCategory         = divSelectDataTypeValue.find('#input-category-type-value'),
                selectTag              = divSelectDataTypeValue.find('#input-tag-type-value');

            switch ( this.value ) {
                case 'all':
                case 'top_news':
                    $(selectCategory).prop('disabled', false);
                    $(selectTag).prop('disabled', 'disabled');

                    divSelectDataTypeValue.addClass('hidden');
                    $(selectCategory).addClass('hidden');
                    $(selectTag).addClass('hidden');
                    break;
                case 'category':
                    $(selectCategory).prop('disabled', false);
                    $(selectTag).prop('disabled', 'disabled');

                    divSelectDataTypeValue.removeClass('hidden');
                    $(selectCategory).removeClass('hidden');
                    $(selectTag).addClass('hidden');

                    $(lblDataTypeValue).text('Category');
                    break;
                case 'tag':
                    $(selectTag).prop('disabled', false);
                    $(selectCategory).prop('disabled', 'disabled');

                    divSelectDataTypeValue.removeClass('hidden');
                    $(selectTag).removeClass('hidden')
                    $(selectCategory).addClass('hidden');

                    $(lblDataTypeValue).text('Tag');
                    break;
            }
        })
    }

    /**
     * Handle rename each section when layout type changed
     */
    function handleRenameEachSectionWhenLayoutTypeChange() {
        $(selectLayoutType).on('change', function() {
            renameSectionTitle($(this));
        });
    }

    /**
     * Rename each section title when layout type change
     * @param $element
     */
    function renameSectionTitle($element) {
        var $sectionTitle = $element.closest('div.portlet-body.form').siblings().find('div.caption.text-capitalize'),
            $selectSectionType = $element.parent().parent().parent().siblings().find('select[name="section_type[]"]');

        $sectionTitle.html($selectSectionType.val() + " " + $element.val());
    }

    /**
     * Handle validate data offset
     */
    function handleValidateDataOffset() {
        $(document).on('blur', '#app-layout-new-action-wrapper input[name="data_offset[]"]', function() {
            $(this).val(parseInt($(this).val()));
        });

        $(document).on('blur', '#app-layout-dashboard-wrapper input[name="data_offset[]"]', function() {
            $(this).val(parseInt($(this).val()));
        });
    }

    /**
     * Clone new section to main form
     */
    function cloneSection() {
        $("#app-layout-new-action-wrapper .select2-multiple").select2('destroy');
        var $original = $(originalSection),
            $cloned = $original.clone(true).attr('id', 'new_form' + indexBox);

        // get original selects into a jq object
        var $originalSelects = $original.find('select');
        $cloned.find('select').each(function(index, item) {
            // set new select to value of old select
            $(item).val( $originalSelects.eq(index).val() );
        });

        // get original textareas into a jq object
        var $originalTextareas = $original.find('textarea');
        $cloned.find('textarea').each(function(index, item) {
            // set new textareas to value of old textareas
            $(item).val($originalTextareas.eq(index).val());
        });

        // remove button "Add Section" and add button "Save"
        $cloned.find('.group-button-action button').remove();
        $cloned.find('.group-button-action').append('<button type="button" class="btn green pull-right btn-each-section-save"><i class="fa fa-save"></i>&nbsp;Save</button>');

        var html = $("#box-item-container").clone(true).removeClass('hidden').attr( 'id', 'frm-section' + indexBox );
        html.find('.caption').html($cloned.find("select[name='section_type[]']").val() + " " + $cloned.find("select[name='layout[]']").val());
        $cloned.appendTo(html.find('.portlet-body'));
        html.appendTo($(appendDestination));

        $("#app-layout-new-action-wrapper .select2-multiple").select2();
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .select2-multiple").select2();
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .select2-container--bootstrap").css('width', '100%');
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .select2-container--bootstrap .select2-search__field").css('width', '100%');
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-count").html($("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-count .input_touchspin_vertical2").clone())
        $("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-offset").html($("#app-layout-dashboard-portlet-body #frm-section" + indexBox + " .form-group-touchspin-data-offset .input_touchspin_vertical2").clone())
        $("#app-layout-new-action-wrapper .select2-container--bootstrap").css('width', '100%');
        $("#app-layout-new-action-wrapper .select2-container--bootstrap .select2-search__field").css('width', '100%');
        indexBox++;

        // subscribe change event to modify section title when Layout type changed
        $($cloned.find("select[name='layout[]']")).on('change', function() {
            renameSectionTitle($(this));
        });
        resetTouchSpin();
    }

    /**
     * Set min height right panel
     */
    function setMinHeightRightPanel() {
        $("#app-layout-new-action-wrapper > .portlet").css('min-height', $("#app-layout-dashboard-wrapper > .portlet").css('height'));
    }

    /**
     * Set min height left panel
     */
    function setMinHeightLeftPanel() {
        $("#app-layout-dashboard-wrapper > .portlet").css('min-height', $("#app-layout-new-action-wrapper > .portlet").css('height'));
    }

    /**
     * Save original form to ask user before they are leaving
     */
    function saveOriginalForm() {
        setTimeout(function() {
            if ( $(form).length > 0 ) {
                originalForm = $(form).serialize();
            }
        }, 1000);
    }

    /**
     * When the form has changed, ask user before they leave
     */
    function askBeforeLeave() {
        // if the form has changed and we are in app dashboard layout setting page
        if ( originalForm != null && $(form).length > 0 && $(form).serialize() != originalForm ) {
            // Refer: https://stackoverflow.com/questions/1119289/how-to-show-the-are-you-sure-you-want-to-navigate-away-from-this-page-when-ch
            return true;
        }
    }

    function resetTouchSpin() {
        if ($.fn.TouchSpin) {
            $(".input_touchspin_vertical2").TouchSpin({
                min: 0,
                max: 10000,
                step: 1,
                verticalbuttons: true,
                verticalupclass: 'glyphicon glyphicon-plus',
                verticaldownclass: 'glyphicon glyphicon-minus'
            });
        }
    }

    return {
        init : function() {
            // enable sort by dragging items
            var sortElement = document.getElementById('frm-new-section');
            if ( sortElement != null ) {
                Sortable.create(sortElement, {});
            }

            // collapse all sections if have
            $('#app-layout-dashboard-portlet-body a.collapse').trigger('click');

            setMinHeightLeftPanel();

            // register event handlers
            handleAddSectionButton();
            handleDeleteSectionButton();
            handleChangeBackground();
            handleSaveAppDashboardLayoutButton();
            handleSelectDataTypeChange();
            handleSaveEachSectionInLeftPanel();
            handleRenameEachSectionWhenLayoutTypeChange();
            handleValidateDataOffset();
            saveOriginalForm();
            resetTouchSpin();
        },
        askBeforeLeave: function() {
            return askBeforeLeave();
        }
    };
}();

jQuery(document).ready(function() {
    FomoAppDashboardLayout.init();

    $(window).on("beforeunload", function() {
        return FomoAppDashboardLayout.askBeforeLeave();
    });
});

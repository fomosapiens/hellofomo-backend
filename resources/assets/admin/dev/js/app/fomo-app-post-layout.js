(function ($) {
    $(document).ready(function () {

        var postSortNestable = null;
        var sorting = $('#fomo-update-post-setting-display .nestable');
        if (sorting.length > 0 ) {
            postSortNestable = sorting.nestable({
                group: 1,
                maxDepth: 1,   // this is important if you have the same case of the question
                reject: [{
                    rule: function () {
                        // The this object refers to dragRootEl i.e. the dragged element.
                        // The drag action is cancelled if this function returns true
                        var ils = $(this).find('>ol.dd-list > li.dd-item');
                        for (var i = 0; i < ils.length; i++) {
                            var datatype = $(ils[i]).data('type');
                            if (datatype === 'child')
                                return true;
                        }
                        return false;
                    },
                    action: function (nestable) {
                        // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element,
                        // and nestable is, well, the nestable root element
                    }
                }]
            });
        }

        $('#fomo-update-post-setting-display').off('click', '.fomo-app-setting-post-display').on('click', '.fomo-app-setting-post-display', function (e) {
            var current = $(e.currentTarget);
            var sortingList = current.closest('form').find('.fomo-post-sorting-list');
            if (sortingList.length > 0) {
                var currentId = current.attr('id');
                var currentItem = sortingList.find('li#' + currentId);
                if (current.is(':checked')) {
                    currentItem.removeClass('disabled');
                } else {
                    currentItem.addClass('disabled');
                }
            }
        });

    });
})(jQuery);
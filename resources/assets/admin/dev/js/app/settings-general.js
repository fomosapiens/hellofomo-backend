(function ($) {
    $(document).ready(function () {
        $('#app_font').off('change').on('change', function (e) {
            var current = $(e.currentTarget);
            var exampleText = current.closest('.tab-pane').find('#fomo-font-example');
            if (exampleText.length > 0) {
                var option = current.find('option:selected');
                var fontClass = option.attr('data-font-name');
                fontClass = typeof fontClass != 'undefined' ? fontClass : '';
                exampleText.attr('class', fontClass);
            }
        });
    });
})(jQuery);
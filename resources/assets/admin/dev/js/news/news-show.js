$(document).ready(function() {
    $(window).on('load', function () {
        $(".news-show #load-more-related-news").trigger("click");
    });

    $(".fancybox").fancybox();

    var $picContainer = $('.blog-single-img'),
        $pic = $('.blog-single-img img'),
        widthPercentage = ($pic.prop('naturalWidth') / $picContainer.width()) * 100;

    // we will not apply focus point plugin if the image width is less than the pic container width
    if ( $picContainer.length && $picContainer.hasClass('focuspoint') && widthPercentage <= 50 ) {
        $picContainer.removeClass('focuspoint');
        $pic.addClass('img-responsive pic-bordered pic-cover');
    } else {
        $('.focuspoint').focusPoint();
    }
});

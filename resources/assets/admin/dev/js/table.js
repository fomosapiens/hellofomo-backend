jQuery(document).ready(function() {
    var onChangeColumnClassesCallback = function(columnName) {
        var name = '';
        if (columnName == 'status') {
            name = 'dt-body-center text-center';
        }
        if (columnName == 'category_names_with_link') {
            name = 'category-cell';
        }
        if (columnName == 'tag_names_with_link') {
            name = 'tag-cell';
        }

        return name;
    };

    AjaxDatatable.init(onChangeColumnClassesCallback);

    AjaxDatatable.onAjaxLoaded(function(oSettings) {
         aclFn.init();

         // set filter select drop down searchable
         $("select[name='category_names_with_link']").select2();
         $("select[name='tag_names_with_link']").select2();
    });

    /**
     * Bulk actions support
     */
    AjaxDatatable.onGroupActionSubmit(function(action, grid) {
        var behaviour = action.val(),
            selectedRows = grid.getSelectedRows();

        switch (behaviour) {
            case 'bulk_set_categories':
                FomoNewsServices.setCategories(selectedRows);
                return;

            case 'bulk_set_tags':
                FomoNewsServices.setTags(selectedRows);
                return;

            case 'bulk_trash':
            case 'bulk_restore':
            case 'bulk_delete':
                var ajaxUrl    = action.find(':selected').data('ajax-url'),
                    ajaxMethod = action.find(':selected').data('method');

                FomoNewsServices.bulkActions(selectedRows, ajaxUrl,  ajaxMethod, behaviour);
                return;
        }

        grid.setAjaxParam("customActionType", "group_action");
        grid.setAjaxParam("customActionName", action.val());
        grid.setAjaxParam("id", grid.getSelectedRows());
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();
    });
});

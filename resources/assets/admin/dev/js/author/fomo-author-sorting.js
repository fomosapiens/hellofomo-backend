$(document).ready(function() {
    var sortNestable = null;
    var modalSorting = '#fomo-modal-update-sorting';

    $(document).on('click', "#btn-open-sorting-nestablelist", function(){
        $(modalSorting).modal('show');
        let dataRow = $("table#authors-datatable tbody >tr");
        let dataThRow = $("table#authors-datatable thead tr.heading").clone();

        dataThRow.find("th:first-child").remove();
        dataThRow.find("th:last-child").remove();

        var max_column = 4;
        let text_li = '<div class="tb-fomo-nestable">';
        dataThRow.find('th').each(function(i, e){
            text_li += "<div class='st-col-left st-col st-col-" + i +  "'>" + $(e).html() + "</div>";
            if (i + 1 >= max_column) {
                return false;
            }
        });
        text_li += "</div>";

        $(modalSorting + " .modal-body ol.dd-list").html('<li class="item-heading"><div class="dd-heading">' + text_li + '</div></li>');

        dataRow.each(function(index,element){
            ele = $(element).clone();
            let dataId = $(ele).find('input[name="id[]"]').val();
            $(ele).find("td:first-child").remove();
            $(ele).find("td:last-child").remove();
            let text_li = '<div class="tb-fomo-nestable">';
            ele.find('td').each(function(i, e){
                text_li += "<div class='st-col-left st-col st-col-" + i +  "'>" + $(e).html() + "</div>";
                if (i + 1 >= max_column) {
                    return false;
                }
            });
            text_li += "</div>";
            $(modalSorting + " .modal-body ol.dd-list").append('<li class="dd-item" data-id="' + dataId + '"> <div class="dd-handle">' + text_li + '</div></li>')
        });

        sortNestable = $(modalSorting + ' .nestable').nestable({
            group: 1,
            maxDepth: 1,   // this is important if you have the same case of the question
            reject: [{
                rule: function () {
                    // The this object refers to dragRootEl i.e. the dragged element.
                    // The drag action is cancelled if this function returns true
                    var ils = $(this).find('>ol.dd-list > li.dd-item');
                    for (var i = 0; i < ils.length; i++) {
                        var datatype = $(ils[i]).data('type');
                        if (datatype === 'child')
                            return true;
                    }
                    return false;
                },
                action: function (nestable) {
                    // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element,
                    // and nestable is, well, the nestable root element
                }
            }]
        })
    });

    //save
    $(document).on('submit', modalSorting + ' form', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {myData:JSON.stringify(sortNestable.nestable('serialize'))},
            beforeSend: function beforeSend() {
                $(modalSorting + " .modal-content .spinner-loading").show();
            },
            success: function success(response) {
                $(modalSorting + " .modal-content .spinner-loading").hide();
                $(modalSorting).modal('hide');
                if (response.errors == false) {
                    toastr.success(response.data.message);
                }
                //reload table
                $("table#authors-datatable .btn-filter-cancel").trigger('click');
            },
            error: function error(jqXHR, exception) {
                $.unblockUI();
                $(modalSorting + " .modal-content .spinner-loading").hide();
                if (jqXHR.status === 422) {
                    let errors = "";
                    data = jqXHR.responseJSON;
                    for (var k in data) {
                        if (data.hasOwnProperty(k)) {
                            data[k].forEach(function (val) {
                                errors += val;
                            });
                        }
                    }
                    toastr.error(errors);
                }
            }
        });
    });
});
$(document).ready(function() {

    var sortNestable = null;
    var sorting = $('#fomo-update-sorting .nestable');
    if (sorting.length > 0 ) {
        sortNestable = sorting.nestable({
            group: 1,
            maxDepth: 1,   // this is important if you have the same case of the question
            reject: [{
                rule: function () {
                    // The this object refers to dragRootEl i.e. the dragged element.
                    // The drag action is cancelled if this function returns true
                    var ils = $(this).find('>ol.dd-list > li.dd-item');
                    for (var i = 0; i < ils.length; i++) {
                        var datatype = $(ils[i]).data('type');
                        if (datatype === 'child')
                            return true;
                    }
                    return false;
                },
                action: function (nestable) {
                    // This optional function defines what to do when such a rule applies. The this object still refers to the dragged element,
                    // and nestable is, well, the nestable root element
                }
            }]
        });
    }

    $('#authors-sorting-full').off('click').on('click', function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var myData = current.closest('form').find('#myData');
        if (myData.length > 0) {
            myData.val(JSON.stringify(sortNestable.nestable('serialize')));
        }
        current.closest('form').submit();
    });

    $('body').off('click', '#btn-open-sorting-all-author').on('click', '#btn-open-sorting-all-author', function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var url = current.data('url');
        if (typeof url !== 'undefined' && url != '') {
            window.open(url, '_blank');
        }
    });
});
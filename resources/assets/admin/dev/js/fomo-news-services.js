FomoNewsServices = function () {

    var modalSelector            = '#fomo-news-listing-set-categories-tags',
        applyCategoryTagForm     = '#apply-categories-tags-form',
        modalTitleSelector       = modalSelector + ' .modal-title',
        categoriesList           = modalSelector + ' .categories-list',
        tagsList                 = modalSelector + ' .tags-list',
        newsSelector             = modalSelector + ' #news',
        typeHiddenInputSelector  = modalSelector + ' input[name="type"]',
        categoryCheckboxSelector = modalSelector + ' input#category-checkbox-',
        tagCheckboxSelector      = modalSelector + ' input#tag-checkbox-',
        btnSubmitSelector        = modalSelector + ' .modal-footer button.btn-set-categories-tags',
        type                     = '',
        newsIds                  = [],
        goodArray                = [],
        badArray                 = [],
        indeterminateArray       = [];

    /**
     * Set categories for each selected news
     */
    function setCategories() {
        if ( newsIds.length == 0 ) {
            return;
        }

        $(newsSelector).val(newsIds); // assign selected news IDs to hidden input
        $(typeHiddenInputSelector).val(type); // assign type of bulk action : set categories
        openModal();
        closeModal();
        selectCheckboxes();
    }

    /**
     * Set tags for each selected news
     */
    function setTags() {
        if ( newsIds.length == 0 ) {
            return;
        }

        $(newsSelector).val(newsIds); // assign selected news IDs to hidden input
        $(typeHiddenInputSelector).val(type); // assign type of bulk action : set tags
        openModal();
        closeModal();
        selectCheckboxes();
    }

    /**
     * Select checkboxes when opening the modal
     */
    function selectCheckboxes() {
        var array1 = [],
            array2 = [],
            currentIds = [];

        // not do if not know set categories or set tags
        if ( typeof type == 'undefined' || type == '' || $.inArray(type, ['category', 'tag']) == -1 ) {
            console.log('invalid type');
            return;
        }

        // not do if not have selected news
        if ( newsIds.length == 0 ) {
            console.log('invalid selected news');
            return;
        }

        goodArray = [], badArray = [], indeterminateArray = [];
        var selector     = (type == 'category') ? categoryCheckboxSelector : tagCheckboxSelector,
            dataSelector = (type == 'category') ? 'data-categories' : 'data-tags';
        $.each(newsIds, function(index, newsId) {
            // get id (category Ids or tag Ids) of each news and convert to array
            currentIds = $("input.checkboxes[data-news-id='" + newsId + "']").attr(dataSelector);
            currentIds = currentIds.split(',');

            // for the first loop, just add these ids to the array
            if ( goodArray.length == 0 ) {
                goodArray = $.unique(currentIds);
                return true; // continue to next loop
            }

            array1 = $.unique(goodArray);   // previous ids
            array2 = $.unique(currentIds);  // current ids
            if ( array1.length != array2.length || array1.join(',') !== array2.join(',') ) {
                // get different items from 2 arrays
                var difference = $.unique($.merge($(array1).not(array2).get(), $(array2).not(array1).get()));
                $.merge(badArray, difference);
            }
        });

        // always disable submit button for the first time the popup dialog open
        $(btnSubmitSelector).prop('disabled', true);

        // get unique elements
        goodArray = $.unique(goodArray);
        badArray = jQuery.grep($.unique(badArray), function(n, i){
            return (n !== "" && n != null && n != 0); // remove empty item
        });

        // if element is existing in badArray, it's not allowed to exist in goodArray
        goodArray = jQuery.grep(goodArray, function(value) {
            return $.inArray(value, badArray);
        });

        // mark these checkboxes as checked state
        if ( goodArray.length > 0 ) {
            $.each(goodArray, function(index, id) {
                $(selector + id).prop('checked', true);
            });
        }

        // mark these checkboxes as indeterminate state
        if ( badArray.length > 0 ) {
            indeterminateArray = badArray;
            $.each(badArray, function(index, id) {
                $(selector + id).prop('indeterminate', true);
                $(selector + id).prop('checked', false);
            });
        }
    }

    /**
     * Enable submit button if "indeterminate" checkboxes are all checked
     *
     * @param string selector
     * @param int totalCheckboxesAreSelected
     */
    function enableSubmitButton(selector, totalCheckboxesSelected) {
        if ( typeof selector == 'undefined' || selector == '' ) {
            return;
        }

        var checkboxSelector   = (type == 'category') ? '.categories-list input[name="categories[]"]:checked' : '.tags-list input[name="tags[]"]:checked',
            dataSelector       = (type == 'category') ? 'data-categories' : 'data-tags',
            currentSelectedIds = $(checkboxSelector).map(function () {return this.value;}).get(),
            finalSelectedIds   = indeterminateArray.concat(currentSelectedIds),
            isDisabled         = false;

        // must set categories & tags for each news row
        $.each(newsIds, function(index, newsId) {
            var currentIds = $("input.checkboxes[data-news-id='" + newsId + "']").attr(dataSelector).split(',');
            var finalIdsExistInCurrentRow = finalSelectedIds.filter(function (elem) {
                return currentIds.indexOf(elem) != -1;
            }).length;
            if ( finalIdsExistInCurrentRow == 0 && totalCheckboxesSelected <= 0 ) {
                $(btnSubmitSelector).prop('disabled', true);
                isDisabled = true;
                return false;
            }
        });

        if ( isDisabled ) {
            return;
        }

        if ( totalCheckboxesSelected > 0 || (totalCheckboxesSelected == 0 && indeterminateArray.length == badArray.length) ) {
            $(btnSubmitSelector).removeAttr('disabled');
        }
    }

    /**
     * Open modal for 2 type: category or tag
     */
    function openModal() {
        if ( type == 'category' ) {
            $(modalTitleSelector).text('Set categories for selected news');
            $(categoriesList).removeClass('hidden');
            $(tagsList).addClass('hidden');
            $(modalSelector).modal('show');

            // listen on change each checkboxes to enable submit button
            $(document).on('change', modalSelector + ' input[name="categories[]"]', function() {
                handleStateOfIndeterminateCheckboxes($(this));
                var totalSelectedCheckboxes = $('.categories-list input[name="categories[]"]:checked').length;
                enableSubmitButton(modalSelector + ' input[name="categories[]"]', totalSelectedCheckboxes);
            });
        } else if ( type == 'tag' ) {
            $(modalTitleSelector).text('Set tags for selected news');
            $(categoriesList).addClass('hidden');
            $(tagsList).removeClass('hidden');
            $(modalSelector).modal('show');

            // listen on change each checkboxes to enable submit button
            $(document).on('change', modalSelector + ' input[name="tags[]"]', function() {
                handleStateOfIndeterminateCheckboxes($(this));
                var totalSelectedCheckboxes = $('.tags-list input[name="tags[]"]:checked').length;
                enableSubmitButton(modalSelector + ' input[name="tags[]"]', totalSelectedCheckboxes);
            });
        }
    }

    /**
     * On closing the modal, reset controls to default value and remove all event listeners
     */
    function closeModal() {
        $(modalSelector).on('hidden.bs.modal', function () {
            // restore these controls to default value
            $(modalSelector + ' input[name="categories[]"]').removeAttr('checked');
            $(modalSelector + ' input[name="categories[]"]').prop('indeterminate', false);
            $(modalSelector + ' input[name="tags[]"]').removeAttr('checked');
            $(modalSelector + ' input[name="tags[]"]').prop('indeterminate', false);
            $(btnSubmitSelector).removeAttr('disabled');
            $(applyCategoryTagForm + ' input[name="indeterminate_checkboxes"]').remove();
            type = '', goodArray = [], badArray = [], indeterminateArray = [], newsIds = [];

            // remove event listeners
            $(modalSelector).off('hidden.bs.modal');
            $(modalSelector + ' input[name="categories[]"]').off('change');
            $(modalSelector + ' input[name="tags[]"]').off('change');
            $(btnSubmitSelector).off('click');
        })
    }

    /**
     * Call ajax to store categories/tags for selected news
     */
    function handleSubmitButton() {
        $(btnSubmitSelector).off('click');
        $(document).on('click', btnSubmitSelector, function() {
            // not process when button is disabled
            if ( $(this).prop('disabled') ) {
                return;
            }

            var ajaxUrl = $(modalSelector).find('input#ajax-url').val();
            if ( typeof ajaxUrl != 'undefined' && ajaxUrl != '' ) {

                // also send indeterminate checkboxes to the server
                $(applyCategoryTagForm + ' input[name="indeterminate_checkboxes"]').remove();
                $('<input>').attr({
                    type: 'hidden',
                    name: 'indeterminate_checkboxes',
                    value: indeterminateArray.join(',')
                }).appendTo(applyCategoryTagForm);

                $.ajax({
                    url: ajaxUrl,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: new FormData($(applyCategoryTagForm)[0]),
                    beforeSend: function () {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function( response ) {
                        $.unblockUI();
                        $(modalSelector).modal('hide');
                        if ( response.errors == false ) {
                            toastr.success(response.data.message);

                            // update UI for newly categories/tags
                            if ( typeof response.data.news != 'undefined' && response.data.news.length ) {
                                $.each(response.data.news, function(index, newsItem) {
                                    // update categories
                                    if ( response.data.type == 'category' ) {
                                        $('input[data-news-id="' + newsItem.id + '"]').attr('data-categories', newsItem.category_ids);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.category-cell').html(newsItem.category_names_with_link);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.category-cell').pulsate({
                                            color: "#51a351",
                                            repeat: false
                                        });
                                    } else if ( response.data.type == 'tag' ) {
                                        // update tags
                                        $('input[data-news-id="' + newsItem.id + '"]').attr('data-tags', newsItem.tag_ids);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.tag-cell').html(newsItem.tag_names_with_link);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.tag-cell').pulsate({
                                            color: "#51a351",
                                            repeat: false
                                        });
                                    }
                                });
                            }
                        }
                    },
                    error: function(jqXHR, exception) {
                        $.unblockUI();
                        if ( jqXHR.status === 400 ) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    }
                });
            }
        });
    }

    /**
     * Change to checked instead of un-check when the checkbox is in "indeterminate" state
     * @param checkbox $element
     */
    function handleStateOfIndeterminateCheckboxes($element) {
        // if this checkbox is one of indeterminate checkboxes
        if ( $.inArray($element.attr('data-id'), indeterminateArray) != -1 ) {
            // if after clicked, it's still not check -> check it
            if ( !$element.prop('checked') ) {
                $element.indeterminate = false;
                $element.prop('checked', true);
            }

            // remove indeterminate item
            var removeItem = $element.attr('data-id');
            indeterminateArray = jQuery.grep(indeterminateArray, function(value) {
                return value != removeItem;
            });
        }
    }

    function bulkActions(ids, ajaxUrl, method, filter) {
        if ( ids.length == 0 ) {
            return;
        }
        $.ajax({
            url: ajaxUrl,
            type: method,
            cache: false,
            data: {'ids' : ids},
            beforeSend: function () {
                $.blockUI(FOMO.blockMetronicUI);
            },
            success: function( response ) {
                $.unblockUI();
                $('.table-container .table-group-action-input').val('');
                if ( response.errors == false ) {
                    toastr.success(response.data.message);
                    AjaxDatatable.filter(filter);
                }
            },
            error: function(jqXHR, exception) {
                $.unblockUI();
                if ( jqXHR.status === 400 ) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            }
        });
    }

    return {
        init : function() {
            handleSubmitButton();
        },

        setCategories: function (ids) {
            newsIds = ids;
            type = 'category';
            setCategories();
        },

        setTags: function (ids) {
            newsIds = ids;
            type = 'tag';
            setTags();
        },
        bulkActions: function(ids, ajaxUrl, method, filter ){
            if( typeof method == 'undefined' ) {
                method = 'post';
            }
            filter = (filter == 'bulk_trash') ? '' : 1;
            bulkActions(ids, ajaxUrl, method, filter);
        }
    };

}();

jQuery(document).ready(function() {
    FomoNewsServices.init();
});

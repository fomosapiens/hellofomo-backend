FomoUserUpdateProfile = function () {

    /**
     *
     * @author hieuluong
     * */
    function handle() {
        $(document).on('click', '#admin-bar-edit-profile', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).data('url'),
                type: 'GET',
                beforeSend: function () {
                    $.blockUI(FOMO.blockMetronicUI);
                    $("#fomo-modal-update-profile").modal('show').find(".modal-content .spinner-loading").show();
                },

                success: function( response ) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    $("#fomo-modal-update-profile .modal-body").html(response);
                    $("#fomo-modal-update-profile .rvMedia_preview_file_avatar, #fomo-modal-update-profile .btn-upload-image").rvMedia({
                        multiple: false,
                        onSelectFiles: function (files, $el) {
                            var firstItem = _.first(files);
                            if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && firstItem.type.match('image')
                                && typeof $el !== 'undefined') {
                                console.log($el.parent().find('.preview_image'));
                                $el.parent().find('.image-data').val(firstItem.id);
                                $el.parent().find('.preview_image').attr('src', firstItem.thumb)
                                    .data('rv-media', [{'selected_file_id': firstItem.id}]).show();
                                $el.parent().find(".btn-remove-image").show();
                            }
                        }
                    });
                    $("#fomo-modal-update-profile .select2").select2();
                },
                error: function(jqXHR, exception) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                }
            });
        });

        $(document).on('submit', '#fomo-modal-update-profile form', function(e) {
            e.preventDefault();
            let formData = $(this).serializeArray();
            $.each(formData, function (i, input) {
                if ( input.name != 'password' && input.name != 'language' && input.value == '')
                    delete input.name;
            });
            formData.push({name: 'image_id', value: $("#fomo-modal-update-profile form input[name=image_id]").val()});

            $.ajax({
                url: $(this).data('url'),
                type: 'PUT',
                data: formData,
                beforeSend: function () {
                    $.blockUI(FOMO.blockMetronicUI);
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").show();
                },
                success: function( response ) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    $("#fomo-modal-update-profile").modal('hide');
                    if ( response.errors == false ) {
                        toastr.success(response.data.message);
                    }
                },
                error: function(jqXHR, exception) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    if (jqXHR.status === 422) {
                        errors = "";
                        data = jqXHR.responseJSON;
                        for(let k in data){
                            if(data.hasOwnProperty(k)){
                                data[k].forEach(function(val){
                                    errors += val;
                                });
                            }
                        }
                        toastr.error(errors);
                    }
                }
            });
        });

        $(document).on('click', '#fomo-modal-update-profile .btn-remove-image', function(e) {
            $(this).closest('.image-box').find('.image-data').val('');

            var $previewImg = $(this).closest('.image-box').find('img.preview_image'),
                defaultSrc = $previewImg.attr('data-default-src');
            if ( typeof defaultSrc !== 'undefined' && defaultSrc !== '' ) {
                $previewImg.attr('src', defaultSrc).show();
            } else {
                $(this).closest('.image-box').find('img').hide();
            }

            $(this).closest('.image-box').find(".btn-upload-image").show();
            $(this).closest('.image-box').find(".btn-remove-image").hide();
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handle();
        }
    };

}();

jQuery(document).ready(function() {
    FomoUserUpdateProfile.init();
});

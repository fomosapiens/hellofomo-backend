@extends('layouts.app')

@section('title')
    {{ __('Reset Password') }}
@endsection

@section('body_class')
    page-reset-password
@endsection

@section('sub_header')
    @component('partial.components.sub_headers.simple')
    <div class="container">
        <h2 class="ttl-sub">{{ __('Reset password') }}</h2>
    </div>
    @endcomponent
@endsection

@section('content')
    <div class="p-container">
        <div class="content-666">
            <div class="sign-section">
                <div class="frm-reset-password blk-all-form">
                    @if (session('status'))
                        <div class="form-action-row">
                            <div class="message">
                                <div class="success">
                                    <ul>
                                        <li>{{ session('status') }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @elseif (isset($errors))
                        <div class="form-action-row">
                            <div class="message">
                                <div class="error">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    <form id="gce-reset-pass-frm" class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group field-group">
                                    <input id="email" type="email" class="form-control ipt--field js-form--control" name="email" value="{{ old('email') }}" required autofocus>
                                    <label class="label--field" for="email">{{ __('E-Mail Address') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group field-group">
                                    <input id="password" type="password" class="form-control ipt--field" name="password" value="{{ old('password') }}" required>
                                    <label class="label--field" for="password">{{ __('Password') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group field-group">
                                    <input id="password_confirmation" type="password" class="form-control ipt--field" name="password_confirmation" value="{{ old('password_confirmation') }}" required>
                                    <label class="label--field" for="password_confirmation">{{ __('Confirm Password') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-action-row">
                            <button class="btn btn-green btn-save" type="submit">{{ __('Reset password') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


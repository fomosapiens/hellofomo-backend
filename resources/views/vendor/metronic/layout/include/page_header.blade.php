@php
    $user = Auth::user();
@endphp

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('admin.dashboard') }}">
                @php $logoId = Settings::get(config('fomo.settings.image_logo')) @endphp
                <img src="{{ !empty($logoId) ? rv_media_get_image_url_from_id($logoId) :  asset('fomo_logo.png') }}" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <!-- END LOGO -->

        <!-- BEGIN TOP NAVIGATION MENU -->
        <?php $locale = session('locale', config('app.locale')) ?>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-language">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        @if ($locale == 'en')
                            <img alt="" src="/vendor/metronic/global/img/flags/us.png"/>
                            <span class="langname">{{ __('EN') }} </span><i class="fa fa-angle-down"></i>
                        @else
                            <img alt="" src="/vendor/metronic/global/img/flags/de.png"/>
                            <span class="langname">{{ __('DE') }} </span><i class="fa fa-angle-down"></i>
                        @endif
                    </a>

                    <ul class="dropdown-menu dropdown-menu-default">
                        <li class="{{ $locale == 'de' ? 'active' : '' }}">
                            <a href="{{ route('admin.locale', ['locale' => 'de']) }}">
                                <img alt="" src="/vendor/metronic/global/img/flags/de.png"> {{ __('DE') }} </a>
                        </li>

                        <li class="{{ $locale == 'en' ? 'active' : '' }}">
                            <a href="{{ route('admin.locale', ['locale' => 'en']) }}">
                                <img alt="" src="/vendor/metronic/global/img/flags/us.png"> {{ __('EN') }} </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->

                <!-- BEGIN USER LOGIN DROPDOWN -->
                @php $img_avatar = !empty($user->image_src) ?  get_image_url($user->image_src, 'thumb') : null @endphp
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="{{ $img_avatar ? $img_avatar : asset('default_user.png') }}"/>
                        <span class="username">{{ $user ? $user->name : __('Administrator') }} </span><i class="fa fa-angle-down"></i>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="#" id="admin-bar-edit-profile" data-url="{{ route("admin.users.updateProfile") }}">
                                <i class="icon-user"></i> {{ __('My Profile') }}
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->

                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="{{ route('admin.logout') }}" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<div class="clearfix"></div>
<!-- END HEADER -->

<div class="modal fade" id="fomo-modal-update-profile" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="" data-url="{{ route("admin.users.updateProfile") }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">{{ __('Update profile') }}</h4>
                </div>

                <div class="modal-body"></div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline btn-close pull-left" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="submit" class="btn btn-default green pull-right">{{ __('Update') }}</button>
                </div>
                <div class="spinner-loading font-green text-center" style="display: none"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

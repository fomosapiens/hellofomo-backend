<?php
    $user = Auth::user();
    $currentRoute = Route::getCurrentRoute();
    $routeName = $currentRoute->getName();
    list($controller, $action) = explode('@', $currentRoute->getActionName());

    // check is current user has super admin role ?
    $isSuperAdmin = $user && $user->hasRole(acl_get_super_admin_role());

    $menus = [
        [
            'label' => __('Dashboard'),
            'icon'  => 'icon-home',
            'url'   => route('admin.dashboard'),
            'selected'  => $controller == 'App\Http\Controllers\Admin\DashboardController',
            'permission'    => ['admin.dashboard']
        ],

        [
            'label' => __('News'),
            'icon'  => 'fa fa-newspaper-o',
            'url'   => route('admin.news.index'),
            'selected'  => $controller == 'App\Http\Controllers\Admin\NewsController'
                        || $controller == 'App\Http\Controllers\Admin\CategoryController'
                        || $controller == 'App\Http\Controllers\Admin\TagController',
            'permission'   => ['news.read', 'categories.read', 'tags.read'],
            'sub_menus'  => [
                [
                    'label' => __('Overview'),
                    'url'   => route('admin.news.index'),
                    'selected'  => $controller == 'App\Http\Controllers\Admin\NewsController',
                    'permission'    => 'news.read'
                ],
                [
                    'label' => __('Categories'),
                    'url'   => route('admin.categories.index'),
                    'selected'  => $controller == 'App\Http\Controllers\Admin\CategoryController',
                    'permission'    => 'categories.read'
                ],
                [
                    'label' => __('Tags'),
                    'url'   => route('admin.tags.index'),
                    'selected'  => $controller == 'App\Http\Controllers\Admin\TagController',
                    'permission'    => 'tags.read'
                ],
            ]
        ],

        [
            'label' => __('App'),
            'icon'  => 'fa fa-tablet',
            'url'   => 'javascript:;',
            'selected'  => $controller == 'App\Http\Controllers\Admin\PushNotificationController'
                || $controller == 'App\Http\Controllers\Admin\AppLayoutController'
                || $controller == 'App\Http\Controllers\Admin\AppSettingController',
            'sub_menus'  => [
                [
                    'label' => __('CMS'),
                    'url'   => 'javascript:;',
                    'selected'  => $controller == 'App\Http\Controllers\Admin\PushNotificationController'
                        || $controller == 'App\Http\Controllers\Admin\AppLayoutController'
                        || $controller == 'App\Http\Controllers\Admin\AppSettingController',
                    'sub_menus'  => [
                        [
                            'label' => __('Dashboard'),
                            'url'   => route('admin.app.layout.settings', ['key' => 'dashboard']),
                            'selected'  => $controller == 'App\Http\Controllers\Admin\AppLayoutController' && $routeName == 'admin.app.layout.settings' && Request::is('admin/app/layout/settings/dashboard'),
                        ],
                        [
                            'label' => __('News'),
                            'url'   => 'javascript:;',
                            'selected'  => ($controller == 'App\Http\Controllers\Admin\AppLayoutController'
                                && $routeName == 'admin.app.layout.settings'
                                && (Request::is('admin/app/layout/settings/news')
                                    || Request::is('admin/app/layout/settings/category')
                                    || Request::is('admin/app/layout/settings/post'))),
                            'sub_menus' => [
                                [
                                    'label' => __('All'),
                                    'url'   => route('admin.app.layout.settings', ['key' => 'news']),
                                    'selected'  => ($controller == 'App\Http\Controllers\Admin\AppLayoutController'
                                        && $routeName == 'admin.app.layout.settings'
                                        && Request::is('admin/app/layout/settings/news')),
                                ],
                                [
                                    'label' => __('Categories'),
                                    'url'   => route('admin.app.layout.settings', ['key' => 'category']),
                                    'selected'  => ($controller == 'App\Http\Controllers\Admin\AppLayoutController'
                                        && $routeName == 'admin.app.layout.settings'
                                        && Request::is('admin/app/layout/settings/category')),
                                ],
                                [
                                    'label' => __('Posts'),
                                    'url'   => route('admin.app.layout.settings', ['key' => 'post']),
                                    'selected'  => ($controller == 'App\Http\Controllers\Admin\AppLayoutController'
                                        && $routeName == 'admin.app.layout.settings'
                                        && Request::is('admin/app/layout/settings/post')),
                                ],
                            ],

                        ],
                        [
                            'label' => __('Service'),
                            'url'   => 'javascript:;',
                            'selected'  => ($controller == 'App\Http\Controllers\Admin\AppLayoutController'
                                && (Request::is('admin/app/layout/setting/contact')
                                    || Request::is('admin/app/layout/setting/privacy')
                                    || Request::is('admin/app/layout/setting/impressum')
                                    || Request::is('admin/app/layout/setting/agb'))),
                            'sub_menus' => [
                                [
                                    'label' => __('Contact'),
                                    'url'   => route('admin.app.layout.setting', 'contact'),
                                    'selected'  => $controller == 'App\Http\Controllers\Admin\AppLayoutController' && $routeName == 'admin.app.layout.setting' && Request::is('admin/app/layout/setting/contact'),
                                ],
                                [
                                    'label' => __('Privacy'),
                                    'url'   => route('admin.app.layout.setting', 'privacy'),
                                    'selected'  => $controller == 'App\Http\Controllers\Admin\AppLayoutController' && $routeName == 'admin.app.layout.setting' && Request::is('admin/app/layout/setting/privacy'),
                                ],
                                [
                                    'label' => __('AGB'),
                                    'url'   => route('admin.app.layout.setting', 'agb'),
                                    'selected' => $controller == 'App\Http\Controllers\Admin\AppLayoutController' && $routeName == 'admin.app.layout.setting' && Request::is('admin/app/layout/setting/agb'),
                                ],
                                [
                                    'label' => __('Impressum'),
                                    'url'   => route('admin.app.layout.setting', 'impressum'),
                                    'selected'  => $controller == 'App\Http\Controllers\Admin\AppLayoutController' && $routeName == 'admin.app.layout.setting' && Request::is('admin/app/layout/setting/impressum'),
                                ],
                            ]
                        ],
                    ]
                ],
                [
                    'label' => __('Push Notification'),
                    'url'   => route('admin.app.push-notification'),
                    'selected'  => $controller == 'App\Http\Controllers\Admin\PushNotificationController',
                ],
                [
                    'label' => __('Settings'),
                    'url'   => route('admin.app.settings.edit'),
                    'selected'  => $controller == 'App\Http\Controllers\Admin\AppSettingController',
                ],
            ],
        ],

        [
            'label' => __('File Management'),
            'icon'  => 'fa fa-files-o',
            'url'   => route('admin.media.index'),
            'selected'  => $controller == 'App\Http\Controllers\Admin\ComponentController',
            'permission'    => ['files.read'],
        ],

        [
            'label' => __('Master Data'),
            'icon'  => 'fa fa-database',
            'url'   => 'javascript:;',
            'selected'  => $controller == 'App\Http\Controllers\Admin\AuthorController',
            'permission' => ['authors.read'],
            'sub_menus'  => [
                [
                    'label' => __('Author'),
                    'url'   => route('admin.authors.index'),
                    'selected'  => $controller == 'App\Http\Controllers\Admin\AuthorController',
                    'permission'    => 'authors.read'
                ],
            ]
        ],

        [
            'label' => __('System'),
            'icon'  => 'fa fa-cogs',
            'url'   => 'javascript:;',
            'selected' => in_array($controller, [
                'App\Http\Controllers\Admin\UserController',
                'App\Http\Controllers\Admin\SalutationController',
                'App\Http\Controllers\Admin\GeneralSettingController',
                'Elidev\ACL\Controllers\AssignmentController',
                'Elidev\ACL\Controllers\RoleController',
                'Elidev\ACL\Controllers\PermissionController',
                'Elidev\ACL\Controllers\SettingController',
                'Barryvdh\TranslationManager\Controller',
                'App\Http\Controllers\Admin\SecurityController',
                'App\Http\Controllers\Admin\EmailSettingController',
            ]),
            'permission' => ['users.read', 'roles.read', 'salutations.read', 'assignments.read', 'permissions.read', 'security.read'],
            'sub_menus' => [
                [
                    'label' => __('User'),
                    'url'   => 'javascript:;',
                    'selected'  => $controller == 'App\Http\Controllers\Admin\UserController'
                                || $controller == 'Elidev\ACL\Controllers\AssignmentController'
                                || $controller == 'Elidev\ACL\Controllers\RoleController'
                                || $controller == 'Elidev\ACL\Controllers\PermissionController'
                                || $controller == 'Elidev\ACL\Controllers\SettingController',
                    'permission' => ['users.read', 'roles.read', 'assignments.read', 'permissions.read'],
                    'sub_menus'  => [
                        [
                            'label' => __('Overview'),
                            'url'   => route('admin.users.index'),
                            'selected'  => $controller == 'App\Http\Controllers\Admin\UserController',
                            'permission' => 'users.read'
                        ],
                        [
                            'label' => __('System Roles'),
                            'url'   => route('acl.assignments.index'),
                            'selected'  => $controller == 'Elidev\ACL\Controllers\AssignmentController'
                                        || $controller == 'Elidev\ACL\Controllers\RoleController'
                                        || $controller == 'Elidev\ACL\Controllers\PermissionController'
                                        || $controller == 'Elidev\ACL\Controllers\SettingController',
                            'permission'    => ['assignments.read', 'roles.read', 'permissions.read']
                        ],
                    ]
                ],
                [
                    'label' => __('Master Data'),
                    'url'   => 'javascript:;',
                    'permission' => 'salutations.read',
                    'selected'  => $controller == 'App\Http\Controllers\Admin\SalutationController',
                    'sub_menus'  => [
                        [
                            'label' => __('Salutations & Titles'),
                            'url'   => route('admin.salutations.index'),
                            'permission'    => 'salutations.read',
                            'selected'  => $controller == 'App\Http\Controllers\Admin\SalutationController',
                        ],
                    ],
                ],
                [
                    'label' => __('Settings'),
                    'url'   => 'javascript:;',
                    'selected'  => $controller == 'App\Http\Controllers\Admin\GeneralSettingController'
                                    || $controller == 'Barryvdh\TranslationManager\Controller'
                                    || $controller == 'App\Http\Controllers\Admin\SecurityController'
                                    || $controller == 'App\Http\Controllers\Admin\EmailSettingController',
                    'sub_menus' => [
                        [
                            'label' => __('General'),
                            'url'   => route('admin.settings.edit'),
                            'selected'  => $controller == 'App\Http\Controllers\Admin\GeneralSettingController',
                        ],
                        [
                            'label' => __('E-Mail'),
                            'url'   => route('admin.settings.email.edit'),
                            'selected'  => $controller == 'App\Http\Controllers\Admin\EmailSettingController',
                        ],
                        [
                            'label' => __('Translation'),
                            'url'   => '/'. config('translation-manager.route.prefix'),
                            'selected'  => $controller == 'Barryvdh\TranslationManager\Controller',
                        ],
                        [
                            'label' => __('Security'),
                            'permission' => 'security.read',
                            'url'   => route('admin.security.read'),
                            'selected'  => $controller == 'App\Http\Controllers\Admin\SecurityController',
                        ],
                    ]
                ],
            ]
        ],
    ];

    $n = count($menus);
?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper" data-auto-scroll="false" data-auto-speed="200">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            @foreach($menus as $i => $menu)
                <?php
                    $startClass = $i == 0 ? 'start' : 'nav-item';
                    $endClass   = $i == $n - 1 ? 'last' : '';
                    $active     = isset($menu['selected']) && $menu['selected'] ? 'active' : '';

                    $authorizedArr = [];
                    if (!$isSuperAdmin && !empty($menu['permission'])) {
                        foreach($menu['permission'] as $permission) {
                            if (acl_is_free_permission($permission)) {
                                $authorizedArr[$permission] = true;
                            }
                            else {
                                try {
                                    if ($permissions && in_array($permission, $permissions)) {
                                        $authorizedArr[$permission] = true;
                                    }
                                }
                                catch(\Exception $e) {}
                            }
                        }
                    }
                ?>

                @if ($isSuperAdmin || !empty($authorizedArr))
                    <li class="{{ $startClass }} {{ $endClass }} {{ $active }}">
                        <a href="{{ $menu['url'] }}" class="nav-link nav-toggle">
                            @if (isset($menu['icon'])) <i class="{{  $menu['icon'] }}"></i> @endif
                            <span class="title">{{ $menu['label'] }}</span>
                            @if (!empty($menu['sub_menus']))
                                <span class="arrow @if (isset($menu['selected']) && $menu['selected']) open @endif"></span>
                            @endif
                        </a>

                        {{-- Render sub menu level 2 --}}
                        @if ( !empty($menu['sub_menus']) )
                            <ul class="sub-menu">
                                @foreach($menu['sub_menus'] as $sub_menu)
                                    <?php
                                        $subActive = isset($sub_menu['selected']) && $sub_menu['selected'] ? 'active' : '';
                                        $mnPermission = isset($sub_menu['permission']) ? $sub_menu['permission'] : '';
                                    ?>

                                    @if ($isSuperAdmin
                                        ||
                                        ($mnPermission && is_array($authorizedArr) && is_string($mnPermission) && isset($authorizedArr[$mnPermission]))
                                        ||
                                        (is_array($mnPermission))
                                    )
                                        <li class="nav-item {{ $subActive }}">
                                            <a href="{{ $sub_menu['url'] }}" @if ( !empty($sub_menu['sub_menus']) ) class="nav-link nav-toggle" @endif>
                                                @if(isset($sub_menu['icon'])) <i class="{{ $sub_menu['icon'] }}"></i> @endif
                                                <span class="title">{{ $sub_menu['label'] }}</span>
                                                @if (!empty($sub_menu['sub_menus']))
                                                    <span class="arrow @if ( $subActive ) open @endif"></span>
                                                @endif
                                            </a>

                                            {{-- Render sub menu level 3 --}}
                                            @if (!empty($sub_menu['sub_menus']))
                                                <ul class="sub-menu">
                                                    @foreach($sub_menu['sub_menus'] as $sub_menu_level3)
                                                        <?php
                                                            $subActiveLevel3 = isset($sub_menu_level3['selected']) && $sub_menu_level3['selected'] ? 'active' : '';
                                                            $mnPermissionLevel3 = isset($sub_menu_level3['permission']) ? $sub_menu_level3['permission'] : '';

                                                            // check is allow display sub menu item 3
                                                            $allowDisplay = false;
                                                            if ( is_array($authorizedArr) && $mnPermissionLevel3 ) {
                                                                if ( is_string($mnPermissionLevel3) && isset($authorizedArr[$mnPermissionLevel3]) ) {
                                                                    $allowDisplay = true;
                                                                } else if ( is_array($mnPermissionLevel3) ) {
                                                                    foreach ($mnPermissionLevel3 as $subItemPermission) {
                                                                        if ( isset($authorizedArr[$subItemPermission]) && $authorizedArr[$subItemPermission] ) {
                                                                            $allowDisplay = true;

                                                                            // get current active role url
                                                                            switch($subItemPermission) {
                                                                                case 'assignments.read':
                                                                                    $sub_menu_level3['url'] = route('acl.assignments.index');
                                                                                    break;
                                                                                case 'roles.read':
                                                                                    $sub_menu_level3['url'] = route('acl.roles.index');
                                                                                    break;
                                                                                case 'permissions.read':
                                                                                    $sub_menu_level3['url'] = route('acl.permissions.index');
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                        @if ($isSuperAdmin || $allowDisplay)
                                                            <li class="nav-item {{ $subActiveLevel3 }}">
                                                                <a href="{{ $sub_menu_level3['url'] }} " @if ( !empty($sub_menu_level3['sub_menus']) ) class="nav-link nav-toggle" @endif>
                                                                    <i class="{{ isset($sub_menu_level3['icon']) ? $sub_menu_level3['icon'] : '' }}"></i>
                                                                    <span class="title">{{ $sub_menu_level3['label'] }}</span>
                                                                    @if (!empty($sub_menu_level3['sub_menus']))
                                                                        <span class="arrow @if ( $subActiveLevel3 ) open @endif"></span>
                                                                    @endif
                                                                </a>
                                                                {{-- Render sub menu level 3 --}}
                                                                @if (!empty($sub_menu_level3['sub_menus']))
                                                                    <ul class="sub-menu">
                                                                        @foreach($sub_menu_level3['sub_menus'] as $sub_menu_level4)
                                                                        <?php
                                                                            if (isset($sub_menu_level4['selected']) && $sub_menu_level4['selected']){
                                                                                $subActiveLevel4 = 'active';
                                                                            } else {
                                                                                $subActiveLevel4 = '';
                                                                            }
                                                                            if (isset($sub_menu_level4['icon'])) {
                                                                                $subIcon4 = $sub_menu_level4['icon'];
                                                                            } else {
                                                                                $subIcon4 = '';
                                                                            }
                                                                            if (isset($sub_menu_level4['permission'])) {
                                                                                $mnPermissionLevel4 = $sub_menu_level4['permission'];
                                                                            } else {
                                                                                $mnPermissionLevel4 = '';
                                                                            }
                                                                            // check is allow display sub menu item 4
                                                                            $allowDisplay4 = false;
                                                                            if ( is_array($authorizedArr) && $mnPermissionLevel4 ) {
                                                                                if ( is_string($mnPermissionLevel4) && isset($authorizedArr[$mnPermissionLevel4]) ) {
                                                                                    $allowDisplay4 = true;
                                                                                } else if ( is_array($mnPermissionLevel4) ) {
                                                                                    foreach ($mnPermissionLevel4 as $subItemPermission4) {
                                                                                        if ( isset($authorizedArr[$subItemPermission4]) && $authorizedArr[$subItemPermission4] ) {
                                                                                            $allowDisplay4 = true;

                                                                                            // get current active role url
                                                                                            switch($subItemPermission4) {
                                                                                                case 'assignments.read':
                                                                                                    $sub_menu_level4['url'] = route('acl.assignments.index');
                                                                                                    break;
                                                                                                case 'roles.read':
                                                                                                    $sub_menu_level4['url'] = route('acl.roles.index');
                                                                                                    break;
                                                                                                case 'permissions.read':
                                                                                                    $sub_menu_level4['url'] = route('acl.permissions.index');
                                                                                                    break;
                                                                                            }
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        ?>
                                                                            @if ($isSuperAdmin || $allowDisplay4)
                                                                                <li class="nav-item {{$subActiveLevel4}}">
                                                                                    <a href="{{ $sub_menu_level4['url'] }}">
                                                                                        <i class="{{ $subIcon4}}"></i>
                                                                                        <span class="title">{{ $sub_menu_level4['label'] }}</span>
                                                                                        @if (!empty($sub_menu_level4['sub_menus']))
                                                                                            <span class="arrow @if ( $subActiveLevel4 ) open @endif"></span>
                                                                                        @endif
                                                                                    </a>
                                                                                </li>
                                                                            @endif
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endif
            @endforeach
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->

<?php
$websiteTitlePrefix = Settings::get(config('fomo.settings.website_title_prefix'), '');
if (empty($websiteTitlePrefix)) {
    $websiteTitlePrefix = config('app.name', '');
}
?>
<title>{{ $websiteTitlePrefix }} - @yield('title', __('Admin Panel'))</title>

<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Dynamic favicon -->
@php $faviconId = Settings::get(config('fomo.settings.favicon')) @endphp
<link rel="shortcut icon" href="{{ !empty($faviconId) ? rv_media_get_image_url_from_id($faviconId) :  'favicon.ico' }}" />

<!-- Plugin styles -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Work+Sans|Titillium+Web:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="/vendor/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/media/packages/fancybox/dist/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="/vendor/media/packages/focuspoint/css/focuspoint.css" rel="stylesheet" type="text/css" />

<!-- Elidev styles -->
<link href="/vendor/ajax-datatable/elidev.metronic.ajax.datatable.css" rel="stylesheet" type="text/css" id="style_color" />

<!-- Metronic styles -->
<link href="/vendor/metronic/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="/vendor/metronic/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/metronic/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" id="style_color" type="text/css" />
<link href="/vendor/metronic/internal/css/app.css" rel="stylesheet" type="text/css" />

<!-- Custom styles -->
@stack('styles')

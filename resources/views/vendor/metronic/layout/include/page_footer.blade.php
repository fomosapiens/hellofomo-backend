<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        &copy; {{ date('Y') }} {{ config('metronic.footer.message') }}
    </div>
</div>
<!-- END FOOTER -->

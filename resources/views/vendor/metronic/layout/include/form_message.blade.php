@if ( $message = Session::get('error') )
    <div style="display: none;" id="fomo-alert-error">{{ $message }}</div>
@elseif ( $message = Session::get('success') )
    <div style="display: none" id="fomo-alert-success">{{ $message }}</div>
@endif

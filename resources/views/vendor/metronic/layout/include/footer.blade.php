<!-- Plugin scripts -->
<script src="/vendor/metronic/global/plugins/respond.min.js"></script>
<script src="/vendor/metronic/global/plugins/ie8.fix.min.js"></script>
<script src="/vendor/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.de.js" charset="UTF-8" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-toastr/toastr.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/sortable/Sortable.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
@if (Session::has('locale') && Session::get('locale') != 'en')
<script src="/vendor/metronic/global/plugins/jquery-validation/js/localization/messages_{{ Session::get('locale') }}.min.js" type="text/javascript"></script>
@endif
<script src="/vendor/metronic/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/vendor/metronic/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/global/scripts/jquery.more.js" type="text/javascript"></script>
<script src="/vendor/media/packages/fancybox/dist/jquery.fancybox.js" type="text/javascript"></script>
<script src="/vendor/media/packages/focuspoint/js/jquery.focuspoint.min.js" type="text/javascript"></script>

<!-- Elidev scripts -->
<script src="/vendor/ajax-datatable/elidev.metronic.ajax.datatable.js"></script>
<script src="/vendor/acl/js/acl.js" type="text/javascript"></script>

<!-- Metronic scripts -->
<script src="/vendor/metronic/global/scripts/app.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/vendor/metronic/internal/js/app.js" type="text/javascript"></script>

<script type="application/javascript">
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    var locale     = '{{ session('locale', config('app.locale')) }}',
        base_url   = '{{ config('app.url') }}',
        alertTitle = "{{ __("Are you sure?") }}",
        alertText  = "{{ __("Yes, delete it!") }}";
</script>

@push('footer')
    @include('media::partials.media')
@endpush

<!-- Custom scripts -->
@stack('scripts')

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
    @include('metronic::layout.include.header')

    @stack('header')
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        @include('metronic::layout.include.page_header')

        <div class="page-container">
            @include('metronic::layout.include.left_sidebar')

            <div class="page-content-wrapper">
                <div class="page-content" style="min-height: calc(100vh - 85px);">
                    @include('metronic::layout.include.breadcrumb')

                    <h1 class="page-title">@yield('main_title')</h1>
                    @include('metronic::layout.include.form_message')

                    @yield('content')
                </div>
            </div>
        </div>

        @include('metronic::layout.include.page_footer')
    </div>

    @include('metronic::layout.include.footer')

    @stack('footer')

</body>
<!-- END BODY -->

</html>

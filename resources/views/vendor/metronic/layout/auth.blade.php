<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
    @php
    $websiteTitlePrefix = Settings::get(config('fomo.settings.website_title_prefix'), '');
    if (empty($websiteTitlePrefix)) {
      $websiteTitlePrefix = config('app.name', '');
    }
    @endphp
    <title>{{ $websiteTitlePrefix }} - @yield('title', __('Authentication'))</title>
    @include('metronic::layout.include.header')

    <link href="{{ asset("vendor/metronic/pages/css/login.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("vendor/metronic/internal/css/login.css") }}" rel="stylesheet" type="text/css" />

    <!-- Dynamic favicon -->
    @php $faviconId = Settings::get(config('fomo.settings.favicon')) @endphp
    <link rel="shortcut icon" href="{{ !empty($faviconId) ? rv_media_get_image_url_from_id($faviconId) :  'favicon.ico' }}" />
</head>
<!-- END HEAD -->

<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{ route('admin-show-login') }}">
            @php $logoId = Settings::get(config('fomo.settings.image_logo')) @endphp
            <img src="{{ $logoId ? rv_media_get_image_url_from_id($logoId) :  asset('fomo_logo.png') }}" alt="fomo-logo" class="logo-default"/>
        </a>
    </div>
    <!-- END LOGO -->

    <!-- BEGIN LOGIN -->
    @yield('content')
    <!-- END LOGIN -->

    <div class="copyright">&copy; {{ date('Y') }}  {{ config('metronic.footer.message') }}</div>

    @include('metronic::layout.include.footer')
</body>

</html>

<?php
    $breadcrumbs = [
        [ 'text' => __('Translation Manager') ]
    ];

    $viewGroupURL = config('translation-manager.route.prefix') . '/view/*';
    if (request()->is($viewGroupURL)) {
        $groupName = str_after(request()->url(), 'view/');
        $groupName = str_replace('/', '_', $groupName);

        $breadcrumbs = [
            [
                'text' => __('Translation Manager'),
                'url' => config('app.url') . '/' . config('translation-manager.route.prefix')
            ],
            [
                'text' => $groupName
            ],
        ];
    }

    // laravel-translation-manager package auto set the locale.
    // In the array of locales [de, en], "de" locale always before "en" locale by alphabetical order.
    // So, we re-set the locale by our current locale.
    App::setLocale(Session::get('locale'));
?>

@extends(config('metronic.layout.root'))

@section('title', __('Translation Manager'))

@push('header')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered no-padding-bottom">
                <div class="portlet-title">
                    <div class="caption font-green">{{ __('Translation') }}</div>
                    <div class="actions">
                        @include('admin.components.full-screen')
                    </div>
                </div>

                <div class="portlet-body">
                    @include('vendor.translation-manager.includes.content')
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="/vendor/translation/js/global.js"></script>

    <script>
        jQuery(document).ready(function($){

            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    console.log('beforesend');
                    settings.data += "&_token=<?= csrf_token() ?>";
                }
            });

            $('.editable').editable().on('hidden', function(e, reason){
                var locale = $(this).data('locale');
                if(reason === 'save'){
                    $(this).removeClass('status-0').addClass('status-1');
                }
                if(reason === 'save' || reason === 'nochange') {
                    var $next = $(this).closest('tr').next().find('.editable.locale-'+locale);
                    setTimeout(function() {
                        $next.editable('show');
                    }, 300);
                }
            });

            $('.group-select').on('change', function(){
                var group = $(this).val();
                if (group) {
                    window.location.href = '<?= action('\Barryvdh\TranslationManager\Controller@getView') ?>/'+$(this).val();
                } else {
                    window.location.href = '<?= action('\Barryvdh\TranslationManager\Controller@getIndex') ?>';
                }
            });

            $("a.delete-key").click(function(event){
                event.preventDefault();

                var answer = confirm($(this).attr('data-confirm-message'));
                if (answer) {
                    var row = $(this).closest('tr');
                    var url = $(this).attr('href');
                    var id = row.attr('id');
                    $.post(url, {id: id}, function() {
                        row.remove();
                    });
                }
            });

            $('.form-import').on('ajax:success', function (e, data) {
                $('div.success-import strong.counter').text(data.counter);
                toastr.success($('div.success-import').text())
            });

            $('.form-find').on('ajax:success', function (e, data) {
                $('div.success-find strong.counter').text(data.counter);
                toastr.success($('div.success-find').text())
            });

            $('.form-publish').on('ajax:success', function (e, data) {

                var ajaxExportTranslationUrl = $('.translation-table').attr('data-export-translation-route'),
                    isSelectExisted          = ( $('select[name="group"]').length && $('select[name="group"] option:selected').val() == '_json' ),
                    isAjaxUrlValid           = ( typeof ajaxExportTranslationUrl != 'undefined' && ajaxExportTranslationUrl != '' );

                // call artisan export command
                if ( isSelectExisted && isAjaxUrlValid ) {
                    $.ajax({
                        url: ajaxExportTranslationUrl,
                        type: 'POST',
                        data: {},
                        beforeSend: function () {
                            $.blockUI(FOMO.blockMetronicUI);
                        },
                        success: function( response ) {
                            $.unblockUI();
                            $('div.success-publish').slideDown();
                            setTimeout(function() {
                                $('div.success-publish').slideUp();
                            }, 2000);
                        },
                        error: function(jqXHR, exception) {
                            $.unblockUI();
                            if ( jqXHR.status === 400 ) {
                                toastr.error(jqXHR.responseJSON.message);
                            }
                        }
                    });
                }
            });

            // paging the table by datatables plugin
            if ( $('.translation-table').length ) {
                var totalColumns = $(".translation-table > tbody > tr:first > td").length,
                    currentLocaleFile = $('.translation-table').attr('data-locale-file');

                $('.translation-table').DataTable({
                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ totalColumns - 1 ] } // un-sort the last column (Action column)
                    ],
                    "language": {
                        "url": currentLocaleFile
                    }
                });
            }

        })
    </script>
@endpush

<div class="alert alert-success success-import" style="display:none;">
    <p>{{ __("Done importing! Reload this page to refresh the groups!") }}</p>
</div>
<div class="alert alert-success success-find" style="display:none;">
    <p>{{ __("Done searching for translations") }}</p>
</div>
<div class="alert alert-success success-publish" style="display:none;">
    <p>{{ sprintf(__("Done publishing the translations for group %s"), $group) }}</p>
</div>
<?php if(Session::has('successPublish')) : ?>
<div class="alert alert-info">
    <?php echo Session::get('successPublish'); ?>
</div>
<?php endif; ?>
<p>
<?php if(!isset($group)) : ?>
<form class="form-inline form-import" method="POST" action="<?= action('\Barryvdh\TranslationManager\Controller@postImport') ?>" data-remote="true" role="form">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <select name="replace" class="form-control">
        <option value="0">{{ __("Append new translations") }}</option>
        <option value="1">{{ __("Replace existing translations") }}</option>
    </select>
    <button type="submit" class="btn btn-success"  data-disable-with="{{ __("Loading ...") }}">{{ __('Import groups') }}</button>
</form>
<form class="form-inline form-find" method="POST" action="<?= action('\Barryvdh\TranslationManager\Controller@postFind') ?>" data-remote="true" role="form" data-confirm="{{ __("Are you sure you want to scan you app folder? All found translation keys will be added to the database.") }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <p></p>
    <button type="submit" class="btn btn-info" data-disable-with="{{ __("Searching ...") }}" >{{ __("Find translations in files") }}</button>
</form>
<?php endif; ?>
<?php if(isset($group)) : ?>
<form class="form-inline form-publish" method="POST" action="<?= action('\Barryvdh\TranslationManager\Controller@postPublish', $group) ?>" data-remote="true" role="form" data-confirm="Are you sure you want to publish the translations group '<?= $group ?>? This will overwrite existing language files.">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <button type="submit" class="btn btn-info" data-disable-with="{{ __("Publishing ...") }}" >{{ __("Publish translations") }}</button>
    <a href="<?= action('\Barryvdh\TranslationManager\Controller@getIndex') ?>" class="btn btn-default">{{ __("Back") }}</a>
</form>
<?php endif; ?>
</p>
<form role="form">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="form-group">
        <select name="group" id="group" class="form-control group-select">
            <?php foreach($groups as $key => $value): ?>
            <option value="<?= $key ?>"<?= $key == $group ? ' selected':'' ?>><?= $value ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</form>


<?php if($group): ?>
<form action="<?= action('\Barryvdh\TranslationManager\Controller@postAdd', array($group)) ?>" method="POST"  role="form" class="form-validation">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="form-group">
        <textarea class="form-control" rows="3" name="keys" placeholder="{{ __("Add 1 key per line, without the group prefix") }}" required data-error-container="#key-error-message"></textarea>
        <span class="help-block" id="key-error-message"></span>
    </div>

    <p></p>
    <input type="submit" value="Add keys" class="btn btn-primary">
</form>
<hr>

@php
    // remove "vendor" column
    if (($key = array_search('vendor', $locales)) !== false) {
        unset($locales[$key]);
    }

    // if current locale is Germany, render datatables plugin by using Germany language
    $localeFile = '';
    if ( Session::get('locale') == 'de' ) {
        $localeFile = asset('datatables_german_language.json');
    }
@endphp

<table class="table translation-table" data-export-translation-route="{{ route('admin.export.translation') }}" data-locale-file="{{ $localeFile }}">
    <thead>
    <tr>
        <th width="15%">{{ __('Key') }}</th>
        <?php foreach($locales as $locale): ?>
        <th><?= $locale ?></th>
        <?php endforeach; ?>
        <?php if($deleteEnabled): ?>
        <th>&nbsp;</th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>

    <?php foreach($translations as $key => $translation): ?>
    <tr id="<?= $key ?>">
        <td><?= $key ?></td>
        <?php foreach($locales as $locale): ?>
        <?php $t = isset($translation[$locale]) ? $translation[$locale] : null?>

        <td>
            <a href="#edit" class="editable status-<?= $t ? $t->status : 0 ?> locale-<?= $locale ?>" data-locale="<?= $locale ?>" data-name="<?= $locale . "|" . $key ?>" id="username" data-type="textarea" data-pk="<?= $t ? $t->id : 0 ?>" data-url="<?= $editUrl ?>" data-title="{{ __('Enter translation') }}"><?= $t ? htmlentities($t->value, ENT_QUOTES, 'UTF-8', false) : '' ?></a>
        </td>
        <?php endforeach; ?>
        <?php if($deleteEnabled): ?>
        <td>
            <a href="<?= action('\Barryvdh\TranslationManager\Controller@postDelete', [$group, $key]) ?>" class="delete-key" data-confirm-message="{{ sprintf(__("Are you sure you want to delete the translations for %s"), $key) }}"><span class="glyphicon glyphicon-trash"></span></a>
        </td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>

    </tbody>
</table>
<?php else: ?>
<p>{{ __("Choose a group to display the group translations.") }}</p>

<?php endif; ?>

@extends(config('metronic.layout.root'))

@section('title', __('Create new author'))

@section('content')
    @include('admin.author.form', [
        'title'         => __('Create new author'),
        'route'         => route('admin.authors.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection

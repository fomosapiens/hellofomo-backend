<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <form role="form" action="{{ empty($route) ? '' : $route }}" method="POST" class="form-horizontal form-validation submit-frm" enctype="multipart/form-data">
                <div class="col-md-9">
                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ empty($title) ? __('Form Author') : $title }}</div>
                            @include('admin.components.portlet-tools')
                            <div class="actions">
                                @include('admin.components.full-screen')
                            </div>
                        </div>

                        <div class="portlet-body form">
                                {{ csrf_field() }}

                                @if ( isset($author) )
                                    {{ method_field('PUT') }}
                                @endif

                                <div class="form-body">
                                    <div class="form-group {{ $errors->has('salutation_id') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label label-required" for="salutation_id">{{ __('Salutation') }}</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="salutation_id" id="salutation_id" required>
                                                <option value="" selected disabled>{{ __('Please choose') }}</option>
                                                @foreach($salutations as $key => $salutation)
                                                    <option @if ( isset($author) && $author->salutation_id == $key ) selected @endif value="{{ $key }}">{{ $salutation }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('salutation_id') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('title_id') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label" for="title_id">{{ __('Title') }}</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="title_id" id="title_id">
                                                <option value="">{{ __('Please choose') }}</option>
                                                @foreach($titles as $key => $title)
                                                    <option @if ( isset($author) && $author->title_id == $key ) selected @endif value="{{ $key }}">{{ $title }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('title_id') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group  {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label label-required" for="first_name">{{ __('First name') }}</label>
                                        <div class="col-md-10">
                                            <input type="text"
                                                   id="first_name"
                                                   name="first_name"
                                                   class="form-control"
                                                   value="{{ old('first_name', isset($author->first_name) ? $author->first_name : '') }}"
                                                   required>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('first_name') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group  {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label label-required" for="last_name">{{ __('Last name') }}</label>
                                        <div class="col-md-10">
                                            <input type="text"
                                                   id="last_name"
                                                   name="last_name"
                                                   class="form-control"
                                                   value="{{ old('last_name', isset($author->last_name) ? $author->last_name : '') }}"
                                                   required>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('last_name') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group  {{ $errors->has('display_name') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label label-required" for="display_name">{{ __('Display name') }}</label>
                                        <div class="col-md-10">
                                            <input type="text"
                                                   id="display_name"
                                                   name="display_name"
                                                   class="form-control"
                                                   value="{{ old('display_name', isset($author->display_name) ? $author->display_name : '') }}"
                                                   required>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('display_name') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group  {{ $errors->has('callback_url') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label " for="callback_url">{{ __('Callback URL') }}</label>
                                        <div class="col-md-10">
                                            <input type="text"
                                                   id="callback_url"
                                                   name="callback_url"
                                                   class="form-control"
                                                   value="{{ old('callback_url', isset($author->callback_url) ? $author->callback_url : '') }}"
                                                   >
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('callback_url') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group  {{ $errors->has('description') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label" for="description">{{ __('Description') }}</label>
                                        <div class="col-md-10">
                                            @include('admin.components.addMedia')
                                            <textarea name="description"
                                                      id="description"
                                                      class="summernote form-control"
                                                      data-editor="summernote"
                                                      rows="15">{{ old('description', isset($author->description) ? $author->description : '') }}</textarea>
                                            <span class="help-block">{{ $errors->first('description') }}</span>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    @include('admin.components.image-box', ['title' => __('Primary image'), 'data' => isset($author) ? $author : ''])
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('footer')
    @include('media::partials.media')
@endpush

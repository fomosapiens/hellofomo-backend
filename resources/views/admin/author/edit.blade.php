@extends(config('metronic.layout.root'))

@section('title', __('Authors Edit'))

@section('action_buttons')
    <div class="clearfix">
        <div class="btn-group pull-right">
            <a href="{{ route('admin.authors.show', [ 'author' => $author->id ]) }}" class="btn btn-default">{{ __('Show detail') }}</a>

            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-angle-down"></i>
            </button>

            <ul class="dropdown-menu pull-left" role="menu">
                <li>
                    <a href="{{ route('admin.authors.trash', [ 'author' => $author->id ]) }}">{{ __('Trash') }}</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    @include('admin.author.form', [
        'title'         => __('Edit author'),
        'route'         => route('admin.authors.update', ['author' => $author->id]),
        'btnSubmitText' => __('Save Changes'),
    ])
@endsection

@extends(config('metronic.layout.root'))

@section('title', __('Authors Overview'))
@section('main_title', __('Authors'))

@push('styles')
    <link href="{{ asset('/vendor/metronic/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    @include('ajaxdatatable::datatable')
    @include('admin.author.includes.modal-sorting-list')
@endsection

@push('scripts')
    <script src="{{ asset('/vendor/metronic/global/plugins/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
@endpush

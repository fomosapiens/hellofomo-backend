<div class="modal fade" id="fomo-modal-update-sorting" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="" data-url="{{ route('admin.authors.updateSorting') }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title font-green">{{ __('Sorting Authors') }}</h4>
                </div>

                <div class="modal-body">
                    <div class="dd nestable">
                        <ol class="dd-list">
                        </ol>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-default green pull-right">{{ __('Update') }}</button>
                </div>
                <div class="spinner-loading font-green text-center" style="display: none"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@extends(config('metronic.layout.root'))

@php $title = __('Push notification'); @endphp

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="{{ route('admin.app.send.push-notification') }}" method="POST" class="form-horizontal form-validation submit-frm">
                        {{ csrf_field() }}
                        <input type="hidden" name="active_tab" value="news">

                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ empty($title) ? __('Push notification') : $title }}</div>
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#push-tab-news" data-toggle="tab"> {{ __('News') }} </a>
                                            </li>
                                            <li>
                                                <a href="#push-tab-custom" data-toggle="tab"> {{ __('Custom text') }} </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="push-tab-news">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has('related_news') ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="related_news">{{ __('Search news') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="related_news[]"
                                                                id="related_news[]"
                                                                class="js-news-data-ajax form-control select2"
                                                                data-ajax-src="{{ route('admin.news.search') }}"
                                                                data-news-detail-url="{{ route('admin.news.edit', ['news' => -1]) }}"
                                                                data-placeholder="{{ __('Search by news title') }}">
                                                        </select>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first('related_news') }}</span>

                                                        <br/>
                                                        @include('admin.app.includes.push_btn')
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="push-tab-custom">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has('custom_text') ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="custom_text">{{ __('Enter custom text to push') }}</label>

                                                    <div class="col-md-6">
                                                        <textarea maxlength="{{ !empty($maxLengthOfCustomText) ? $maxLengthOfCustomText : 1024 }}"
                                                                  class="form-control"
                                                                  name="custom_text"
                                                                  id="fomo-push-custom-text"
                                                                  rows="10"></textarea>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first('custom_text') }}</span>

                                                        <br/>
                                                        @include('admin.app.includes.push_btn')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

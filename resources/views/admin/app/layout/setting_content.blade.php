@extends(config('metronic.layout.root'))

@section('title', __(isset($settingLabel) ? $settingLabel : ''))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="" method="POST" class="form-horizontal form-validation submit-frm" enctype="multipart/form-data">
                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ isset($settingLabel) ? $settingLabel : '' }}</div>
                                    @include('admin.components.portlet-tools')
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    {{ csrf_field() }}

                                    @if ( isset($author) )
                                        {{ method_field('PUT') }}
                                    @endif

                                    <div class="form-body">
                                        <div class="form-group  {{ $errors->has('description') ? 'has-error' : '' }}">
                                            <div class="col-md-12">
                                                @include('admin.components.addMedia', ['mediaDestination' => "#app-layout-contact-value"])
                                                <textarea name="value"
                                                          id="app-layout-contact-value"
                                                          class="summernote form-control"
                                                          data-editor="summernote"
                                                          rows="15">{{ old('value', isset($settingValue) ? $settingValue : '') }}</textarea>
                                                <span class="help-block">{{ $errors->first('value') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

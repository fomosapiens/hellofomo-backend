@extends(config('metronic.layout.root'))

@section('title', $title)

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ $title }}</div>
                            <div class="actions">
                                <button class="btn btn-icon-only btn-default fullscreen" data-original-title="Vollbild" title="{{ __('Full screen') }}"></button>
                            </div>
                        </div>

                        <div class="portlet-body form">
                                <form action="{{ route('admin.categories.updateSorting') }}" method="POST" id="fomo-update-post-setting-display">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="portlet light bordered no-padding-bottom">
                                            <div class="portlet-title">
                                                <div class="caption font-green">{{ __('Sort Order') }}</div>
                                                @include('admin.components.portlet-tools')
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="portlet-body form publish-group">
                                                        <div class="dd nestable">
                                                            <ol class="dd-list fomo-post-sorting-list">
                                                                @if(!empty($layoutSettings))
                                                                    @foreach($layoutSettings as $key => $value)
                                                                        @php
                                                                            $disabledClass = $value ? '' : 'disabled';
                                                                            $optionText = $defaultFields[$key]
                                                                        @endphp
                                                                        @include('admin.app.includes.sorting_item')
                                                                    @endforeach
                                                                @endif
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
<link href="{{ asset('/vendor/metronic/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script src="{{ asset('/vendor/metronic/global/plugins/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
@endpush
<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('New Section') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body form publish-group">
        @include('admin.app.includes.new_section_detail')
    </div>
</div>

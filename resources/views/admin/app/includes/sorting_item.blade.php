<li class="dd-item dd3-item {{ $disabledClass }}" data-id="{{ $key }}">
    <div class="dd-handle dd3-handle"></div>
    <div class="dd3-content">
        <div class="form-group">
            <div class="input-checkbox">
                <div class="md-checkbox">
                    <input type="checkbox"
                           id="{{ $key }}"
                           name="{{ $key }}"
                           class="md-check test-checked-on-sorting-list"
                            {{ (isset($layoutSettings[$key]) && $layoutSettings[$key]) ? 'checked' : '' }}
                    >
                    <label for="{{ $key }}">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span></label>
                </div>
            </div>
            <label for="{{ $key }}">{{ $optionText }}</label>
        </div>
    </div>
</li>
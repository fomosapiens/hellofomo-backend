@php
    // default data type is display Category
    $isCategoryType = true;
    $isTagType      = false;
    $isAllType      = false;
    $isTopNewsType  = false;
    if ( !empty($isCheckDataType) && $isCheckDataType ) {
        $isCategoryType = !empty($setting) && $setting['type'] == 'category';
        $isTagType      = !empty($setting) && $setting['type'] == 'tag';
        $isAllType      = !empty($setting) && $setting['type'] == 'all';
        $isTopNewsType  = !empty($setting) && $setting['type'] == 'top_news';
    }
@endphp

<div class="box-section-wrap form-horizontal" id="new-section-box{{ isset($index) && $index >= 0 ? $index : '' }}">
    <div class="col-xs-12">
        <div class="form-body">
            <div class="form-group-wrap">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="input-section-description">{{ __('Section description') }}</label>
                    <div class="col-md-9">
                        <input type="text"
                               name="section_description[]"
                               id="input-section-description"
                               class="form-control"
                               value="{{ !empty($setting) && !empty($setting['section_description']) ? $setting['section_description'] : '' }}">
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group-wrap">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="input-section-type">{{ __('Section Type') }}</label>
                    <div class="col-md-9">
                        <select class="form-control" name="section_type[]" id="input-section-type">
                            <option value="news">{{ __('News') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group-wrap">
                <div class="form-group ">
                    <label class="col-md-3 control-label " for="title"></label>
                    <div class="col-md-9 bold">{{ __('Settings') }}</div>
                </div>

                <div class="form-group ">
                    <label class="col-md-3 control-label" for="input-layout">{{ __('Layout') }}</label>
                    <div class="col-md-9">
                        <select class="form-control" name="layout[]" id="input-layout">
                            <option value="list" @if ( !empty($setting) && $setting['layout'] == 'list') selected @endif>{{ __('List') }}</option>
                            <option value="single" @if ( !empty($setting) && $setting['layout'] == 'single') selected @endif>{{ __('Single') }}</option>
                            <option value="slider" @if ( !empty($setting) && $setting['layout'] == 'slider') selected @endif>{{ __('Slider') }}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-md-3 control-label" for="input-show-picture">{{ __('Show Picture') }}</label>
                    <div class="col-md-9">
                        <select class="form-control" name="show_picture[]" id="input-show-picture">
                            <option value="1" @if ( !empty($setting) && $setting['show_picture'] == 1) selected @endif>{{ __('Yes') }}</option>
                            <option value="0" @if ( !empty($setting) && $setting['show_picture'] == 0) selected @endif>{{ __('No') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group-wrap">
                <div class="form-group ">
                    <label class="col-md-3 control-label" for="title"></label>
                    <div class="col-md-9 bold">{{ __('Data & Filter') }}</div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="input-type">{{ __('Type') }}</label>
                    <div class="col-md-9">
                        <select class="form-control" name="type[]" id="input-type">
                            <option value="all" @if ($isAllType) selected @endif>{{ __('All') }}</option>
                            <option value="category" @if ($isCategoryType) selected @endif>{{ __('Category') }}</option>
                            <option value="tag" @if ($isTagType) selected @endif>{{ __('Tags') }}</option>
                            <option value="top_news" @if ($isTopNewsType) selected @endif>{{ __('Top News') }}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group select-data-type-value {{ $isAllType || $isTopNewsType ? 'hidden' : '' }}">
                    <label class="col-md-3 control-label" for="input-type-value">{{ $isCategoryType ? __('Category') : __('Tag') }}</label>
                    <div class="col-md-9">
                        @php
                            $arr_type_value_cat = !empty($setting) ? explode(",", $setting['type_value']) : [];
                            $arr_type_value_tag = !empty($setting) ? explode(",", $setting['type_value']) : [];
                        @endphp
                        <select class="select2-multiple form-control {{ $isTagType ? 'hidden' : '' }}" data-placeholder="{{ __("Select Categories") }}" name="type_value[]" multiple="multiple" id="input-category-type-value" @if ( $isTagType ) disabled @endif>
                            @foreach($categories as $key => $value)
                                <option value="{{ $key }}" @if ( !empty($setting) && in_array($key, $arr_type_value_cat)) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>

                        <select class="select2-multiple form-control {{ $isCategoryType ? 'hidden' : '' }}" data-placeholder="{{ __("Select Tags") }}" name="type_value[]" multiple="multiple" id="input-tag-type-value" @if ( $isCategoryType || $isAllType ) disabled @endif>
                            @foreach($tags as $key => $value)
                                <option value="{{ $key }}" @if ( !empty($setting) && in_array($key, $arr_type_value_tag))  selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="input-data-count">{{ __('Data Count') }}</label>
                    <div class="col-md-9 form-group-touchspin-data-count">
                        <input id="input-data-count" value="{{ !empty($setting) && !empty($setting['data_count']) ? $setting['data_count'] : 0 }}" name="data_count[]" class="input_touchspin_vertical2">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="input_data_offset">{{ __('Data Offset') }}</label>
                    <div class="col-md-9 form-group-touchspin-data-offset">
                        <input id="input_data_offset" value="{{ !empty($setting) && !empty($setting['data_offset']) ? $setting['data_offset'] : 0 }}" name="data_offset[]" class="form-control input_touchspin_vertical2">
                    </div>
                </div>

                <div class="form-group ">
                    <label class="col-md-3 control-label" for="title">{{ __('Ordering') }}</label>
                    <div class="col-md-9">
                        <select class="form-control" name="ordering[]" id="input-ordering">
                            <option value="available_date">{{ __('Available date') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group-wrap">
                <div class="form-group text-center">
                    <div class="col-md-12 group-button-action">
                        @if ( !empty($setting) )
                            <button type="button" class="btn green pull-right btn-each-section-save"><i class="fa fa-save"></i>&nbsp;{{ __('Save') }}</button>
                        @else
                            <button data-msg="{{ __("Added a new section successfully") }}" class="btn green btn-block"><i class="fa fa-plus"></i>&nbsp;{{ __('Add Section') }}</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

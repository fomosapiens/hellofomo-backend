@extends(config('metronic.layout.root'))

@section('title', __('Create new Salutation or Title'))

@section('content')
    @include('admin.salutation.form', [
        'title'         => __('Create new Salutation or Title'),
        'route'         => route('admin.salutations.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection

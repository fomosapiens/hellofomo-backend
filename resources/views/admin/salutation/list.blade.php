@extends(config('metronic.layout.root'))

@section('title', $pageTitle ? $pageTitle : 'Overview')

@section('content')
    @include('ajaxdatatable::datatable')
@endsection

@extends(config('metronic.layout.root'))

@section('title', __('Create new user'))

@section('content')
    @include('admin.users.form', [
        'title'         => __('Create new user'),
        'route'         => route('admin.users.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection

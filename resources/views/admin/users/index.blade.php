@extends(config('metronic.layout.root'))

@section('title', __('Users Overview'))

@section('content')
    @include('ajaxdatatable::datatable')
@endsection

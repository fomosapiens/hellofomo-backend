@extends(config('metronic.layout.root'))

@section('title', __('Edit'))

@section('content')
    @include('admin.category.form', [
        'title'         => __('Edit'),
        'route'         => route('admin.categories.update', ['category' => $category->id]),
        'btnSubmitText' => __('Save Changes'),
    ])
@endsection

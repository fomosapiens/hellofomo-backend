@extends(config('metronic.layout.root'))

@section('title', __('Category Detail'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-10">
                        <div class="portlet light bordered no-padding-bottom">
                            <div class="portlet-title">
                                <div class="caption font-green">{{ __('News Category') }}</div>
                                <div class="actions">
                                    @include('admin.components.full-screen')
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <div class="form-body">
                                    <div class="form-group ">
                                        <label class="col-md-2 control-label" for="name">{{ __('Name') }}</label>
                                        <div class="col-md-10">
                                            {{ $category->name }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

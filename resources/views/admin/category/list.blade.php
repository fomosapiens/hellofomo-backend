@extends(config('metronic.layout.root'))

@section('title', $pageTitle ? $pageTitle : __("Overview"))

@section('content')
    @include('ajaxdatatable::datatable')
@endsection

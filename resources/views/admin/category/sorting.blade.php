@extends(config('metronic.layout.root'))

@section('title', __('Categories Sorting'))
@section('main_title', __('Categories'))

@section('action_buttons')
    <div class="clearfix">
        <div class="btn-group pull-right">
            <a href="{{ route('admin.categories.showSorting') }}" class="btn btn-default">{{ __('Edit') }}</a>

            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-angle-down"></i>
            </button>

        </div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ __('Sorting Categories') }}</div>
                            <div class="actions">
                                <button class="btn btn-icon-only btn-default fullscreen" data-original-title="Vollbild" title="{{ __('Full screen') }}"></button>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <form action="{{ route('admin.categories.updateSorting') }}" method="POST" id="fomo-update-sorting">
                                {{ csrf_field() }}
                                <div class="dd nestable">
                                    <ol class="dd-list">
                                        <li class="item-heading">
                                            <div class="dd-heading">
                                                <div class="tb-fomo-nestable">
                                                    <div class="st-col-left st-col st-col-0">
                                                        @php echo __('Name') @endphp
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @if (!empty($categories))
                                            @foreach($categories as $key => $category)
                                                <li class="dd-item" data-id="@php echo $category->id @endphp">
                                                    <div class="dd-handle">
                                                        <div class="tb-fomo-nestable">
                                                            <div class="st-col-left st-col st-col-2">@php echo $category->name @endphp</div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" id="authors-sorting-full" class="btn btn-default green pull-right">{{ __('Update') }}</button>
                                </div>
                                <div class="spinner-loading font-green text-center" style="display: none"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
                                <input type="hidden" name="myData" id="myData"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
<link href="{{ asset('/vendor/metronic/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script src="{{ asset('/vendor/metronic/global/plugins/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
@endpush
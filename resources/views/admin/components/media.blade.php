@section('title', __('Media Management'))

@extends(config('media.layouts.master'))

@push(config('media.layouts.header'))
    {!! RvMedia::renderHeader() !!}
@endpush

@section(config('media.layouts.main'))
    {!! RvMedia::renderContent() !!}
@endsection

@push(config('media.layouts.footer'))
    {!! RvMedia::renderFooter() !!}
@endpush

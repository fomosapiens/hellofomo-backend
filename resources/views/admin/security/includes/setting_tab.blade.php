<div class="tab-pane @if($currentTab == 'setting') active @endif" id="tab_setting">
    <div class="form-group {{ $errors->has($failed_logins) ? 'has-error' : '' }}">
        <label class="col-md-2 control-label" for="{{ $failed_logins }}">{{ __('Failed logins') }}</label>
        <div class="col-md-6 form-group-touchspin-data-count">
            <input min=1
                   class="form-control input_touchspin_vertical2"
                   name="{{ $failed_logins }}"
                   id="{{ $failed_logins }}"
                   value="{{ old($failed_logins, isset($security[$failed_logins]) ? $security[$failed_logins] : '') }}">
            <span class="help-block">{{ __('The user will be blocked after tried more than this number') }}</span>
            <div class="form-control-focus"></div>
            <span class="help-block">{{ $errors->first($failed_logins) }}</span>
        </div>
    </div>

    <div class="form-group {{ $errors->has($failed_logins_time_minute) ? 'has-error' : '' }}">
        <label class="col-md-2 control-label" for="{{ $failed_logins_time_minute }}">{{ __('Failed logins between time in Minutes') }}</label>
        <div class="col-md-6 form-group-touchspin-data-count">
            <input min=1
                   class="form-control input_touchspin_vertical2"
                   name="{{ $failed_logins_time_minute }}"
                   id="{{ $failed_logins_time_minute }}"
                   value="{{ old($failed_logins_time_minute, isset($security[$failed_logins_time_minute]) ? $security[$failed_logins_time_minute] : '') }}">
            <div class="form-control-focus"></div>
            <span class="help-block">{{ $errors->first($failed_logins_time_minute) }}</span>
        </div>
    </div>

    <div class="form-group {{ $errors->has($time_minute_block) ? 'has-error' : '' }}">
        <label class="col-md-2 control-label" for="{{ $time_minute_block }}">{{ __('Time in minutes to block') }}</label>
        <div class="col-md-6 form-group-touchspin-data-count">
            <input min=1
                   class="form-control input_touchspin_vertical2"
                   name="{{ $time_minute_block }}"
                   id="{{ $time_minute_block }}"
                   value="{{ old($time_minute_block, isset($security[$time_minute_block]) ? $security[$time_minute_block] : '') }}">
            <div class="form-control-focus"></div>
            <span class="help-block">{{ $errors->first($time_minute_block) }}</span>
        </div>
    </div>
</div>

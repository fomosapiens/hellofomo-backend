@extends(config('metronic.layout.root'))

@php
    $title                     = __('Security');
    $failed_logins             = config('fomo.security_settings.failed_logins');
    $failed_logins_time_minute = config('fomo.security_settings.failed_logins_time_minute');
    $time_minute_block         = config('fomo.security_settings.time_minute_block');
    $currentTab                = Session::get('currentTab', '');
@endphp

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="{{ route('admin.security.update') }}" method="POST" class="form-horizontal form-validation submit-frm">
                        {{ csrf_field() }}

                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ empty($title) ? __('Form General Settings') : $title }}</div>
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li @if($currentTab != 'setting') class="active" @endif data-acl-role="security.read">
                                                <a href="#tab_blocked_ip" data-toggle="tab"> {{ __('Blocked IP Addresses') }} </a>
                                            </li>
                                            <li @if($currentTab == 'setting') class="active" @endif data-acl-role="security.edit">
                                                <a href="#tab_setting" data-toggle="tab"> {{ __('Settings') }} </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            @include('admin.security.includes.blocked_ip_tab', ['currentTab' => $currentTab])
                                            @include('admin.security.includes.setting_tab', ['currentTab' => $currentTab])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

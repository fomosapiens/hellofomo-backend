@extends(config('metronic.layout.root'))

@section('title', __('Create new tag'))

@section('content')
    @include('admin.tag.form', [
        'title'         => __('Create new tag'),
        'route'         => route('admin.tags.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection

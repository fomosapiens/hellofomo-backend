@extends(config('metronic.layout.root'))

@php
    $title                   = __('Email Setting');
    $senderNameText          = __('Sender name');
    $senderAddressText       =  __('Sender Address');
    $mailerToUserSMTPText    = __('Set mailer to use SMTP');
    $specifyMainText         = __('Specify main and backup SMTP servers');
    $smtpAuthenticationText  = 'Enable SMTP authentication';
    $smtpUsernameText        = "SMTP username";
    $smtpPasswordText        = "SMTP password";
    $encryptionText          = "Encryption";
    $tcpPortText             = "TCP port to connect to";

    $accountSenderName       = config('fomo.email_settings.account_sender_name');
    $accountSenderAddress    = config('fomo.email_settings.account_sender_address');
    $mailerToUserSMTP        = config('fomo.email_settings.mailer_smpt');
    $specifyMain             = config('fomo.email_settings.main_backup_smtp_servers');
    $smtpAuthentication      = config('fomo.email_settings.enable_smtp_auth');
    $smtpUsername            = config('fomo.email_settings.smpt_username');
    $smtpPassword            = config('fomo.email_settings.smtp_password');
    $encryption              = config('fomo.email_settings.email_encryption');
    $tcpPort                 = config('fomo.email_settings.tcp_port');
    $systemSenderName        = config('fomo.email_settings.system_sender_name');
    $systemSenderAddress     = config('fomo.email_settings.system_sender_address');
@endphp

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="{{ route('admin.settings.email.update') }}" method="POST" class="form-horizontal form-validation submit-frm">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ empty($title) ? __('Form Email Settings') : $title }}</div>
                                    @include('admin.components.portlet-tools')
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">

                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_account" data-toggle="tab"> {{ __('Settings Account') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_system" data-toggle="tab"> {{ __('Settings System') }} </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_account">
                                            <div class="form-body">
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $accountSenderName }}">{{ $senderNameText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $accountSenderName }}"
                                                               id="{{ $accountSenderName }}"
                                                               placeholder="{{ $senderNameText }}"
                                                               value="{{ old($accountSenderName, isset($emailSettings[$accountSenderName]) ? $emailSettings[$accountSenderName] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $accountSenderAddress }}">{{ $senderAddressText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control email"
                                                               name="{{ $accountSenderAddress }}"
                                                               id="{{ $accountSenderAddress }}"
                                                               placeholder="{{ $senderAddressText }}"
                                                               value="{{ old($accountSenderAddress, isset($emailSettings[$accountSenderAddress]) ? $emailSettings[$accountSenderAddress] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label email-settings-label" for="{{ $mailerToUserSMTP }}">{{ $mailerToUserSMTPText }}</label>
                                                    <div class="col-md-9">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox"
                                                                   id="{{ $mailerToUserSMTP }}"
                                                                   name="{{ $mailerToUserSMTP }}"
                                                                   class="md-check" {{ old($mailerToUserSMTP, (isset($emailSettings[$mailerToUserSMTP]) && $emailSettings[$mailerToUserSMTP])) ? 'checked' : ''}} >
                                                            <label for="{{ $mailerToUserSMTP }}">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span></label>
                                                        </div>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="$specifyMain">{{ $specifyMainText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $specifyMain }}"
                                                               id="{{ $specifyMain }}"
                                                               value="{{ old($specifyMain, isset($emailSettings[$specifyMain]) ? $emailSettings[$specifyMain] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label email-settings-label" for="{{ $smtpAuthentication }}">{{ $smtpAuthenticationText }}</label>
                                                    <div class="col-md-9 ">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox"
                                                                   id="{{ $smtpAuthentication }}"
                                                                   name="{{ $smtpAuthentication }}"
                                                                   class="md-check" {{ old($smtpAuthentication, (isset($emailSettings[$smtpAuthentication]) && $emailSettings[$smtpAuthentication])) ? 'checked' : ''}} >
                                                            <label for="{{ $smtpAuthentication }}">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span></label>
                                                        </div>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $smtpUsername }}">{{ $smtpUsernameText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $smtpUsername }}"
                                                               id="{{ $smtpUsername }}"
                                                               placeholder="{{ $smtpUsernameText }}"
                                                               value="{{ old($smtpUsername, isset($emailSettings[$smtpUsername]) ? $emailSettings[$smtpUsername] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $smtpPassword }}">{{ $smtpPasswordText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="password"
                                                               class="form-control"
                                                               name="{{ $smtpPassword }}"
                                                               id="{{ $smtpPassword }}"
                                                               placeholder="{{ $smtpPasswordText }}"
                                                               value="{{ old($smtpPassword, isset($emailSettings[$smtpPassword]) ? $emailSettings[$smtpPassword] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $encryption }}">{{ $encryptionText }}</label>
                                                    <div class="col-md-9">
                                                        <div class="md-radio-inline">
                                                            <div class="md-radio">
                                                                <input type="radio"
                                                                       id="radio1"
                                                                       name="{{ $encryption }}"
                                                                       class="md-radiobtn"
                                                                       value=""
                                                                       {{ old($encryption, !isset($emailSettings[$encryption]) || empty($emailSettings[$encryption])) ? 'checked' : ''}}
                                                                >
                                                                <label for="radio1">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> {{ __('None') }} </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio"
                                                                       id="radio2"
                                                                       name="{{ $encryption }}"
                                                                       class="md-radiobtn"
                                                                       value="tls"
                                                                       {{ old($encryption, (!empty($emailSettings[$encryption]) && $emailSettings[$encryption] == 'tls')) ? 'checked' : ''}}
                                                                >
                                                                <label for="radio2">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> {{ __('TLS') }} </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio"
                                                                       id="radio3"
                                                                       name="{{ $encryption }}"
                                                                       class="md-radiobtn"
                                                                       value="ssl"
                                                                        {{ old($encryption, (!empty($emailSettings[$encryption]) && $emailSettings[$encryption] == 'ssl')) ? 'checked' : ''}}
                                                                >
                                                                <label for="radio3">
                                                                    <span class="inc"></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span>{{ __('SSL') }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $tcpPort }}">{{ $tcpPortText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control number"
                                                               name="{{ $tcpPort }}"
                                                               id="{{ $tcpPort }}"
                                                               value="{{ old($tcpPort, isset($emailSettings[$tcpPort]) ? $emailSettings[$tcpPort] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab_system">
                                            <div class="form-body">
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $systemSenderName }}">{{ $senderNameText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $systemSenderName }}"
                                                               id="{{ $systemSenderName }}"
                                                               placeholder="{{ $senderNameText }}"
                                                               value="{{ old($systemSenderName, isset($emailSettings[$systemSenderName]) ? $emailSettings[$systemSenderName] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-md-3 control-label" for="{{ $systemSenderAddress }}">{{ $senderAddressText }}</label>
                                                    <div class="col-md-9">
                                                        <input type="text"
                                                               class="form-control email"
                                                               name="{{ $systemSenderAddress }}"
                                                               id="{{ $systemSenderAddress }}"
                                                               placeholder="{{ $senderAddressText }}"
                                                               value="{{ old($systemSenderAddress, isset($emailSettings[$systemSenderAddress]) ? $emailSettings[$systemSenderAddress] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends(config('metronic.layout.root'))

@php
    $title            = __('Edit Setting');
    $supportLanguages = config('fomo.support_languages');
    $dateTimeKeyEn    = config('fomo.settings.date_time_en');
    $dateTimeKeyDe    = config('fomo.settings.date_time_de');
    $languageKey      = config('fomo.settings.language');
    $file_types       = config('fomo.settings.news_files_types');
    $image_logo       = config('fomo.settings.image_logo');
    $favicon          = config('fomo.settings.favicon');
    $websiteTitlePrefix = config('fomo.settings.website_title_prefix');
@endphp

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="{{ route('admin.settings.update') }}" method="POST" class="form-horizontal form-validation submit-frm">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ empty($title) ? __('Form General Settings') : $title }}</div>
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_generate" data-toggle="tab"> {{ __('General') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_date_time" data-toggle="tab"> {{ __('Date & Time') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_news" data-toggle="tab"> {{ __('News') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_ci" data-toggle="tab"> {{ __('CI') }} </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_generate">
                                                <div class="form-group {{ $errors->has($languageKey) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $languageKey }}">{{ __('Language') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="{{ $languageKey }}" id="{{ $languageKey }}" class="form-control">
                                                            @foreach($supportLanguages as $name => $code)
                                                                <option value="{{ $code }}" @if (isset($settings[$languageKey]) && $settings[$languageKey] == $code) selected @endif >{{ $name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($languageKey) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab_date_time">
                                                <div class="form-group {{ $errors->has($dateTimeKeyEn) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $dateTimeKeyEn }}">{{ __('English') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $dateTimeKeyEn }}"
                                                               id="{{ $dateTimeKeyEn }}"
                                                               placeholder="'Y/m/d H:i'"
                                                               value="{{ old($dateTimeKeyEn, isset($settings[$dateTimeKeyEn]) ? $settings[$dateTimeKeyEn] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($dateTimeKeyEn) }}</span>
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has($dateTimeKeyDe) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $dateTimeKeyDe }}">{{ __('Germany') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $dateTimeKeyDe }}"
                                                               id="{{ $dateTimeKeyDe }}"
                                                               value="{{ old($dateTimeKeyDe, isset($settings[$dateTimeKeyDe]) ? $settings[$dateTimeKeyDe] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($dateTimeKeyDe) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab_news">
                                                <div class="form-group{{ $errors->has($file_types) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $file_types }}">{{ __('Files Types') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $file_types }}"
                                                               id="{{ $file_types }}"
                                                               placeholder="{{ __('For example: png,jpeg,jpg,gif') }}"
                                                               value="{{ old($file_types, isset($settings[$file_types]) ? $settings[$file_types] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($file_types) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab_ci">
                                                <div class="form-group {{ $errors->has($file_types) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $file_types }}">{{ __('Change Logo') }}</label>

                                                    <div class="col-md-6">
                                                        <div id="crop-avatar" class="page-admin-setting">
                                                            <div class="image-box avatar-view" style="margin-top: 7px;">
                                                                <input type="hidden" name="{{ $image_logo }}" value="{{ isset($settings[$image_logo]) ? $settings[$image_logo] : '' }}" class="image-data">
                                                                <img style="@if (!isset($settings[$image_logo]) || empty($settings[$image_logo])) display: none; @endif" src="{{ !empty($settings[$image_logo]) ? rv_media_get_image_url_from_id($settings[$image_logo]) : '' }}"
                                                                     alt="{{ __('Preview image') }}"
                                                                     class="rvMedia_preview_file preview_image img-responsive" data-rv-media="[@if (!empty($settings[$image_logo])){{ json_encode(['selected_file_id' => $settings[$image_logo], 'file_type' => 'image', 'full_url' => true]) }}@else {{ json_encode(['file_type' => 'image', 'full_url' => true]) }} @endif]"  />

                                                                <div class="image-box-actions">
                                                                    @php
                                                                        $displayBrowser = empty($settings[$image_logo]);
                                                                    @endphp
                                                                    <a style="@if(!$displayBrowser) display: none @endif" href="javascript:;" class="btn-upload-image" data-rv-media="[{{ json_encode([ 'file_type' => 'image', 'full_url' => true]) }}]">{{ __('Browse an image') }}</a>
                                                                    <a style="@if($displayBrowser) display: none @endif" href="javascript:;" class="btn-remove-image">{{ __('Remove the image') }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix padding-bottom-tab"></div>
                                                </div>

                                                <div class="form-group {{ $errors->has($file_types) ? 'has-error' : '' }}">
                                                    <div class="col-md-10 col-md-push-2">
                                                        <hr></div>
                                                    <div class="clearfix"></div>
                                                    <label class="col-md-2 control-label" for="{{ $file_types }}">{{ __('Change Favicon') }}</label>

                                                    <div class="col-md-6">
                                                        <div id="crop-avatar" class="page-admin-setting">
                                                            <div class="image-box avatar-view" style="margin-top: 7px;">
                                                                <input type="hidden" name="{{ $favicon }}" value="{{ isset($settings[$favicon]) ? $settings[$favicon] : '' }}" class="image-data">
                                                                <img style="@if (!isset($settings[$favicon]) || empty($settings[$favicon])) display: none; @endif" src="{{ !empty($settings[$favicon]) ? rv_media_get_image_url_from_id($settings[$favicon]) : '' }}"
                                                                     alt="{{ __('Preview image') }}"
                                                                     class="rvMedia_preview_file preview_image img-responsive" data-rv-media="[@if (!empty($settings[$favicon])){{ json_encode(['selected_file_id' => $settings[$favicon], 'file_type' => 'image', 'full_url' => true]) }}@else {{ json_encode(['file_type' => 'image', 'full_url' => true]) }} @endif]"  />

                                                                <div class="image-box-actions">
                                                                    @php
                                                                        $displayBrowser = empty($settings[$favicon]);
                                                                    @endphp
                                                                    <a style="@if(!$displayBrowser) display: none @endif" href="javascript:;" class="btn-upload-image" data-rv-media="[{{ json_encode([ 'file_type' => 'image', 'full_url' => true]) }}]">{{ __('Browse an image') }}</a>
                                                                    <a style="@if($displayBrowser) display: none @endif" href="javascript:;" class="btn-remove-image">{{ __('Remove the image') }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix padding-bottom-tab"></div>
                                                </div>
                                                <div class="form-group{{ $errors->has($websiteTitlePrefix) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $websiteTitlePrefix }}">{{ __('Website title prefix') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $websiteTitlePrefix }}"
                                                               id="{{ $websiteTitlePrefix }}"
                                                               placeholder="{{ __('For example: HelloFomo') }}"
                                                               value="{{ old($websiteTitlePrefix, isset($settings[$websiteTitlePrefix]) ? $settings[$websiteTitlePrefix] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($websiteTitlePrefix) }}</span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends(config('metronic.layout.root'))

@section('title', __('News Detail'))

@push('styles')
    <link href="{{ asset('/vendor/metronic/pages/css/blog.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('action_buttons')
    <div class="clearfix">
        <div class="btn-group pull-right">
            <a href="{{ route('admin.news.edit', [ 'news' => $news->id ]) }}" class="btn btn-default">{{ __('Edit') }}</a>

            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-angle-down"></i>
            </button>

            <ul class="dropdown-menu pull-left" role="menu">
                <li>
                    <a href="{{ route('admin.news.trash', [ 'news' => $news->id ]) }}">{{ __('Trash') }}</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="blog-page blog-content-2 news-show">
        <div class="row">
            <div class="col-md-8">
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ $news->title }}</div>
                                    @include('admin.components.portlet-tools')
                                </div>
                                <div class="portlet-body form">
                                    <div class="row">
                                        <div class="blog-single-content">
                                            @php
                                                $coverPic = !isset($news->image_src) || empty($news->image_src) ? '' : $news->image_src;

                                                // get cover image height value
                                                $coverImageHeight = 0;
                                                $focusPointClass = '';
                                                $coverPicClasses = 'img-responsive pic-bordered pic-cover';
                                                if ( !empty($news->image->focus['data_attribute']) ) {
                                                    $focusPointClass  = 'focuspoint';
                                                    $coverPicClasses = '';

                                                    $dataAttributes   = explode(' ', $news->image->focus['data_attribute']);
                                                    $lastAttribute    = collect($dataAttributes)->last();
                                                    $coverImageHeight = !empty(explode('"', $lastAttribute)[1]) ? explode('"', $lastAttribute)[1] : 300;
                                                }
                                            @endphp
                                            @if ($coverPic)
                                                <div class="blog-single-img {{ $focusPointClass }}"
                                                     @if ( $coverImageHeight > 0 ) style="height: {{ $coverImageHeight }}px;" @endif
                                                     {!! $news->image->focus['data_attribute'] or '' !!}>
                                                    <a href="{{ $coverPic }}" class="fancybox wrap-pic-cover">
                                                        <img class="{{ $coverPicClasses }}" src="{{ $coverPic }}" alt="{{ __('Image') }}">
                                                    </a>
                                                </div>
                                            @endif

                                            @if (!empty($news->formatted_available_date))
                                            <div class="">
                                                <div class="blog-single-head-date">
                                                    <i class="icon-calendar font-blue"></i>
                                                    <span class="news-text-time font-blue">{{ $news->formatted_available_date }}</span>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="blog-single-desc">
                                                <div class="blog-introduction bold">{{ $news->introduction }}</div>
                                                <div class="blog-description">{!! $news->description !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.news.includes.show.related_news')
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.news.includes.show.meta_tags')
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ __('Status') }}</div>
                                    @include('admin.components.portlet-tools')
                                </div>

                                <div class="portlet-body caption @if ( $news->status == config('elidev.list_default_status.pending') ) font-red-flamingo @else font-green-jungle @endif text-center">
                                    <span class="caption-subject news-status">{{ $news->status == 'publish' ? 'Published' : ucfirst(__($news->status)) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.news.includes.show.assigments')
                        </div>
                        <div class="col-md-12">
                            @include('admin.news.includes.show.attachments')
                        </div>
                        <div class="col-md-12">
                            @include('admin.news.includes.show.galleries')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('SEO meta tags') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body">
        <div class="blog-single-sidebar">
            <div class="blog-single-sidebar-tags">
                <div class="box-news-tag">
                    <h3 class="blog-sidebar-title">{{ __('SEO title') }}</h3>
                    <p>{{ $news->meta_title or __('No data available.') }}</p>
                </div>

                <div class="box-news-tag">
                    <h3 class="blog-sidebar-title">{{ __('Permalink') }}</h3>
                    <p>{{ $news->meta_permalink or __('No data available.') }}</p>
                </div>

                <div class="box-news-tag">
                    <h3 class="blog-sidebar-title">{{ __('Meta description') }}</h3>
                    <p>{{ $news->meta_description or __('No data available.') }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

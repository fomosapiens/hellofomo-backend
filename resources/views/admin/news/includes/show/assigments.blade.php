<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Assignments') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body">
        <div class="blog-single-sidebar">
            <div class="blog-single-sidebar-tags">
                <div class="box-news-category-link">
                    <h3 class="blog-sidebar-title uppercase">{{ __('Categories') }}</h3>
                    <ul class="blog-post-tags">
                        <li>
                            @if ( empty($news->category_names_with_link) )
                                {{ __('No data available.') }}
                            @else
                                {!! $news->category_names_with_link !!}
                            @endif
                        </li>
                    </ul>
                </div>

                <div class="box-news-tag">
                    <h3 class="blog-sidebar-title uppercase">{{ __('Tags') }}</h3>
                    <ul class="blog-post-tags">
                        <li>
                            @if ( empty($news->tag_names_with_link) )
                                {{ __('No data available.') }}
                            @else
                                {!! $news->tag_names_with_link !!}
                            @endif
                        </li>
                    </ul>
                </div>

                <div class="box-news-author">
                    <h3 class="blog-sidebar-title uppercase">{{ __('Authors') }}</h3>
                    <ul class="blog-post-tags">
                        <li>
                            @if ( empty($news->author_names_with_link) )
                                {{ __('No data available.') }}
                            @else
                                {!! $news->author_names_with_link !!}
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

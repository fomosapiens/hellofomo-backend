<div class="portlet light bordered no-padding-bottom" data-acl-role="files.read">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Files') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body form">
        <div class="list-files">
            <ul>

                @if ( isset($news) && !empty($news->attachment_ids) && isset($news->attachments) )
                    @foreach($news->attachments as $key => $mediaFile)
                        <li data-id="{{ $mediaFile->id }}">
                            <i class="{{ $mediaFile->icon }}"></i> <a href="{{ url($mediaFile->url) }}" title="{{ $mediaFile->name }}">{{ File::basename($mediaFile->url) }}</a>
                            <input type="hidden" name="attachments[]" value="{{ $mediaFile->id }}"/>
                            <a href="javascript:;"
                               class="btn-delete-gallery-img font-red-thunderbird"
                               data-id="{{ $mediaFile->id }}">
                                <i class="fa fa-times"></i>
                            </a>
                        </li>
                    @endforeach
                @endif

                <li class="file-placeholder hidden">
                    <i class="file-icon"></i>
                    <a href="" class="file-item" target="_blank"></a>
                    <input type="hidden"/>
                    <a href="javascript:;" class="btn-delete-file font-red-thunderbird" data-id=""><i class="fa fa-times"></i></a>
                </li>
            </ul>
        </div>

        <div class="clearfix"></div>

        <p style="margin-top: 1em;" class="text-center">
            <a href="javascript:;" class="btn-gallery-select-file btn-upload-icon"><i class="fa fa-upload"></i></a>
        </p>
    </div>
</div>

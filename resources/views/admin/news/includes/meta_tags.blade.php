<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('SEO meta tags') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body">
        <div class="form-body custom-select2-container">
            <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                <label class="col-md-2 control-label" for="meta_title">{{ __('SEO title') }}</label>
                <div class="col-md-10">
                    <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{ old('meta_title', !empty($news->meta_title) ? $news->meta_title : '') }}">
                    <span class="help-block">{{ $errors->first('meta_title') }}</span>
                </div>
            </div>

            <div class="form-group {{ $errors->has('meta_permalink') ? 'has-error' : '' }}">
                <label class="col-md-2 control-label" for="meta_permalink">{{ __('Permalink') }}</label>
                <div class="col-md-10">
                    <input type="text" name="meta_permalink" id="meta_permalink" class="form-control" value="{{ old('meta_permalink', !empty($news->meta_permalink) ? $news->meta_permalink : '') }}">
                    <span class="help-block">{{ $errors->first('meta_permalink') }}</span>
                </div>
            </div>

            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                <label class="col-md-2 control-label" for="meta_description">{{ __('Meta description') }}</label>
                <div class="col-md-10">
                    <textarea name="meta_description" id="meta_description" class="form-control" rows="5">{{ old('meta_description', !empty($news->meta_description) ? $news->meta_description : '') }}</textarea>
                    <span class="help-block">{{ $errors->first('meta_description') }}</span>
                </div>
            </div>
        </div>
    </div>
</div>

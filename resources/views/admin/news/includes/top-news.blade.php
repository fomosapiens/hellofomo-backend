@php
    $format        = \App\Utils\Utils::dateTimeFormatBasedOnLanguage();
    $dateFormat    = $format['date_format'];
    $expiryDate    = isset($news->expiry_date) && $dateFormat ? $news->expiry_date->format($dateFormat) : '';
@endphp

<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Top News') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body form publish-group">
        <div class="col-xs-12">
            <div class="form-group {{ $errors->has('top_news') ? 'has-error' : '' }}">
                <div class="input-checkbox top-news-checkbox">
                    <div class="md-checkbox">
                        <input type="checkbox" id="top_news" name="top_news" class="md-check" {{ old('top_news', (isset($news) && $news->top_news )) ? 'checked' : '' }}>
                        <label for="top_news">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span></label>
                    </div>
                </div>
                <label class="control-label" for="top_news">{{ __('set as Top-News') }}</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group {{ $errors->has('expiry_date') ? 'has-error' : '' }}">
                <label class="control-label horizontal-control-label" for="expiry_date">{{ __('Days after expire') }}</label>
                <div class="input-group date form_datetime form_datetime bs-datetime date-time-picker"
                     data-date-format="{{ \App\Utils\Utils::convertToJSDateTimeFormat($dateFormat, true) }}"
                >
                    <input type="text"
                           size="16"
                           class="form-control"
                           name="expiry_date"
                           id="expiry_date"
                           onkeydown="return false"
                           data-date-format="{{ \App\Utils\Utils::convertToJSDateTimeFormat($dateFormat, true) }}"
                           value="{{ old('expiry_date', $expiryDate) }}" />
                    <span class="help-block">{{ $errors->first('expiry_date') }}</span>
                    <span class="input-group-addon">
                        <button class="btn default date-reset" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                    <span class="input-group-addon">
                        <button class="btn default" type="button">
                            <span class="fa fa-fw fa-calendar"></span>
                        </button>
                    </span>
                </div>
                <label class="control-label horizontal-control-label" for="expiry_date">{{ __('leave blank if never') }}</label>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

@extends(config('metronic.layout.root'))

@section('title', __('News Overview'))

@section('content')
    @include('ajaxdatatable::datatable')

    {{-- Include modal to set categories/tags --}}
    @include('admin.partials.modal.set_categories_tags')
@endsection

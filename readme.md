# Table of Contents

* [Resources](#resources)
* [Installation](#installation)
* [Development](#development)
* [Customization directories](#customization-directories)
    * [Service Provider](#service-provider)
* [Packages](#packages)
* [Functionality](#functionality)
    * [Activation Account](#activation-account)
* [Testing](#testing)
* [Deployment](#deployment)



# Resources

* ERD Diagram:
* Requirements Specification:  https://drive.google.com/drive/folders/0B6xHGYEAyU5xcHFNaUIzTFpLakE
* Diagram:
* Mobile API Documentation: http://docs.fomomobile.apiary.io/
    
# Installation <a id="installation"></a>

- Basic requirement: https://laravel.com/docs/5.4/#server-requirements

- The **storage**, **public/uploads** and the **bootstrap/cache** directories should be writable by your web server.

- .env
Create a new file call **.env** > copy the content of the **.env.example** file > paste to this file

- Install npm
```
 $ npm install
```

- Composer:

```
$ composer update
$ composer dump-autoload
$ php artisan key:generate
```

- Migrate database:

```
$ php artisan migrate --seed
```
  
- Initial Passport:

 ```
$ php artisan passport:install
```

- Install default app client to allow mobile app can authentication

```
$ php artisan passport:client
```

Notes: please answer questions like below hints

* Question 1: Which user ID should the client be assigned to? : Answer: we will enter our default admin user ID. For example: 1
* Question 2: What should we name the client? : Answer: we will input our domain. For example: http://fomo.elidev.info
* Question 3: Where should we redirect the request after authorization? : Answer: we will input our domain. For example: http://fomo.elidev.info

Then, please update ```.env``` file to specify the app url. For example:
```
APP_URL=http://fomo.elidev.info
```

Also need to change owner and permission for oauth private key
```
$ sudo chown www-data:www-data storage/oauth-*.key
$ sudo chmod 600 storage/oauth-*.key
```


- Assets: mixing almost CSS and JS files https://laravel.com/docs/5.4/mix

```
$ npm run dev-linux
```

Then update the HOST (your domain name), [CLIENT_ID, CLIENT_KEY](fomo.elidev.info/get-client) parameters

- Media translation

```
php artisan vendor:publish --provider="Botble\Media\Providers\MediaServiceProvider" --tag=lang --force
cp resources/lang/en/media.php resources/lang/de/media.php

```

- Generate media language files

Refer: https://docs.botble.com/media/1.0/customization

```
$ php artisan vendor:publish --provider="Botble\Media\Providers\MediaServiceProvider" --tag=lang --force
```

# Development

## Back-end

### Asset files 

All static files must be located in the **resources/assets** folder, we need to use below command to mix all CSS or JS files:

```
$ npm run dev
```

If you couldn't run the above command, please use the below instead
```
$ npm run dev-linux
```

P/s: If both commands can not execute, please update your **npm** and **node** to the latest version
Please refer https://laravel.com/docs/5.4/mix for more detail

# Customization directories

## Notifications 
       * app
              * Notifications
       * resources
              * views
                     * vendors
                            * notifications (Default notification view)
                     * mail
                            * default (default custom view for mail)
                            * custom (save all email template from client)       

## Service Provider

A customization service provider should be registered in the `register` method of the `AppServiceProvider.php`

### app\PackageServiceProvider.php

Using to register all packages service provider

### app\Providers\RepositoryServiceProvider.php

Using to bind all repositories that are registered in the first time loading of our application

### app\Providers\DeferredRepositoryServiceProvider.php

Using to bind all repositories that are registered until one of the registered bindings is actually needed

### app\Models

Containing all custom models

### app\Repository

Usage: http://git.elidev.info/vulh/laravel-repository-plugin

Notes:

- Do not declare a repository in another one
- Each repository must bind in the `RepositoryServiceProvider` or `DeferredRepositoryServiceProvider` by its interface then using this one for initialize an instance in your controller

### app\Factories

In object-oriented programming (OOP), a factory is an object for creating other objects – formally a factory is a function or method that returns objects of a varying prototype or class[1] from some method call, which is assumed to be "new".
(Source: [WikiMedia](https://en.wikipedia.org/wiki/Factory_(object-oriented_programming)))

### app\Services

# Functionality

## Activation Account

* After registering successfully account our system will send to this user a notification email after 4 seconds
* Using Notification queue to send email https://laravel.com/docs/5.4/notifications
* Command to execute the queue (https://laravel.com/docs/5.4/queues) for testing

# Deployment

* Cache route, config
    
    ```
        php artisan optimize
        php artisan config:clear
        php artisan config:cache
        php artisan route:clear
        php artisan route:cache
        php artisan queue:restart
    ```

- [Important]: Hotfix Laravel core if the server uses https://www.gentoo.org/:
vendor/laravel/framework/src/Illuminate/Foundation/Application.php > "runningInConsole" function, update code like:

```
 public function runningInConsole()
    {
        return php_sapi_name() == 'cgi-fcgi'|| php_sapi_name() == 'cli' || php_sapi_name() == 'phpdbg';
    }

```
+ Fix [ERROR] Use of undefined constant STDIN - assumed 'STDIN' (this will throw an Error in a future version of PHP)

```
    if (!defined('STDIN')) {
        define('STDIN',fopen("php://stdin","r"));
}

```    

+ [ERROR] <br />
  <b>Warning</b>:  Unexpected character in input:  '\' (ASCII=92) state=1 in <b>/kunden/556045_30173/webseiten/_HelloFomo.org/httpdocs_stage.hellofomo.org/artisan</b> on line <b>39</b><br />
  <br />
  <b>Parse error</b>:  syntax error, unexpected T_STRING in <b>/kunden/556045_30173/webseiten/_HelloFomo.org/httpdocs_stage.hellofomo.org/artisan</b> on line <b>39</b><br />
  
  Remove from the composer.json:
  
```
    "php artisan ide-helper:generate",
    "php artisan ide-helper:meta"
```

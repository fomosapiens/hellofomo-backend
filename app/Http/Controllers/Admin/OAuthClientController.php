<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class OAuthClientController extends Controller
{
    public function index()
    {
        $client = \DB::table('oauth_clients')
            ->where('personal_access_client', '=', 0)
            ->where('password_client', '=', 0)
            ->where('revoked', '=', 0)
            ->first();


        return view('admin.oauth-client.index', compact('client'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Security\Admin\ListingIpAddressesService;
use App\Services\Security\Admin\UnblockIpAddressService;
use Illuminate\Http\Request;
use Log;
use Settings;

class SecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = config('fomo.security_settings');
        $security = [];
        foreach ($setting as $k => $item) {
            $security[$k] = Settings::get($k);
        }

        // display blocked IP addresses
        $table = [
            'id' => 'block-ip-addresses-datatable',
            'ajax' => [
                'src' => 'admin.security.more', // route name
            ],
            'filter_navigation' => false,
            'order_default' => [
                'column' => 'created_at',
                'order' => 'desc'
            ],
            'columns' => [
                'id' => [
                    'text' => '#',
                    'width' => '2%'
                ],
                'ip' => [
                    'text' => 'IP',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'width' => '10%',
                    'orderable' => true,
                ],
                'email' => [
                    'text' => 'Email',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'width' => '20%',
                    'orderable' => true,
                ],
                'user_agent' => [
                    'text' => 'User agent',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'width' => '30%'
                ],
                'created_at' => [
                    'text' => 'Created at',
                ],
                'status' => [
                    'text' => 'Status',
                ],
            ],
            'actions' => [
                'width' => '100',
                'unblock' => [
                    'role' => 'security.unblock',
                    'route' => 'admin.security.unblock', // route name
                ],
            ],
        ];

        return view('admin.security.edit', [
            'security' => $security,
            'table' => $table,
            'breadcrumbs' => [
                [
                    'text' => __('Security'),
                    'url' => route('admin.security.read'),
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'failed_logins'             => 'sometimes|numeric|min:1',
            'failed_logins_time_minute' => 'sometimes|numeric|min:1',
            'time_minute_block'         => 'sometimes|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $items = $request->only(config('fomo.security'));
        try {
            foreach($items as $key => $val) {
                Settings::set($key, intval($val));
                unset($items[$key]);
            }

            return back()
                ->with('success', __('Edit the security setting successfully'))
                ->with('currentTab', 'setting')
                ->withInput();
        } catch (\Exception $ex) {
            Log::error('Failed to edit the security with key = ' . json_encode($items) . '. Error message: ' . $ex->getMessage());
        }
        return back()->with('error', __('Edit failed. Please try again'))->withInput();
    }

    /**
     * Get blocked IP addresses
     *
     * @param \Illuminate\Http\Request                               $request
     * @param \App\Services\Security\Admin\ListingIpAddressesService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #13471 - HFBACKEND-101 System / Security - Block IPs listing
     */
    public function more(Request $request, ListingIpAddressesService $service)
    {
        $ipAddresses = $service->execute($request);
        $total       = $ipAddresses->total();

        // check the cache is expired or not
        $ipAddresses->getCollection()->each(function($item) {
            $item->status = $item->is_expired ? __('Expired') : __('Active');
            $item->is_disabled = $item->is_expired;

            unset($item->cache_key);
            unset($item->expired_at);
        });

        return json_encode([
            'data'            => $ipAddresses->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }

    /**
     * Unblock specific IP address
     *
     * @param \Illuminate\Http\Request                             $request
     * @param \App\Services\Security\Admin\UnblockIpAddressService $service
     *
     * @return \Illuminate\Http\RedirectResponse
     * @ticket #13471 - HFBACKEND-101 System / Security - Block IPs listing
     */
    public function unblock(Request $request, UnblockIpAddressService $service)
    {
        $id = intval($request->get('id', -1));
        if ($id <= 0) {
            return back()->with('error', __('Invalid ID'));
        }

        $result = $service->execute($request);
        if ($result) {
            return back()->with('success', __('Unblock successfully'));
        }

        return back()->with('error', __('Failed to unblock. Please try again'));
    }
}

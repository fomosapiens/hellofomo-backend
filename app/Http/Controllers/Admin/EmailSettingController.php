<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Artisan;
use Illuminate\Http\Request;
use Log;
use Settings;

class EmailSettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $setting = config('fomo.email_settings');
        $emailSettings = [];
        foreach ($setting as $k => $item) {
            $emailSettings[$k] = Settings::get($k);
        }
        return view('admin.setting-email.edit', [
            'emailSettings' => $emailSettings,
            'breadcrumbs' => [
                [ 'text' => __('Email Settings') ],
                [ 'text' => __('Edit') ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $items = $request->only(config('fomo.email_settings'));
        $emailSettings = config('fomo.email_settings');
        $itemCheckbox = array(
            config('fomo.email_settings.mailer_smpt'),
            config('fomo.email_settings.enable_smtp_auth'),
        );
        try {
            foreach($emailSettings as $key => $val) {
                if (in_array($key, $itemCheckbox)) {
                    $data = (isset($items[$key]) && $items[$key] == 'on') ? 1 : 0;
                } else {
                    $data = isset($items[$key]) ? $items[$key] : '';
                }

                Settings::set($key, $data);
                unset($items[$key]);
            }

            // create cache config
            Artisan::call('config:cache');

            return back()->with('success', __('Edit the setting successfully'))->withInput();
        } catch (\Exception $ex) {
            Log::error('Failed to edit the setting with key = ' . json_encode($items) . '. Error message: ' . $ex->getMessage());
        }
        return back()->with('error', __('Edit failed. Please try again'))->withInput();
    }
}

<?php

namespace App\Http\Controllers\Admin\Auth\Traits;

use App\Http\Controllers\Admin\Auth\FomoRateLimiter;
use Illuminate\Http\Request;

trait FomoThrottlesLogins
{
    /**
     * The number of max login attempts
     *
     * @var int
     */
    protected $maxAttempts = 10;

    /**
     * Failed logins between time in minutes
     *
     * @var int
     */
    protected $failedLoginsTimeMinutes = 10;

    /**
     * The number in minutes to block failed login
     *
     * @var int
     */
    protected $decayMinutes = 30;

    /**
     * Init brute force variables
     * These variables are stored in setting (@ticket - #13446)
     *
     * @return void
     */
    public function initBruteForceVariables()
    {
        $this->maxAttempts             = elidev_setting(config('fomo.security_settings.failed_logins'), 10);
        $this->failedLoginsTimeMinutes = elidev_setting(config('fomo.security_settings.failed_logins_time_minute'), 10);
        $this->decayMinutes            = elidev_setting(config('fomo.security_settings.time_minute_block'), 30);
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function incrementLoginAttempts(Request $request)
    {
        $this->limiter()->hit(
            $this->throttleKey($request), $this->failedLoginsTimeMinutes
        );
    }

    /**
     * Get the rate limiter instance.
     *
     * @return \Illuminate\Cache\RateLimiter
     */
    protected function limiter()
    {
        return app(FomoRateLimiter::class);
    }
}

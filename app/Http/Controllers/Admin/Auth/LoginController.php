<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\Auth\Traits\FomoThrottlesLogins;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    // override protected name collision functions
    use FomoThrottlesLogins {
        FomoThrottlesLogins::incrementLoginAttempts insteadof AuthenticatesUsers;
        FomoThrottlesLogins::limiter insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->middleware(
            ['guest', 'web', 'admin.login.throttle'],
            ['except' => ['adminLogout', 'setLocale']
        ]);

        $this->initBruteForceVariables();
    }

    /**
     * Show the admin's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('metronic::auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $request->merge(['cache_key' => $this->throttleKey($request)]);
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminLogout(Request $request)
    {
        Auth::logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect(route('admin-show-login'));
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public function attemptLogin(Request $request)
    {
        $isRemember = $request->get('remember', 0);
        $credential = [
            'email'    => $request->get('email', ''),
            'password' => $request->get('password', ''),
            'status'   => config('elidev.list_default_status.publish'),
        ];

        if (Auth::attempt($credential, $isRemember)) {
            Session::put('locale', \Auth::user()->language);
            return $this->sendLoginResponse($request);
        }
    }

    /**
     * Set Locale
     *
     * @param $locale
     * @return mixed
     */
    public function setLocale($locale)
    {
        Session::put('locale', $locale);
        return redirect()->back();
    }
}

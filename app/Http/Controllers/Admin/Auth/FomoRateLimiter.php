<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Cache\RateLimiter;

class FomoRateLimiter extends RateLimiter
{
    /**
     * Determine if the given key has been "accessed" too many times.
     *
     * @param  string  $key
     * @param  int  $maxAttempts
     * @param  float|int  $decayMinutes
     * @return bool
     */
    public function tooManyAttempts($key, $maxAttempts, $decayMinutes = 1)
    {
        $blockCacheKey = $key . config('fomo.block_cache_key');
        if ($this->cache->has($blockCacheKey)) {
            return true;
        }

        if ($this->attempts($key) >= $maxAttempts) {
            $this->lockout($key, $decayMinutes);

            $this->resetAttempts($key);

            return true;
        }

        return false;
    }

    /**
     * Increment the counter for a given key for a given decay time.
     *
     * @param  string  $key
     * @param  float|int  $decayMinutes
     * @return int
     */
    public function hit($key, $decayMinutes = 1)
    {
        $added = $this->cache->add($key, 0, $decayMinutes);

        $hits = (int) $this->cache->increment($key);

        if (! $added && $hits == 1) {
            $this->cache->put($key, 1, $decayMinutes);
        }

        return $hits;
    }

    /**
     * Add the lockout key to the cache.
     *
     * @param  string  $key
     * @param  int  $decayMinutes
     * @return void
     */
    protected function lockout($key, $decayMinutes)
    {
        $blockCacheKey = $key . config('fomo.block_cache_key');

        $this->cache->add(
            $blockCacheKey, $this->availableAt($decayMinutes * 60), $decayMinutes
        );
    }
}

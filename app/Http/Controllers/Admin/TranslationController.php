<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Artisan;
use Illuminate\Http\Request;
use Log;

class TranslationController extends Controller
{
    /**
     * Export translation _json files
     * 1. resources/lang/de/_json.php -> resources/lang/de.json
     * 2. resources/lang/en/_json.php -> resources/lang/en.json
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @ticket #13399 - HFBACKEND-85 System / Settings - Language translations
     */
    public function exportTranslationToJsonFile(Request $request)
    {
        try {
            // refer : https://github.com/barryvdh/laravel-translation-manager#export-command
            Artisan::call('translations:export', ['--json' => '--json']);
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            Log::error('Failed to export the translation files. Error message: ' . $message);
            response()->error($message, 400);
        }

        return response()->success(['message' => 'ok']);
    }
}

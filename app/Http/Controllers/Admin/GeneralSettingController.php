<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Artisan;
use Illuminate\Http\Request;
use Log;
use Settings;

class GeneralSettingController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('admin.setting.edit', [
            'settings' => Settings::getAll(),
            'breadcrumbs' => [
                [ 'text' => __('Settings') ],
                [ 'text' => __('Edit') ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $items = $request->only(config('fomo.settings'));
        try {
            foreach($items as $key => $val) {
                if ( $key == config('fomo.settings.date_time_en') || $key == config('fomo.settings.date_time_de') ) {
                    $val = str_replace('D', 'd', $val);
                    $val = str_replace('M', 'm', $val);
                    $val = str_replace('y', 'Y', $val);
                    $val = str_replace('h', 'H', $val); // only use 24h format
                    $val = str_replace('I', 'i', $val);
                }

                Settings::set($key, $val);
                unset($items[$key]);
            }

            // create cache config
            Artisan::call('config:cache');

            return back()->with('success', __('Edit the setting successfully'))->withInput();
        } catch (\Exception $ex) {
            Log::error('Failed to edit the setting with key = ' . json_encode($items) . '. Error message: ' . $ex->getMessage());
        }
        return back()->with('error', __('Edit failed. Please try again'))->withInput();
    }
}

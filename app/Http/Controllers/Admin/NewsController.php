<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\News\StoreNewsRequest;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Repositories\Contracts\TagsRepositoryInterface;
use App\Services\News\Admin\ApplyCategoriesTagsService;
use App\Services\News\Admin\DeleteNewsService;
use App\Services\News\Admin\ListingNewsService;
use App\Services\News\Admin\SingleNewsService;
use App\Services\News\Admin\StoreNewsService;
use App\Services\News\Admin\UpdateNewsService;
use App\Utils\Utils;
use Illuminate\Http\Request;
use Log;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     * @param \App\Repositories\Contracts\TagsRepositoryInterface     $tagRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12969 - HFBACKEND-13 - News - Listing
     */
    public function index(CategoryRepositoryInterface $categoryRepo, TagsRepositoryInterface $tagRepo)
    {
        $categories = $categoryRepo->getNewsCategories()->pluck('name', 'id')->all();
        $tags = $tagRepo->getNewsTags()->pluck('name', 'id')->all();
        $dateFormatBasedOnLanguage = Utils::dateTimeFormatBasedOnLanguage();

        $table = [
            'title' => 'News Overview',
            'id' => 'news-datatable',
            'ajax' => [
                'src' => 'admin.news.more', // route name
            ],
            'checkbox_column' => true,
            'order_default' => [
                'column' => 'available_date_only',
                'order' => 'desc',
            ],
            'columns' => [
                'available_date_only' => [
                    'text'   => 'Date',
                    'width' => '125',
                    'filter' => [
                        'type' => 'date',
                        'format' => empty($dateFormatBasedOnLanguage['date_format']) ? 'Y-m-d' : $dateFormatBasedOnLanguage['date_format'],
                    ],
                    'orderable' => true,
                ],
                'available_time_only' => [
                    'text'   => 'Time',
                    'width' => '60',
                    'filter' => [
                        'type' => 'time',
                    ],
                    'orderable' => true,
                ],
                'title' => [
                    'text' => 'Title',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                ],
                'category_names_with_link' => [
                    'text' => 'Categories',
                    'filter' => [
                        'type' => 'select',
                        'data' => $categories,
                    ],
                ],
                'tag_names_with_link' => [
                    'text' => 'Tags',
                    'filter' => [
                        'type' => 'select',
                        'data' => $tags,
                    ],
                    'width' => 80,
                ],
                'status' => [
                    'text' => 'Status',
                    'width' => '60',
                    'orderable' => true,
                    'html' => '<i class="fa fa-check font-green-jungle tooltips" data-placement="top" data-original-title="Publish"></i>',
                    'filter' => [
                        'type' => 'select',
                        'data' => [
                            config('elidev.list_default_status.pending') => ucfirst(config('elidev.list_default_status.pending')),
                            config('elidev.list_default_status.publish') => ucfirst(config('elidev.list_default_status.publish')),
                        ],
                    ],
                ],
            ],
            'actions' => [
                'width'  => '100',
                'show'    => [
                    'route' => 'admin.news.show', // route name
                    'role' => 'news.read',
                ],
                'add'    => [
                    'name'  => 'Add new',
                    'route' => 'admin.news.create', // route name
                    'role' => 'news.create',
                ],
                'edit'   => [
                    'route' => 'admin.news.edit', // route name
                    'role' => 'news.edit',
                ],
                'trash' => [
                    'route' => 'admin.news.trash', // route name
                    'icon' => 'fa fa-trash',
                    'color' => 'red-intense',
                    'role' => 'news.trash',
                ],
                'restore'   => [
                    'route' => 'admin.news.restore', // route name
                    'color' => 'green-jungle',
                    'role' => 'news.trash',
                ],
                'delete' => [
                    'route' => 'admin.news.destroy', // route name
                    'icon' => 'fa fa-times',
                    'color' => 'red-intense',
                    'role' => 'news.destroy',
                ],
            ],
            'bulk_actions' => [
                [
                    'name'   => 'bulk_set_categories',
                    'text'   => 'Set categories',
                    'is_displayed' => true
                ],
                [
                    'name'   => 'bulk_set_tags',
                    'text'   => 'Set tags',
                    'is_displayed' => true
                ],
                [
                    'name'   => 'bulk_trash',
                    'text'   => 'Move to trash',
                    'route'  => 'admin.news.bulk_trash_multi', // route name
                    'method' => 'post',
                    'is_displayed' => true
                ],
                [
                    'name'   => 'bulk_restore',
                    'text'   => 'Restore items',
                    'route'  => 'admin.news.restore_multi', // route name
                    'method' => 'post',
                    'is_displayed' => false
                ],
                [
                    'name'   => 'bulk_delete',
                    'text'   => 'Delete items',
                    'route'  => 'admin.news.delete_multi', // route name
                    'method' => 'post',
                    'is_displayed' => false
                ],
            ],
        ];

        return view('admin.news.list', [
            'table'       => $table,
            'categories'  => $categories,
            'tags'        => $tags,
            'breadcrumbs' => [
                ['text' => __('News')],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     * @param \App\Repositories\Contracts\TagsRepositoryInterface     $tagRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRepositoryInterface $categoryRepo, TagsRepositoryInterface $tagRepo)
    {
        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'text' => __('Create'),
            ],
        ];

        return view('admin.news.create', [
            'categories'  => $categoryRepo->getNewsCategories(),
            'tags'        => $tagRepo->getNewsTags(),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\News\StoreNewsRequest $request
     * @param \App\Services\News\Admin\StoreNewsService      $service
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsRequest $request, StoreNewsService $service)
    {
        $news = $service->execute($request);

        if ( $news ) {
            return redirect(route('admin.news.show', [ 'news' => $news->id ]))
                ->with('success', __('Create news successfully'))
                ->withInput();
        }

        return back()->with('error', __('Create failed. Please try again'))->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request                   $request
     * @param  int                                       $id
     * @param \App\Services\News\Admin\SingleNewsService $service
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, SingleNewsService $service)
    {
        $request->merge(['id' => $id]);
        $news = $service->execute($request);

        if ( !$news ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'text' => $news->title,
            ],
        ];

        return view('admin.news.show', [
            'news' => $news,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                                                    $id
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Services\News\Admin\SingleNewsService              $service
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     * @param \App\Repositories\Contracts\TagsRepositoryInterface     $tagRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request, SingleNewsService $service, CategoryRepositoryInterface $categoryRepo, TagsRepositoryInterface $tagRepo)
    {
        $request->merge(['id' => $id]);
        $news = $service->execute($request);

        if ( !$news ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'url' => route('admin.news.show', ['author' => $news->id]),
                'text' => $news->title,
            ],
            [
                'text' => __('Edit')
            ],
        ];

        return view('admin.news.edit', [
            'news'        => $news,
            'categories'  => $categoryRepo->getNewsCategories(),
            'tags'        => $tagRepo->getNewsTags(),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\News\StoreNewsRequest|\Illuminate\Http\Request $request
     * @param  int                                                                    $id
     * @param \App\Services\News\Admin\UpdateNewsService                              $service
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNewsRequest $request, $id, UpdateNewsService $service)
    {
        $request->merge(['id' => $id]);
        $result = $service->execute($request);

        $key     = $result ? 'success' : 'error';
        $message = $result ? __('Updated successfully') : __('Update failed. Please try again');

        return back()->with($key, $message);
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int                                       $id
     * @param \App\Services\News\Admin\DeleteNewsService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12969 - HFBACKEND-13 - News - Listing
     */
    public function trash($id, DeleteNewsService $service)
    {
        if ( $service->softDelete($id) ) {
            return redirect(route('admin.news.index'))->with('success', __('Trashed news successfully'));
        }

        Log::error(sprintf('Failed to trash the news (ID = %s)', $id));

        return back()->with('error', __('Failed to trash the news. Please try again.'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int                                       $id
     * @param \App\Services\News\Admin\DeleteNewsService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12969 - HFBACKEND-13 - News - Listing
     */
    public function restore($id, DeleteNewsService $service)
    {
        if ( $service->restore($id) ) {
            return back()->with('success', __('Restored news successfully'));
        }

        Log::error(sprintf('Failed to restore the news (ID = %s)', $id));

        return back()->with('error', __('Failed to restore the news. Please try again.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                                       $id
     * @param \App\Services\News\Admin\DeleteNewsService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12969 - HFBACKEND-13 - News - Listing
     */
    public function destroy($id, DeleteNewsService $service)
    {
        if ( $service->destroy($id) ) {
            return redirect(route('admin.news.index'))->with('success', __('Permanently deleted news successfully'));
        }

        Log::error(sprintf('Failed to delete the news (ID = %s) permanently.', $id));

        return back()->with('error', __('Failed to delete the news permanently. Please try again.'));
    }

    /**
     * Get news
     *
     * @param \Illuminate\Http\Request                    $request
     * @param \App\Services\News\Admin\ListingNewsService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12969 - HFBACKEND-13 - News - Listing
     */
    public function more(Request $request, ListingNewsService $service)
    {
        $news = $service->execute($request);
        $news->getCollection()->each(function($newsItem) {
            $categoryIds = implode(',', $newsItem->categories->pluck('id')->all());
            $tagIds      = implode(',', $newsItem->tags->pluck('id')->all());

            $newsItem->category_names = $newsItem->category_names;
            $newsItem->category_names_with_link = $newsItem->category_names_with_link;
            $newsItem->tag_names_with_link = $newsItem->tag_names_with_link;
            $newsItem->available_date_only = $newsItem->available_date_only;
            $newsItem->available_time_only = $newsItem->available_time_only;

            $newsItem->checkbox = view('admin.components.checkbox', [
                'value' => $newsItem->id,
                'attr' => [
                    'data-news-id'    => $newsItem->id,
                    'data-categories' => $categoryIds,
                    'data-tags'       => $tagIds,
                ]
            ])->render();
        });
        $total = $news->total();

        return json_encode([
            'data'            => $news->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }

    /**
     * Search news
     *
     * @param \Illuminate\Http\Request                            $request
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12968 - HFBACKEND-13 - News - Assigning related articles
     */
    public function search(Request $request, NewsRepositoryInterface $newsRepo)
    {
        $news = $newsRepo->search(['id', 'title'], $request->get('q', ''), $request->get('excerpt_news_id', -1));
        $totalNews = $news->total();
        $news = $news->map(function($item) {
            return [
                'id' => $item->id,
                'text' => $item->title,
            ];
        })->toArray();

        return json_encode([ 'items' => $news, 'total_count' => $totalNews]);
    }

    /**
     * Bulk apply multiple categories or tags for news
     *
     * @param \Illuminate\Http\Request                            $request
     * @param \App\Services\News\Admin\ApplyCategoriesTagsService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #13038 - HFBACKEND-23 - News / Overview - Datagrid actions
     */
    public function apply(Request $request, ApplyCategoriesTagsService $service)
    {
        // must specify the type of bulk action
        if ( empty($type = $request->get('type', '')) ) {
            return response()->error(__('There is no type selected'));
        }

        // validate enough data before process
        if ( $request->get('news', '') == '' ) {
            return response()->error(__('There is no news are selected'));
        }
        if ( $type == 'category' && count($request->get('categories', [])) <= 0 && empty($request->get('indeterminate_checkboxes', '')) ) {
            return response()->error(__('There is no categories are selected'));
        }
        if ( $type == 'tag' && count($request->get('tags', [])) <= 0 && empty($request->get('indeterminate_checkboxes', '')) ) {
            return response()->error(__('There is no tags are selected'));
        }

        try {
            $result = $service->execute($request);
            if ( $result ) {
                $message = $type == 'category' ? __('Applied categories successfully') : __('Applied tags successfully');
                return response()->success([ 'message' => $message, 'news' => $result, 'type' => $type ]);
            }
        } catch(\Exception $ex) {
            $message = sprintf(
                'Failed to apply for type=%s, news=%s, categories=%s, tags=%s. Error message: %s',
                $type,
                $request->get('news', ''),
                implode(',', $request->get('categories', [])),
                implode(',', $request->get('tags', [])),
                $ex->getMessage()
            );
            Log::error($message);
        }

        return response()->error(__('Failed to apply. Please try again'));
    }

    /**
     * Display a listing of the related news.
     *
     * @param \Illuminate\Http\Request                            $request
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     *
     * @return String
     */
    public function moreRelatedNews(Request $request, NewsRepositoryInterface $newsRepo)
    {
        if ($request->has('id') && $request->has('page') && $request->has('per_page')) {
            $relatedNews = $newsRepo->getRelatedNewsPagination($request->get('id'), $request->get('page'), $request->get('per_page'));
            $html = view('admin.news.includes.show.ajax_related_news', compact('relatedNews'))->render();
            return \Response::json([
                'hasMore' => count($relatedNews) == 5 ? true : false,
                'url'  => route("admin.news.moreRelatedNews", ['id' => $request->get('id'), 'page' => $request->get('page') + 1, 'per_page' => $request->get('per_page')]),
                'html' => $html,
            ]);
        }
        return \Response::json();
    }

    /**
     * trash multi author
     *
     * @param \Illuminate\Http\Request                           $request
     * @param \App\Services\News\Admin\DeleteNewsService         $service
     *
     * @return \Illuminate\Http\Response
     */
    public function trashMulti(Request $request, DeleteNewsService $service)
    {
        if($request->has('ids')) {
            $ids = $request->input('ids');
            $result = $service->multiSoftDelete($ids);
            if ( $result ) {
                return response()->success([ 'message' => __('Trashed news successfully') ]);
            }
        }
        return response()->error(__('Failed to trash the news. Please try again.'));
    }

    /**
     * restore multi author
     *
     * @param \Illuminate\Http\Request                           request
     * @param \App\Services\News\Admin\DeleteNewsService         $service
     *
     * @return \Illuminate\Http\Response
     */
    public function restoreMulti(Request $request, DeleteNewsService $service)
    {
        if($request->has('ids')) {
            $ids = $request->input('ids');
            $result = $service->multiRestore($ids);
            if ( $result ) {
                return response()->success([ 'message' => __('Restored news successfully') ]);
            }
        }
        return response()->error(__('Failed to restore the news. Please try again.'));
    }

    /**
     * delete multi news
     *
     * @param \Illuminate\Http\Request                           request
     * @param \App\Services\News\Admin\DeleteNewsService         $service
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteMulti(Request $request, DeleteNewsService $service)
    {
        if($request->has('ids')) {
            $ids = $request->input('ids');
            $result = $service->multiDestroy($ids);
            if ( $result ) {
                return response()->success([ 'message' => __('Delete news successfully') ]);
            }
        }
        return response()->error(__('Failed to delete the news. Please try again.'));
    }
}

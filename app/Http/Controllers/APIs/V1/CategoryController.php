<?php

namespace App\Http\Controllers\APIs\V1;

use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Criterias\Category\FilterByCategoryType;
use App\Repositories\Criterias\Category\SortByCategoryName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Repositories\Criterias\Category\SortByIndex;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoryRepositoryInterface $catRepo
     * @return \Illuminate\Http\Response
     */
    public function index(CategoryRepositoryInterface $catRepo)
    {
        $filePath = get_categories_caching_file();

        $items = file_exists($filePath) ? json_decode(File::get($filePath), true) : [];
        if ( count($items) > 0 ) {
            return response()->success($items);
        } else {

            $tags = $catRepo->pushCriteria(new SortByIndex())
                ->pushCriteria(new FilterByCategoryType(['news']))
                ->paginate(config('fomo.category.limit'), ['id', 'name']);

            File::put(
                $filePath,
                json_encode($tags->getCollection())
            );

            return response()->success($tags->getCollection());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

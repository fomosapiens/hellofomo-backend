<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Admin\Auth\FomoRateLimiter;
use App\Http\Controllers\Admin\Auth\Traits\FomoThrottlesLogins;
use Closure;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginThrottleRequests
{
    use AuthenticatesUsers;

    // override protected name collision functions
    use FomoThrottlesLogins {
        FomoThrottlesLogins::incrementLoginAttempts insteadof AuthenticatesUsers;
        FomoThrottlesLogins::limiter insteadof AuthenticatesUsers;
    }

    /**
     * The rate limiter instance.
     *
     * @var \Illuminate\Cache\RateLimiter
     */
    protected $limiter;

    /**
     * AdminLoginThrottleRequests constructor.
     *
     * @param \Illuminate\Cache\RateLimiter $limiter
     */
    public function __construct(FomoRateLimiter $limiter)
    {
        $this->limiter = $limiter;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if the user has been blocked, just redirect them here, not allow them to go further
        $isPostLogin = !empty($request->get($this->username(), ''));
        $availableIn = $this->limiter->availableIn($this->throttleKey($request));

        if ($isPostLogin && $availableIn > 0) {
            return $this->sendLockoutResponse($request);
        }

        return $next($request);
    }
}

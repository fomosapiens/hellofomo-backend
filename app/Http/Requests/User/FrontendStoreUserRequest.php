<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use Auth;
use Settings;

class FrontendStoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set allow params
     *
     * @return array parameters
     */
    function allowParams()
    {
        //
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'country' => 'string',
            'city' => 'sometimes|string|nullable',
            'phone' => 'sometimes|string|nullable|max:45',
            'language' => 'string|max:5',
        ];

        // if update user request (PUT)
        if ($this->request->get('_method') == 'PUT') {

            $userId = $this->user ? $this->user : Auth::user()->id;

            $maxSize = Settings::get(config('elidev.settings.general_image_size'), config('elidev.default_max_image_size'));

            $additionalRules = [
                'email' => 'required|string|max:255|email|unique:users,email,' . $userId,
                'avatar' => 'mimes:' . config('elidev.image_extension_logo') .
                    '|image|image_dimensions|max:' . $maxSize,
            ];

            // only validate new password if user have inputted the value
            if (!is_null($this->request->get('password'))) {
                $additionalRules[] = [
                    'password' => 'required|string|confirmed|min:6',
                    'password_confirmation' => 'required|string',
                ];
            }
        } else {
            // if add new user request (POST)
            $additionalRules = [
                'email' => 'required|string|max:255|email|unique:users',
                'password' => 'required|string|confirmed|min:6',
                'password_confirmation' => 'required|string',
            ];
        }

        return array_merge($rules, $additionalRules);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'avatar.image_dimensions' => __(sprintf('Please upload an image with the max resolution %sx%s pixels',
                config('elidev.default_max_image_width'), config('elidev.default_max_image_height')
            ))
        ];
    }
}

<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\Request;

class GeneralSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Set allow params
     * @return array parameters
     */
    public function allowParams()
    {
        $settingsKey = array_keys(config('elidev.settings'));
        return $settingsKey;
    }
}

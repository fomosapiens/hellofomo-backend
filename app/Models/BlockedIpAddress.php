<?php

namespace App\Models;

use App\Utils\Utils;
use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlockedIpAddress extends Model
{
    /**
     * Mass assignment
     *
     * @var array
     */
    protected $fillable = [
        'ip', 'email', 'url', 'user_agent', 'cache_key', 'expired_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expired_at'];

    /**
     * Get & format created at
     *
     * @param $value
     *
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $format = Utils::dateTimeFormatBasedOnLanguage()['date_time_format'];

        return Carbon::parse($value)->format($format);
    }

    /**
     * Check current row has the cache was expired or not
     *
     * @return bool
     */
    public function getIsExpiredAttribute()
    {
        if (empty($this->expired_at)) {
            return true;
        }

        $isExpired = true;
        $cacheExpiredAt = Cache::get($this->cache_key . config('fomo.block_cache_key'));
        if (!is_null($cacheExpiredAt) && $this->expired_at->getTimestamp() == $cacheExpiredAt) {
            $isExpired = false;
        }

        return $isExpired;
    }
}

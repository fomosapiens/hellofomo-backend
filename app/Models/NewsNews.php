<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsNews extends Model
{
    protected $fillable = ['news_id', 'related_news_id'];

}

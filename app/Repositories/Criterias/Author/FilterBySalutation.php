<?php

namespace App\Repositories\Criterias\Author;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterBySalutation implements CriteriaInterface
{
    /**
     * @var int
     */
    protected $salutationId;

    /**
     * FilterBySalutation constructor.
     *
     * @param int $salutationId
     */
    public function __construct($salutationId = -1)
    {
        $this->salutationId = empty($salutationId) ? -1 : $salutationId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( $this->salutationId == -1 ) {
            return $model;
        }

        return $model->where('salutation_id', '=', $this->salutationId);
    }
}

<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByTagId implements CriteriaInterface
{
    /**
     * @var int
     */
    protected $tagId;

    /**
     * FilterByTagId constructor.
     *
     * @param int $tagId
     */
    public function __construct($tagId = -1)
    {
        $this->tagId = $tagId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->tagId) || $this->tagId == -1 ) {
            return $model;
        }

        return $model
            ->join('news_tags', 'news_tags.news_id', '=', 'news.id')
            ->where('news_tags.tag_id', '=', $this->tagId);
    }
}

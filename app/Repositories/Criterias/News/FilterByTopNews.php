<?php

namespace App\Repositories\Criterias\News;

use Carbon\Carbon;
use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByTopNews implements CriteriaInterface
{
    /**
     * @var bool
     */
    protected $topNews;

    /**
     * FilterByTopNews constructor.
     *
     * @param bool $topNews
     */
    public function __construct($topNews = true)
    {
        $this->topNews = $topNews;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( !($this->topNews) ) {
            return $model;
        }
        return $model
            ->where('top_news', '=', 1)
            ->where(function($query){
            $currentDate = Carbon::now()->format('Y-m-d');
              $query
                  ->whereDate('expiry_date', '>=', $currentDate)
                  ->orWhereNull('expiry_date');
            });
    }
}

<?php

namespace App\Repositories\Criterias\Security;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByIpCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $ip;

    /**
     * FilterByIP constructor.
     *
     * @param string $ip
     */
    public function __construct($ip = '')
    {
        $this->ip = $ip;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->ip)) {
            return $model;
        }

        return $model->where('ip', 'LIKE', '%' . $this->ip . '%');
    }
}

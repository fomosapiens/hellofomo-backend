<?php

namespace App\Repositories\Criterias\Security;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByUserAgentCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $userAgent;

    /**
     * FilterByUserAgentCriteria constructor.
     *
     * @param string $userAgent
     */
    public function __construct($userAgent = '')
    {
        $this->userAgent = $userAgent;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->userAgent)) {
            return $model;
        }

        return $model->where('user_agent', 'LIKE', '%' . $this->userAgent . '%');
    }
}

<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface TagsRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface TagsRepositoryInterface extends RepositoryInterface
{
    /**
     * List tags (also filter: name, type)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC');

    /**
     * Get news tags
     *
     * @param array $columns
     *
     * @return array
     */
    public function getNewsTags($columns = []);
}

<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface BlockedIPAddressRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface BlockedIPAddressRepositoryInterface extends RepositoryInterface
{
    /**
     * List blocked IP addresses
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC');
}

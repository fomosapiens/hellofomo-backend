<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsNewsRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface NewsNewsRepositoryInterface extends RepositoryInterface
{
    //
}

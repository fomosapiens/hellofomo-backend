<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsTagsRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface NewsTagsRepositoryInterface extends RepositoryInterface
{
    //
}

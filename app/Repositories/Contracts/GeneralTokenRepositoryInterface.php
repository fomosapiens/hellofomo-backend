<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface GeneralTokenRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface GeneralTokenRepositoryInterface extends RepositoryInterface
{
    /**
     * Get available token
     *
     * @param $email
     * @param $type
     * @return mixed
     */
    public function getAvailableToken($email, $type);

    /**
     * Check is valid token with email
     * @param $token
     * @param $type
     * @param $email
     * @return bool
     */
    public function isValidToken($token, $type, $email);

    /**
     * Generate token
     *
     * @param string $type
     * @param string $email
     *
     * @return string
     */
    public function generate($type, $email);
}


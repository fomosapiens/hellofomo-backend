<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface NewsRepositoryInterface extends RepositoryInterface
{
    /**
     * List news (also filter: title)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool   $onlyTrashed
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false);

    /**
     * Search news based on title
     *
     * @param array  $columns
     * @param string $keyword
     * @param int    $excerptNewsId
     *
     * @return mixed
     */
    public function search($columns = ['*'], $keyword = '', $excerptNewsId = -1);

    /**
     * Find selected news
     *
     * @param array $columns
     * @param array $selectedNews
     *
     * @return mixed
     * @ticket #13038 - HFBACKEND-23 - News / Overview - Datagrid actions
     */
    public function findSelectedNews($columns = ['*'], $selectedNews = []);

    /**
     * Get related news pagination
     *
     * @param int  $id
     * @param int  $page
     * @param int  $per_page
     *
     * @return mixed
     */
    public function getRelatedNewsPagination($id, $page, $per_page);

    /**
     * List news (also filter: title)
     *
     * @param array $columns
     * @param array $terms
     * @param int $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool $onlyTrashed
     *
     * @param $offset
     * @param $page
     * @return mixed
     */
    public function listingAdvance($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false, $offset = null, $page = 1);

}
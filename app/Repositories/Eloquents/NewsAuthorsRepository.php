<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\NewsAuthorsRepositoryInterface;
use App\Models\NewsAuthors;

/**
 * Class NewsAuthorsRepository
 * @package namespace App\Repositories\Eloquents;
 */
class NewsAuthorsRepository extends BaseRepository implements NewsAuthorsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NewsAuthors::class;
    }
}

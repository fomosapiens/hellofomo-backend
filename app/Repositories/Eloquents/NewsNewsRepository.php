<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\NewsNewsRepositoryInterface;
use App\Models\NewsNews;

/**
 * Class NewsNewsRepository
 * @package namespace App\Repositories\Eloquents;
 */
class NewsNewsRepository extends BaseRepository implements NewsNewsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NewsNews::class;
    }
}

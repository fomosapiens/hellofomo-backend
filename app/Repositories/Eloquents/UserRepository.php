<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\Collection;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Criterias\FilterStatusCriteria;
use App\Repositories\Criterias\FilterTrashCriteria;
use App\Repositories\Criterias\FilterByEmailCriteria;
use App\Repositories\Criterias\User\FilterByNameCriteria;
use App\Repositories\Criterias\User\FilterByPhoneCriteria;
use App\User;
use Elidev\Repository\Eloquent\BaseRepository;

/**
 * Class UserRepository
 * @package namespace App\Repositories\Eloquents;
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * List users (also filter: name, email or phone)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool   $onlyTrashed
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false)
    {
        $this->resetCriteria();

        return $this->pushCriteria(new FilterTrashCriteria($onlyTrashed))
                    ->pushCriteria(new FilterByNameCriteria(array_get($terms, 'name', '')))
                    ->pushCriteria(new FilterByEmailCriteria(array_get($terms, 'email', '')))
                    ->pushCriteria(new FilterByPhoneCriteria(array_get($terms, 'phone', '')))
                    ->pushCriteria(new FilterStatusCriteria(array_get($terms, 'status', ''), 'users'))
                    ->orderBy($sortColumn, $sortOrder)
                    ->paginate($limit, $columns);
    }

    /**
     * Get by email
     * @param $email
     * @return Collection
     */
    public function getByEmail($email)
    {
        return $this->findWhere([
            'email' => $email
        ]);
    }

    /**
     * Get all publish users
     *
     * @return Collection
     */
    public function getPublishUsers()
    {
        return $this->findWhere([
            'status'    => config('elidev.list_default_status.publish')
        ]);
    }

    /**
     * @param $email
     * @param $userID
     * @return mixed
     */
    public function checkEmailExist($email, $userID)
    {
        if ( $userID ) {
            $user = $this->findWhere(['email' => $email, ['id', '<>', $userID]], ['email']);
        } else {
            $user = $this->findWhere(['email' => $email], ['email']);
        }

        return $user && $user->count() > 0;
    }
}

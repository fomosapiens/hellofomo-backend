<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\NewsCategorysRepositoryInterface;
use App\Models\NewsCategorys;

/**
 * Class NewsCategorysRepository
 * @package namespace App\Repositories\Eloquents;
 */
class NewsCategorysRepository extends BaseRepository implements NewsCategorysRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NewsCategorys::class;
    }
}

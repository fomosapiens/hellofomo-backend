<?php

namespace App\Repositories\Eloquents;

use App\Models\BlockedIpAddress;
use App\Repositories\Contracts\BlockedIPAddressRepositoryInterface;
use App\Repositories\Criterias\FilterByEmailCriteria;
use App\Repositories\Criterias\Security\FilterByIpCriteria;
use App\Repositories\Criterias\Security\FilterByUserAgentCriteria;
use Elidev\Repository\Eloquent\BaseRepository;

/**
 * Class BlockedIPAddressRepository
 * @package namespace App\Repositories\Eloquents;
 */
class BlockedIPAddressRepository extends BaseRepository implements BlockedIPAddressRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BlockedIpAddress::class;
    }

    /**
     * List blocked IP addresses
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC')
    {
        $this->resetCriteria();

        return $this->pushCriteria(new FilterByIpCriteria(array_get($terms, 'ip', '')))
                    ->pushCriteria(new FilterByEmailCriteria(array_get($terms, 'email', '')))
                    ->pushCriteria(new FilterByUserAgentCriteria(array_get($terms, 'user_agent', '')))
                    ->orderBy($sortColumn, $sortOrder)
                    ->paginate($limit, $columns);
    }
}

<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\UserActivationRepositoryInterface;
use App\Models\UserActivation;
use Carbon\Carbon;

/**
 * Class UserActivationRepository
 * @package namespace App\Repositories\Eloquents;
 */
class UserActivationRepository extends BaseRepository implements UserActivationRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserActivation::class;
    }

    /**
     * Create user activation
     *
     * @param App/User $user
     * @return string $token
     */
    public function createActivation($user)
    {
        $activation = $this->find($user->id);
        $token = $this->generateToken();

        if (!$activation) {
            $this->create([
                'id' => $user->id,
                'token' => $token
            ]);
        }
        else {
            $this->update([
                'token' => $token,
                'created_at' => new Carbon()
            ], $user->id);
        }

        return $token;

    }

    /**
     * Generate token
     * @return string token
     */
    protected function generateToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    /**
     * Delete by token
     * @param string token
     * @return boolean
     */
    public function deleteByToken($token)
    {
        $activation = $this->findWhere(['token' => $token]);
        if ($activation && $activation->first()) {
            return $activation->first()->delete();
        }
        return false;
    }
}

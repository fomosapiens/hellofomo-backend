<?php

namespace App\Repositories\Eloquents;

use App\Models\News;
use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Repositories\Criterias\FilterByTitle;
use App\Repositories\Criterias\FilterStatusCriteria;
use App\Repositories\Criterias\FilterTrashCriteria;
use App\Repositories\Criterias\News\FilterByAvailableDateOnly;
use App\Repositories\Criterias\News\FilterByAvailableTimeOnly;
use App\Repositories\Criterias\News\FilterByCategoryId;
use App\Repositories\Criterias\News\FilterByTagId;
use App\Repositories\Criterias\News\FilterByCategoryIds;
use App\Repositories\Criterias\News\FilterByTagIds;
use App\Repositories\Criterias\News\FilterByTopNews;
use App\Repositories\Criterias\News\FilterWithSelectedNews;
use App\Repositories\Criterias\News\GroupBy;
use App\Repositories\Criterias\News\OrderByRaw;
use App\Repositories\Criterias\WithRelationsCriteria;
use Elidev\Repository\Eloquent\BaseRepository;

class NewsRepository extends BaseRepository implements NewsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return News::class;
    }

    /**
     * List news (also filter: title)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool   $onlyTrashed
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false)
    {
        $this->resetCriteria();

        return $this->pushCriteria(new WithRelationsCriteria(['categories', 'tags']))
                    ->pushCriteria(new FilterTrashCriteria($onlyTrashed))
                    ->pushCriteria(new FilterByTitle(array_get($terms, 'title', '')))
                    ->pushCriteria(new FilterStatusCriteria(array_get($terms, 'status', ''), 'news'))
                    ->pushCriteria(new FilterByAvailableDateOnly(array_get($terms, 'available_date', '')))
                    ->pushCriteria(new FilterByAvailableTimeOnly(array_get($terms, 'available_time', '')))
                    ->pushCriteria(new FilterByCategoryId(array_get($terms, 'category_id', -1)))
                    ->pushCriteria(new FilterByTagId(array_get($terms, 'tag_id', -1)))
                    ->pushCriteria(new OrderByRaw($sortColumn, $sortOrder))
                    ->paginate($limit, $columns);
    }

    /**
     * Search news based on title
     *
     * @param array  $columns
     * @param string $keyword
     * @param int    $excerptNewsId
     *
     * @return mixed
     */
    public function search($columns = ['*'], $keyword = '', $excerptNewsId = -1)
    {
        $keyword = '%' . $keyword . '%';

        return $this->model->where('id', '<>', $excerptNewsId)
            ->where('title', 'LIKE', $keyword)
            ->where('status', '=', config('elidev.list_default_status.publish'))
            ->paginate(config('elidev.default_limit', 10), $columns);
    }

    /**
     * Find selected news
     *
     * @param array $columns
     * @param array $selectedNews
     *
     * @return mixed
     * @ticket #13038 - HFBACKEND-23 - News / Overview - Datagrid actions
     */
    public function findSelectedNews($columns = ['*'], $selectedNews = [])
    {
        if ( count($selectedNews) == 0 || !is_array($selectedNews) ) {
            return null;
        }

        return $this->resetCriteria()
            ->pushCriteria(new WithRelationsCriteria(['categories', 'tags']))
            ->pushCriteria(new FilterWithSelectedNews($selectedNews))
            ->all($columns);
    }

    /**
     * Get related news pagination
     *
     * @param int  $id
     * @param int  $page
     * @param int  $per_page
     *
     * @return mixed
     */
    public function getRelatedNewsPagination($id, $page, $per_page)
    {
        $page = ($page == 0) ? 1 : $page;
        return \DB::table('news')
            ->leftJoin('news_news as rn', function ($join) use($id) {
                $join->on('rn.related_news_id', '=', 'news.id')
                    ->where('rn.news_id', '=', $id);
            })
            ->leftJoin('news_news as rn1', function ($join) use($id) {
                $join->on('rn1.news_id', '=', 'news.id')
                    ->where('rn1.related_news_id', '=', $id);
            })
            ->where('rn.news_id', '<>', 'NULL')
            ->orWhere('rn1.related_news_id', '<>', 'NULL')
            ->select('news.id', 'news.title', 'news.available_date', 'news.introduction')
            ->latest('available_date')
            ->take($per_page)
            ->skip(($page - 1) * $per_page)
            ->get();
    }

    /**
     * List news, multi filter apply for api
     *
     * @param array $columns
     * @param array $filters
     * @param int $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool $onlyTrashed
     *
     * @param null $offset
     * @param int $page
     * @return mixed
     */
    public function listingAdvance($columns = ['*'], $filters = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false, $offset = null, $page = 1)
    {
        $this->resetCriteria();
        $result = $this->pushCriteria(new WithRelationsCriteria(['categories', 'tags']))
            ->pushCriteria(new FilterTrashCriteria($onlyTrashed))
            ->pushCriteria(new FilterByTitle(array_get($filters, 'title', '')))
            ->pushCriteria(new FilterStatusCriteria(array_get($filters, 'status', ''), 'news'))
            ->pushCriteria(new FilterByAvailableDateOnly(array_get($filters, 'available_date', '')))
            ->pushCriteria(new FilterByAvailableTimeOnly(array_get($filters, 'available_time', '')))
            ->pushCriteria(new FilterByCategoryIds(array_get($filters, 'category_ids', [])))
            ->pushCriteria(new FilterByTagIds(array_get($filters, 'tag_ids', [])))
            ->pushCriteria(new FilterByTopNews(array_get($filters, 'top_news', [])))
            ->pushCriteria(new OrderByRaw($sortColumn, $sortOrder));
        if ($offset) {
            return $result->getLimitOffset($limit, $offset, $columns, $page);
        }

        return $result->paginate($limit, $columns);
    }
}

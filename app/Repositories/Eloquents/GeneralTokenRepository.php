<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\GeneralTokenRepositoryInterface;
use App\Models\GeneralToken;
use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * Class GeneralTokenRepository
 * @package namespace App\Repositories\Eloquents;
 */
class GeneralTokenRepository extends BaseRepository implements GeneralTokenRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GeneralToken::class;
    }

    /**
     * Get available token
     *
     * @param $email
     * @param $type
     * @return mixed
     */
    public function getAvailableToken($email, $type)
    {
        return $this->findWhere([
            'email' => $email,
            'type'  => $type,
            ['expired_date', '>=', Carbon::now()->toDateTimeString()]
        ]);
    }

    /**
     * Check is valid token with email
     * @param $token
     * @param $type
     * @param $email
     * @return boolean
     */
    public function isValidToken($token, $type, $email)
    {
        $result = $this->findWhere([
            'token' => $token,
            'email' => $email,
            'type'  => $type,
            ['expired_date', '>=', Carbon::now()->toDateTimeString()]
        ]);

        return $result->count() > 0;
    }

    /**
     * Generate token
     *
     * @param string $type
     * @param string $email
     *
     * @return string
     */
    public function generate($type, $email)
    {
        $existedTokenData = $this->getAvailableToken($email, $type);
        if ($existedTokenData->first()) {
            $existedToken = $existedTokenData->first()->token;
        }
        else {
            $expiredDateObject = Carbon::now()->addDays(config('elidev.general.token.expired_token_day'));
            $expiredDate = $expiredDateObject->toDateTimeString();

            $existedToken = Str::random(40);
            $this->create([
                'email' => $email,
                'type'  => $type,
                'token' => $existedToken,
                'expired_date'  => $expiredDate
            ]);
        }
        return $existedToken;
    }
}

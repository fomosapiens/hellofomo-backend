<?php

namespace App\Listeners;

use App\Repositories\Contracts\BlockedIPAddressRepositoryInterface;
use Cache;
use Carbon\Carbon;
use Log;

class BlockIPAddressEventSubscriber
{
    /**
     * @var \App\Repositories\Contracts\BlockedIPAddressRepositoryInterface
     */
    protected $repo;

    /**
     * BlockIPAddressEventSubscriber constructor.
     *
     * @param \App\Repositories\Contracts\BlockedIPAddressRepositoryInterface $repo
     */
    public function __construct(BlockedIPAddressRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Handle block IP address event.
     *
     * @param $event
     */
    public function onBlockIPAddress($event)
    {
        $request = !empty($event->request) ? $event->request : null;
        if (is_null($request)) {
            Log::error('[Block IP] The request is not found');
            return;
        }

        $cacheKey  = $request->get('cache_key', '');
        $expiredAt = Cache::get($cacheKey . config('fomo.block_cache_key'), '');
        if (!empty($expiredAt)) {
            $expiredAt = Carbon::createFromTimestamp($expiredAt);
        }

        $this->repo->create([
            'ip'         => $request->ip(),
            'email'      => $request->get('email', ''),
            'url'        => $request->url(),
            'user_agent' => $request->header('User-Agent'),
            'cache_key'  => $cacheKey,
            'expired_at' => $expiredAt,
        ]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Lockout',
            'App\Listeners\BlockIPAddressEventSubscriber@onBlockIPAddress'
        );
    }
}

<?php

namespace App\Services\Contracts;

interface ProduceServiceInterface
{
    /**
     * Execute produce an entity
     *
     * @param \Illuminate\Http\Request $request
     * @author vulh
     * @return mixed
     */
    public function execute($request);
}

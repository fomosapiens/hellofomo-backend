<?php

namespace App\Services\Contracts;


interface DeleteEntityServiceInterface
{
    /**
     * Soft delete entity
     *
     * @param integer $id
     * @return boolean
     */
    public function softDelete($id);

    /**
     * Restore an entity
     *
     * @param integer $id
     * @return boolean
     */
    public function restore($id);

    /**
     * Destroy an entity
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id);
}
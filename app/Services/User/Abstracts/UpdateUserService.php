<?php

namespace App\Services\User\Abstracts;

use App\Mail\ChangeEmail;
use App\Services\User\Traits\UserTrait;
use Illuminate\Http\Request;
use Mail;

abstract class UpdateUserService extends ProduceUserService
{
    use UserTrait;

    /**
     * @var object
     */
    protected $user;

    /**
     * @var string
     */
    public $errorMessage;

    /**
     * The hook method is executed after user is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterUpdate($request);

    /**
     * Prepare attributes before insert/update
     *
     * @param Request $request
     *
     * @return array
     */
    public function prepareAttributes($request)
    {
        $attributes = $request->all();

        // if user want update password -> encode password before update
        if ( isset($attributes['password']) && !empty($attributes['password']) ) {
            $attributes['password'] = bcrypt($attributes['password']);
        } else {
            unset($attributes['password']);
            unset($attributes['password_confirmation']);
        }

        if ( isset($attributes['country']) ) {
            $attributes['country'] = $attributes['country'] != -1 ? strtolower($attributes['country']) : '';
        }

        $attributes['status'] = !isset($attributes['status']) ? config('elidev.list_default_status.pending') : config('elidev.list_default_status.publish');

        return $attributes;
    }

    /**
     * Execute produce a user
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $this->errorMessage = __('Bad Request - Failed to update user.');
        $user = $this->repo->find($request->id);
        if ( !$request->id || !$user ) {
            return false;
        }

        $attributes = $this->prepareAttributes($request);
        $this->user = $this->repo->update($attributes, $request->id);

        if ( isset($attributes['role']) && $this->user ) {
            $this->user->syncRoles($attributes['role']);
        }

        // execute callback method
        $this->afterUpdate($request);

        $this->user = $this->formatUser($this->user);

        return $this->user;
    }
}

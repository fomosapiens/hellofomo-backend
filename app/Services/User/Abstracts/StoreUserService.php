<?php

namespace App\Services\User\Abstracts;


use App\Services\User\Traits\UserTrait;

abstract class StoreUserService extends ProduceUserService
{
    use UserTrait;

    /**
     * @var object
     */
    protected $user;

    /**
     * The hook method is executed after user is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterStored($request);

    /**
     * Prepare attributes before insert/update
     *
     * @param \App\Http\Request $request
     *
     * @return array
     */
    public function prepareAttributes($request)
    {
        $attributes             = $request->only('name', 'password', 'email', 'phone', 'status', 'image_id');
        $attributes['password'] = bcrypt($attributes['password']);
        $attributes['status']   = empty($attributes['status'])  ? config('elidev.list_default_status.pending') : config('elidev.list_default_status.publish') ;
        // assign default role
        if ( !isset($attributes['role']) || empty($attributes['role']) ) {
            $attributes['role'] = config('elidev.default_role');
        }

        return $attributes;
    }

    /**
     * Execute produce a user
     *
     * @param \App\Http\Request $request
     *
     * @return object
     */
    public function execute($request)
    {
        $attributes = $this->prepareAttributes($request);
        $this->user = $this->repo->create($attributes);

        if ( $this->user ) {
            $this->user->assignRole($attributes['role']);
        }

        // execute hook method
        $this->afterStored($request);

        $this->user = $this->formatUser($this->user);

        return $this->user;
    }
}

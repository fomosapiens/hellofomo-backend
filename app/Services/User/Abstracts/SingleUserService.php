<?php

namespace App\Services\User\Abstracts;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;
use App\Services\User\Traits\UserTrait;

abstract class SingleUserService implements ProduceServiceInterface
{
    use UserTrait;

    /**
     * @var object
     */
    protected $user;

    /**
     * @var \App\Repositories\Contracts\UserRepositoryInterface
     */
    protected $repo;

    /**
     * SingleUserService constructor.
     *
     * @param \App\Repositories\Contracts\UserRepositoryInterface $repo
     */
    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * After get single user, use this function to get more data you want
     *
     * @return mixed
     */
    abstract function afterSingled();

    /**
     * Specify fields need to return
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'name', 'email', 'language',
            'phone', 'status', 'image_id'
        ];
    }

    /**
     * Execute a single user
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $this->user = $this->repo->find($request->id, $this->fields());

        // Hook after singled user to get more data you want
        $this->afterSingled();
        $this->user = $this->formatUser($this->user);

        return $this->user;
    }
}

<?php

namespace App\Services\Author\Abstracts;

use App\Services\Author\Traits\AuthorTrait;

abstract class StoreAuthorService extends ProduceAuthorService
{
    use AuthorTrait;

    /**
     * @var \App\Models\Author
     */
    protected $author;

    /**
     * The hook method is executed after author is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterStored($request);

    /**
     * Prepare attributes before insert/update
     *
     * @param \App\Http\Request $request
     *
     * @return array
     */
    public function prepareAttributes($request)
    {
    }

    /**
     * Specify fields need to store
     *
     * @return array
     */
    public function fields()
    {
        return [ 'first_name', 'last_name', 'display_name', 'salutation_id', 'title_id', 'description', 'image_id', 'callback_url' ];
    }

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $this->author = $this->authorRepo->create($request->only($this->fields()));

        // execute hook method
        $this->afterStored($request);

        $this->author = $this->formatAuthor($this->author);

        return $this->author;
    }
}

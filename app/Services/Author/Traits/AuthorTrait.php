<?php

namespace App\Services\Author\Traits;

trait AuthorTrait
{
    /**
     * @param $collection
     */
    public function formatAuthors($collection)
    {
        $collection->each(function($item) {
            return $this->formatAuthor($item);
        });

        return $collection;
    }

    /**
     * @param \App\Models\Author $author
     *
     * @return \App\Models\Author
     */
    public function formatAuthor($author)
    {
        if ( $author ) {
        }

        return $author;
    }
}

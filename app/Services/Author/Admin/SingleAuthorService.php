<?php

namespace App\Services\Author\Admin;

use App\Services\Author\Abstracts\SingleAuthorService as SingleAbstract;

class SingleAuthorService extends SingleAbstract
{
    /**
     * After get single author, use this function to get more data you want
     *
     * @return mixed
     */
    function afterSingled()
    {
    }
}

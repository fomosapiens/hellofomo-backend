<?php

namespace App\Services\News\Abstracts;

use App\Utils\Utils;
use Carbon\Carbon;
use Illuminate\Http\Request;

abstract class UpdateNewsService extends ProduceNewsService
{
    /**
     * @var \App\Models\News
     */
    protected $news;

    /**
     * Prepare attributes before insert/update
     *
     * @param Request $request
     *
     * @return array
     */
    public function prepareAttributes($request)
    {
        $data = $request->only($this->fields());

        if ( !isset($data['status']) || $data['status'] == '' ) {
            $data['status'] = config('elidev.list_default_status.pending');
        }

        if ( isset($data['available_date']) ) {
            $format = Utils::dateTimeFormatBasedOnLanguage()['date_time_format'];
            $format = str_replace('i:H', 'H:i', $format);
            $available_date = $data['available_date'] . ' ' . $request->input('available_time', '00:00');
            $data['available_date'] = Carbon::createFromFormat($format, $available_date)->format('Y-m-d H:i:s');
        }

        if(isset($data['galleries']) && is_array($data['galleries'])) {
            $data['gallery_ids'] = serialize($data['galleries']);
            unset($data['galleries']);
        }

        if(isset($data['attachments']) && is_array($data['attachments'])) {
            $data['attachment_ids'] = serialize($data['attachments']);
            unset($data['attachments']);
        }

        if (isset($data['top_news']) && $data['top_news'] == 'on') {
            $data['top_news'] = 1;
        } else {
            $data['top_news'] = 0;
        }

        if (isset($data['expiry_date'])) {
            $format = Utils::dateTimeFormatBasedOnLanguage()['date_time_format'];
            $format = str_replace('i:H', 'H:i', $format);
            $data['expiry_date'] = Carbon::createFromFormat($format, $data['expiry_date'] . ' 23:59')->format('Y-m-d H:i:s');
        }
        return $data;
    }

    /**
     * Specify fields need to store
     *
     * @return array
     */
    public function fields()
    {
        return [
            'title', 'available_date', 'keywords',
            'status', 'introduction', 'description',
            'image_id', 'galleries', 'attachments',
            'meta_title', 'meta_permalink', 'meta_description',
            'top_news', 'expiry_date'
        ];
    }

    /**
     * The hook method is executed after news is update to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterUpdate($request);

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $data = $this->prepareAttributes($request);
        $this->news = $this->newsRepo->update($data, $request->id);
        if ( !$this->news ) {
            return false;
        }

        // sync categories
        $this->news->categories()->sync($request->get('categories', []));

        // sync tags
        $this->news->tags()->sync($request->get('tags', []));

        // sync authors
        $this->news->authors()->sync($request->get('authors', []));

        // sync related news
        $this->news->relatedNews()->sync($request->get('related_news', []));
        $this->news->relatedNewsOf()->sync($request->get('related_news', []));

        $this->afterUpdate($request);

        return $this->news;
    }
}

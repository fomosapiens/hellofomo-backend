<?php

namespace App\Services\News\Abstracts;

use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;

abstract class ProduceNewsService implements ProduceServiceInterface
{
    /**
     * @var \App\Repositories\Contracts\NewsRepositoryInterface
     */
    protected $newsRepo;

    /**
     * ProduceNewsService constructor.
     *
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    /**
     * Prepare attributes before insert/update
     *
     * @param \App\Http\Request $request
     *
     * @return array
     */
    abstract public function prepareAttributes($request);

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @author vulh
     * @return mixed
     */
    abstract public function execute($request);
}

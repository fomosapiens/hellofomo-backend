<?php

namespace App\Services\News\Admin;

use App\Services\News\Abstracts\SingleNewsService as SingleAbstract;
use App\Utils\Utils;
use Carbon\Carbon;

class SingleNewsService extends SingleAbstract
{
    /**
     * After get single news, use this function to get more data you want
     *
     * @return mixed
     */
    function afterSingled()
    {
        // convert description
        if (!empty($this->news->description)) {
            $this->news->description = Utils::convertLinkMarkedText($this->news->description);
        }
        if($this->news->top_news && (empty($this->news->expiry_date) || ($this->news->expiry_date >= Carbon::now()))) {
            $this->news->top_news = 1;
        } else {
            $this->news->top_news = 0;
        }
    }
}

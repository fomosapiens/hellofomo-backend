<?php

namespace App\Services\News\Admin;

use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Services\Abstracts\ListingAbstract;
use App\Utils\Utils;

class ListingNewsService extends ListingAbstract
{
    /**
     * @var \App\Repositories\Contracts\NewsRepositoryInterface
     */
    protected $newsRepo;

    /**
     * ListingNewsService constructor.
     *
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    /**
     * Handle ajax URL
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function ajaxURL($request)
    {
    }

    /**
     * Returned columns
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'title', 'available_date', 'status'
        ];
    }

    /**
     * Rename search columns for easy understand
     *
     * @param array $pagination
     *
     * @return array
     */
    public function renameSearchColumns($pagination)
    {
        if ( !is_array($pagination) || count($pagination) <= 0 ) {
            return $pagination;
        }

        if ( array_has($pagination['terms'], 'category_names_with_link') ) {
            $pagination['terms']['category_id'] = $pagination['terms']['category_names_with_link'];
            unset($pagination['terms']['category_names_with_link']);
        }

        if ( array_has($pagination['terms'], 'tag_names_with_link') ) {
            $pagination['terms']['tag_id'] = $pagination['terms']['tag_names_with_link'];
            unset($pagination['terms']['tag_names_with_link']);
        }

        if ( array_has($pagination['terms'], 'available_date_only') ) {
            $pagination['terms']['available_date'] = $pagination['terms']['available_date_only'];
            unset($pagination['terms']['available_date_only']);
        }

        if ( array_has($pagination['terms'], 'available_time_only') ) {
            $pagination['terms']['available_time'] = $pagination['terms']['available_time_only'];
            unset($pagination['terms']['available_time_only']);
        }

        $invalidSortColumns = [
            'category_names_with_link', 'tag_names_with_link',
            'checkbox',
        ];
        if ( in_array($pagination['sortColumn'], $invalidSortColumns) ) {
            $pagination['sortColumn'] = 'id';
        }

        if ( $pagination['sortColumn'] == 'available_date_only' ) {
            $pagination['sortColumn'] = 'available_date';
        } else if ( $pagination['sortColumn'] == 'available_time_only' ) {
            $pagination['sortColumn'] = 'available_time_only';
        }
        return $pagination;
    }

    /**
     * Get listing data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function execute($request)
    {
        // set filter params (title, available date & time, categories, tags, status)
        $filterColumns = [
            'title',
            'status',
            'available_date_only',
            'available_time_only',
            'category_names_with_link',
            'tag_names_with_link',
        ];
        $pagination = Utils::getDataTablePagination($request, $filterColumns);
        $pagination = $this->renameSearchColumns($pagination);

        return $this->newsRepo->listing(
            $this->fields(),
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder'],
            $pagination['onlyTrashed']
        );
    }
}

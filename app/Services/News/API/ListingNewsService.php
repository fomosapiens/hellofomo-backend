<?php

namespace App\Services\News\API;

use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Services\Abstracts\ListingAbstract;
use App\Utils\Utils;

class ListingNewsService extends ListingAbstract
{
    /**
     * @var \App\Repositories\Contracts\NewsRepositoryInterface
     */
    protected $newsRepo;

    /**
     * ListingNewsService constructor.
     *
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }



    /**
     * Returned columns
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'title', 'introduction', 'description', 'image_id', 'available_date',
        ];
    }

    /**
     * Get listing data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function execute($request)
    {
        $filters = [];
        if ( $request->has('categories') ) {
            $filters['category_ids'] = explode(',', $request->input('categories'));
        }
        if ( $request->has('tags') ) {
            $filters['tag_ids'] = explode(',', $request->input('tags'));
        }

        if ($request->has('top_news')) {
            $filters['top_news'] = $request->input('top_news') == 1;
        }
        $pagination['page'] = $request->has('page') ? $request->input('page') : 1;
        $pagination['page'] =  $pagination['page'] >= 1 ?  $pagination['page'] : 1;
        $pagination['limit'] = $request->has('limit') ? $request->input('limit') : 10;
        $pagination['offset'] = $request->has('offset') ? $request->input('offset') : null;
        $sort['sortColumn'] = 'available_date';
        $sort['sortOrder'] = 'DESC';
        $filters['onlyTrashed'] = false;
        $filters['status'] = config('elidev.list_default_status.publish');

        $news = $this->newsRepo->listingAdvance(
            $this->fields(),
            $filters,
            $pagination['limit'],
            $sort['sortColumn'],
            $sort['sortOrder'],
            $filters['onlyTrashed'],
            $pagination['offset'],
            $pagination['page']
        );

        $data = null;

        if ( $news && count($news) > 0 ) {
            foreach ($news as $item) {
                $data[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'introduction' => $item->introduction,
                    'description' => $item->description,
                    'available_date' => $item->formatted_available_date,
                    'primary_image' => $item->image_src,
                ];
            }
        }

        if ( $request->has('offset') ) {
            return ['data' => $data];
        }

        return ['data' => $data, 'has_more' => $news->lastItem() != null && $news->lastItem() < $news->total() ? true : false];
    }

    /**
     * Handle ajax URL
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function ajaxURL($request)
    {
        // TODO: Implement ajaxURL() method.
    }
}

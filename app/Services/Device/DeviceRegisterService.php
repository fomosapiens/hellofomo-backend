<?php

namespace App\Services\Device;

use App\Repositories\Contracts\DeviceRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;
use DateInterval;
use GuzzleHttp\Client;
use Laravel\Passport\Bridge\AuthCodeRepository;
use Laravel\Passport\Bridge\ClientRepository as BridgeClientRepository;
use Laravel\Passport\ClientRepository;
use League\OAuth2\Server\CryptTrait;
use Log;

class DeviceRegisterService implements ProduceServiceInterface
{
    use CryptTrait;

    /**
     * @var int
     */
    protected $errorCode;

    /**
     * @var string
     */
    protected $errorMessage;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var \App\Repositories\Contracts\DeviceRepositoryInterface
     */
    protected $deviceRepo;

    /**
     * @var \Laravel\Passport\ClientRepository
     */
    protected $clientRepo;

    /**
     * @var \Laravel\Passport\Bridge\AuthCodeRepository
     */
    protected $authCodeRepo;

    /**
     * @var \Laravel\Passport\Client
     */
    protected $client;

    /**
     * DeviceRegisterService constructor.
     *
     * @param \App\Repositories\Contracts\DeviceRepositoryInterface $deviceRepo
     */
    public function __construct(DeviceRepositoryInterface $deviceRepo)
    {
        $this->deviceRepo = $deviceRepo;

        $this->clientRepo = app()->make(ClientRepository::class);
        $this->authCodeRepo = app()->make(AuthCodeRepository::class);
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Check the client id & client secret is valid
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    private function isClientInvalid($request) {
        $this->client = $this->clientRepo->findActive($request->get('client_id', ''));
        $clientSecret = $request->get('client_secret', '');

        if ( !$this->client || $this->client->secret != $clientSecret ) {
            $this->errorCode    = 1007;
            $this->errorMessage = __('Invalid client');

            return true;
        }

        return false;
    }

    /**
     * Store device information to DB
     *
     * @param \Illuminate\Http\Request $request
     */
    private function storeDeviceToDB($request)
    {
        // store device information
        $this->deviceRepo->create([
            'device_id'    => $request->get('device_id'),
            'device_token' => $request->get('device_token'),
            'fcm_token'    => $request->get('fcm_token', ''),
            'agent'        => $request->get('agent'),
        ]);
    }

    /**
     * Bypass approve/deny step when use grant type: authorization_code
     * In this step, we will generate authorization code (using Authorization Code Grant flow, but bypass the approval screen)
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    private function byPassAuthorizationApproveStep($request)
    {
        try {
            $bridgeClient = app()->make(BridgeClientRepository::class)->getClientEntity(
                $request->get('client_id', ''),
                'authorization_code',
                $request->get('client_secret', '')
            );

            // set encryption key. Refer: vendor/laravel/passport/src/PassportServiceProvider.php, L203
            $this->encryptionKey = app('encrypter')->getKey();

            // set auth code TTL. Refer: vendor/laravel/passport/src/PassportServiceProvider.php, L146
            $authCodeTTL = new DateInterval('PT10M');

            // init auth code object
            $authCode = $this->authCodeRepo->getNewAuthCode();
            $authCode->setExpiryDateTime((new \DateTime())->add($authCodeTTL));
            $authCode->setClient($bridgeClient);
            $authCode->setRedirectUri(config('app.url'));
            $authCode->setIdentifier(bin2hex(random_bytes(40)));

            // set default associated user is admin user
            $authCode->setUserIdentifier(1);

            // store authCode to DB
            $this->authCodeRepo->persistNewAuthCode($authCode);

            // generate the authorization code
            $this->code = $this->encrypt(
                json_encode(
                    [
                        'client_id'    => (string) $authCode->getClient()->getIdentifier(),
                        'redirect_uri' => $authCode->getRedirectUri(),
                        'auth_code_id' => $authCode->getIdentifier(),
                        'scopes'       => $authCode->getScopes(),
                        'user_id'      => $authCode->getUserIdentifier(),
                        'expire_time'  => (new \DateTime())->add($authCodeTTL)->format('U'),
                    ]
                )
            );

            return true;
        } catch(\Exception $ex) {
            $this->errorCode    = 1008;
            $this->errorMessage = __('Failed to bypass the approve step');

            Log::error('Failed to bypass approve/deny step. Error: ' . $ex->getMessage());
        }

        return false;
    }

    /**
     * Finally, we will issue an access token to return back to mobile app
     *
     * @return mixed
     */
    private function issueAccessToken()
    {
        try {
            $http = new Client();
            $url = config('app.url') . '/oauth/token';
            $response = $http->post($url, [
                'form_params' => [
                    'grant_type'    => 'authorization_code',
                    'client_id'     => $this->client->id,
                    'client_secret' => $this->client->secret,
                    'redirect_uri'  => config('app.url'),
                    'code'          => $this->code,
                ],
            ]);

            return json_decode((string) $response->getBody(), true);
        } catch(\Exception $ex) {
            $this->errorCode    = 1009;
            $this->errorMessage = __('Failed to issue an access token');

            Log::error('Failed to issue an access token for mobile device. Request URL: ' . $url . '. Error: ' . $ex->getMessage());
        }

        return false;
    }

    /**
     * Execute produce an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        if ( $this->isClientInvalid($request) ) {
            return false;
        }

        // only store if device id is not existed in DB
        $count = $this->deviceRepo->countWhere(['device_id' => $request->get('device_id', '')]);
        if ( !$count || $count == 0 ) {
            $this->storeDeviceToDB($request);
        }

        $result = $this->byPassAuthorizationApproveStep($request);
        if ( !$result ) {
            return false;
        }

        return $this->issueAccessToken();
    }
}

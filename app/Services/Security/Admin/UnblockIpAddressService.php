<?php

namespace App\Services\Security\Admin;

use App\Repositories\Contracts\BlockedIPAddressRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;
use Illuminate\Cache\RateLimiter;
use Log;

class UnblockIpAddressService implements ProduceServiceInterface
{
    /**
     * @var \App\Repositories\Contracts\BlockedIPAddressRepositoryInterface
     */
    protected $repo;

    /**
     * @var \Illuminate\Cache\RateLimiter
     */
    protected $limiter;

    /**
     * UnblockIpAddressService constructor.
     *
     * @param \App\Repositories\Contracts\BlockedIPAddressRepositoryInterface $repo
     * @param \Illuminate\Cache\RateLimiter                                   $limiter
     */
    public function __construct(BlockedIPAddressRepositoryInterface $repo, RateLimiter $limiter)
    {
        $this->repo = $repo;
        $this->limiter = $limiter;
    }

    /**
     * Execute produce an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $id = $request->get('id', 0);
        $blockInfo = $this->repo->find($id, ['id', 'ip', 'cache_key']);
        if (empty($blockInfo)) {
            return false;
        }

        try {
            // unblock (remove cache files)
            $this->limiter->clear($blockInfo['cache_key']);

            // delete blocked IP in DB
            $this->repo->delete($id);
        } catch(\Exception $ex) {
            Log::error(sprintf('[Unblock IP] Failed to unblock for IP: %s, ID: %s. Error message: %s', $blockInfo['ip'], $id, $ex->getMessage()));
            return false;
        }

        return true;
    }
}

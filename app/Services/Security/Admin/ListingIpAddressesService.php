<?php

namespace App\Services\Security\Admin;

use App\Repositories\Contracts\BlockedIPAddressRepositoryInterface;
use App\Services\Abstracts\ListingAbstract;
use App\Utils\Utils;

class ListingIpAddressesService extends ListingAbstract
{
    /**
     * @var \App\Repositories\Contracts\BlockedIPAddressRepositoryInterface
     */
    protected $repo;

    /**
     * ListingIPAddressesService constructor.
     *
     * @param \App\Repositories\Contracts\BlockedIPAddressRepositoryInterface $repo
     */
    public function __construct(BlockedIPAddressRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Returned columns
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'ip', 'email', 'user_agent', 'created_at', 'cache_key', 'expired_at'
        ];
    }

    /**
     * Handle ajax URL
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function ajaxURL($request)
    {
    }

    /**
     * Get listing data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function execute($request)
    {
        // set filter params
        $pagination = Utils::getDataTablePagination($request, [ 'ip', 'email', 'user_agent' ]);

        return $this->repo->listing(
            $this->fields(),
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder']
        );
    }
}

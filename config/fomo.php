<?php

return [

    'category' => [
        'types' => [
            'news'       => 'news',
            'salutation' => 'salutation',
            'title'      => 'title',
        ],
        'limit' => 1000,
    ],

    'image' => [
        'default' => 'admin-assets/global/img/placeholder/default.png',
        'avatar_default' => 'admin-assets/global/img/placeholder/avatar.png',
    ],

    'support_languages' => [
        'English' => 'en',
        'German' => 'de',
    ],

    // @ticket #13182, #13238
    'support_fonts' => [
        'Arial'             => 'Arial',
        'Arial Hebrew'      => 'Arial Hebrew',
        'ArialHebrew-Bold'  => 'ArialHebrew-Bold',
        'ArialHebrew-Light' => 'ArialHebrew-Light',
        'Courier'           => 'Courier',
        'Courier-Bold'      => 'Courier-Bold',
        'Helvetica'         => 'Helvetica',
        'Open Sans'         => 'Open Sans',
        'Roboto'            => 'Roboto',
        'Times New Roman'   => 'Times New Roman',
        'Titilium'          => 'Titilium',
        'Work Sans'         => 'Work Sans',
    ],

    'settings'  => [
        'date_time_en'      => 'date_time_en',
        'date_time_de'      => 'date_time_de',
        'language'          => 'language',
        'news_files_types'  => 'news_files_types',
        'image_logo'        => 'image_logo',
        'favicon'           => 'favicon',
        'website_title_prefix' => 'website_title_prefix',
    ],

    /** @Ticket #16660 */
    'email_settings' => [
        'account_sender_name'        => 'account_sender_name',
        'account_sender_address'     => 'account_sender_address',
        'mailer_smpt'                => 'mailer_smpt',
        'main_backup_smtp_servers'   => 'main_backup_smtp_servers',
        'enable_smtp_auth'           => 'enable_smtp_auth',
        'smpt_username'              => 'smpt_username',
        'smtp_password'              => 'smtp_password',
        'email_encryption'           => 'email_encryption',
        'tcp_port'                   => 'tcp_port',
        'system_sender_name'         => 'system_sender_name',
        'system_sender_address'      => 'system_sender_address',
    ],

    // @ticket #13446
    'security_settings'  => [
        'failed_logins'             => 'failed_logins',
        'failed_logins_time_minute' => 'failed_logins_time_minute',
        'time_minute_block'         => 'time_minute_block',
    ],

    // @ticket #13238
    'app_settings' => [
        'font'                         => 'app_font',
        'app_layout_dashboard'         => 'app_layout_dashboard',
        'app_layout_news'              => 'app_layout_news',
        'app_layout_contact'           => 'app_layout_contact',
        'app_layout_privacy'           => 'app_layout_privacy',
        'app_layout_impressum'         => 'app_layout_impressum',
        'app_layout_agb'               => 'app_layout_agb',
        'fcm_sender_id'                => 'fcm_sender_id',
        'fcm_server_key'               => 'fcm_server_key',
        'push_notification_max_length' => 'push_notification_max_length',
        'app_layout_category'          => 'app_layout_category',
        'app_layout_post'              => 'app_layout_post',
    ],

    // @ticket #13295
    'push_notification' => [
        'default_topic' => 'fomo_news'
    ],

    // @ticket #13471, #13684
    'block_cache_key' => ':timer',
];

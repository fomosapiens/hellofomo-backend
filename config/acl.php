<?php

$defaultActions = ['read', 'create', 'edit', 'trash', 'destroy'];
$simpleActions = ['read', 'create', 'edit', 'destroy'];
$mediaPackageActions = ['read', 'create', 'edit', 'trash', 'delete'];

return [

    'roles' => ['Super Administrator', 'Administrator', 'New Publisher'],

    'cache_file'  => 'role_permissions.json',

    'permissions'   => [
        'user' => [
            'actions'   => array_merge($defaultActions, ['unblock']),
            'children'   => [
                'users' => $defaultActions,
                'assignments'   => ['read', 'edit'],
                'roles'    => $simpleActions,
                'permissions'   => ['read'],
                'security' => ['read', 'edit', 'unblock']
            ]
        ],

        'news'   => [
            'actions' => array_merge(['publish'], $defaultActions),
            'children'  => [
                'news'  =>  array_merge(['publish'], $defaultActions),
                'categories'    => $simpleActions,
                'tags'  => $simpleActions
            ]
        ],

        'author'    => [
            'actions' => $defaultActions,
            'children'  => [
                'authors'   => $defaultActions,
                'salutations' => $simpleActions,
            ]
        ],

        'file management'   => [
            'actions'   => $mediaPackageActions,
            'children'  => [
                'files' => $mediaPackageActions,
                'folders' => $mediaPackageActions,
            ]
        ]
    ],

    // Controller action name - Permission name
    'map_actions'   => [
        'index' => 'read',
        'more' => 'read',
        'show'  => 'read',
        'search'    => 'read',
        'update'    => 'edit',

        'store' => 'create',

        'edit-assign'   => 'edit',
        'update-assign' => 'edit',
        'restore'   => 'trash',

        // Media package
        'popup' => 'read',
        'list'  => 'read',
        'quota' => 'read',
        'download'  => 'read',
        'upload'    => 'create',
        'rename'    => 'edit',
        'delete'    => 'destroy',

        'checkEmailExist'   => 'read',
    ],

    // Group with full permission
    'group_with_full_permissions'   => 'Super Administrator',

    // Actions that allows all users
    'free_permissions'  => [
        'admin.dashboard',
        'admin.logout',
    ],

    'map_mods'   => [
        'media'   => 'files'
    ],

    // Config parent template
    'root_view'   => 'metronic::layout.admin',

    // Config route prefix
    'route_prefix'  => 'admin',

    // Publish assets to
    'publish_asset_to' => 'admin',

    // additional middleware here
    'middleware'    => 'acl.role',

    // setting keys
    'settings' => [
        'super_admin_group' => 'super_admin_group',
    ],
];

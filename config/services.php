<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => elidev_setting('mailgun_domain', env('MAILGUN_DOMAIN')),
        'secret' => elidev_setting('mailgun_secret', env('MAILGUN_SECRET')),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => elidev_setting('payment_stripe_key', env('STRIPE_KEY')),
        'secret' => elidev_setting('payment_stripe_secret', env('STRIPE_SECRET')),
    ],

    // Socialite
    'facebook' => [
        'client_id' => elidev_setting('facebook_app_id', env('FACEBOOK_APP_ID')),
        'client_secret' => elidev_setting('facebook_app_secret', env('FACEBOOK_APP_SECRET')),
        'redirect' => env('APP_URL'). '/facebook/callback',
    ],
];

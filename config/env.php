<?php

return [
    'app_env' => env('APP_ENV'),
    'mail_from_address' => env('MAIL_FROM_ADDRESS'),
];
